#!/usr/bin/env bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Security/bz2005819-bidi_override_characters_can_cause_trojan_source_attack
#   Description: Test for BZ#2005819 Unicode’s bidirectional (‘Bidi’) override characters can cause trojan source attacks
#   Author: Vaclav Kadlcik <vkadlcik@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2021 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

# Notes on relevancy
#
# Fixed in system GCC of RHEL 8.5+ and any currently supported toolset, except
# DTS-10/RHEL-6 and RHEL-8.4 GTS-10 (which is the only toolset there).
# Suggested TCMS relevancy:
#   distro < rhel-8 && collection !defined: False
#   distro = rhel-8 and distro < rhel-8.5: False
#   distro < rhel-7 && collection contains devtoolset-10: False

# NOTE: Just smoke for new. To be extended later. (TODO)

GCC="${GCC:-$(type -P gcc)}"

GCC_RPM_NAME=$(rpm --qf '%{name}' -qf $GCC)
GCCCPP_RPM_NAME=${GCC_RPM_NAME}-c++

PACKAGES="$GCC_RPM_NAME $GCCCPP_RPM_NAME"

WBIDI_OPTION='-Wbidi-chars'

rlJournalStart
    rlPhaseStartSetup
        rlRun "yum -y install --skip-broken $PACKAGES" 0-255 # best effort

        rlLogInfo "PACKAGES=$PACKAGES"
        rlLogInfo "COLLECTIONS=$COLLECTIONS"
        rlLogInfo "GCC=$GCC"
        rlLogInfo "SKIP_COLLECTION_METAPACKAGE_CHECK=$SKIP_COLLECTION_METAPACKAGE_CHECK"

        # We optionally need to skip checking for the presence of the metapackage
        # because that would pull in all the dependent toolset subrpms.  We do not
        # always want that, especially in CI.
        _COLLECTIONS="$COLLECTIONS"
        if ! test -z $SKIP_COLLECTION_METAPACKAGE_CHECK; then
            for c in $SKIP_COLLECTION_METAPACKAGE_CHECK; do
                rlLogInfo "ignoring metapackage check for collection $c"
                export COLLECTIONS=$(shopt -s extglob && echo ${COLLECTIONS//$c/})
            done
        fi
        rlLogInfo "(without skipped) COLLECTIONS=$COLLECTIONS"
        rlAssertRpm --all
        export COLLECTIONS="$_COLLECTIONS"

        if gcc --help=warnings |& grep -q -- -Wbidirectional; then
            rlLogInfo 'Obsolete option -Wbidirectional detected'
            WBIDI_OPTION='-Wbidirectional'
        fi
        rlLogInfo "WBIDI_OPTION=$WBIDI_OPTION"

        rlRun "TmpDir=\$(mktemp -d)"

        # Reproducers taken from or based on:
        # 1. https://bugzilla.redhat.com/show_bug.cgi?id=2005819#c32 by Huzaifa S. Sidhpurwala
        #    https://svn.devel.redhat.com/repos/srtvulns/trunk/components/gcc/bidi-override/
        # 2. Red Hat's downstream patches, hopefully to be opensourced later
        rlRun "cp files/* $TmpDir"

        rlRun "pushd $TmpDir"
    rlPhaseEnd

    for PROGRAM in commenting-out-new stretched-string-new; do

        rlPhaseStartTest $PROGRAM.c

            # =none for backward compatibility
            rlRun -ts "gcc $WBIDI_OPTION=none -o $PROGRAM $PROGRAM.c"
            if [[ -s $rlRun_LOG ]]; then
                rlLogFail 'Unexpected output from gcc:'
                while read line; do rlLogError "$line"; done <$rlRun_LOG
            else
                rlPass 'As desired, no warning from gcc'
            fi
            rlRun "./$PROGRAM | grep 'You are an admin'"

            # By default (=unpaired) we should be warned
            rlRun -ts "gcc -o $PROGRAM $PROGRAM.c"
            rlAssertGrep 'warning: unpaired UTF-8 bidirectional' $rlRun_LOG
            if [[ $? -ne 0 ]]; then
                rlLogFail 'Unexpected output from gcc:'
                while read line; do rlLogError "$line"; done <$rlRun_LOG
            fi
            rlRun "./$PROGRAM | grep 'You are an admin'"

        rlPhaseEnd

    done

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
