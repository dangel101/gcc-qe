#!/usr/bin/env python
# -*- coding: utf8 -*-

# Dump the test cases present in our general test plan.
# Note: Not all data is dumped (namely the document, if any, is skipped).
#       We focus on data related to our test plan/run automation, especially
#       the sortkey, tags and relevancy (which is part of the notes).

from __future__ import absolute_import, division, print_function, unicode_literals

import nitrate

p = nitrate.TestPlan(7919)
for c in p.testcases:
    print('==============================================================================')
    print('summary={}'.format(c.summary))
    print(' script={}'.format(c.script))
    print('   args={}'.format(c.arguments))
    print('   link={}'.format(c.link))
    print('sortkey={} id={} status={} auto={} manual={} category={} time={}'.format(p.sortkey(c), c.id, str(c.status), str(c.automated), str(c.manual), str(c.category), str(c.time)))
    print('   tags={}'.format(str(sorted([str(tag) for tag in c.tags]))))
    print('  notes:')
    print(c.notes)
