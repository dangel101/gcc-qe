#!/usr/bin/env python
# -*- coding: utf8 -*-

# See http://wiki.test/BaseOs/Projects/TestCaseRelevancy for the basics

from __future__ import absolute_import, division, print_function, unicode_literals

import argparse
import nitrate
import qe

parser = argparse.ArgumentParser()
group = parser.add_mutually_exclusive_group()
group.add_argument(
        '-d', '--debug',
        help='Print debug info from the nitrate library',
        action='store_true')
parser.add_argument(
        'tc_ids',
        metavar='TEST_CASE_ID',
        nargs='*',
        help="TCMS test cases' IDs")
args = parser.parse_args()

if args.debug:
    nitrate.log.level = nitrate.log.DEBUG

distros = ['rhel-5.11', 'rhel-6.8', 'rhel-6.10', 'rhel-7.2', 'rhel-7.3', 'rhel-7.4', 'rhel-7.5', 'rhel-7.6', 'rhel-7.7', 'rhel-7.8', 'rhel-7.9', 'rhel-8.1.0', 'rhel-8.2.0', 'rhel-8.2.1', 'rhel-8.3.0', 'rhel-8.3.1', 'rhel-8.4.0', 'rhel-8.4.1', 'rhel-8.5.0', 'rhel-8.5.1', 'rhel-8.6.0', 'rhel-8.6.1', 'rhel-8.7.0', 'rhel-8.7.1', 'rhel-8.8.0', 'rhel-9.0.0', 'rhel-9.0.1', 'rhel-9.1.0', 'rhel-9.1.1', 'rhel-9.2.0', 'rhel-9.2.1', 'rhel-10.0.0']
collections = [None, 'devtoolset-10', 'devtoolset-11', 'devtoolset-12', 'devtoolset-13', 'gcc-toolset-10', 'gcc-toolset-11', 'gcc-toolset-12', 'gcc-toolset-13']

for tc_id in args.tc_ids:
    case = nitrate.TestCase(id=int(tc_id))
    print('ID: ' + tc_id)
    print('URL: https://tcms.engineering.redhat.com/case/' + tc_id)
    print('SUMMARY: ' + case.summary)
    print('NOTES:')
    print(case.notes + '\n')
    print('{:<15} {:<12} {:<9} {}'.format('COLLECTION', 'DISTRO', 'ARCH', 'RELEVANT'))
    for c in collections:
        for d in distros:
            if c is not None:
                if d.startswith('rhel-5.'):
                    continue
                if c.startswith('devtoolset-') and not d.startswith(('rhel-6.', 'rhel-7.')):
                    continue
                if c.startswith('gcc-toolset-') and d.startswith(('rhel-6.', 'rhel-7.')):
                    continue
            architectures = ['x86_64', 'aarch64', 'ppc64le', 's390x'] # RHEL8+
            if d.startswith('rhel-5.'):
                architectures = ['x86_64'] # the rest has been dropped from support
            if d.startswith('rhel-6.'):
                architectures = ['i386', 'x86_64', 's390x'] # the rest has been dropped from support
            if d.startswith('rhel-7.'):
                architectures = ['i386', 'x86_64', 'aarch64', 'ppc', 'ppc64', 'ppc64le', 's390', 's390x']
            for a in architectures:
                if c is not None and c.startswith('devtoolset-'):
                    if d.startswith('rhel-6.') and a not in ['x86_64']:
                        continue
                    if d.startswith('rhel-7.') and a not in ['x86_64', 'ppc64', 'ppc64le', 's390x']:
                        continue
                env = qe.Environment(collection=c, distro=d, arch=a)
                rel = env.relevant(case.notes, extract=True)
                if c is None:
                    cf = '(base)'
                else:
                    cf = c
                print('{:<15} {:<12} {:<9} {}'.format(cf, d, a, rel))
