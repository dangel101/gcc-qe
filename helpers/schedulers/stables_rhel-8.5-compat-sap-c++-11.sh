#!/usr/bin/env bash

set -o errexit

# hint: tps-make-lists -c <repo>

JOBS=''
JOBS+=",$(bkr workflow-tomorrow --id --arch={x86_64,ppc64le} --stable --profile=stable-rhel-8-server-sap)" # repo: rhel-8-for-$(arch)-sap-solutions-rpms
JOBS+=",$(bkr workflow-tomorrow --id --arch={x86_64,ppc64le} --stable --profile=sap-netweaver-rhel-8)"     # rhel-8-for-$(arch)-sap-netweaver-rpms
JOBS=${JOBS#,}

beaker-jobwatch --skip-broken-machines --critical-task=/distribution/install/stable-system --job=$JOBS
