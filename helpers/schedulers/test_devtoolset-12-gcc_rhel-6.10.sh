#!/usr/bin/env bash

BUILD=${BUILD:-''}
ERRATUM=104034
PRODUCT=rhscl-3.9
DISTRO=RHEL-6.10
COMPOSE=${COMPOSE:-'http://download.eng.bos.redhat.com/rhel-6/nightly/RHSCL/latest-RHSCL-3.9-RHEL-6/compose/Server/$basearch/os'} # no /updates/, contrary to RHEL-7
COLLECTION=devtoolset-12
COLLECTION_MINOR=0 # TODO waiting for the installer
ARCHITECTURES=${ARCHITECTURES:-x86_64}; ARCHITECTURES="--arch=${ARCHITECTURES//,/ --arch=}"
FORCE_UPSTREAM_TESTS=${FORCE_UPSTREAM_TESTS:-false}
COMPONENT=gcc
GENERAL_PLAN=7919
NAME="$COLLECTION-$COMPONENT/${PRODUCT}/ER#$ERRATUM"

set -o errexit
set -o pipefail

if [[ -z "$BUILD" ]]; then
    # If no build given then take it from the erratum
    BUILD=$(curl --silent --user ':' --negotiate "https://errata.devel.redhat.com/api/v1/erratum/$ERRATUM/builds" | jq -c -r '.[].builds[] | keys[]' | grep "${COLLECTION}-${COMPONENT}" | head -n 1)
    if [[ -z "$BUILD" ]]; then
        echo 'ERROR: no build found' >&2
        exit 1
    fi
fi

DISTRO=$(bkr distros-list --name="${DISTRO}-updates-%" --limit=1 | awk '/Name:/ {print $2}')
if [[ -z "$DISTRO" ]]; then
    echo 'ERROR: no distro found' >&2
    exit 1
fi

# Create a plan
PLAN=$(tcms-plan --id --clean --no-group --no-checklist --automated --general=$GENERAL_PLAN --product=$PRODUCT --distro=$DISTRO $ARCHITECTURES --collection=$COLLECTION --component=$COMPONENT --name="$NAME")

# Create runs; one for the upstream testsuite, one for the rest ("downstream")
TCMS_RUN_COMMON="--id --plan=$PLAN --product=$PRODUCT --distro=$DISTRO --collection=$COLLECTION --component=$COMPONENT --duplicate"
RUN_UP=$(tcms-run $TCMS_RUN_COMMON --filter='tag:  upstream_testsuite' --summary="$NAME/${BUILD// /,}/$DISTRO/upstream")
RUN_DN=$(tcms-run $TCMS_RUN_COMMON --filter='tag: -upstream_testsuite' --summary="$NAME/${BUILD// /,}/$DISTRO/downstream")

# Schedule jobs for the runs
WOW_COMMON="--id --no-errata"\
" $ARCHITECTURES"\
" --distro=$DISTRO"\
" --repo='$COMPOSE'"\
" --collection=$COLLECTION"\
" --install=kernel-debuginfo"\
" --first-task=/tools/${COLLECTION}.${COLLECTION_MINOR}/Install/latest"\
" --first-task=/tools/${COLLECTION}.${COLLECTION_MINOR}/Sanity/filelist"\
" --first-task='! rpm -e devtoolset-12-libstdc++-devel.i686'"\
" --init-task='BUILDS=\"$BUILD\" METHOD=multi PARAMS=\"-x *debuginfo*.i686 -x $COLLECTION-gcc.i686 -x *-docs.i686\" /distribution/install/brew-build'"\
" --init-task='! grep -q x86_64 /proc/version && yum -y install --skip-broken -x *debuginfo*.i686 -x $COLLECTION-gcc.i686 -x *-docs.i686 -x $COLLECTION.i686 $COLLECTION-*.i686'"
$FORCE_UPSTREAM_TESTS && eval bkr workflow-tomorrow "$WOW_COMMON" --run=$RUN_UP
true                  && eval bkr workflow-tomorrow "$WOW_COMMON" --run=$RUN_DN --max=30

# Baby-sit the jobs
if $FORCE_UPSTREAM_TESTS; then
    beaker-jobwatch --run=$RUN_UP --skip-broken-machines &
    beaker-jobwatch --run=$RUN_DN --skip-broken-machines &
    wait
else
    beaker-jobwatch --run=$RUN_DN --skip-broken-machines
fi

# Collect the results
$FORCE_UPSTREAM_TESTS && l-results --no-bugzilla --run=$RUN_UP
true                  && l-results --no-bugzilla --run=$RUN_DN
