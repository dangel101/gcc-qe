#!/usr/bin/env bash

BUILD=${BUILD:-''}
DISTRO=${DISTRO:-''}
ERRATUM=83860
PRODUCT=RHEL-9.0.0

set -o errexit
set -o pipefail

if [[ -z "$BUILD" ]]; then
    # If no build given then take it from the erratum
    BUILD=$(curl --silent --user ':' --negotiate "https://errata.devel.redhat.com/api/v1/erratum/$ERRATUM/builds" | jq -c -r '.[].builds[0] | keys[0]')
    if [[ -z "$BUILD" ]]; then
        echo 'ERROR: no build found' >&2
        exit 1
    fi
fi
if [[ -z "$DISTRO" ]]; then
    DISTRO=$(bkr distros-list --name="${PRODUCT%.*}.%-updates-%" --limit=1 | awk '/Name:/ {print $2}')
    if [[ -z "$DISTRO" ]]; then
        echo 'ERROR: no distro found' >&2
        exit 1
    fi
fi

beaker-jobwatch --skip-broken-machines --job=$(bkr workflow-tomorrow \
    --id \
    --distro=$DISTRO \
    --no-errata \
    --crb \
    --init-task="BUILDS='$BUILD' METHOD=install PARAMS='--skip-broken' /distribution/install/brew-build" \
    --install=tmux --install=rsync \
    --init-task='! dnf -y --enablerepo=beaker-tasks install "gcc-tools-gcc*"' \
    --reserve)
