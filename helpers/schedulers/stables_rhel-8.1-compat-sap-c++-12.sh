#!/usr/bin/env bash

set -o errexit

# hint: tps-make-lists -c <repo>

JOBS=''
JOBS+=",$(bkr workflow-tomorrow --id --arch={x86_64,ppc64le} --stable --profile=stable-rhel-8.1-e4s-sap)" # repo: rhel-8-for-$(arch)-sap-solutions-e4s-rpms-8.1
JOBS+=",$(bkr workflow-tomorrow --id --arch={x86_64,ppc64le} --stable --profile=stable-rhel-8.1-e4s-sap)" # repo: rhel-8-for-$(arch)-sap-netweaver-e4s-rpms-8.1
JOBS=${JOBS#,}

beaker-jobwatch --skip-broken-machines --critical-task=/distribution/install/stable-system --job=$JOBS
