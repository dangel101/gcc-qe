#!/usr/bin/env bash

set -o errexit

# hint: tps-make-lists -c <repo>

# TODO:
#   There's no appropriate profile at the moment and I couldn't create one.
#   As a workaround, let's reserve as if for 8.1 and then
#     1. Edit repos/channels on the machines: 8.1 -> 8.2
#        (anything in /etc and /var/cache/tps/settings/tps_server.conf*)
#     2. Update them
#     3. Then use them for the actual TPS testing

JOBS=''
JOBS+=",$(bkr workflow-tomorrow --id --arch={x86_64,ppc64le} --stable --profile=stable-rhel-8.1-eus-sap)" # repo: rhel-8-for-$(arch)-sap-solutions-eus-rpms-8.2
JOBS+=",$(bkr workflow-tomorrow --id --arch={x86_64,ppc64le} --stable --profile=stable-rhel-8.1-eus-sap)" # repo: rhel-8-for-$(arch)-sap-netweaver-eus-rpms-8.2
JOBS=${JOBS#,}

beaker-jobwatch --skip-broken-machines --critical-task=/distribution/install/stable-system --job=$JOBS
