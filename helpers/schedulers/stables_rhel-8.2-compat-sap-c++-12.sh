#!/usr/bin/env bash

set -o errexit

# hint: tps-make-lists -c <repo>

JOBS=''
JOBS+=",$(bkr workflow-tomorrow --id --arch={x86_64,ppc64le} --stable --profile=stable-rhel-8.2-aus-sap)" # repo: rhel-8-for-$(arch)-sap-solutions-e4s-rpms
JOBS+=",$(bkr workflow-tomorrow --id --arch={x86_64,ppc64le} --stable --profile=stable-rhel-8.2-aus-sap)" # repo: rhel-8-for-$(arch)-sap-netweaver-e4s-rpms
JOBS=${JOBS#,}

beaker-jobwatch --skip-broken-machines --critical-task=/distribution/install/stable-system --job=$JOBS
