#!/usr/bin/env bash

set -o errexit

# hint: tps-make-lists -c <repo>

JOBS=''
JOBS+=",$(bkr workflow-tomorrow --id --arch={aarch64,ppc64le,s390x,x86_64} --stable --profile=rhel-8-for-appstream-rpms)" # repo: rhel-8-for-$(arch)-appstream-rpms
JOBS=${JOBS#,}

beaker-jobwatch --skip-broken-machines --critical-task=/distribution/install/stable-system --job=$JOBS
