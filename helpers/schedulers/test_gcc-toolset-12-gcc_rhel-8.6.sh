#!/usr/bin/env bash

BUILD=${BUILD:-''} # TODO: If GTS-12 doesn't get published in the standard way then we'll need something like BUILD='gcc-toolset-12-12.0-5.el8 gcc-toolset-12-binutils-2.38-16.el8 gcc-toolset-12-gcc-12.1.1-3.2.el8 gcc-toolset-12-annobin-10.76-5.el8 gcc-toolset-12-gdb-11.2-3.el8 gcc-toolset-12-dwz-0.14-2.el8'
DISTRO=${DISTRO:-''}
ERRATUM=95883
PRODUCT=RHEL-8.6.0
COLLECTION=gcc-toolset-12
ARCHITECTURES=aarch64,ppc64le,s390x,x86_64; ARCHITECTURES="--arch=${ARCHITECTURES//,/ --arch=}"
FORCE_UPSTREAM_TESTS=${FORCE_UPSTREAM_TESTS:-false}
COMPONENT=gcc
GENERAL_PLAN=7919
NAME="$COLLECTION-$COMPONENT/$PRODUCT/ER#$ERRATUM"

set -o errexit
set -o pipefail

if [[ -z "$BUILD" ]]; then
    # If no build given then take it from the erratum
    BUILD=$(curl --silent --user ':' --negotiate "https://errata.devel.redhat.com/api/v1/erratum/$ERRATUM/builds" | jq -c -r '.[].builds[0] | keys[0]')
    if [[ -z "$BUILD" ]]; then
        echo 'ERROR: no build found' >&2
        exit 1
    fi
fi
if [[ -z "$DISTRO" ]]; then
    # If no distro given then take the latest nightly compose by EXD (stuff present in errata, i.e. preverifed builds)
    DISTRO=$(bkr distros-list --name="$PRODUCT-%" --tag=CTS_NIGHTLY --limit=1 | awk '/Name:/ {print $2}')
    if [[ -z "$DISTRO" ]]; then
        echo 'ERROR: no distro found' >&2
        exit 1
    fi
fi

# Create a plan
PLAN=$(tcms-plan --id --clean --no-group --no-checklist --automated --general=$GENERAL_PLAN --product=$PRODUCT --distro=$DISTRO $ARCHITECTURES --collection=$COLLECTION --component=$COMPONENT --name="$NAME")

# Create runs; one for the upstream testsuite, one for the rest ("downstream")
TCMS_RUN_COMMON="--id --plan=$PLAN --product=$PRODUCT --distro=$DISTRO --collection=$COLLECTION --component=$COMPONENT --duplicate"
RUN_UP=$(tcms-run $TCMS_RUN_COMMON --filter='tag:  upstream_testsuite' --summary="$NAME/${BUILD// /,}/upstream")
RUN_DN=$(tcms-run $TCMS_RUN_COMMON --filter='tag: -upstream_testsuite' --summary="$NAME/${BUILD// /,}/downstream")

# Schedule jobs for the runs
WOW_COMMON="--id --no-errata"\
" --host-filter=MACHINE_SET__STRONG"\
" --distro=$DISTRO"\
" --crb"\
" --collection=$COLLECTION"\
" --init-task='BUILDS=\"$BUILD\" METHOD=install /distribution/install/brew-build'"\
" --init-task='! dnf -y install --skip-broken $COLLECTION $COLLECTION-*'"\
" --init-task='! grep -q x86_64 /proc/version && dnf -y install --skip-broken -x *debuginfo* -x $COLLECTION-libstdc++-docs -x $COLLECTION-gcc $COLLECTION-*.i686'"
$FORCE_UPSTREAM_TESTS && eval bkr workflow-tomorrow "$WOW_COMMON" --run=$RUN_UP
true                  && eval bkr workflow-tomorrow "$WOW_COMMON" --run=$RUN_DN --max=30

# Baby-sit the jobs
if $FORCE_UPSTREAM_TESTS; then
    beaker-jobwatch --run=$RUN_UP --skip-broken-machines &
    beaker-jobwatch --run=$RUN_DN --skip-broken-machines &
    wait
else
    beaker-jobwatch --run=$RUN_DN --skip-broken-machines
fi

# Collect the results
$FORCE_UPSTREAM_TESTS && l-results --no-bugzilla --run=$RUN_UP
true                  && l-results --no-bugzilla --run=$RUN_DN
