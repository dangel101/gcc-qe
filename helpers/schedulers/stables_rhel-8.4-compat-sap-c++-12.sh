#!/usr/bin/env bash

set -o errexit

# hint: tps-make-lists -c <repo>

JOBS=''
JOBS+=",$(bkr workflow-tomorrow --id --arch={x86_64,ppc64le}       --stable --profile=stable-rhel-8.4-eus-sap)" # -c rhel-8-for-$(arch)-sap-solutions-eus-rpms
JOBS+=",$(bkr workflow-tomorrow --id --arch={x86_64,ppc64le,s390x} --stable --profile=stable-rhel-8.4-eus-sap)" # -c rhel-8-for-$(arch)-sap-netweaver-eus-rpms
JOBS=${JOBS#,}

beaker-jobwatch --skip-broken-machines --critical-task=/distribution/install/stable-system --job=$JOBS
