#!/usr/bin/env bash

# Schedule stable systems, run TPS and reserve them for further processing,
# e.g. reporting the results to Errata Tools.

# Some of the comments below come from problems in our tools (or possibly
# from my my understanding them). Apparently more people have doubts, see
# for example  https://bugzilla.redhat.com/show_bug.cgi?id=1701139.

# Ready-to-use profiles but not very accurate
#   PROFILES='stable-rhel-8-baseos rhel-8-for-appstream-rpms stable-rhel-8-crb'
# My hand-crafted profiles, disabled. See below.
#   PROFILES='vkadlcik-test-rhel8baseos vkadlcik-test-rhel8appstream stable-rhel-8-crb'
if [[ -z "$PROFILES" ]]; then
    echo 'ERROR: PROFILES not set' >&2
    exit 1
fi

ERRATUM=92807

set -o errexit

##############################################################################
##                            NOTES ON PROFILES                             ##
##############################################################################

##############################################################################
# Accuracy of profiles
##############################################################################
#
# Actually, -baseos and -appstream abive are not what I expect from them. How
# the TPS profiles are created, maintained and selected in Errata Tool TPS and
# TPSInBeaker is a complete mystery to me. For the sake of my sanity I
# created:
#   vkadlcik-test-rhel8baseos     http://tps-server.lab.bos.redhat.com/test_profiles/900
#   vkadlcik-test-rhel8appstream  http://tps-server.lab.bos.redhat.com/test_profiles/899
# instead but set them non-active so some unrelated automation doesn't pick
# them up. If you need them, activate them but just temporarily.

##############################################################################
# Skip the RHVN profile
##############################################################################
#
# There should be TPS testing for rhvh-4-for-rhel-8-x86_64-rpms. It's the base
# repo of RHVN (Red Hat Virtualization Host) and is expected to work without
# any RHEL repo. However https://bugzilla.redhat.com/show_bug.cgi?id=1405912
# added to that repo a few debugging tools including gcc (meant for our support
# people, not for customers) that may or may not have all the dependencies
# present. Moreover, we often don't have "good" TPS profiles to build a RHVH
# stable system. To sum it up, the rhvh-4-for-rhel-8-x86_64-rpms TPS jobs is
# unlikely to pass. The QE consensus is just to waive the job (as can be read
# in the ticket above).

##############################################################################
# The "reserve" task needs AppStream
##############################################################################
#
# "wow --reserve" requires (unnecessarily, one might say) several RPMs of
# AppStream. So if we apply a just-BaseOS profile then the reserve task aborts
# the whole beaker job, unless we preinstall the RPMs upfront. That's the
# reason why you see the ugly --init-task below.

##############################################################################
# In theory, one can work even with a bad profile
##############################################################################
#
# # tps-make-lists --help # shows option "-c"
# # dnf repolist
# ...
# # tps-make-lists -c rhel-8-for-s390x-baseos-rpms,rhel-8-for-s390x-baseos-debug-rpms,rhel-8-for-s390x-baseos-source-rpms

JOBS=''
for p in $PROFILES; do
    # Consider --hostrequire=hostlabcontroller=lab-02.rhts.eng.bos.redhat.com
    # for faster access for the NFS volumes used. TODO
    JOBS+=,$(bkr workflow-tomorrow \
        --id \
        --arch={aarch64,ppc64le,s390x,x86_64} \
        --errata=$ERRATUM \
        --profile=$p \
        --tps-rpmtest \
        --tps-srpmtest \
        --init-task='! dnf --repofrompath=tmpappstr,http://rhsm-pulp.corp.redhat.com/content/dist/rhel8/8/\$basearch/appstream/os install -y emacs vim-enhanced sendmail psmisc' \
        --reserve)
done
JOBS=${JOBS#,}
beaker-jobwatch --skip-broken-machines --critical-task=/distribution/install/stable-system --job=$JOBS

##############################################################################
##                           HOW TO REPORT RESULTS                          ##
##############################################################################
#
# * Option 1: Just waive the test in Errata Tool
#
# * Option 2: tps-results --errata ERRATA_ID --job JOB_ID
#
#     However it's not very good at finding the proper TPS job ID, e.g. it
#     ignores TPS Stream and Repo Name. For components like gcc (split across
#     several streams) it's a wrong tool. Unfortunately this option is what
#     TPSInBeaker uses so TPSInBeaker is better to be avoided.
#
# * Option 3: rbiba's script
#
#     On a machine that has passed TPS and is being reserved by the jobs
#     above:
#       * cat /etc/sysconfig/tps-profile # to orient yourself
#       * mount /mnt/redhat; mount /mnt/qa
#       * # Find the directory where TPS ran (e.g. in taskout.log in Beaker) and
#         # and go there. With some luck, this might work:
#         cd $(grep SCRATCH_PATH= /var/tmp/restraintd/logs/*/task.log | tail -n 1 | sed 's/.*=//')
#       * /mnt/qa/scratch/rbiba/tps-utils/tps-report-interactive
#         # id=... # the correspondig job id in Errata Tool
#         # echo -e "${id}\n\n" | /mnt/qa/scratch/rbiba/tps-utils/tps-report-interactive
