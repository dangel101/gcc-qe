#!/usr/bin/env bash

BUILD=${BUILD:-''}
ERRATUM=83566
PRODUCT=rhscl-3.7
DISTRO=RHEL-7.9
COMPOSE=${COMPOSE:-'http://download.lab.bos.redhat.com/rhel-7/nightly/RHSCL/latest-RHSCL-3.7-RHEL-7/compose/Server/$basearch/os'}
COLLECTION=devtoolset-10
ARCHITECTURES=${ARCHITECTURES:-x86_64,ppc64,ppc64le,s390x}; ARCHITECTURES="--arch=${ARCHITECTURES//,/ --arch=}"
FORCE_UPSTREAM_TESTS=${FORCE_UPSTREAM_TESTS:-false}
COMPONENT=gcc
GENERAL_PLAN=7919
NAME="$COLLECTION-$COMPONENT/${PRODUCT}/ER#$ERRATUM"

set -o errexit
set -o pipefail

if [[ -z "$BUILD" ]]; then
    # If no build given then take it from the erratum
    BUILD=$(curl --silent --user ':' --negotiate "https://errata.devel.redhat.com/api/v1/erratum/$ERRATUM/builds" | jq -c -r '.[].builds[0] | keys[0]' | head -n 1)
    if [[ -z "$BUILD" ]]; then
        echo 'ERROR: no build found' >&2
        exit 1
    fi
fi

DISTRO=$(bkr distros-list --name="${DISTRO}-updates-%" --limit=1 | awk '/Name:/ {print $2}')
if [[ -z "$DISTRO" ]]; then
    echo 'ERROR: no distro found' >&2
    exit 1
fi

# Create a plan
PLAN=$(tcms-plan --id --clean --no-group --no-checklist --automated --general=$GENERAL_PLAN --product=$PRODUCT --distro=$DISTRO $ARCHITECTURES --collection=$COLLECTION --component=$COMPONENT --name="$NAME")

# Create runs; one for the upstream testsuite, one for the rest ("downstream")
TCMS_RUN_COMMON="--id --plan=$PLAN --product=$PRODUCT --distro=$DISTRO --collection=$COLLECTION --component=$COMPONENT --duplicate"
RUN_UP=$(tcms-run $TCMS_RUN_COMMON --filter='tag:  upstream_testsuite' --summary="$NAME/$BUILD/$DISTRO/upstream")
RUN_DN=$(tcms-run $TCMS_RUN_COMMON --filter='tag: -upstream_testsuite' --summary="$NAME/$BUILD/$DISTRO/downstream")

# Schedule jobs for the runs
WOW_COMMON="--id --no-errata"\
" --host-filter=MACHINE_SET__STRONG"\
" $ARCHITECTURES"\
" --distro=$DISTRO"\
" --collection=$COLLECTION"\
" --repo='$COMPOSE'"\
" --install=kernel-debuginfo"\
" --init-task='BUILDS=\"$BUILD\" METHOD=install PARAMS=--skip-broken /distribution/install/brew-build'"\
" --init-task='! yum -y install --skip-broken $COLLECTION $COLLECTION-*'"\
" --init-task='! grep -q x86_64 /proc/version && yum -y install --skip-broken -x *debuginfo* -x $COLLECTION-*-docs -x $COLLECTION-gcc -x $COLLECTION-runtime $COLLECTION-*.i686'"\
" --taskparam=SKIP_COLLECTION_METAPACKAGE_CHECK=$COLLECTION"
$FORCE_UPSTREAM_TESTS && eval bkr workflow-tomorrow "$WOW_COMMON" --run=$RUN_UP
true                  && eval bkr workflow-tomorrow "$WOW_COMMON" --run=$RUN_DN --max=30

# Baby-sit the jobs
if $FORCE_UPSTREAM_TESTS; then
    beaker-jobwatch --run=$RUN_UP --skip-broken-machines &
    beaker-jobwatch --run=$RUN_DN --skip-broken-machines &
    wait
else
    beaker-jobwatch --run=$RUN_DN --skip-broken-machines
fi

# Collect the results
$FORCE_UPSTREAM_TESTS && l-results --no-bugzilla --run=$RUN_UP
true                  && l-results --no-bugzilla --run=$RUN_DN
