#!/usr/bin/env bash

set -o errexit
set -o pipefail

COMPOSE_EL6='http://download.lab.bos.redhat.com/rhel-6/nightly/RHSCL/latest-RHSCL-3.9-RHEL-6/compose/Server/$basearch/os' # no /updates/, contrary to RHEL-7
COMPOSE_EL7='http://download.lab.bos.redhat.com/rhel-7/nightly/updates/RHSCL/latest-RHSCL-3.9-RHEL-7/compose/Server/$basearch/os'

DISTRO6=$(bkr distros-list --name='RHEL-6.10-updates-%' --limit=1 | awk '/Name:/ {print $2}')
DISTRO7=$(bkr distros-list --name='RHEL-7.9-updates-%'  --limit=1 | awk '/Name:/ {print $2}')
echo "DISTRO6=$DISTRO6"
echo "DISTRO7=$DISTRO7"
if [[ -z "$DISTRO6" ]] || [[ -z "$DISTRO7" ]]; then
    exit 1
fi

reserve () {
    bkr workflow-tomorrow \
        --id --no-errata \
        --host-filter=MACHINE_SET__STRONG \
        --arch=$ARCHITECTURES \
        --distro=$DISTRO \
        --repo="$COMPOSE" \
        --install=kernel-debuginfo \
        --init-task='! yum -y install --skip-broken rsync tmux screen' \
        --init-task=/tools/devtoolset-12.0/Install/latest \
        --init-task=/tools/devtoolset-12.0/Sanity/filelist \
        --reserve
}

  ids=$(DISTRO=$DISTRO6 ARCHITECTURES=x86_64                     COMPOSE="$COMPOSE_EL6" reserve)
ids+=,$(DISTRO=$DISTRO7 ARCHITECTURES=x86_64,s390x,ppc64le,ppc64 COMPOSE="$COMPOSE_EL7" reserve)
beaker-jobwatch --skip-broken-machines --job=$ids
