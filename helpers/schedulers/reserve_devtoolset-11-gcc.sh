#!/usr/bin/env bash

PRODUCT=rhscl-3.8
COLLECTION=devtoolset-11
BUILD_EL6=devtoolset-11-gcc-11.2.1-9.el6
BUILD_EL7=devtoolset-11-gcc-11.2.1-9.1.el7
COMPOSE_EL6='http://download.lab.bos.redhat.com/rhel-6/nightly/RHSCL/latest-RHSCL-3.8-RHEL-6/compose/Server/$basearch/os'
COMPOSE_EL7='http://download.lab.bos.redhat.com/rhel-7/nightly/RHSCL/latest-RHSCL-3.8-RHEL-7/compose/Server/$basearch/os'

set -o errexit
set -o pipefail

reserve () {
# TODO:
#   - If keeping these init-tasks then
#     - Streamline the exclusions in them if possible
#   - Else
#     - Switch to our team installer and filechecker once they appear
WOW_COMMON="--id --no-errata"\
" --host-filter=MACHINE_SET__STRONG"\
" --arch=$ARCHITECTURES"\
" --distro=$DISTRO"\
" --collection=$COLLECTION"\
" --repo='$COMPOSE'"\
" --install=kernel-debuginfo"\
" --init-task='! yum -y install --skip-broken rsync tmux screen'"\
" --init-task='BUILDS=\"$BUILD\" METHOD=install PARAMS=\"--skip-broken\" /distribution/install/brew-build'"\
" --init-task='! yum -y install --skip-broken $COLLECTION $COLLECTION-*'"\
" --init-task='! yum -y install --skip-broken -x *debuginfo* -x $COLLECTION-*-docs -x $COLLECTION-gcc -x $COLLECTION-runtime $COLLECTION-*.i686'"
eval bkr workflow-tomorrow "$WOW_COMMON" --reserve
}

  ids=$(DISTRO=RHEL-6.10-updates-20201110.17 ARCHITECTURES=x86_64                     COMPOSE="$COMPOSE_EL6" BUILD=$BUILD_EL6 reserve)
ids+=,$(DISTRO=RHEL-7.9-updates-20220104.0   ARCHITECTURES=x86_64,s390x,ppc64le,ppc64 COMPOSE="$COMPOSE_EL7" BUILD=$BUILD_EL7 reserve)
ids+=,$(DISTRO=RHEL-7.7-updates-20200310.0   ARCHITECTURES=x86_64,ppc64le             COMPOSE="$COMPOSE_EL7" BUILD=$BUILD_EL7 reserve)
beaker-jobwatch --skip-broken-machines --job=$ids
