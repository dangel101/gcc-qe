#!/usr/bin/env bash

# Based on https://gitlab.cee.redhat.com/baseos-qe/citool-config/-/blob/production/brew-dispatcher-rhel9.yaml.
# The same tests used to be used for "buildroot-ready" testing of GCC.
#
# Example: BUILD=gcc-11.2.1-2.1.el9,glibc-2.33.9000-55.el9,redhat-rpm-config-187-1.el9,annobin-9.83-2.el9 DISTRO=RHEL-9.0.0-20210729.2 ./gating_gcc-rhel-9.sh

BUILD=${BUILD:-''}
DISTRO=${DISTRO:-''}
if [[ -z "$BUILD" ]] || [[ -z "$DISTRO" ]]; then
    echo "Incomplete data: BUILD=$BUILD DISTRO=$DISTRO" >&2
    exit 1
fi

PLAN=14493
ENABLE_BUILDROOT='--taskparam=BUILDROOT_COMPOSE=latest --init-task=/distribution/install/rhel-buildroot'
ENABLE_CRB=--crb

JOB1=$(bkr workflow-tomorrow --id --no-errata --brew-build=$BUILD --brew-method=install --distro=$DISTRO --no-arch=ppc64le --host-filter=MACHINE_SET__STRONG --taskparam=DISABLE_SEC_ARCH_FILTER=yes $ENABLE_BUILDROOT $ENABLE_CRB --first-testing-task=/distribution/runtime_tests/verify-nvr-installed --task='DONT_CHECK_BUGS=1 /tools/glibc/Sanity/compile-test' --plan=$PLAN)
JOB2=$(bkr workflow-tomorrow --id --no-errata --brew-build=$BUILD --brew-method=install --distro=$DISTRO --arch=ppc64le    --host-filter=IBM__POWER9         --taskparam=DISABLE_SEC_ARCH_FILTER=yes $ENABLE_BUILDROOT $ENABLE_CRB --first-testing-task=/distribution/runtime_tests/verify-nvr-installed --task='DONT_CHECK_BUGS=1 /tools/glibc/Sanity/compile-test' --plan=$PLAN)

beaker-jobwatch --job=$JOB1,$JOB2 --skip-broken-machines
