#!/usr/bin/env bash

set -o errexit

JOBS=''
JOBS+=",$(bkr workflow-tomorrow --id --arch=x86_64                       --stable --profile=stable-rhel-7-workstation)"
JOBS+=",$(bkr workflow-tomorrow --id --arch={x86_64,ppc64le,ppc64,s390x} --stable --profile=stable-rhel-7-server)"
JOBS+=",$(bkr workflow-tomorrow --id --arch={x86_64,ppc64le}             --stable --profile=stable-rhel-7.7-z-server)"
JOBS=${JOBS#,}

beaker-jobwatch --skip-broken-machines --critical-task=/distribution/install/stable-system --job=$JOBS
