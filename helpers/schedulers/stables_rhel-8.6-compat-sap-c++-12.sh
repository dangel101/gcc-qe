#!/usr/bin/env bash

set -o errexit

# hint: tps-make-lists -c <repo>

# TODO: Is it OK? At the moment it's too soon to create a 8.6 EUS profile, the web UI doesn't allow it
JOBS=''
JOBS+=",$(bkr workflow-tomorrow --id --arch={x86_64,ppc64le}       --stable --profile=stable-rhel-8-server-sap)" # repo: rhel-8-for-$(arch)-sap-solutions-rpms
JOBS+=",$(bkr workflow-tomorrow --id --arch={x86_64,ppc64le,s390x} --stable --profile=sap-netweaver-rhel-8)"     # rhel-8-for-$(arch)-sap-netweaver-rpms
JOBS=${JOBS#,}

beaker-jobwatch --skip-broken-machines --critical-task=/distribution/install/stable-system --job=$JOBS
