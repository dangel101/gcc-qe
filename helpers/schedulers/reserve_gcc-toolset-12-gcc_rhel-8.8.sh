#!/usr/bin/env bash

BUILD=${BUILD:-''}
DISTRO=${DISTRO:-''}
ERRATUM=103071
PRODUCT=RHEL-8.8.0

set -o errexit
set -o pipefail

if [[ -z "$BUILD" ]]; then
    # If no build given then take it from the erratum
    BUILD=$(curl --silent --user ':' --negotiate "https://errata.devel.redhat.com/api/v1/erratum/$ERRATUM/builds" | jq -c -r '.[].builds[0] | keys[0]')
    if [[ -z "$BUILD" ]]; then
        echo 'ERROR: no build found' >&2
        exit 1
    fi
fi
if [[ -z "$DISTRO" ]]; then
    # If no distro given then take the latest nightly compose by EXD (stuff present in errata, i.e. preverifed builds)
    DISTRO=$(bkr distros-list --name="$PRODUCT-%" --tag=CTS_NIGHTLY --limit=1 | awk '/Name:/ {print $2}')
    if [[ -z "$DISTRO" ]]; then
        echo 'ERROR: no distro found' >&2
        exit 1
    fi
fi

beaker-jobwatch --skip-broken-machines --job=$(bkr workflow-tomorrow \
    --id \
    --distro=$DISTRO \
    --crb \
    --no-errata --brew-build=$BUILD --brew-method=install \
    --install=tmux --install=rsync \
    --init-task='! dnf -y install --skip-broken gcc-toolset-12 "gcc-toolset-12-*"' \
    --init-task='! dnf -y install --skip-broken -x "*debuginfo*" -x gcc-toolset-12-libstdc++-docs -x gcc-toolset-12-gcc "gcc-toolset-12-*.i686"' \
    --init-task='! dnf -y --enablerepo=beaker-tasks install "gcc-tools-gcc*"' \
    --reserve)
