#!/usr/bin/env bash

# Based on https://gitlab.cee.redhat.com/baseos-qe/citool-config/-/blob/production/brew-dispatcher.yaml.
#
# Example: BUILD=gcc-toolset-12-gcc-12.1.1-1.2.el8 DISTRO=RHEL-8.7.0-20220601.0 ./gating_gcc-toolset-12-gcc_rhel-8.sh

BUILD=${BUILD:-''}
DISTRO=${DISTRO:-''}
if [[ -z "$BUILD" ]] || [[ -z "$DISTRO" ]]; then
    echo "Incomplete data: BUILD=$BUILD DISTRO=$DISTRO" >&2
    exit 1
fi

ENABLE_CRB=--crb

JOB=$(bkr workflow-tomorrow --id \
    --no-errata \
    --brew-build=$BUILD --brew-method=install \
    --distro=$DISTRO $ENABLE_CRB \
    --collection=gcc-toolset-12 \
    --first-task '! yum -y remove libasan\* libubsan\* libatomic\* libitm\* libtsan\*' \
    --init-task /tools/toolchain-common/Install/report-system-packages \
    --plan=7919 --tag CI-Tier-1 --no-tag=CI-NO-DTS)

beaker-jobwatch --job=$JOB --skip-broken-machines
