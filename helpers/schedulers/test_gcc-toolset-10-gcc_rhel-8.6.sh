#!/usr/bin/env bash

BUILD=${BUILD:-''}
DISTRO=${DISTRO:-''}
BUILDROOT_COMPOSE=${BUILDROOT_COMPOSE:-latest-BUILDROOT-8-RHEL-8} # TODO use latest when it starts to work again
ERRATUM=83570
PRODUCT=RHEL-8.6.0
COLLECTION=gcc-toolset-10
ARCHITECTURES=aarch64,ppc64le,s390x,x86_64; ARCHITECTURES="--arch=${ARCHITECTURES//,/ --arch=}"
FORCE_UPSTREAM_TESTS=${FORCE_UPSTREAM_TESTS:-false}
COMPONENT=gcc
GENERAL_PLAN=7919
NAME="$COLLECTION-$COMPONENT/$PRODUCT/ER#$ERRATUM"

set -o errexit
set -o pipefail

if [[ -z "$BUILD" ]]; then
    # If no build given then take it from the erratum
    BUILD=$(curl --silent --user ':' --negotiate "https://errata.devel.redhat.com/api/v1/erratum/$ERRATUM/builds" | jq -c -r '.[].builds[0] | keys[0]')
    if [[ -z "$BUILD" ]]; then
        echo 'ERROR: no build found' >&2
        exit 1
    fi
fi
if [[ -z "$DISTRO" ]]; then
    DISTRO=$(bkr distros-list --name="${PRODUCT%.*}.%-updates-%" --limit=1 | awk '/Name:/ {print $2}')
    if [[ -z "$DISTRO" ]]; then
        echo 'ERROR: no distro found' >&2
        exit 1
    fi
fi

# TODO:
# Properly tag all our tests needing buildroot packages, then drop
#   --taskparam='BUILDROOT_COMPOSE=search-for-specific' --init-task=/distribution/install/rhel-buildroot
# and use
#   --split-buildroot-compose=search-for-specific
# instead so we'll use buildroots only when necessary.

# Create a plan
PLAN=$(tcms-plan --id --clean --no-group --no-checklist --automated --general=$GENERAL_PLAN --product=$PRODUCT --distro=$DISTRO $ARCHITECTURES --collection=$COLLECTION --component=$COMPONENT --name="$NAME")

# Create runs; one for the upstream testsuite, one for the rest ("downstream")
TCMS_RUN_COMMON="--id --plan=$PLAN --product=$PRODUCT --distro=$DISTRO --collection=$COLLECTION --component=$COMPONENT --duplicate"
RUN_UP=$(tcms-run $TCMS_RUN_COMMON --filter='tag:  upstream_testsuite' --summary="$NAME/$BUILD/upstream")
RUN_DN=$(tcms-run $TCMS_RUN_COMMON --filter='tag: -upstream_testsuite' --summary="$NAME/$BUILD/downstream")

# Schedule jobs for the runs
WOW_COMMON="--id --no-errata --brew-build=$BUILD --brew-method=install --distro=$DISTRO --collection=$COLLECTION --host-filter=MACHINE_SET__STRONG --crb --taskparam=BUILDROOT_COMPOSE=$BUILDROOT_COMPOSE --init-task=/distribution/install/rhel-buildroot --init-task='! dnf -y install --skip-broken $COLLECTION $COLLECTION-*' --init-task='! grep -q x86_64 /proc/version && dnf -y install --skip-broken -x *debuginfo* -x $COLLECTION-libstdc++-docs -x $COLLECTION-gcc $COLLECTION-*.i686'"
$FORCE_UPSTREAM_TESTS && eval bkr workflow-tomorrow "$WOW_COMMON" --run=$RUN_UP
true                  && eval bkr workflow-tomorrow "$WOW_COMMON" --run=$RUN_DN --max=30

# Baby-sit the jobs
if $FORCE_UPSTREAM_TESTS; then
    beaker-jobwatch --run=$RUN_UP --skip-broken-machines &
    beaker-jobwatch --run=$RUN_DN --skip-broken-machines &
    wait
else
    beaker-jobwatch --run=$RUN_DN --skip-broken-machines
fi

# Collect the results
$FORCE_UPSTREAM_TESTS && l-results --no-bugzilla --run=$RUN_UP
true                  && l-results --no-bugzilla --run=$RUN_DN
