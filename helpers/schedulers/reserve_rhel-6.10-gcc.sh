#!/usr/bin/env bash

BUILD=${BUILD:-''}
DISTRO=${DISTRO:-''}
ERRATUM=31785
PRODUCT=RHEL-6.10

set -o errexit
set -o pipefail

if [[ -z "$BUILD" ]]; then
    # If no build given then take it from the erratum
    BUILD=$(curl --silent --user ':' --negotiate "https://errata.devel.redhat.com/api/v1/erratum/$ERRATUM/builds" | jq -c -r '.[].builds[0] | keys[0]')
    if [[ -z "$BUILD" ]]; then
        echo 'ERROR: no build found' >&2
        exit 1
    fi
fi
if [[ -z "$DISTRO" ]]; then
    # If no distro given then take the latest nightly compose by EXD (stuff present in errata, i.e. preverifed builds)
    DISTRO=$(bkr distros-list --name="$PRODUCT-%" --limit=1 | awk '/Name:/ {print $2}')
    if [[ -z "$DISTRO" ]]; then
        echo 'ERROR: no distro found' >&2
        exit 1
    fi
fi

beaker-jobwatch --skip-broken-machines --job=$(bkr workflow-tomorrow \
    --id \
    --no-errata \
    --host-filter=MACHINE_SET__STRONG \
    --arch=x86_64,s390x,i386 \
    --distro=$DISTRO \
    --install=kernel-debuginfo \
    --init-task="BUILDS='$BUILD' METHOD=install PARAMS='--skip-broken' /distribution/install/brew-build" \
    --install=rsync \
    --install=libevent2-devel \
    --install=ncurses-devel \
    --init-task='! yum -y --skip-broken --enablerepo=beaker-tasks install "gcc-tools-gcc*"' \
    --init-task='! wget https://github.com/tmux/tmux/releases/download/2.9a/tmux-2.9a.tar.gz && tar xf tmux-2.9a.tar.gz && cd tmux-2.9a && ./configure --prefix=/usr/local && make install' \
    --reserve)
