#!/usr/bin/env bash

set -o errexit

# hint: tps-make-lists -c <repo>

# TODO: unify with tps_rhel-8-gcc.sh

# Notes
# * psmisc MTA vim-enhanced emacs pre-installed because they come from AppStream;
#   machines without AppStream cannot install the reserve task
# * perl-XML-Twig pre-installed because it comes from CRB, machines without CRB
#   cannot run the stable-system task
# * The profiles used below are the best I could find but usually unfit anyway.
#   Once you login you'd better check and fix (remove, mostly) the active
#   repositories to the correct set (in *both* the setups, live *and* qa!), i.e.
#   * just the *-baseos-* repos for the baseos profile
#   * baseos+appstream for the appstream profile
#   * baseos+appstream+crb for the codeready-builder profile
#   * rhvh-4 is some special thing we don't really need to care, mcermak told
#     (gcc is there just for debugging purposes for RHEL support, not to be used
#     by customers) -> we can just waive its TPS

JOBS=''
JOBS+=",$(bkr workflow-tomorrow --id --arch={aarch64,ppc64le,s390x,x86_64} --distro=RHEL-8 --task='!dnf -y install psmisc MTA vim-enhanced emacs' --task='!dnf -y --enablerepo=beaker-CRB install perl-XML-Twig' --task='STABLE=stable-rhel-8-baseos      /distribution/install/stable-system' --reserve)" # repo: rhel-8-for-$(arch)-baseos-rpms__8
JOBS+=",$(bkr workflow-tomorrow --id --arch={aarch64,ppc64le,s390x,x86_64} --distro=RHEL-8 --task='!dnf -y install psmisc MTA vim-enhanced emacs' --task='!dnf -y --enablerepo=beaker-CRB install perl-XML-Twig' --task='STABLE=rhel-8-for-appstream-rpms /distribution/install/stable-system' --reserve)" # repo: rhel-8-for-$(arch)-appstream-rpms__8
JOBS+=",$(bkr workflow-tomorrow --id --arch={aarch64,ppc64le,s390x,x86_64} --distro=RHEL-8 --task='!dnf -y install psmisc MTA vim-enhanced emacs' --task='!dnf -y --enablerepo=beaker-CRB install perl-XML-Twig' --task='STABLE=stable-rhel-8-crb         /distribution/install/stable-system' --reserve)" # repo: codeready-builder-for-rhel-8-$(arch)-rpms__8
JOBS+=",$(bkr workflow-tomorrow --id --arch=x86_64                         --distro=RHEL-8 --task='!dnf -y install psmisc MTA vim-enhanced emacs' --task='!dnf -y --enablerepo=beaker-CRB install perl-XML-Twig' --task='STABLE=stable-rhvh-4-profile     /distribution/install/stable-system' --reserve)" # repo: rhvh-4-for-rhel-8-$(arch)-rpms
JOBS=${JOBS#,}

beaker-jobwatch --skip-broken-machines --critical-task=/distribution/install/stable-system --job=$JOBS
