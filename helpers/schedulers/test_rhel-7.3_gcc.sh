#!/usr/bin/env bash

BUILD=${BUILD:-''}
DISTRO=RHEL-7.3.z
ERRATUM=33233
PRODUCT=RHEL-7.3
ARCHITECTURES=x86_64; ARCHITECTURES="--arch=${ARCHITECTURES//,/ --arch=}"
FORCE_UPSTREAM_TESTS=${FORCE_UPSTREAM_TESTS:-false}
COMPONENT=gcc
GENERAL_PLAN=7919
NAME="$COMPONENT/$PRODUCT/ER#$ERRATUM"

set -o errexit
set -o pipefail

if [[ -z "$BUILD" ]]; then
    # If no build given then take it from the erratum
    BUILD=$(curl --silent --user ':' --negotiate "https://errata.devel.redhat.com/api/v1/erratum/$ERRATUM/builds" | jq -c -r '.[].builds[0] | keys[0]' | head -n 1)
    if [[ -z "$BUILD" ]]; then
        echo 'ERROR: no build found' >&2
        exit 1
    fi
fi

echo "DISTRO=$DISTRO"
echo "BUILD=$BUILD"

# Create a plan
PLAN=$(tcms-plan --id --clean --no-group --no-checklist --automated --general=$GENERAL_PLAN --product=$PRODUCT --distro=$DISTRO $ARCHITECTURES --component=$COMPONENT --name="$NAME")

# Create runs; one for the upstream testsuite, one for the rest ("downstream")
TCMS_RUN_COMMON="--id --plan=$PLAN --product=$PRODUCT --distro=$DISTRO --component=$COMPONENT --duplicate"
RUN_UP=$(tcms-run $TCMS_RUN_COMMON --filter='tag:  upstream_testsuite' --summary="$NAME/$BUILD/upstream")
RUN_DN=$(tcms-run $TCMS_RUN_COMMON --filter='tag: -upstream_testsuite' --summary="$NAME/$BUILD/downstream")

# Schedule jobs for the runs
WOW_COMMON="--id --no-errata"\
" --host-filter=MACHINE_SET__STRONG"\
" $ARCHITECTURES"\
" --distro=$DISTRO --update"\
" --install=kernel-debuginfo"\
" --init-task='! yum-config-manager --disable *rhscl*'"\
" --init-task='BUILDS=$BUILD METHOD=install PARAMS=--skip-broken /distribution/install/brew-build'"\
" --init-task='! yum -y install --skip-broken gcc gcc-* lib*san* libatomic* libgcc* libgfortran* libgomp* libitm* libquadmath* libstdc++*'"\
" --init-task='! grep -q x86_64 /proc/version && yum -y install --skip-broken -x *debuginfo* -x *-docs -x gcc gcc-*.i686 lib*san*.i686 libatomic*.i686 libgcc*.i686 libgfortran*.i686 libgomp*.i686 libitm*.i686 libquadmath*.i686 libstdc++*.i686'"
$FORCE_UPSTREAM_TESTS && eval bkr workflow-tomorrow "$WOW_COMMON" --run=$RUN_UP
true                  && eval bkr workflow-tomorrow "$WOW_COMMON" --run=$RUN_DN --max=30

# Baby-sit the jobs
if $FORCE_UPSTREAM_TESTS; then
    beaker-jobwatch --run=$RUN_UP --skip-broken-machines &
    beaker-jobwatch --run=$RUN_DN --skip-broken-machines &
    wait
else
    beaker-jobwatch --run=$RUN_DN --skip-broken-machines
fi

# Collect the results
$FORCE_UPSTREAM_TESTS && l-results --no-bugzilla --run=$RUN_UP
true                  && l-results --no-bugzilla --run=$RUN_DN
