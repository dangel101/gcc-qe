#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Sanity/fortran-compat-test-suite-RedHatDrop
#   Description: Runs a fortran testsuite we got from Bloomberg
#   Author: Michael Petlan <mpetlan@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2018 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="gcc"

# the testcase no. 014 is expected to fail
BLACKLIST="014"

rlJournalStart
	rlPhaseStartSetup
		rlAssertRpm $PACKAGE
		rlAssertRpm $PACKAGE-gfortran

		# hack the System::Command perl module in the system
		# (it seems that package perl-System-Command is only on Fedora)
		rlRun "mkdir -p /usr/share/perl5/vendor_perl/System/Command"
		rlRun "cp Command.pm /usr/share/perl5/vendor_perl/System/" 0 "Copying Command.pm perl module"
		rlRun "cp Reaper.pm /usr/share/perl5/vendor_perl/System/Command/" 0 "Copying Reaper.pm perl module"

		# unpack testsuite
		rlRun "zcat fortran-compat-test-suite-RedHatDrop-20180507.tar.gz | tar x" 0 "Unpacking the testsuite"
		rlRun "pushd fortran-compat-test-suite-RedHatDrop-20180507"
	rlPhaseEnd

	rlPhaseStartTest
		# run the testsuite
		rlRun "perl runtests > testsuite.out 2> testsuite.err" 0-255 "Running the testsuite"

		# print logs
		for i in out err; do
			echo "============== testsuite.$i =============="
			cat testsuite.$i
			echo "=========================================="
		done

		# process the results
		cat testsuite.out | grep -P '\s\.\.+\s' | grep -v -P 'ok$' > failed.log
		# apply blacklisting of XFAILs
		for tc in $BLACKLIST; do
			cat failed.log | grep -v $tc > failed.1
			mv failed.1 failed.log
		done
		rlAssert0 "There are zero unexpected failures" `cat failed.log | wc -l`
	rlPhaseEnd

	rlPhaseStartCleanup
		rlRun "popd"
		rlRun "rm -rf fortran-compat-test-suite-RedHatDrop-20180507" 0 "Removing tmp directory"
	rlPhaseEnd
rlJournalPrintText
rlJournalEnd
