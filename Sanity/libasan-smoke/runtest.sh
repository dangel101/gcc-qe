#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Sanity/libasan-smoke
#   Description: What the test does
#   Author: Martin Cermak <mcermak@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2016 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

# Notes on asan packaging
# =======================
#
# system rhel-6 - no asan
# system rhel-7 - libasan and libasan-static on x86_64 and ppc64 (64bit and 32 on both)
# system rhel-8 - libasan on all, 32bit on x86_64 only
# system rhel-9 - libasan on all, 32bit on x86_64 only
#
# dts-10 rhel-6 - libasan6 and devtoolset-10-libasan-devel on x86_64, both 32bit too
# dts-11 rhel-6 - libasan6 and devtoolset-11-libasan-devel on x86_64, both 32bit too
# dts-12 rhel-6 - libasan8 and devtoolset-12-libasan-devel on x86_64, both 32bit too
#
# dts-10 rhel-7 - libasan6 on all (32bit on x86_64 only),  devtoolset-10-libasan-devel an all (32bit on x86_64 only)
# dts-11 rhel-7 - libasan6 on all (32bit on x86_64 only),  devtoolset-11-libasan-devel an all (32bit on x86_64 only)
# dts-12 rhel-7 - libasan8 on all (32bit on x86_64 only),  devtoolset-12-libasan-devel an all (32bit on x86_64 only)
#
# gts-10 rhel-8 - libasan6 on all (32bit on x86_64 only), gcc-toolset-10-libasan-devel an all (32bit on x86_64 only)
# gts-11 rhel-8 - libasan6 on all (32bit on x86_64 only), gcc-toolset-11-libasan-devel an all (32bit on x86_64 only)
# gts-12 rhel-8 - libasan8 on all (32bit on x86_64 only), gcc-toolset-12-libasan-devel on all (32bit on x86_64 only)
#
# gts-12 rhel-9 - libasan8 on all (32bit on x86_64 only), gcc-toolset-12-libasan-devel on all (32bit on x86_64 only)
#
# system libasan installs some libasan.so
# system libasan-static installs libasan.a
# libasanX of toolset Y installs libasan.so.X
# *toolset-Y-libasan-X of toolset Y installs libasan.so.X (linker script referring libasanX) and libasan.a
#
# Notes on relevancy
# ==================
#
# This test is not relevant for:
#   - system GCC of RHEL <7
#   - system GCC of RHEL-7 on ppc64le and s390x
# For system GCC of RHEL-7 before 7.9 on x86_64 and ppc64 *is* relevant but too
# much effort to run properly when using workflow-tomorrow. The problem is that
# those systems get some RHSCL repos enabled and installing libasan leads to
# installing libasan of some older DTS. Same name as the system one but newer
# version, incompatible with system GCC; the test fails. Seems not worthy of
# fixing, the older RHEL-7 z-streams are long behind their prime. Nevertheless,
# a possible workaround can be seen in ../../Regression/bz1312850-*
# Suggested TCMS relevancy:
#   distro < rhel-7 && collection !defined: False
#   distro = rhel-7 && distro < rhel-7.9 && collection !defined: False
#   distro = rhel-7 && collection !defined && arch = ppc64le: False
#   distro = rhel-7 && collection !defined && arch = s390x: False

GCC=${GCC:-$(type -P gcc)}
GCC_RPM_NAME=$(rpm --qf '%{name}' -qf $GCC)
PRI_ARCH=$(rlGetPrimaryArch)

if [[ "$GCC_RPM_NAME" == *toolset* ]]; then
    TOOLSET=${GCC_RPM_NAME%-gcc}
else
    TOOLSET=''
fi

PACKAGES="$GCC_RPM_NAME"
if [[ -z "$TOOLSET" ]]; then
    PACKAGES+=" libasan.$PRI_ARCH"
else
    PACKAGES+=" ${TOOLSET}-libasan-devel.$PRI_ARCH" # libasanX of the toolset will come as a dep
fi

rlJournalStart
    rlPhaseStartSetup
        rlLogInfo "GCC=$GCC"
        rlLogInfo "GCC_RPM_NAME=$GCC_RPM_NAME"
        rlLogInfo "PRI_ARCH=$PRI_ARCH"
        rlLogInfo "TOOLSET=$TOOLSET"

        rlRun "yum install -y --skip-broken $PACKAGES"
        for i in $PACKAGES; do
          rlAssertRpm $i
        done
    rlPhaseEnd

    rlPhaseStartTest
        # See bz1386146 for how to use libasan, and what RPMs does it requires.
        rlRun "echo 'int main() { return 0; }' | gcc -fsanitize=address -o /dev/null -xc -"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
