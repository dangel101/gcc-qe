#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Sanity/libitm-smoke
#   Description: Just runs prebuilt binaries
#   Author: Michael Petlan <mpetlan@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2016 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

# Relevancy: All supported GCCs except for base RHEL-6/s390x where there's
# no libitm. Suggested TCMS relevancy:
#   distro = rhel-6 && arch = s390x && collection !defined: False

GCC=${GCC:-$(type -P gcc)}
GCC_RPM_NAME=$(rpm --qf '%{name}' -qf $GCC)
PACKAGES="$GCC_RPM_NAME"

if [[ "$GCC_RPM_NAME" == *toolset* ]]; then
    TOOLSET=${GCC_RPM_NAME%-gcc}
    PACKAGES+=" ${TOOLSET}-libitm-devel"
else
    PACKAGES+=" libitm"
    if ! rlIsRHEL 6; then
        PACKAGES+=" libitm-devel"
    fi
fi

rlJournalStart
    rlPhaseStartSetup
        rlRun "yum install -y --skip-broken $PACKAGES" # just best effort

        rlLogInfo "GCC=$GCC"
        rlLogInfo "GCC_RPM_NAME=$GCC_RPM_NAME"

        for i in $PACKAGES; do
          rlAssertRpm $i
        done

        TARBALL="bins_$(arch).tar.gz"
        if [ ! -f $TARBALL ]; then
            rlDie "We do not have binaries for your architecture $(arch)"
        fi
        rlRun "zcat $TARBALL | tar x"
        rlRun "pushd bins"
    rlPhaseEnd

    rlPhaseStartTest
        for i in T_*; do
            rlRun "./$i"
        done
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -rf bins" 0 "Removing the stuff we created"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
