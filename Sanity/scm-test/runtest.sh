#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Sanity/scm-test
#   Description: Download the linux kernel from git, compile.
#   Author: Marek Polacek <polacek@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2012 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

# Notes on relevancy
#
# The default kernel configuration requires dwarves which makes us limit the
# relevancy to RHEL-8.3+ and RHEL-9+. Avoiding the default config would allow
# older distros too but it IMHO isn't worth the hassle.
#
# Suggested TCMS relevancy:
#   distro < rhel-8: False
#   distro = rhel-8 && distro < rhel-8.3: False

GCC="${GCC:-$(type -P gcc)}"
GCC_RPM_NAME=$(rpm --qf '%{name}' -qf $GCC)
PACKAGES="$GCC_RPM_NAME ${GCC_RPM_NAME%gcc}binutils bison dwarves flex git openssl-devel"
if [[ "$GCC_RPM_NAME" = gcc-toolset-12-gcc ]] && [[ $(rpm -q --qf='%{VERSION}' $GCC_RPM_NAME) == 12.1.* ]]; then
    PACKAGES+=" elfutils elfutils-libelf-devel" # no toolset version of them in GTS 12.0
else
    PACKAGES+=" ${GCC_RPM_NAME%gcc}elfutils ${GCC_RPM_NAME%gcc}elfutils-libelf-devel"
fi

rlJournalStart
    rlPhaseStartSetup
        rlRun "yum -y install --skip-broken $PACKAGES" 0-255 # best effort

        # We optionally need to skip checking for the presence of the metapackage
        # because that would pull in all the dependent toolset subrpms.  We do not
        # always want that, especially in CI.
        _COLLECTIONS="$COLLECTIONS"
        if ! test -z $SKIP_COLLECTION_METAPACKAGE_CHECK; then
            for c in $SKIP_COLLECTION_METAPACKAGE_CHECK; do
                rlLogInfo "ignoring metapackage check for collection $c"
                export COLLECTIONS=$(shopt -s extglob && echo ${COLLECTIONS//$c/})
            done
        fi
        rlLogInfo "(without skipped) COLLECTIONS=$COLLECTIONS"
        rlAssertRpm --all
        export COLLECTIONS="$_COLLECTIONS"

        rlRun "TmpDir=\$(mktemp --tmpdir=/home -d)"
        rlRun "pushd $TmpDir"

        # Let's guess what the dependencies would be by taking the dependencies
        # of the running kernel
        rlRun "RUNNING_KERNEL_RPM=$(rpm -qa | grep -F kernel-$(uname -r) | grep ^kernel-)"
        rlFetchSrcForInstalled $RUNNING_KERNEL_RPM || yumdownloader --source $RUNNING_KERNEL_RPM
        if yum-builddep -h 2>&1 | grep -q -- --skip-broken; then
            YUM_OPTS='--skip-broken'
        elif yum-builddep -h 2>&1 | grep -q ' -t[, ]'; then
            YUM_OPTS='-t'
        else
            YUM_OPTS=''
        fi
        rlRun "yum-builddep $YUM_OPTS -y *.src.rpm" 0,1
    rlPhaseEnd

    rlPhaseStartTest "Download the linux kernel"
        rlRun "git clone git://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git"
        rlRun "cd linux"
    rlPhaseEnd

    rlPhaseStartTest "Compile the linux kernel"
        rlRun 'unset ARCH' # A must on s390x and possibly others
        rlRun 'make -w -d defconfig &>BUILD_LOG' # Use the default config
        rlRun "make -j$(nproc) all &>>BUILD_LOG" || tail -n 20 BUILD_LOG
    rlPhaseEnd

    rlPhaseStartCleanup
        rlBundleLogs "Build-log" BUILD_LOG
        rlRun "popd"
        rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
