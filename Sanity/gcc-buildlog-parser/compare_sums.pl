#!/usr/bin/perl

# usage:
#    ./compare_sums.pl old.gcc.sum new.gcc.sum

$old_sum_file = $ARGV[0];
$new_sum_file = $ARGV[1];

$quiet = 0;
#$quiet = 1;
#$quiet = 0 if (defined $ENV{TESTLOG_VERBOSITY} && $ENV{TESTLOG_VERBOSITY} ge 2);

$fails = 0;
$regressions = 0;

($MPASS, $MFAIL, $MWARN, $MREGR, $MEND) = ("\e[1;32m", "\e[1;31m", "\e[1;35m", "\e[1;41m", "\e[m") if -t STDOUT || $ENV{'FORCE_COLOR'};

sub my_die
{
	my $msg = shift;
	unless ($quiet)
	{
		print STDERR "$msg\n";
	}
	exit 1;
}

sub print_pass
{
	my $msg = shift;
	unless ($quiet)
	{
		print "$MPASS-- PASS --$MEND  $msg\n";
	}
}

sub print_warn
{
	my $msg = shift;
	unless ($quiet)
	{
		print "$MWARN-- WARN --$MEND  $msg\n";
	}
}

sub print_fail_casual
{
	my $msg = shift;
	$fails++;
	unless ($quiet)
	{
		print "$MFAIL-- FAIL --$MEND  $msg\n";
	}
}

sub print_fail_regression
{
	my $msg = shift;
	$regressions++;
	unless ($quiet)
	{
		print "$MREGR## FAIL ##$MEND  $msg\n";
	}
}

%general_tc_db = ();

# load passes/failures from the old log (sum, in fact) into hashes
%old_tc_hash_passes = ();
%old_tc_hash_failures = ();
open (INFILE, $old_sum_file) or my_die "ERROR: Unable to open $old_sum_file.";
@old_lines = <INFILE>;
close INFILE or my_die "ERROR: Unable to close $old_sum_file";

for (@old_lines)
{
	chomp;
	next unless /^(FAIL|PASS):\s(\S+)\s/;

	# skip useless lines, like:
	#     FAIL: 115 PASS, 4 FAIL, 0 UNRESOLVED
	next if /^(?:FAIL|PASS):\s+\d+\s+PASS,\s*\d+\s+FAIL/;
	#     FAIL: b is -1, not 1
	next if /^(?:FAIL|PASS):\s+\w+\s+is\s/;

	$general_tc_db{$2}++;

	if ($1 eq "PASS")
	{
		$old_tc_hash_passes{$2}++;
	}
	else
	{
		$old_tc_hash_failures{$2}++;
	}
}

# load passes/failures from the new log (sum, in fact) into hashes
%new_tc_hash_passes = ();
%new_tc_hash_failures = ();
open (INFILE, $new_sum_file) or my_die "ERROR: Unable to open $new_sum_file";
@new_lines = <INFILE>;
close INFILE or my_die "ERROR: Unable to close $new_sum_file";

for (@new_lines)
{
	chomp;
	next unless /^(FAIL|PASS):\s(\S+)\s/;

	# skip useless lines, like:
	#     FAIL: 115 PASS, 4 FAIL, 0 UNRESOLVED
	next if /^(?:FAIL|PASS):\s+\d+\s+PASS,\s*\d+\s+FAIL/;
	#     FAIL: b is -1, not 1
	next if /^(?:FAIL|PASS):\s+\S+\s+is\s/;

	$general_tc_db{$2}++;

	if ($1 eq "PASS")
	{
		$new_tc_hash_passes{$2}++;
	}
	else
	{
		$new_tc_hash_failures{$2}++;
	}
}

# check the new against the old
for (sort keys %general_tc_db)
{
	if ($new_tc_hash_failures{$_})
	{
		if ($old_tc_hash_passes{$_})
		{
			unless ($old_tc_hash_failures{$_} && $new_tc_hash_passes{$_})
			{
				print_fail_regression "REGRESSION --> $_ has fails which have not been seen before.";
			}
		}
		else
		{
			unless ($old_tc_hash_failures{$_})
			{
				print_fail_casual "Newly added test $_ failed.";
			}
		}
	}
	else
	{
		if ($old_tc_hash_passes{$_} || $old_tc_hash_failures{$_})
		{
			unless ($new_tc_hash_passes{$_})
			{
				print_warn "Testcase $_ seems to be removed from testsuite.";
			}
			else
			{
				if ($old_tc_hash_failures{$_})
				{
					print_pass "Testcase $_ has been fixed.";
				}
			}
		}
		else
		{
			if ($new_tc_hash_passes{$_})
			{
				print_pass "Testcase $_ has been added and passes.";
			}
		}
	}
}

exit !($fails || $regressions);

###################################################################################################

# The logic behind ...

#
# P = there are some PASS results for the particular testcase
# F = there are some FAIL results for the particular testcase
#
# P F
# ---
# 0 0 = unsupported/non-existing test
# 1 0 = OK, the whole testcase passes
# 0 1 = KO, the whole testcase fails
# 1 1 = the testcase has both passes and failures (e.g. per optimization levels, or so)
#
#
# The following logic is applied on the resulting hashes:
#
# OLD  NEW
# P F  P F  action
# ---  ---  ------
# 0 0  0 0  nothing (this should not even happen)
# 0 0  1 0  PASS (new test that passes)
# 0 0  0 1  FAIL (new test that fails)
# 0 0  1 1  FAIL (new test that partially fails)
#
# 1 0  0 0  WARN (passing test was removed)
# 1 0  1 0  nothing (passed before, passed now)
# 1 0  0 1  FAIL REGRESSION (passed before, now fails)
# 1 0  1 1  FAIL REGRESSION (passed before, now partially fails (that is also a fail))
#
# 0 1  0 0  WARN (failing test was removed)
# 0 1  1 0  PASS (failing test was fixed)
# 0 1  0 1  nothing (failing test keeps failing)
# 0 1  1 1  nothing (failing test now has some (random) passes, but also fails)
#
# 1 1  0 0  WARN (a partially failing test was removed)
# 1 1  1 0  PASS (a partially failing test was fixed and passes now)
# 1 1  0 1  FAIL REGRESSION (partially failing before, but now fails completely)
# 1 1  1 1  nothing (partially failing before, partially fails now, nothing to do)
#
