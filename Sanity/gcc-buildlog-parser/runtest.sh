#!/usr/bin/env bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Sanity/gcc-buildlog-parser
#   Description: Parses buildlogs and searches for regressions. Designed for CI.
#   Author: Michael Petlan <mpetlan@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2018 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

# Some URLs and paths
BREW_URL="https://brewweb.engineering.redhat.com/koji/taskinfo?taskID="
DATA_URL="http://download.eng.bos.redhat.com/brewroot/work/tasks"
GOLDEN_PATH="/mnt/qa/scratch/michael/gcc_gold"

silently_terminate()
{
	rlLogWarning "$@"
	rlPhaseEnd ; rlJournalPrintText ; rlJournalEnd
	exit 0
}

rlJournalStart
	rlPhaseStartSetup
		# Are we running inside CI?
		if [ "$BASEOS_CI" = "true" ]; then
			rlLog "CI detected for component $BASEOS_CI_COMPONENT"
		else
			silently_terminate "CI NOT DETECTED. Exiting..."
		fi

		# Detect GCC category to compare against (6, 7, 8, ... or D)
		if [[ $BASEOS_CI_COMPONENT =~ .*toolset.* ]]; then
			export VERS="D"
		else
			export VERS=`rlGetDistroRelease`
		fi

		# Check the Brew build ID
		TASK_ID=`echo $BASEOS_CI_TASKS | cut -d, -f 1`

		# Make sure we have all necessary tools in PATH
		rlRun "cp compare_sums.pl GCC_buildlog_extract GCC_log_to_sum /usr/local/bin/" 0 "Copying necessary tools to /usr/local/bin"

		# Obtain the subtask list
		echo "==== DETECTED SUBTASKS FROM $TASK_ID ===="
		wget --no-check-certificate -O - $BREW_URL$TASK_ID | perl -ne 'print "$1 $2 $3\n" if /a href="taskinfo\?taskID=(\d+)".*buildArch\s+\(([^,]+),\s+(\w+)\)/' | tee subtasks.list
		rlAssertExists "subtasks.list"

		# Parse out the subtask for my architecture
		# ... (in fact, we could run this for all in one arch)
		ARCH=`rlGetPrimaryArch`
		MY_LINE=`grep $ARCH\$ subtasks.list`
		SUBTASK_ID=`echo $MY_LINE | cut -d' ' -f 1`
		SRPM_NAME=`echo $MY_LINE | cut -d' ' -f 2`
		rlLog "THIS TEST RUNS AGAINST: $SRPM_NAME"
		wget $DATA_URL/${SUBTASK_ID: -4}/$SUBTASK_ID/build.log
		if [ $? -eq 0 -a -f "build.log" ]; then
			rlLog "Build logs seem to be successfully downloaded."
		else
			silently_terminate "Unable to download the build logs (probably deleted, happens when the build is too old) -- SKIPPING..."
		fi
		rlAssertExists "build.log"

		# Unpack the logs
		test -d LOGS || rlRun "mkdir LOGS"
		test -d SUMS || rlRun "mkdir SUMS"
		rlRun "pushd LOGS"
		rlRun "GCC_buildlog_extract" 0 "Extracting the logs from build.log"
		rlRun "rm -rf *.tar.bz2"
		rlRun "mv testlogs*/*.log ./"
		rlRun "chmod -x *.log"
		for i in *.log; do
			rlRun "GCC_log_to_sum < $i > ../SUMS/`basename $i .log`.sum" 0 "Extractiong summary from $i"
		done
		rlRun "popd"

		# We need /mnt/qa/scratch for golden logs
		test -d /mnt/qa/scratch || rlMountQa
		rlAssertExists "$GOLDEN_PATH"

		# It is necessary that the whole setup passed
		if [ `rlGetTestState` -ne 0 ]; then
			rlDie "FATAL: Setup failed, cannot continue. Please investigate and fix."
		fi
	rlPhaseEnd

	rlPhaseStartTest "Regression test"
		grep -P -v '^#' bug_matching_table.txt | while read -r line; do
			SPLIT=($line)
			BUG=${SPLIT[0]}
			CHECK_ENV=${SPLIT[1]}
			CHECK_ARCHES=${SPLIT[2]}
			TC_STR=${SPLIT[3]}
			echo "$CHECK_ARCHES " | grep -q -P "all|$ARCH[,\s]"
			if [ $? -eq 0 ] && [[ $CHECK_ENV =~ .*$VERS.* ]]; then
				grep -P "$TC_STR" SUMS/*.sum | tee tmp
				GREP_RESULT=${PIPESTATUS[0]}
				FAIL_NO=`awk -F':' '{print $2}' tmp | grep -P '^FAIL$' | wc -l`
				PASS_NO=`awk -F':' '{print $2}' tmp | grep -P '^PASS$' | wc -l`
				SKIP_NO=`awk -F':' '{print $2}' tmp | grep -P '^UNSUPPORTED$' | wc -l`
				XFAIL_NO=`awk -F':' '{print $2}' tmp | grep -P '^XFAIL$' | wc -l`
				XPASS_NO=`awk -F':' '{print $2}' tmp | grep -P '^XPASS$' | wc -l`
				if [ $PASS_NO -gt 0 ]; then
					if [ $FAIL_NO -eq 0 ]; then
						rlPass "Test for $BUG OK -- [ PASS=$PASS_NO  FAIL=$FAIL_NO ]"
					else
						rlLogWarning "Test for $BUG PASSed and FAILed -- [ PASS=$PASS_NO  FAIL=$FAIL_NO ]"
					fi
				else
					if [ $FAIL_NO -ne 0 ]; then
						rlFail "Test for $BUG FAILed -- [ PASS=$PASS_NO  FAIL=$FAIL_NO ]"
					else
						rlLogWarning "Test for $BUG not found -- [ GREP_RESULT=$GREP_RESULT  SKIP_NO=$SKIP_NO ]"
					fi
				fi
			fi
		done
	rlPhaseEnd

	export MY_GOLDEN_PATH="$GOLDEN_PATH/$VERS/$ARCH"
	rlPhaseStartTest "Comparison OLD : NEW"
		if [ -d $MY_GOLDEN_PATH ]; then
			for sumfile in SUMS/*.sum; do
				sumf=`basename $sumfile`
				if [ -f $MY_GOLDEN_PATH/$sumf.xz ]; then
					rlRun "./compare_sums.pl <(xzcat $MY_GOLDEN_PATH/$sumf.xz) $sumfile | tee temp" 0 "Comparing $MY_GOLDEN_PATH/$sumf $sumfile"
					grep -q -e 'REGRESSION' temp && rlFail "REGRESSION(s) FOUND."
					grep -q -e '-- FAIL --' temp && rlLogWarning "Some newly added tests failed."
					grep -q -e '-- WARN --' temp && rlLog "There were some removed testcases."
				else
					rlLog "Skipping $sumf (we do not have matching golden brick..."
				fi
			done
		else
			rlLog "THERE ARE NOT SUITABLE GOLDEN LOGS. SKIPPING THIS."
			rlLog "( I was searching in $MY_GOLDEN_PATH ...)"
		fi
	rlPhaseEnd

	if [ -z "$KEEP_LOGS" ]; then
		rlPhaseStartCleanup
			rlRun "rm -rf SUMS LOGS build.log"
		rlPhaseEnd
	fi
rlJournalPrintText
rlJournalEnd
