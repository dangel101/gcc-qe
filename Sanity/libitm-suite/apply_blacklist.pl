#!/usr/bin/perl

#### blacklist sets

@BL = ();

@BL_DTS7 = (
	'libitm.c++/eh-2.C',
	'libitm.c++/eh-3.C',
	'libitm.c++/eh-5.C',
	'libitm.c++/libstdc++-safeexc.C',
	'libitm.c++/newdelete.C' );


#### sorting out which blacklist to use

# here can be some conditional stuff, but now we just use all

@BL = (@BL, @BL_DTS7);

# HERE CAN BE ANOTHER BLACKLISTING LOGIC IF NEEDED


#### go!

while (<STDIN>)
{
	unless (/^FAIL/) { print; next; }

	my $known_fail = 0;
	for $substring (@BL)
	{
		if(index($_, $substring) != -1)
		{
			$known_fail = 1;
			last;
		}
	}
	print unless $known_fail;
}
