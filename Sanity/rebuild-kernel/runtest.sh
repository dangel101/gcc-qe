#!/bin/bash
# Copyright (c) 2009, 2012 Red Hat, Inc. All rights reserved.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# 
# Author: Michal Nowak <mnowak@redhat.com>
# Rewrite: Marek Polacek <polacek@redhat.com>

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

# Notes on relevancy
#
# Over time, RHEL kernels grow too old to be built by new GCCs (ie. by toolset
# GCC). As of today (2022-03-15), the supported toolset GCCs are 10 and 11 and
# they cannot be used for the kernels we ship for their respective platforms.
# I can imagine gcc-toolset-12 will be usable for RHEL-9 kernels but expect
# gcc-toolset-13 will be again too new, which we will find out after loosing
# time on investigations. On the other hand we have a nearby test for
# compiling newest upstream kernels (Sanity/scm-test) that is suitable for new
# toolset GCCs but much less so for system GCCs of RHELs, especially the older
# ones.
#
# Considering various pros and cons mentioned above I recommend using this
# particular test for toolset GCCs only. Suggested TCMS relevancy:
#   collection defined: False

# Nice guide: http://fedoraproject.org/wiki/Docs/CustomKernel

# Optional parameters
REBUILD_KERNEL_BASEONLY="${REBUILD_KERNEL_BASEONLY:-no}" # "yes" saves a lost of space
RPM_BUILD_ID="${RPM_BUILD_ID:-}"

GCC="${GCC:-$(type -P gcc)}"
GCC_RPM_NAME=$(rpm --qf '%{name}' -qf $GCC)
PACKAGES="$GCC_RPM_NAME ${GCC_RPM_NAME}-c++ ${GCC_RPM_NAME%gcc}binutils-devel ${GCC_RPM_NAME%gcc}gdb"
PACKAGES+=" asciidoc audit-libs-devel bison elfutils-devel elfutils-libelf-devel fuse-devel gettext glibc-static gmp-devel graphviz libcap-devel libcap-ng-devel libmnl-devel mpfr-devel ncurses-devel net-tools newt-devel openssl-devel pciutils-devel perl-ExtUtils-Embed rng-tools rpm-build rsync texinfo wget xmlto"

rlJournalStart
  rlPhaseStartSetup

        # Work around troubles with buildroot packages being out-of-sync
        if rlIsRHEL; then
            rlMountRedhat
            i=numactl
            rpm -q $i &>/dev/null || rlRun "yum -y install $i" 0-255
            for j in libs devel; do
                rpm -q ${i}-${j} &>/dev/null || rlRun "yum -y install ${i}-${j}" 0-255
                if rpm -q $i &>/dev/null && ! rpm -q ${i}-${j} &>/dev/null; then
                    p=/mnt/redhat/brewroot/packages/$i/$(rpm -q --qf='%{VERSION}/%{RELEASE}/%{ARCH}' $i)/${i}-${j}-$(rpm -q --qf='%{VERSION}-%{RELEASE}.%{ARCH}' $i).rpm
                    [[ -e $p ]] && rlRun "yum -y install $p"
                fi
            done
        fi

        rlRun "yum -y install --skip-broken $PACKAGES" 0-255 # best effort

        # We optionally need to skip checking for the presence of the metapackage
        # because that would pull in all the dependent toolset subrpms.  We do not
        # always want that, especially in CI.
        _COLLECTIONS="$COLLECTIONS"
        if ! test -z $SKIP_COLLECTION_METAPACKAGE_CHECK; then
            for c in $SKIP_COLLECTION_METAPACKAGE_CHECK; do
                rlLogInfo "ignoring metapackage check for collection $c"
                export COLLECTIONS=$(shopt -s extglob && echo ${COLLECTIONS//$c/})
            done
        fi
        rlLogInfo "(without skipped) COLLECTIONS=$COLLECTIONS"
        rlAssertRpm --all
        export COLLECTIONS="$_COLLECTIONS"

        rlRun "TmpDir=\$(TMPDIR=$HOME mktemp -d)"
        rlRun "pushd $TmpDir"

        # Speed up keygen.
        rngd -r /dev/hwrandom || rngd -r /dev/urandom

        # The RPM name may vary
        pkg=kernel
        rpm -q $pkg &>/dev/null || pkg=kernel-PAE
        rlFetchSrcForInstalled $pkg || yumdownloader --source $pkg
        rlRun "rpm --define '_topdir $TmpDir' -Uvh *.src.rpm"

        # The spec file name may vary (kernel.spec, kernel-aarch64.spec,
        # kernel-alt.spec, etc).
        rlRun "spec_dir=`rpm --define '_topdir $TmpDir' --eval=%_specdir`"
        rlRun "spec_file=$(ls -tr $spec_dir/*kernel* | tail -n 1)"
        rlRun "yum-builddep -y $spec_file"
    rlPhaseEnd

    rlPhaseStartTest
        if [ "$(uname -i)" == "ppc64" ]; then
            target="--target=ppc64"
        else
            target="--target=$(uname -m)"
        fi

        baseonly=""
        [ "$REBUILD_KERNEL_BASEONLY" = "yes" ] && baseonly="--with baseonly --without debuginfo"

        if [ "$RPM_BUILD_ID" != "" ]; then
            rlRun "sed -i \"s/# % define buildid .local/%define buildid $RPM_BUILD_ID/\" $spec_file"
        fi

        rlRun 'unset ARCH' # ARCH is set by Beaker, may break kernel builds on some architectures

        # --define='_unpackaged_files_terminate_build 0' ... Workaround for potentially unpackaged files (kernel bug bz1542543)
        rlRun "rpmbuild --define '_topdir $TmpDir' --define='_unpackaged_files_terminate_build 0' -bb $target $baseonly --clean $spec_file &> BUILD_LOG"
        rlLogInfo "$(tail -n 20 BUILD_LOG)"
    rlPhaseEnd

    rlPhaseStartCleanup
        rlBundleLogs "Build-log" BUILD_LOG
        rlRun "popd"
        rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
