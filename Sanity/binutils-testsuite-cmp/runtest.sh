#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Sanity/binutils-testsuite-cmp
#   Description: Run the binutils testsuite.
#   Author: Marek Polacek <polacek@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2012 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGES=(glibc binutils gcc)

# Choose the compiler.
GCC=${GCC:-gcc}

rlJournalStart
  rlPhaseStartSetup
    # Check needed packages.
    for p in "${PACKAGES[@]}"; do
      rlCheckRpm "$p" || yum -y install "$p"
      rlAssertRpm "$p"
    done; unset p
    rpmquery -a | grep -e yum-utils -e dnf-utils && rlPass "yum- or dnf-utils seem to be in place" || rlRun "yum -y install yum-utils"
    rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
    rlRun "pushd $TmpDir"

    # Get the SRPM.
    rlFetchSrcForInstalled binutils || yumdownloader --source binutils

    # Get the SRPM name.
    srpm=$(find binutils*.src.rpm | tail -n1)

    # Install the SRPM.
    rlRun "rpm -Uvh $srpm"

    # Get some info.
    spec_dir=$(rpm --eval=%_specdir)
 
    # Install the dependencies.
    yum-builddep -y $spec_dir/binutils.spec

 rlPhaseEnd

  rlPhaseStartTest
    # Rebuild binutils. 
    if [[ $(uname -i) == "ppc64" ]]; then
      if rlIsRHEL 6; then
	target='--target=ppc64'
      else
	target='--target=ppc'
      fi
    fi

    # Ick!  The CC=gcc-foo mechanism won't work, since in binutils.spec
    # there's:
    # CC="gcc -L`pwd`/bfd/.libs/" CFLAGS="${CFLAGS:-%optflags} -D_FILE_OFFSET_BITS=64" ../configure
    # So, we need to edit binutils.spec manually and put gcc-foo there.
    if [[ $GCC != "gcc" ]]; then
      sed -i s/CC=\"gcc/CC=\"$GCC/ $spec_dir/binutils.spec
      rlLog "Changing CC=gcc to CC=$GCC..."
    fi

    # We're ready to build.
    rlRun "rpmbuild -bc $target $spec_dir/binutils.spec 2>&1 | tee BUILD_LOG"

    # Go to rpmbuild dir and create a directory for the log files.
    cd $spec_dir/..
    mkdir -pv BINUT_LOGS

    # Now, go to the BUILD directory, descend into binutils dir.
    cd BUILD/binutils*

    # Find all log files and copy them to BINUT_LOGS.
    > dummy.sum # Prevent `cp: missing file operand'.
    cp -v `find . -name \*.sum` -t ../../BINUT_LOGS/

    # Append $RANDOM to all the *.sum files so they won't get overwritten.
    cd ../../BINUT_LOGS/
    rand=$RANDOM
    for f in *.sum; do
      rlRun "mv -v $f{,.$rand}"
    done

    # Kill all dummy files.
    rm -f dummy.sum.*

    # We're done.  Now install newer version of GCC and then run this again.
    # Then manually compare all the .sum files.
  rlPhaseEnd

  rlPhaseStartCleanup
    rlRun "popd"
    # Don't do this here.
    # rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
  rlPhaseEnd
rlJournalPrintText
rlJournalEnd
