int main (void)
{
  int a = 0;
  int b = 3;
  __atomic_store_n (&a, b, __ATOMIC_RELEASE);
  int tem = 42;
  __atomic_store (&a, &tem, __ATOMIC_SEQ_CST);
  tem = 0;
  __atomic_exchange (&a, &b, &tem, __ATOMIC_ACQ_REL);
  a = 2;
  b = 0;
  int exp = 12;
  int des = 88;
  __atomic_compare_exchange_n (&a, &exp, des, 0, __ATOMIC_RELAXED, __ATOMIC_RELAXED);
  a = 2;
  b = 0;
  exp = 2;
  des = 88;
  __atomic_compare_exchange_n (&a, &exp, des, 0, __ATOMIC_RELAXED, __ATOMIC_RELAXED);
  return 0;
}
