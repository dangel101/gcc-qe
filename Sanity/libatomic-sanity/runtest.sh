#!/usr/bin/env bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Sanity/libatomic-sanity
#   Description: Run a few examples of the libatomic test suite
#   Author: Dagmar Prokopova <dprokopo@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2012 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

# Not relevant for GCC<4.7. Suggested TCMS relevancy:
#   distro < rhel-7 && collection !defined: False

GCC="${GCC:-$(type -P gcc)}"
GCC_RPM_NAME=$(rpm --qf '%{name}' -qf $GCC)

GCCCPP_RPM_NAME=${GCC_RPM_NAME}-c++

# libatomic ships as libatomic with base GCC but as <toolset>-libatomic-devel
# in Collections (where it's just a linker script referring the base variant)
if [[ "$GCC_RPM_NAME" = gcc ]]; then
    LIBATOMIC_RPM_NAME=libatomic
else
    LIBATOMIC_RPM_NAME=${GCC_RPM_NAME%%gcc}libatomic-devel
fi

PRI_ARCH=$(rlGetPrimaryArch)
SEC_ARCH=$(rlGetSecondaryArch)
if [[ "$GCC_RPM_NAME" == devtoolset-* && ( "$SEC_ARCH" = s390 || "$SEC_ARCH" = ppc ) ]]; then
    # DTS on s390x and ppc64 dropped the secondary architecture support even though
    # it remains in the base system inself
    SEC_ARCH=''
fi

PACKAGES="$GCC_RPM_NAME $GCCCPP_RPM_NAME"
for p in $LIBATOMIC_RPM_NAME ${GCC_RPM_NAME%%gcc}libstdc++-devel glibc-devel; do
    for a in $PRI_ARCH $SEC_ARCH; do
        PACKAGES+=" ${p}.${a}"
    done
done

rlJournalStart
    rlPhaseStartSetup
        rlRun "yum -y install --skip-broken $PACKAGES" 0-255 # best effort

        rlLogInfo "PACKAGES=$PACKAGES"
        rlLogInfo "COLLECTIONS=$COLLECTIONS"
        rlLogInfo "GCC=$GCC"
        rlLogInfo "SKIP_COLLECTION_METAPACKAGE_CHECK=$SKIP_COLLECTION_METAPACKAGE_CHECK"

        rlLogInfo "PRI_ARCH=$PRI_ARCH"
        rlLogInfo "SEC_ARCH=$PRI_ARCH"

        # We optionally need to skip checking for the presence of the metapackage
        # because that would pull in all the dependent toolset subrpms.  We do not
        # always want that, especially in CI.
        _COLLECTIONS="$COLLECTIONS"
        if ! test -z $SKIP_COLLECTION_METAPACKAGE_CHECK; then
            for c in $SKIP_COLLECTION_METAPACKAGE_CHECK; do
                rlLogInfo "ignoring metapackage check for collection $c"
                export COLLECTIONS=$(shopt -s extglob && echo ${COLLECTIONS//$c/})
            done
        fi
        rlLogInfo "(without skipped) COLLECTIONS=$COLLECTIONS"
        rlAssertRpm --all
        export COLLECTIONS="$_COLLECTIONS"

        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "cp -r files $TmpDir" # example programs
        rlRun "pushd $TmpDir"

        # We will not only compile and run the example programs, we'll run them
        # in a chroot environment without the respective GTS/DTS environment so
        # we can be sure the programs don't need GTS/DTS and can run on the
        # respective base system. Let's prepare the chroot environment.
        # Create the jail directory.
        rlRun 'mkdir -pv jail/{bin,dev,lib,lib64,proc}'
        rlRun "cp -v /bin/bash jail/bin/"
        for i in $(ldd /bin/bash | grep -oE '/lib64/.*\.so\.[0-9]+'); do
            rlRun "cp $i jail/lib64/"
        done
        for i in $(ldd /bin/bash | grep -oE '/lib/.*\.so\.[0-9]+'); do
            rlRun "cp $i jail/lib/"
        done
        for LIB in lib lib64; do
            for libfile in "/$LIB/libgcc_s.so.1" "/$LIB/libpthread.so.0" "/usr/$LIB/libatomic.so.1" \
                    "/$LIB/ld-linux.so.2" "/$LIB/libc.so.6" "/$LIB/libdl.so.2" "/$LIB/ld.so.1"; do
                [[ -e $libfile ]] && rlRun "cp $libfile jail/$LIB" || rlLog "$libfile does not exist"
            done
        done
    rlPhaseEnd

    rlPhaseStartTest
        for i in files/*/*; do
            if [[ $i == */64/* ]]; then
                # a 64-bit program: use it no matter what, no special options needed
                GCC_BITS_REQUEST=''
            else
                # a 32-bit program: special care needed
                if [[ -z "$SEC_ARCH" ]]; then
                    # a 32-bit program doesn't make sense on a machine without
                    # a secondary architecture, skip it altogether
                    continue
                fi
                if [[ "$PRI_ARCH" != s390x ]]; then
                    GCC_BITS_REQUEST='-m32'
                else
                    GCC_BITS_REQUEST='-m31'
                fi
            fi

            rlRun 'rm -f program.c'
            rlRun "cp $i program.c"

            rlRun 'rm -f a.out'
            rlRun "gcc $GCC_BITS_REQUEST -O2 -fno-inline-atomics -latomic program.c"

            # run in natura
            rlRun ./a.out

            # run in the chroot
            rlRun 'rm -f jail/a.out'
            rlRun 'cp a.out jail'
            rlRun 'chroot jail ./a.out'
        done
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun popd
        rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
