#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Sanity/Rebuild-systemtap
#   Description: Rebuild systemtap
#   Author: Marek Polacek <polacek@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2011, 2012 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

# Note on relevancy
# Building Collection packages using Collection toolchain is a bit trick,
# AFAIK they are built using the system toolchain so we may occasionally
# run into failures that aren't actually bugs and we can exclude those
# situations from testing. One such example is DTS-10 on RHEL-6.
# Suggested TCMS relevancy:
#   distro < rhel-7 && collection contains devtoolset-10: False

GCC="${GCC:-$(type -P gcc)}"
GCC_RPM=$(rpm --qf '%{name}' -qf $GCC)
SYSTEMTAP_RPM=${GCC_RPM%gcc}systemtap # build the same base/collection systemtap as gcc is part of
PACKAGES="$GCC_RPM $SYSTEMTAP_RPM avahi-devel boost-devel crash-devel elfutils-devel emacs gettext-devel latex2html libvirt-devel libxml2-devel nss-devel rpm-build sqlite-devel xmlto"

if [[ $GCC_RPM =~ toolset- ]]; then
    PACKAGES+=" ${GCC_RPM%gcc}build"
fi

rlJournalStart
    rlPhaseStartSetup
        rlRun "yum -y install --skip-broken $PACKAGES" 0-255 # best effort

        # We optionally need to skip checking for the presence of the metapackage
        # because that would pull in all the dependent toolset subrpms.  We do not
        # always want that, especially in CI.
        _COLLECTIONS="$COLLECTIONS"
        if ! test -z $SKIP_COLLECTION_METAPACKAGE_CHECK; then
            for c in $SKIP_COLLECTION_METAPACKAGE_CHECK; do
                rlLogInfo "ignoring metapackage check for collection $c"
                export COLLECTIONS=$(shopt -s extglob && echo ${COLLECTIONS//$c/})
            done
        fi
        rlLogInfo "(without skipped) COLLECTIONS=$COLLECTIONS"
        rlAssertRpm --all
        export COLLECTIONS="$_COLLECTIONS"

        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "pushd $TmpDir"

        rlFetchSrcForInstalled $SYSTEMTAP_RPM || yumdownloader --source $SYSTEMTAP_RPM
        srpm=$(rpmquery $SYSTEMTAP_RPM --queryformat=%{NAME}-%{VERSION}-%{RELEASE})".src.rpm"
        rlRun "rpm -Uvh $srpm"
        spec_dir=$(rpm --eval=%_specdir)
        rlRun "yum-builddep -y --skip-broken $spec_dir/systemtap.spec" 0-255
    rlPhaseEnd

    rlPhaseStartTest
        # _unpackaged_files_terminate_build - workaround for RHBZ#1544689
        rlRun "rpmbuild --define='_unpackaged_files_terminate_build 0' -bb $spec_dir/systemtap.spec &> BUILD_LOG"
        test $? -eq 0 || tail -n 40 BUILD_LOG
    rlPhaseEnd

    rlPhaseStartCleanup
        rlBundleLogs "Build-log" BUILD_LOG
        rlRun "popd"
        rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
