#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Sanity/libgfortran4-smoke
#   Description: Smoke test of libgfortran4 with a pre-built app
#   Author: Michael Petlan <mpetlan@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2017 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

# The package being test was shipped as a system RPM in RHEL 6 and 7 only.
# Running the test in the toolset environmets is not necessary but does
# not hurt either.
#
# There's an architecture dependency here as well, the package was *not*
# shipped on all the available architectures.
#
# Suggested TCMS relevancy:
#   distro != rhel-6, rhel-7: False
#   distro = rhel-6 && arch = s390x: False
#   collection defined: False

PACKAGE="libgfortran4"

rlJournalStart
	rlPhaseStartSetup
		rlCheckRpm $PACKAGE || yum -y install $PACKAGE
		rlAssertRpm $PACKAGE
		rlAssertRpm "strace"
		BINARY="linpack1000d.`arch`.4"
		rlAssertExists "$BINARY" || rlDie "ERROR: Cannot find binary for your arch (`arch`)"
	rlPhaseEnd

	rlPhaseStartTest
		rlRun "ldd $BINARY | grep libgfortran.so.4" 0 "$BINARY links against proper libgfortran (so version 4)"
		GFLIB=`ldd $BINARY | grep libgfortran.so.4 | awk '{print $3}'`
		rlAssertExists "$GFLIB"
		rlRun "rpmquery -f $GFLIB | grep $PACKAGE" 0 "$PACKAGE installed the proper libgfortran"
		rlRun "./$BINARY" 0 "$BINARY can run" # THIS IS THE MOST IMPORTANT ASSERT HERE
		rlRun "strace -e trace=file -o strace.log ./$BINARY" 0 "$BINARY under strace"
		rlAssertGrep "libgfortran.so.4" "strace.log"
		rm strace.log # clean-up
	rlPhaseEnd
rlJournalPrintText
rlJournalEnd
