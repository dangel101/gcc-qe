#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Sanity/upstream-qemu
#   Description: Rebuilds upstream qemu
#   Author: Michael Petlan <mpetlan@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2018 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

# On RHEL-6, s390x is missing some libraries for a successful build.
# Suggested TCMS relevancy:
#   distro < rhel-7 && arch = s390x: False

GCC="${GCC:-$(type -P gcc)}"
GCC_RPM_NAME=$(rpm --qf '%{name}' -qf $GCC)
PACKAGES="$GCC_RPM_NAME ${GCC_RPM_NAME}-c++ bzip2 glib2-devel pixman-devel wget zlib-devel"
if ! rlIsRHEL 6 7; then
    PACKAGES+=' ninja-build'
    if ! rlIsRHEL 8; then
        PACKAGES+=' python3'
    fi
fi

rlJournalStart
    rlPhaseStartSetup
        rlRun "yum -y install --skip-broken $PACKAGES" 0-255 # best effort

        # We optionally need to skip checking for the presence of the metapackage
        # because that would pull in all the dependent toolset subrpms.  We do not
        # always want that, especially in CI.
        _COLLECTIONS="$COLLECTIONS"
        if ! test -z $SKIP_COLLECTION_METAPACKAGE_CHECK; then
            for c in $SKIP_COLLECTION_METAPACKAGE_CHECK; do
                rlLogInfo "ignoring metapackage check for collection $c"
                export COLLECTIONS=$(shopt -s extglob && echo ${COLLECTIONS//$c/})
            done
        fi
        rlLogInfo "(without skipped) COLLECTIONS=$COLLECTIONS"
        rlAssertRpm --all
        export COLLECTIONS="$_COLLECTIONS"

        # RHEL-8 has the situation with pythons a bit complicated
        test -z "$PYTHON" && /usr/bin/env python3 -V         && export PYTHON="/usr/bin/python3"
        test -z "$PYTHON" && /usr/bin/env python -V          && export PYTHON="/usr/bin/python"
        test -z "$PYTHON" && /usr/bin/env python2 -V         && export PYTHON="/usr/bin/python2"
        test -z "$PYTHON" && /usr/libexec/platform-python -V && export PYTHON="/usr/libexec/platform-python"

        if rlIsRHEL '<=6'; then
            QEMU_VERSION=qemu-2.12.0 # the last one not needing Python 3
        elif rlIsRHEL 7; then
            QEMU_VERSION=qemu-3.0.0 # newer ones (but before ninja-build) should work too
        else
            QEMU_VERSION=qemu-7.1.0 # TODO: make this a moving target
        fi
        # TODO cache the qemu source files in RH
        test -f $QEMU_VERSION.tar.xz || rlRun "wget --no-check-certificate https://download.qemu.org/$QEMU_VERSION.tar.xz"
        rlAssertExists  "$QEMU_VERSION.tar.xz"
        rlRun "xzcat $QEMU_VERSION.tar.xz | tar x"
    rlPhaseEnd

    rlPhaseStartTest
        rlRun "pushd $QEMU_VERSION"
        if [[ $? -eq 0 ]]; then
            rlRun "./configure --python=$PYTHON"
            rlRun "make -j$(nproc)"
            rlRun popd
        fi
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "rm -rf $QEMU_VERSION"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
