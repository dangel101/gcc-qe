#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Sanity/libitm-sanity
#   Description: We pick up some tests from libitm suite, we compile
#   them with DTS GCC, then we create a chroot jail
#   where except a few others libraries also libitm.so
#   resides.  We try to run those compiled programs
#   in this environment (they'll link with libitm.so
#   from libitm package).
#   Author: Marek Polacek <polacek@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2012 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

# Relevancy: Needs GCC >= 4.7 (-fgnu-tm). In practice, the following aren't
# relevant:
#   - system gcc of RHEL <7

GCC=${GCC:-$(type -P gcc)}
GXX=${GCC%gcc}g++

GCC_RPM_NAME=$(rpm --qf '%{name}' -qf $GCC)
GXX_RPM_NAME=${GCC_RPM_NAME}-c++
GFORTRAN_RPM_NAME=${GCC_RPM_NAME}-gfortran

PRI_ARCH=$(rlGetPrimaryArch)
SEC_ARCH=$(rlGetSecondaryArch)

if [[ "$GCC_RPM_NAME" == *toolset* ]]; then
    TOOLSET=${GCC_RPM_NAME%-gcc}
else
    TOOLSET=''
fi

PACKAGES="glibc-devel.$PRI_ARCH tar sed libitm.$PRI_ARCH $GCC_RPM_NAME $GXX_RPM_NAME $GFORTRAN_RPM_NAME"

TEST_BITS=64

if [[ -z "$TOOLSET" ]]; then
    PACKAGES+=" libitm.$PRI_ARCH libitm-devel.$PRI_ARCH"
    if [[ -n "$SEC_ARCH" ]]; then
        PACKAGES+=" libitm.$SEC_ARCH libitm-devel.$SEC_ARCH libstdc++.$SEC_ARCH glibc-devel.$SEC_ARCH"
        TEST_BITS+=' 32'
    fi
else
    PACKAGES+=" ${TOOLSET}-libitm-devel.$PRI_ARCH"
    if [[ -n "$SEC_ARCH" ]] && [[ "$PRI_ARCH" = x86_64 ]]; then
        PACKAGES+=" ${TOOLSET}-libitm-devel.$SEC_ARCH ${TOOLSET}-libstdc++-devel.$SEC_ARCH glibc-devel.$SEC_ARCH"
        TEST_BITS+=' 32'
    fi
fi


rlJournalStart
    rlPhaseStartSetup
        rlLogInfo "GCC=$GCC"
        rlLogInfo "GXX=$GXX"
        rlLogInfo "GCC_RPM_NAME=$GCC_RPM_NAME"
        rlLogInfo "GXX_RPM_NAME=$GXX_RPM_NAME"
        rlLogInfo "GFORTRAN_RPM_NAME=$GFORTRAN_RPM_NAME"
        rlLogInfo "PRI_ARCH=$PRI_ARCH"
        rlLogInfo "SEC_ARCH=$SEC_ARCH"
        rlLogInfo "TOOLSET=$TOOLSET"
        rlLogInfo "TEST_BITS=$TEST_BITS"

        rlRun "yum install -y --skip-broken $PACKAGES"
        for i in $PACKAGES; do
          rlAssertRpm $i
        done

        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "cp -v tests.tar libitm.h $TmpDir"
        rlRun "pushd $TmpDir"
        # Extract the tests.  We get directories libitm.c and libitm.c++.
        rlRun "tar vxf tests.tar"

        # We need to change <libitm.h> (we don't ship any) to "../libitm.h".
        rlRun "sed -i 's/<libitm\.h>/\"..\/libitm.h\"/' libitm.c/*.c libitm.c++/*.C"
    rlPhaseEnd

    # OK, we're about to compile a few programs.
    # First, the C programs. We always link -lpthread, it's needed
    # by some tests. Also, run them all.

    for B in $TEST_BITS; do
        if [[ "$B" = 32 ]]; then
            if [[ "$PRI_ARCH" != s390x ]]; then
                m=-m32
            else
                m=-m31
            fi
        else
            m=''
        fi

        rlPhaseStartTest "Compile C programs :: ${B}bit"
            cd libitm.c
            for p in *.c; do
                # Add the T_${B}_ prefix to simplify globbing later.
                rlRun "$GCC $p $m -O2 -o T_${B}_${p%.c} -fgnu-tm -lpthread"
                rlRun "./T_${B}_${p%.c}"
            done; unset p
            cd ..
        rlPhaseEnd

        rlPhaseStartTest "Compile C++ programs :: ${B}bit"
            cd libitm.c++
            for p in *.C; do
                # These are the do-compile tests.
                case "$p" in
                    throwdown.C)
                        rlRun "$GXX $p $m -c -O2 -fgnu-tm -lpthread"
                        continue
                        ;;
                esac
                # These are the do-run tests.
                # Add the T_${B}_ prefix to simplify globbing later.
                rlRun "$GXX $p $m -O2 -o T_${B}_${p%.C} -fgnu-tm -lpthread"
                rlRun "./T_${B}_${p%.C}"
            done; unset p
            cd ..
        rlPhaseEnd

    done; unset B

    rlPhaseStartSetup "Prepare the chroot environment."
        # Create the jail directory.
        rlRun "mkdir -pv =jail/{bin,dev,lib,lib64,proc}"

        # Copy bash itself, so /bin/bash is available.
        rlRun "cp -v /bin/bash =jail/bin/"

        # We'll need a few libraries needed to run bash.
        cp -v $(ldd /bin/bash  | grep -oE '/lib64/.*\.so\.[0-9]+') =jail/lib64
        cp -v $(ldd /bin/bash  | grep -oE '/lib/.*\.so\.[0-9]+') =jail/lib

        for LIB in "lib" "lib64"; do
            # We'll need a few other libraries
            for libfile in "/$LIB/libgcc_s.so.1" "/$LIB/libpthread.so.0" "/usr/$LIB/libitm.so.1" "/usr/$LIB/libstdc++.so.6" \
                    "/$LIB/libm.so.6" "/$LIB/ld-linux.so.2" "/$LIB/libc.so.6" "/$LIB/libgcc_s.so.1" "/$LIB/libdl.so.2" \
                    "/$LIB/ld.so.1"; do
                test -e "$libfile" && rlRun "cp -v $libfile =jail/$LIB" || rlLog "$libfile does not exist"
            done
        done

        # Finally copy all test binaries.
        rlRun "cp -v libitm.c/T_* =jail/"
        rlRun "cp -v libitm.c++/T_* =jail/"
    rlPhaseEnd

    rlPhaseStartTest "Run programs in the chroot jail."
        # Construct what we want to do in jail.
        cat > cmds <<\EOF
pwd
# We cannot use ls!
echo *
:> out
# Also we cannot use rlRun.
for f in T_*; do
  echo "Running $f..."
  ./$f || echo "$f failed!" >> out
done
exit
EOF

        # Create dummy empty file.
        rlRun ":> out.ok"

        # Now we can switch to the chroot jail.
        rlRun "chroot =jail /bin/bash < cmds" || rlDie "Cannot switch to chroot jail!"

        # The `out' file should be empty.
        rlAssertNotDiffer =jail/out out.ok || rlRun "cat =jail/out"
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r -f $TmpDir a.out"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
