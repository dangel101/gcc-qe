#!/bin/bash

# Copyright (c) 2009, 2012 Red Hat, Inc. All rights reserved.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# 
# Author: Michal Nowak <mnowak@redhat.com>
# Rewrite: Marek Polacek <polacek@redhat.com>

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

##############################################################################
# Notes on relevancy
##############################################################################
#
# See similar notes in Sanity/rebuild-kernel. In short, this test makes sense
# for system GCCs, although toolset GCCs may pass the test now and then, too.
# However for toolset GCCs it's much better to compile up-to-dage upstream
# glibc code (see test Sanity/upstream-glibc). Suggested TCMS relevancy:
#   collection defined: False

##############################################################################
# Notes on HW specifics
##############################################################################
#
# libc can create multilib binaries: On some architectures it can create
# several alternative CPU-specific bits that are selected in runtime. However
# the "check" phase tries to test all of them and fails when testing a more
# "advanced" binary than the SUT's CPU. For example after [1], when rebuilding
# RHEL-8's glibc on POWER8 there's a set of binaries built having POWER9-
# specific instructions (build-ppc64le-redhat-linux-power9) and rpmbuild fails
# with "Illegal instruction (core dumped)". How to deal with that?
#
# The easiest, "escapist" option would be skipping "make check" altogether.
#
# The ideal option would be using the the highest required CPU. It's doable,
# e.g. through https://wiki.test.redhat.com/Faq/Beaker/BeakerMachineFiltering.
# For the example above we'd need a new filter (something like POWER9_OR_BETTER
# meaning <cpu><model op="&lt;" value="5111808"/></cpu>). However it's a sort
# of high-maintenance approach and not that much useful here; we test gcc, not
# glibc, and probability of finding a gcc bug while running a downstream
# glibc's "make check" is very low, IMHO, so it isn't worth of doing at all
# cost.
#
# This test implements a middle path approach. If it detects the CPU would fail
# in "make check" then it skips it. This approach will run "make check" if
# possible and maintaining the exceptions should be much easier than
# maintaining and enforcing BeakerMachineFiltering.
#
# [1] http://pkgs.devel.redhat.com/cgit/rpms/glibc/commit/?h=rhel-8.5.0&id=3bef91d75e3df7398fc41b961ec250f7924000d7

cpu_good_for_make_check () {
    if rlIsRHEL '>=8' && [[ $(arch) = ppc64le ]] && grep -q 'POWER[2-8]' /proc/cpuinfo; then
        rlLogInfo 'RHEL8+ on <POWER9, make check will be skipped'
        return 1
    fi
    return 0
}

GCC="${GCC:-$(type -P gcc)}"
GCC_RPM_NAME=$(rpm --qf '%{name}' -qf $GCC)
BINUTILS_RPM_NAME=${GCC_RPM_NAME%gcc}binutils
PACKAGES="$GCC_RPM_NAME $BINUTILS_RPM_NAME audit-libs-devel gd-devel glibc-devel glibc-static glibc-utils libcap-devel libgcc libpng-devel libstdc++ libstdc++-devel libXpm-devel nspr-devel nss-devel nss-softokn-devel nss-util-devel rpm-build systemtap-sdt-devel"

rlJournalStart
    rlPhaseStartSetup
        rlRun "yum -y install --skip-broken $PACKAGES" 0-255 # best effort

        # We optionally need to skip checking for the presence of the metapackage
        # because that would pull in all the dependent toolset subrpms.  We do not
        # always want that, especially in CI.
        _COLLECTIONS="$COLLECTIONS"
        if ! test -z $SKIP_COLLECTION_METAPACKAGE_CHECK; then
            for c in $SKIP_COLLECTION_METAPACKAGE_CHECK; do
                rlLogInfo "ignoring metapackage check for collection $c"
                export COLLECTIONS=$(shopt -s extglob && echo ${COLLECTIONS//$c/})
            done
        fi
        rlLogInfo "(without skipped) COLLECTIONS=$COLLECTIONS"
        rlAssertRpm --all
        export COLLECTIONS="$_COLLECTIONS"

        rlRun "rpmquery -a | grep -e yum-utils -e dnf-utils" 0 "YUM or DNF utils are installed"

        rlLogInfo "Running kernel: $(uname -r)"
        rlLogInfo "Installed kernel(s): $(rpm -q kernel)"
        rlLogInfo "Installed headers: $(rpm -q kernel-headers)"

        # Decide if "make check" should be run
        cpu_good_for_make_check && CHECK_PARAM='' || CHECK_PARAM='--nocheck'

        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "pushd $TmpDir"

        rlFetchSrcForInstalled glibc || yumdownloader --source glibc
        srpm=$(find glibc*.src.rpm | tail -n1)
        rlRun "rpm -Uvh $srpm"
        spec_dir=$(rpm --eval=%_specdir)
        rlRun "yum-builddep -y $spec_dir/glibc.spec" 0-255
    rlPhaseEnd

    rlPhaseStartTest
        # TODO: Why do we need this?
        if [ "$(uname -i)" == "ppc64" ]; then
            if rlIsRHEL 7 || rlIsRHEL 6; then
                target='--target=ppc64'
            else
                   target='--target=ppc'
            fi
        fi
        rlRun "rpmbuild -bb $target $CHECK_PARAM --clean $spec_dir/glibc.spec &> BUILD_LOG"
        test $? -eq 0 || tail -n 20 BUILD_LOG
    rlPhaseEnd

    rlPhaseStartCleanup
        rlBundleLogs "Build-log" BUILD_LOG
        rlRun "popd"
        rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
