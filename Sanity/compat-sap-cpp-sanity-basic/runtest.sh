#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Sanity/compat-sap-cpp-sanity-basic
#   Description: compat-sap-cpp-sanity-basic
#   Author: Martin Cermak <mcermak@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2015 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

export PACKAGE=${BASEOS_CI_COMPONENT:-compat-sap-c++}

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm $PACKAGE
        LIBRARY=`rpmquery -l $PACKAGE | grep $PACKAGE.so`
    rlPhaseEnd

    rlPhaseStartTest
        rlIsRHEL 6 && rlRun "./sap-gcc-4.7.2-testcase" 1
        rlLog "Testing $LIBRARY"
        rlRun "LD_PRELOAD=$LIBRARY ./sap-gcc-4.7.2-testcase"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
