#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Sanity/libatomic-suite
#   Description: Run the libatomic testsuite.
#   Author: Marek Polacek <polacek@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2012 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

# Not relevant for GCC<4.7. Suggested TCMS relevancy:
#   distro < rhel-7 && collection !defined: False

GCC="${GCC:-$(type -P gcc)}"
GCC_RPM_NAME=$(rpm --qf '%{name}' -qf $GCC)

# libatomic ships as libatomic with base GCC but as <toolset>-libatomic-devel
# in Collections (where it's just a linker script referring the base variant)
if [[ "$GCC_RPM_NAME" = gcc ]]; then
    LIBATOMIC_RPM_NAME=libatomic
else
    LIBATOMIC_RPM_NAME=${GCC_RPM_NAME%%gcc}libatomic-devel
fi

PACKAGES="${LIBATOMIC_RPM_NAME} ${GCC_RPM_NAME} ${GCC_RPM_NAME}-c++ ${GCC_RPM_NAME}-gfortran rpm-build dejagnu yum-utils perl"

rlJournalStart
    rlPhaseStartSetup
        rlRun "yum -y install --skip-broken $PACKAGES" 0-255 # best effort

        # We optionally need to skip checking for the presence of the metapackage
        # because that would pull in all the dependent toolset subrpms.  We do not
        # always want that, especially in CI.
        _COLLECTIONS="$COLLECTIONS"
        if ! test -z $SKIP_COLLECTION_METAPACKAGE_CHECK; then
            for c in $SKIP_COLLECTION_METAPACKAGE_CHECK; do
                rlLogInfo "ignoring metapackage check for collection $c"
                export COLLECTIONS=$(shopt -s extglob && echo ${COLLECTIONS//$c/})
            done
        fi
        rlLogInfo "(without skipped) COLLECTIONS=$COLLECTIONS"
        rlAssertRpm --all
        export COLLECTIONS="$_COLLECTIONS"

        export TESTDIR=$PWD
        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "pushd $TmpDir"

        if [[ "$GCC_RPM_NAME" == *toolset* ]]; then
            rlFetchSrcForInstalled $GCC_RPM_NAME || yumdownloader --source $GCC_RPM_NAME
        else
            rlFetchSrcForInstalled libatomic || yumdownloader --source gcc-libraries
        fi

        rlRun "ls *.src.rpm" 0 "Some source has been downloaded"
        rlRun "rpm -Uvh *.src.rpm"
        rlRun "yum-builddep --nogpgcheck -y *.src.rpm" 0-255

        spec_dir=$(rpm --eval=%_specdir)
        build_dir=$(rpm --eval=%_builddir)

        # Prebuild sources, apply patches.
        rlRun "rpmbuild -bp --nodeps $spec_dir/gcc*spec"

        target_dir=$(ls -td $build_dir/gcc* | grep gcc | head -n1)
        # We'll be e.g. in /root/rpmbuild/BUILD/gcc-4.4.5-20110214/gcc/testsuite.
    rlPhaseEnd

    rlPhaseStartTest
        # Run the libatomic testsuite.
        # We don't run this under rlRun, it will always fail.
        rlRun "cd $target_dir"
        rlLogInfo "PWD=$PWD"
        if [ -d ./libatomic ]; then
            cd ./libatomic/testsuite
            runtest --tool libatomic GXX_UNDER_TEST=1 GFORTRAN_UNDER_TEST=1
            chmod -x *.log
            rlFileSubmit libatomic.sum
            rlFileSubmit libatomic.log
            $TESTDIR/apply_blacklist.pl < libatomic.sum > libatomic.sum.filtered
            rlRun "grep -P '^FAIL' libatomic.sum.filtered" 1 "There are no (uknown) failures in the libatomic.sum"
        else
            rlFail "libatomic/ not found."
            ls
        fi
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
