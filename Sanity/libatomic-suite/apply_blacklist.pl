#!/usr/bin/perl

#### blacklist sets

@BL = ();

@BL_s390x__lt_z13 = (
	'libatomic.c/atomic-compare-exchange-5.c',
	'libatomic.c/atomic-exchange-5.c',
	'libatomic.c/atomic-load-5.c',
	'libatomic.c/atomic-store-5.c' );


#### sorting out which blacklist to use

$arch = `arch`; chomp $arch;
if ($arch eq 's390x')
{
	# if the CPU is older than z13, use the blacklist
	open (INFILE, '< /proc/cpuinfo') or die "ERROR: Unable to read from /proc/cpuinfo";
	@cpuinfo = <INFILE>;
	close INFILE or die "$!";
	for (@cpuinfo)
	{
		if (/machine/)
		{
			(my $cpu_version) = $_ =~ /machine\s*=\s*(\d+)/;
			last if $cpu_version;
		}
	}
	@BL = (@BL, @BL_s390x__lt_z13) if $cpu_version < 2964;
}

# HERE CAN BE ANOTHER BLACKLISTING LOGIC IF NEEDEDE


#### go!

while (<STDIN>)
{
	unless (/^FAIL/) { print; next; }

	my $known_fail = 0;
	for $substring (@BL)
	{
		if(index($_, $substring) != -1)
		{
			$known_fail = 1;
			last;
		}
	}
	print unless $known_fail;
}
