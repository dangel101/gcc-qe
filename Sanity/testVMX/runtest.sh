#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Sanity/testVMX
#   Description: a test program to check the correctness of VMX instructions
#   Author: Michal Nowak <mnowak@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2010 Red Hat, Inc. All rights reserved.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 3 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

# Notes on relevancy
#
# Relevant for ppc and ppc64 only and older versions of gcc. Of all supported
# GCCs it's now just system GCC of RHEL-7/ppc64.
#
# Suggested TCMS relevancy:
# arch != ppc64: False
# distro > rhel-7: False
# collection defined: False

GCC="${GCC:-$(type -P gcc)}"
GCC_RPM_NAME=$(rpm --qf '%{name}' -qf $GCC)

PRI_ARCH=$(rlGetPrimaryArch)
SEC_ARCH=$(rlGetSecondaryArch)

PACKAGES="$GCC_RPM_NAME ${GCC_RPM_NAME}-c++"
for i in $PRI_ARCH $SEC_ARCH; do
    PACKAGES+=" glibc-devel.$i ${GCC_RPM_NAME%gcc}libstdc++-devel.$i"
done

rlJournalStart
    rlPhaseStartSetup
        rlRun "yum -y install --skip-broken $PACKAGES" 0-255 # best effort

        rlLogInfo "PACKAGES=$PACKAGES"
        rlLogInfo "COLLECTIONS=$COLLECTIONS"
        rlLogInfo "GCC=$GCC"
        rlLogInfo "SKIP_COLLECTION_METAPACKAGE_CHECK=$SKIP_COLLECTION_METAPACKAGE_CHECK"

        # We optionally need to skip checking for the presence of the metapackage
        # because that would pull in all the dependent toolset subrpms.  We do not
        # always want that, especially in CI.
        _COLLECTIONS="$COLLECTIONS"
        if ! test -z $SKIP_COLLECTION_METAPACKAGE_CHECK; then
            for c in $SKIP_COLLECTION_METAPACKAGE_CHECK; do
                rlLogInfo "ignoring metapackage check for collection $c"
                export COLLECTIONS=$(shopt -s extglob && echo ${COLLECTIONS//$c/})
            done
        fi
        rlLogInfo "(without skipped) COLLECTIONS=$COLLECTIONS"
        rlAssertRpm --all
        export COLLECTIONS="$_COLLECTIONS"

        rlRun "TmpDir=$(mktemp -d)"
        rlRun "cp testVMX-0.2.2.tar.gz $TmpDir"
        rlRun "pushd $TmpDir"
        rlRun 'tar xvzf testVMX-0.2.2.tar.gz'
        rlRun 'pushd testVMX-0.2.2'
    rlPhaseEnd

    rlPhaseStartTest
        # You need POWER6 or POWER7 to run this
        if ! grep -q POWER5 /proc/cpuinfo; then
            rlRun 'make gcc' # 'Build 32- and 64-bit testsuite'
            for i in testVMX-gcc-32 testVMX-gcc-64 testVMX-g++-32 testVMX-g++-64; do
                rlAssertExists $i
                rlRun "./$i >$i.log"
                rlAssertExists $i.log
                rlRun "grep Error $i.log" 1
            done
        else
            echo '*** cpuinfo follows ***'
            cat /proc/cpuinfo
            echo
            rlDie 'Untested, you are running POWER5. Find POWER6 or POWER7.'
        fi
    rlPhaseEnd

    rlPhaseStartCleanup
        rlBundleLogs testVMX *.log
        rlRun popd
        rlRun popd
        rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd

