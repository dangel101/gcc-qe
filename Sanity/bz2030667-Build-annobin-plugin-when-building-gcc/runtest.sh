#!/usr/bin/env bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Sanity/bz2030667-Build-annobin-plugin-when-building-gcc
#   Description: Test for BZ#2030667 (Build annobin plugin when building gcc)
#   Author: Vaclav Kadlcik <vkadlcik@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2022 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

# So far, the feature has been implemented in base GCC in RHEL 9.
# It's clear it won't be implemented in base GCC of RHEL <8.6, nor
# in any gcc/dev-toolset <12. Suggested TCMS relevancy:
#   distro < rhel-8 && collection !defined: False
#   distro < rhel-8.7 && collection !defined: False
#   collection contains gcc-toolset-10, gcc-toolset-11, gcc-toolset-12, devtoolset-10, devtoolset-11, devtoolset-12: False
# We may need to extend it if the feature.

GCC="${GCC:-$(type -P gcc)}"

GCC_RPM_NAME=$(rpm --qf '%{name}' -qf $GCC)
PLUGIN_RPM_NAME=${GCC_RPM_NAME}-plugin-annobin

PACKAGES="$GCC_RPM_NAME $PLUGIN_RPM_NAME"

rlJournalStart
    rlPhaseStartSetup
        rlRun "yum -y install --skip-broken $PACKAGES" 0-255 # best effort

        rlLogInfo "PACKAGES=$PACKAGES"
        rlLogInfo "COLLECTIONS=$COLLECTIONS"
        rlLogInfo "GCC=$GCC"
        rlLogInfo "SKIP_COLLECTION_METAPACKAGE_CHECK=$SKIP_COLLECTION_METAPACKAGE_CHECK"

        # We optionally need to skip checking for the presence of the metapackage
        # because that would pull in all the dependent toolset subrpms.  We do not
        # always want that, especially in CI.
        _COLLECTIONS="$COLLECTIONS"
        if ! test -z $SKIP_COLLECTION_METAPACKAGE_CHECK; then
            for c in $SKIP_COLLECTION_METAPACKAGE_CHECK; do
                rlLogInfo "ignoring metapackage check for collection $c"
                export COLLECTIONS=$(shopt -s extglob && echo ${COLLECTIONS//$c/})
            done
        fi
        rlLogInfo "(without skipped) COLLECTIONS=$COLLECTIONS"
        rlAssertRpm --all
        export COLLECTIONS="$_COLLECTIONS"

        rlRun "PLUGIN_BASE_NAME=gcc-annobin.so"
        rlRun "PLUGIN_LIB_FULL_VERSION=0.0.0" # TODO Do not hardcode
        rlRun "PLUGIN_LIB_MAJOR_VERSION=${PLUGIN_LIB_FULL_VERSION%%.*}"
        rlRun "PLUGIN_DIR=$(gcc -print-file-name=plugin)"
        rlRun "PLUGIN_BASE_NAME=gcc-annobin.so"
        rlRun "PLUGIN_FILE=${PLUGIN_DIR}/${PLUGIN_BASE_NAME}.${PLUGIN_LIB_FULL_VERSION}"
        rlRun "PLUGIN_SYMLINKS='${PLUGIN_DIR}/${PLUGIN_BASE_NAME} ${PLUGIN_DIR}/${PLUGIN_BASE_NAME}.${PLUGIN_LIB_MAJOR_VERSION}'"

        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    rlPhaseStartTest LIB_EXISTS
        rlAssertExists "$PLUGIN_FILE"
        if ! [[ -f "$PLUGIN_FILE" ]]; then
            rlFail "$PLUGIN_FILE is not a regular file"
        fi
        for i in $PLUGIN_SYMLINKS; do
            rlAssertExists "$i"
            if ! [[ -h "$i" ]]; then
                rlFail "$i is not a symlink"
            fi
            if ! [[ $(readlink -f "$i") = "$PLUGIN_FILE" ]]; then
                rlFail "$i does not link to $PLUGIN_FILE"
            fi
        done
    rlPhaseEnd

    rlPhaseStartTest RPM_MAKES_SENSE
        rlRun "rpm -ql $PLUGIN_RPM_NAME >files.txt"
        rlRun 'sed -i"" -e "s/^/|/" -e "s/\$/|/" files.txt'

        rlLogInfo "Check if rpm knows about $PLUGIN_BASE_NAME"
        for i in $PLUGIN_FILE $PLUGIN_SYMLINKS; do
            rlAssertGrep "|$i|" files.txt
        done

        rlLogInfo 'Check that rpm does not contain related garbage'
        # see https://bugzilla.redhat.com/show_bug.cgi?id=2030667#c12
        rlRun "grep -v '|${PLUGIN_FILE}[\.0-9]*|' <files.txt >files_without_annobin.txt"
        rlRun 'grep "|${PLUGIN_FILE}" <files_without_annobin.txt >suspected.txt' 0,1
        for i in $(<suspected.txt); do
            rlFail "${i//|/} should not be in rpm -ql $PLUGIN_RPM_NAME"
        done
    rlPhaseEnd

    rlPhaseStartTest NO_RPATH_IN_LIBRARY
        # RHBZ#2030667 (#c61 and #c64) and RHBZ#2047356
        rlLogInfo 'Check there is no RPATH embedded'
        rlRun "chrpath $PLUGIN_FILE &>chrpath.out" 2
        rlAssertGrep 'no rpath or runpath' chrpath.out -i
    rlPhaseEnd

    rlPhaseStartTest LIBRARY_WORKS
        rlRun 'echo "int main() {return 0;}" >p.c'
        rlRun "gcc -fplugin=$PLUGIN_FILE p.c"
        rlRun 'readelf --notes --wide a.out >notes.txt'
        rlAssertGrep 'plugin name: .*gcc-annobin' notes.txt
        rlGetPhaseState || rlFileSubmit notes.txt notes.txt
    rlPhaseEnd

    # TODO
    # - redhat-rpm-config is aware - https://bugzilla.redhat.com/show_bug.cgi?id=2030671

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
