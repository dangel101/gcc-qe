/* taken from https://gcc.gnu.org/git/gitweb.cgi?p=gcc.git;h=a65b9ad863c5fc0aea12db58557f4d286a1974d7 */

void __attribute__((noinline, noclone))
vadd (int *dst, int *op1, int *op2, int count)
{
  for (int i = 0; i < count; ++i)
    dst[i] = op1[i] + op2[i];
}
