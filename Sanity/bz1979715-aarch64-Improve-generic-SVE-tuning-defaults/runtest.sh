#!/usr/bin/env bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Sanity/bz1979715-aarch64-Improve-generic-SVE-tuning-defaults
#   Description: Test for BZ#1979715 (aarch64 Improve generic SVE tuning defaults)
#   Author: Vaclav Kadlcik <vkadlcik@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2021 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

# Specific to aarch64 and GCC>=11.
#
# We don't won't have any aarch64 GCC 11+ on RHEL7. On RHEL8, just gcc-toolset
# 11 and newer are relevant. RHEL9 has system GCC 11, no GTS yet and any GTS
# will be 12 or newer. Suggested relevancy for TCMS:
#   arch != aarch64: False
#   distro < rhel-8: False
#   distro = rhel-8 && collection !defined: False
#   collection contains gcc-toolset-10: False

GCC="${GCC:-$(type -P gcc)}"
PACKAGE=$(rpm --qf '%{name}' -qf $GCC)
PACKAGES="${PACKAGE}"

rlJournalStart
    rlPhaseStartSetup
        rlMountRedhat

        rlLogInfo "PACKAGES=$PACKAGES"
        rlLogInfo "COLLECTIONS=$COLLECTIONS"
        rlLogInfo "GCC=$GCC"
        rlLogInfo "SKIP_COLLECTION_METAPACKAGE_CHECK=$SKIP_COLLECTION_METAPACKAGE_CHECK"

        # We optionally need to skip checking for the presence of the metapackage
        # because that would pull in all the dependent toolset subrpms.  We do not
        # always want that, especially in CI.
        _COLLECTIONS="$COLLECTIONS"
        if ! test -z $SKIP_COLLECTION_METAPACKAGE_CHECK; then
            for c in $SKIP_COLLECTION_METAPACKAGE_CHECK; do
                rlLogInfo "ignoring metapackage check for collection $c"
                export COLLECTIONS=$(shopt -s extglob && echo ${COLLECTIONS//$c/})
            done
        fi
        rlLogInfo "(without skipped) COLLECTIONS=$COLLECTIONS"
        rlAssertRpm --all
        export COLLECTIONS="$_COLLECTIONS"

        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "cp files/* $TmpDir"
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    rlPhaseStartTest cse_sve_vl_constants_recognized
        rlRun -c 'gcc -c -O3 -moverride=tune=cse_sve_vl_constants ./cse_sve_vl_constants_1.c'
    rlPhaseEnd

    rlPhaseStartTest cse_sve_vl_constants_used_internally
        # I am not aware of a better option than checking if patch
        #    https://gcc.gnu.org/git/?p=gcc.git;a=commit;h=8f0c9d53ef3a9b8ba2579b53596cc2b7f5d8bf69
        # is present in the sources.
        # Let's do the check in a "relax" way so we allow for some future developemnt.

        rlFetchSrcForInstalled $PACKAGE
        SRPM=$(find . -name '*.src.rpm')
        rlLogInfo "SRPM=$SRPM"
        rlRun 'mkdir rpmbuild'
        rlRun "rpm -ivh $SRPM --define='_topdir $PWD/rpmbuild'"
        rlRun "yum-builddep -y --skip-unavailable $PWD/rpmbuild/SPECS/gcc.spec"
        rlRun "rpmbuild --nodeps --define='_topdir $PWD/rpmbuild' -bp $PWD/rpmbuild/SPECS/gcc.spec"

        # AARCH64_EXTRA_TUNE_CSE_SVE_VL_CONSTANTS should be enabled ...
        rlRun 'cat rpmbuild/BUILD/*/gcc/config/aarch64/aarch64.c{c,} | awk "/^static const struct tune_params generic_tunings =/,/^}/" >generic_tunings.txt'
        rlAssertGrep AARCH64_EXTRA_TUNE_CSE_SVE_VL_CONSTANTS generic_tunings.txt

        # .. but then disabled for SVE2 and above
        rlRun 'cat rpmbuild/BUILD/*/gcc/config/aarch64/aarch64.c{c,} | awk "/^aarch64_adjust_generic_arch_tuning/,/^}/" | tr \\012 " " >aarch64_adjust_generic_arch_tuning.txt'
        rlAssertGrep 'if (TARGET_SVE2).*~AARCH64_EXTRA_TUNE_CSE_SVE_VL_CONSTANTS' aarch64_adjust_generic_arch_tuning.txt
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
