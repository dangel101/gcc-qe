#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Sanity/compile-rpm
#   Description: Compile a Red Hat RPM package.
#   Author: Marek Polacek <polacek@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2012 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

GCC="${GCC:-$(type -P gcc)}"
GCC_RPM_NAME=$(rpm --qf '%{name}' -qf $GCC)
GDB_RPM_NAME=${GCC_RPM_NAME%gcc}gdb

PACKAGES="$GCC_RPM_NAME $GDB_RPM_NAME autoconf bison dejagnu elfutils-libelf-devel expat-devel flex gcc gdb glibc-devel grep libselinux-devel make mpfr-devel ncurses-devel readline-devel rpm-build rpm-devel texinfo xz-devel yum-utils zlib-devel"

rlJournalStart
    rlPhaseStartSetup

        # Work around troubles with buildroot packages being out-of-sync
        if rlIsRHEL; then
            rlMountRedhat
            for i in libipt source-highlight libbabeltrace; do
            rpm -q $i &>/dev/null || rlRun "yum -y install $i" 0-255
            rpm -q ${i}-devel &>/dev/null || rlRun "yum -y install ${i}-devel" 0-255
            d=/mnt/redhat/brewroot/packages/$i
            if rpm -q $i &>/dev/null && ! rpm -q ${i}-devel &>/dev/null; then
                if [[ -e /mnt/redhat/brewroot/packages/$i ]]; then
                    d=/mnt/redhat/brewroot/packages/$i
                else
                    d=/mnt/redhat/brewroot/packages/${i#lib}
                fi
                rlRun "yum -y install $d/$(rpm -q --qf='%{VERSION}/%{RELEASE}/%{ARCH}' $i)/${i}-devel-$(rpm -q --qf='%{VERSION}-%{RELEASE}.%{ARCH}' $i).rpm"
            fi
            done
        fi

        rlRun "yum -y install --skip-broken $PACKAGES" 0-255 # best effort

        rlLogInfo "PACKAGES=$PACKAGES"
        rlLogInfo "COLLECTIONS=$COLLECTIONS"
        rlLogInfo "GCC=$GCC"
        rlLogInfo "SKIP_COLLECTION_METAPACKAGE_CHECK=$SKIP_COLLECTION_METAPACKAGE_CHECK"

        # We optionally need to skip checking for the presence of the metapackage
        # because that would pull in all the dependent toolset subrpms.  We do not
        # always want that, especially in CI.
        _COLLECTIONS="$COLLECTIONS"
        if ! test -z $SKIP_COLLECTION_METAPACKAGE_CHECK; then
            for c in $SKIP_COLLECTION_METAPACKAGE_CHECK; do
                 rlLogInfo "ignoring metapackage check for collection $c"
                 export COLLECTIONS=$(shopt -s extglob && echo ${COLLECTIONS//$c/})
            done
        fi
        rlLogInfo "(without skipped) COLLECTIONS=$COLLECTIONS"
        rlAssertRpm --all
        export COLLECTIONS="$_COLLECTIONS"

        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "pushd $TmpDir"

        rlFetchSrcForInstalled "$GDB_RPM_NAME" || yumdownloader --source "$GDB_RPM_NAME"
        srpm=$(rpmquery "$GDB_RPM_NAME" --queryformat=%{NAME}-%{VERSION}-%{RELEASE})".src.rpm"
        rlRun "rpm -Uvh $srpm"
        spec_dir=$(rpm --eval=%_specdir)
        build_dir=$(rpm --eval=%_builddir)
        pkg_dir=$(rpmquery "$GDB_RPM_NAME" --queryformat=%{NAME}-%{VERSION})

        rlRun "yum-builddep -y $spec_dir/gdb.spec" 0-255

    rlPhaseEnd

    rlPhaseStartTest
        rlRun "rpmbuild -bb $spec_dir/gdb.spec &> BUILD_LOG"
        test $? -eq 0 || tail -n 20 BUILD_LOG
    rlPhaseEnd

    rlPhaseStartCleanup
        rlBundleLogs "Build-log" BUILD_LOG
        rlRun "popd"
        rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
