#!/usr/bin/env bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Sanity/bz2074614-Backport-Default-widths-with-fdec-format-defaults
#   Description: Test for BZ#2074614 (Backport Default widths with -fdec-format-defaults)
#   Author: Vaclav Kadlcik <vkadlcik@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2022 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

# Expected to pass on
#   * System GCC of RHEL 8.7+, 9+
#   * Any currently supported DTS GCC
#   * Any currently supported GTS GCC on RHEL 8.7+. Note that on RHEL 8.0-8.6
#     it will fail because the backport not only affects gfortran-the-compiler
#     but libgfotran-the-runtime as well and libgfotran in RHEL 8 is shipped
#     with system GCC only. The testing programs below will fail in runtime
#     because of libgfotran of RHEL 8.0-8.6 lacks the backport we are testing
#     here.
# Suggested TCMS relevancy:
#   collection !defined && distro < rhel-8: False
#   collection !defined && distro < rhel-8.7: False
#   collection defined && distro = rhel-8 && distro < rhel-8.7: False

GCC="${GCC:-$(type -P gcc)}"

GCC_RPM_NAME=$(rpm --qf '%{name}' -qf $GCC)
GFORTRAN_RPM_NAME=${GCC_RPM_NAME}-gfortran

PACKAGES="$GCC_RPM_NAME $GFORTRAN_RPM_NAME"

rlJournalStart
    rlPhaseStartSetup
        rlRun "yum -y install --skip-broken $PACKAGES" 0-255 # best effort

        rlLogInfo "PACKAGES=$PACKAGES"
        rlLogInfo "COLLECTIONS=$COLLECTIONS"
        rlLogInfo "GCC=$GCC"
        rlLogInfo "SKIP_COLLECTION_METAPACKAGE_CHECK=$SKIP_COLLECTION_METAPACKAGE_CHECK"

        # We optionally need to skip checking for the presence of the metapackage
        # because that would pull in all the dependent toolset subrpms.  We do not
        # always want that, especially in CI.
        _COLLECTIONS="$COLLECTIONS"
        if ! test -z $SKIP_COLLECTION_METAPACKAGE_CHECK; then
            for c in $SKIP_COLLECTION_METAPACKAGE_CHECK; do
                rlLogInfo "ignoring metapackage check for collection $c"
                export COLLECTIONS=$(shopt -s extglob && echo ${COLLECTIONS//$c/})
            done
        fi
        rlLogInfo "(without skipped) COLLECTIONS=$COLLECTIONS"
        rlAssertRpm --all
        export COLLECTIONS="$_COLLECTIONS"

        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "cp files/* $TmpDir"
        rlRun "pushd $TmpDir"

        # The following tests programs have been copied from the upstream
        # testsuite thus are not Red Hat's IP => do not claim copyright
        # on these files.
        #
        # Note that RHBZ#2074614's patch includes some build-time tests but
        # they are not 1:1 with the upstream so it won't hurt to double
        # check...
        rlRun "TEST_PROGRAMS='$(echo fmt_{f,g,i}_default_field_width_{1,2,3}.f90)'"
    rlPhaseEnd

    for i in $TEST_PROGRAMS; do
        rlPhaseStartTest $i
            rm -f a.out
            rlRun 'gfortran -fdec ./fmt_f_default_field_width_1.f90'
            rlRun ./a.out
        rlPhaseEnd
    done

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
