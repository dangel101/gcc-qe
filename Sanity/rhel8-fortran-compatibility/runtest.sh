#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Sanity/rhel8-fortran-compatibility
#   Description: The test runs fortran binaries built on RHEL-6 and RHEL-7 with compat-library on RHEL-8
#   Author: Michael Petlan <mpetlan@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2019 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

# Notes on relevancy - see main.fmf

rlJournalStart
	rlPhaseStartSetup
		ARCH=`rlGetPrimaryArch`
		SECARCH=`rlGetSecondaryArch`
		if rlIsRHEL '>=8'; then
			test "$SECARCH" = "s390" && unset SECARCH
		fi

		for i in "$ARCH" "$SECARCH"; do
			if [[ -n "$i" ]]; then
				rpm -q compat-libgfortran-48.$i 2>/dev/null || rlRun "yum -y install compat-libgfortran-48.$i"
			fi
		done

		rlAssertRpm "compat-libgfortran-48.$ARCH"
		rlAssertRpm "libgfortran.$ARCH"
		if [ -n "$SECARCH" ]; then
			# this basically means that on x86_64, we want to test the 32bit package too
			rlAssertRpm "compat-libgfortran-48.$SECARCH"
			rlAssertRpm "libgfortran.$SECARCH"
		fi
		rlRun "xzcat bins.tar.xz | tar x" 0 "Unpacking the binaries"
	rlPhaseEnd

	rlPhaseStartTest "Compatible binaries"
		cd pass
		for a in $ARCH $SECARCH; do
			for b in linpack1000d-*-$a; do
				rlRun "./$b" 0 "$b"
			done
		done
		cd ..
	rlPhaseEnd

	rlPhaseStartTest "Incompatible binaries"
		cd fail
		for a in $ARCH $SECARCH; do
			for b in linpack1000d-*-$a; do
				rlRun "./$b" 1-255 "$b"
			done
		done
		cd ..
	rlPhaseEnd

	rlPhaseStartCleanup
		rlRun "rm -rf pass fail" 0 "Removing temporary directories"
	rlPhaseEnd
rlJournalPrintText
rlJournalEnd
