#!/usr/bin/env bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Sanity/bz1974402-Sane-DWARF-default
#   Description: Test for BZ#1974402 (Make gcc-toolset-11-gcc default to DWARF4)
#   Author: Vaclav Kadlcik <vkadlcik@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2021 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

GCC="${GCC:-$(type -P gcc)}"
PACKAGE=$(rpm --qf '%{name}' -qf $GCC)
PACKAGES="$PACKAGE"

rlJournalStart
    rlPhaseStartSetup
        rlLogInfo "PACKAGES=$PACKAGES"
        rlLogInfo "COLLECTIONS=$COLLECTIONS"
        rlLogInfo "GCC=$GCC"
        rlLogInfo "SKIP_COLLECTION_METAPACKAGE_CHECK=$SKIP_COLLECTION_METAPACKAGE_CHECK"

        # We optionally need to skip checking for the presence of the metapackage
        # because that would pull in all the dependent toolset subrpms.  We do not
        # always want that, especially in CI.
        _COLLECTIONS="$COLLECTIONS"
        if ! test -z $SKIP_COLLECTION_METAPACKAGE_CHECK; then
            for c in $SKIP_COLLECTION_METAPACKAGE_CHECK; do
                rlLogInfo "ignoring metapackage check for collection $c"
                export COLLECTIONS=$(shopt -s extglob && echo ${COLLECTIONS//$c/})
            done
        fi
        rlLogInfo "(without skipped) COLLECTIONS=$COLLECTIONS"
        rlAssertRpm --all
        export COLLECTIONS="$_COLLECTIONS"

        DESIRED_DWARF_VERSION='who_knows'
        if rlIsRHEL 6; then
            DESIRED_DWARF_VERSION=3
        elif rlIsRHEL 7; then
            DESIRED_DWARF_VERSION=4
        elif rlIsRHEL 8; then
            DESIRED_DWARF_VERSION=4
        elif rlIsRHEL 9; then
            DESIRED_DWARF_VERSION=5
        else
            rlFail 'Unknown RHEL version'
        fi
        rlLogInfo "DESIRED_DWARF_VERSION=$DESIRED_DWARF_VERSION"

        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    rlPhaseStartTest
        rlRun 'echo "int main() {return 0;}" >p.c'
        rlRun 'gcc -g -o p p.c'
        rlRun 'readelf --debug-dump=info p &>readelf.out'
        rlRun 'MANWIDTH=4096 man gcc &>man.out'
        FOUND_DWARF_VERSION=$(awk '{if ($1 == "Version:") {print $2; exit;}}' <readelf.out)
        rlAssertEquals "DWARF version in binaries is $DESIRED_DWARF_VERSION" "$DESIRED_DWARF_VERSION" "$FOUND_DWARF_VERSION"
        if rlIsRHEL '<=6' && [[ "$GCC" = /opt/rh/devtoolset-10/root/usr/bin/gcc ]]; then
            # Bug in the man page of DTS-10/RHEL-6 (officially not supported build anyway), let's skip it
            rlLogError 'man gcc of DTS-10/RHEL-6 is wrong but we do not care anymore'
        else
            rlAssertGrep "DWARF.*default version.*is $DESIRED_DWARF_VERSION[^0-9]" man.out
        fi
        rlGetPhaseState || rlFileSubmit readelf.out readelf.out
        rlGetPhaseState || rlFileSubmit man.out man.out
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
