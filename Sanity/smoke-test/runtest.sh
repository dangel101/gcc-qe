#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Sanity/smoke-test
#   Description: Basic smoke test.
#   Author: Marek Polacek <polacek@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2012 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

GCC="${GCC:-$(type -P gcc)}"
GCC_RPM_NAME=$(rpm --qf '%{name}' -qf $GCC)

PRI_ARCH=$(rlGetPrimaryArch)

PACKAGES="$GCC_RPM_NAME ${GCC_RPM_NAME}-c++ ${GCC_RPM_NAME}-gfortran ${GCC_RPM_NAME%%gcc}libstdc++-devel glibc-devel libgcc libgomp"

# Define where libitm-devel is expected
if rlIsRHEL '<7' && [[ "$GCC_RPM_NAME" = gcc ]]; then
    : # libitm-devel not present in system GCC of RHEL<7
else
    PACKAGES+=" ${GCC_RPM_NAME%%gcc}libitm-devel"
fi

# Define where libquadmath-devel is expected
if [[ "$GCC_RPM_NAME" = gcc ]]; then
    # base GCC
    if rlIsRHEL '<7'; then
        : # no libquadmath-devel there
    elif rlIsRHEL 7 && [[ "$PRI_ARCH" != x86_64 ]]; then
        : # no libquadmath-devel there
    elif rlIsRHEL 8 9 && [[ "$PRI_ARCH" != x86_64 ]] && [[ "$PRI_ARCH" != ppc64le ]]; then
        : # no libquadmath-devel there
    else
        PACKAGES+=" ${GCC_RPM_NAME%%gcc}libquadmath-devel"
    fi
else
    # toolset GCC
    if rlIsRHEL 7 && [[ "$PRI_ARCH" = s390x ]]; then
        : # no libquadmath-devel there
    elif rlIsRHEL 8 9 && [[ "$PRI_ARCH" != x86_64 ]] && [[ "$PRI_ARCH" != ppc64le ]]; then
        : # no libquadmath-devel there
    else
        PACKAGES+=" ${GCC_RPM_NAME%%gcc}libquadmath-devel"
    fi
fi

rlJournalStart
    rlPhaseStartSetup
        rlRun "yum -y install --skip-broken $PACKAGES" 0-255 # best effort

        rlLogInfo "PACKAGES=$PACKAGES"
        rlLogInfo "COLLECTIONS=$COLLECTIONS"
        rlLogInfo "GCC=$GCC"
        rlLogInfo "SKIP_COLLECTION_METAPACKAGE_CHECK=$SKIP_COLLECTION_METAPACKAGE_CHECK"

        # We optionally need to skip checking for the presence of the metapackage
        # because that would pull in all the dependent toolset subrpms.  We do not
        # always want that, especially in CI.
        _COLLECTIONS="$COLLECTIONS"
        if ! test -z $SKIP_COLLECTION_METAPACKAGE_CHECK; then
            for c in $SKIP_COLLECTION_METAPACKAGE_CHECK; do
                rlLogInfo "ignoring metapackage check for collection $c"
                export COLLECTIONS=$(shopt -s extglob && echo ${COLLECTIONS//$c/})
            done
        fi
        rlLogInfo "(without skipped) COLLECTIONS=$COLLECTIONS"
        rlAssertRpm --all
        export COLLECTIONS="$_COLLECTIONS"

        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "cp -v hello.{c,cpp,f90} tm.c quad.c thr-init-2.c clear_cache.c omphello.c $TmpDir"
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    rlPhaseStartSetup 'Showing compiler versions'
        for compiler in gcc g++ gfortran; do
            rlLogInfo "Version of compiler: $compiler"
            eval "$compiler --version 2>&1" | while read line; do
                rlLogInfo "  $line"
            done
        done
    rlPhaseEnd

    rlPhaseStartTest Compile
        rlRun 'gcc hello.c -o hello_c'
        rlRun 'g++ hello.cpp -o hello_cpp'
        rlRun 'gfortran hello.f90 -o hello_fortran'

        # Test TM
        if rpm -q ${GCC_RPM_NAME%%gcc}libitm-devel &>/dev/null; then
            rlRun 'gcc -O2 -std=gnu99 -fgnu-tm tm.c -o tm'
            rlRun ./tm
        fi

        # Test OpenMP
        rlRun 'gcc omphello.c -O2 -std=gnu99 -fopenmp -o omp'
        rlRun ./omp

        # Test __thread.
        rlRun 'gcc thr-init-2.c -O2 -std=gnu99 -ftls-model=initial-exec -o thr'
        rlRun ./thr

        # Now test some libquadmath stuff (__float128 support).
        if rpm -q ${GCC_RPM_NAME%%gcc}libquadmath-devel &>/dev/null; then
            rlRun 'gcc quad.c -O2 -std=gnu99 -lquadmath -lm -o quad'
            rlRun ./quad
        fi

        # And now something from libgcc, e.g. __builtin___clear_cache
        rlRun 'gcc clear_cache.c -O2 -std=gnu99 -o cache'
        rlRun ./cache
    rlPhaseEnd

    rlPhaseStartTest 'Check dependant libraries'
        rlRun 'ldd hello_{c,cpp,fortran} &> ldd.out'
        # Nothing should be linked against anything in /opt
        rlAssertNotGrep /opt ldd.out
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun popd
        rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
