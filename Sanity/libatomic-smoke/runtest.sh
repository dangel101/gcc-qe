#!/usr/bin/env bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Sanity/libatomic-smoke
#   Description: Just runs pre-built binaries
#   Author: Michael Petlan <mpetlan@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2016 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

# Relevant for x86_64 only (we are using x86_64 test binaries). Suggested
# TCMS relevancy:
#   arch != x86_64: False

GCC="${GCC:-$(type -P gcc)}"
GCC_RPM_NAME=$(rpm --qf '%{name}' -qf $GCC)

# libatomic ships as libatomic with base GCC but as <toolset>-libatomic-devel
# in Collections (where it's just a linker script referring the base variant)
if [[ "$GCC_RPM_NAME" = gcc ]]; then
    LIBATOMIC_RPM_NAME=libatomic
else
    LIBATOMIC_RPM_NAME=${GCC_RPM_NAME%%gcc}libatomic-devel
fi

PACKAGES="$GCC_RPM_NAME $LIBATOMIC_RPM_NAME"

rlJournalStart
    rlPhaseStartSetup
        rlRun "yum -y install --skip-broken $PACKAGES" 0-255 # best effort

        rlLogInfo "PACKAGES=$PACKAGES"
        rlLogInfo "COLLECTIONS=$COLLECTIONS"
        rlLogInfo "GCC=$GCC"
        rlLogInfo "SKIP_COLLECTION_METAPACKAGE_CHECK=$SKIP_COLLECTION_METAPACKAGE_CHECK"

        # We optionally need to skip checking for the presence of the metapackage
        # because that would pull in all the dependent toolset subrpms.  We do not
        # always want that, especially in CI.
        _COLLECTIONS="$COLLECTIONS"
        if ! test -z $SKIP_COLLECTION_METAPACKAGE_CHECK; then
            for c in $SKIP_COLLECTION_METAPACKAGE_CHECK; do
                rlLogInfo "ignoring metapackage check for collection $c"
                export COLLECTIONS=$(shopt -s extglob && echo ${COLLECTIONS//$c/})
            done
        fi
        rlLogInfo "(without skipped) COLLECTIONS=$COLLECTIONS"
        rlAssertRpm --all
        export COLLECTIONS="$_COLLECTIONS"

        # This REALLY_WANT_TO_RUN stuff is inherited from mpetlan. I don't think
        # it's really needed but...
        REALLY_WANT_TO_RUN="true"
        if [ "$BASEOS_CI" = "true" ]; then
            # in CI, we need to be able to skip this testcase
            # in case libatomic is not a part of gcc-libraries
            rlCheckRpm "libatomic" || REALLY_WANT_TO_RUN="false"
        fi

        rlRun "zcat bins.tar.gz | tar x"
        rlRun "pushd bins"
    rlPhaseEnd

    if [ "$REALLY_WANT_TO_RUN" = "true" ]; then
        rlPhaseStartTest
            for i in T_*; do
                rlRun "./$i"
            done
        rlPhaseEnd
    else
        rlPhaseStartTest
            rlLogWarning "SKIPPING THIS TEST -- libatomic is probably not shipped within current gcc-libraries"
        rlPhaseEnd
    fi

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -rf bins"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
