#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Sanity/rebuild-binutils
#   Description: Rebuild binutils.
#   Author: Marek Polacek <polacek@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2012 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

# Notes on relevancy
#
# The test is expected to fail in devtoolset-* on RHEL-6 because of
# the "Unresolvable `R_X86_64_NONE` relocation" family of bugs, e.g.
#   https://bugzilla.redhat.com/show_bug.cgi?id=1545386
# They have been fixed for both the base and devtoolset binutils
# on RHEL-7 but on RHEL-6, it was just the base binutils.
#
# Suggested TCMS relevancy:
#   distro = rhel-6 && collection defined: False

GCC="${GCC:-$(type -P gcc)}"
GCC_RPM_NAME=$(rpm --qf '%{name}' -qf $GCC)
BINUTILS_RPM_NAME=${GCC_RPM_NAME%gcc}binutils
PACKAGES="$GCC_RPM_NAME $BINUTILS_RPM_NAME autoconf automake glibc glibc-devel glibc-static libstdc++ libstdc++-devel perl rpm-build sharutils texinfo zlib-devel zlib-static"

rlJournalStart
    rlPhaseStartSetup
        rlRun "yum -y install --skip-broken $PACKAGES" 0-255 # best effort

        # We optionally need to skip checking for the presence of the metapackage
        # because that would pull in all the dependent toolset subrpms.  We do not
        # always want that, especially in CI.
        _COLLECTIONS="$COLLECTIONS"
        if ! test -z $SKIP_COLLECTION_METAPACKAGE_CHECK; then
            for c in $SKIP_COLLECTION_METAPACKAGE_CHECK; do
                rlLogInfo "ignoring metapackage check for collection $c"
                export COLLECTIONS=$(shopt -s extglob && echo ${COLLECTIONS//$c/})
            done
        fi
        rlLogInfo "(without skipped) COLLECTIONS=$COLLECTIONS"
        rlAssertRpm --all
        export COLLECTIONS="$_COLLECTIONS"

        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "pushd $TmpDir"

        rlFetchSrcForInstalled $BINUTILS_RPM_NAME || yumdownloader --source $BINUTILS_RPM_NAME
        rlRun "rpm -Uvh *.src.rpm"
        spec_dir=$(rpm --eval=%_specdir)
        rlRun "yum-builddep -y $spec_dir/binutils.spec" 0-255
    rlPhaseEnd

    rlPhaseStartTest
        # TODO: Is this still necessary?
        if [ "$(uname -i)" == "ppc64" ]; then
            if rlIsRHEL 6; then
                target='--target=ppc64'
            else
                target='--target=ppc'
            fi
        fi
        if [ "$(uname -i)" == "i386" ]; then
            target='--target=i686'
        fi

        rlRun "setsebool allow_execmod 1"
        rlRun "rpmbuild -bb $target --clean $spec_dir/binutils.spec &> BUILD_LOG" || ( echo "========== BUILD_LOG tail ==========" ; tail -n 20 BUILD_LOG )
        rlRun "setsebool allow_execmod 0"
    rlPhaseEnd

    rlPhaseStartCleanup
        rlBundleLogs "Build-log" BUILD_LOG
        rlRun "popd"
        rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
