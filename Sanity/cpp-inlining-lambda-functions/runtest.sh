#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Sanity/cpp-inlining-lambda-functions
#   Description: Verifies that Lambda functions are correctly inlined
#   Author: Michael Petlan <mpetlan@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2017 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

# GCC supporting -std=c++11 needed. In practice,
# - any supported gcc-toolset or devtoolset GCC
# - system GCC of RHEL 7+
# Suggested TCMS relevancy:
#   distro < rhel-7 && collection !defined: False

GCC="${GCC:-$(type -P gcc)}"
GCC_RPM_NAME=$(rpm --qf '%{name}' -qf $GCC)
GXX_RPM_NAME=${GCC_RPM_NAME}-c++
BINUTILS_RPM_NAME=${GCC_RPM_NAME%gcc}binutils
PACKAGES="$GCC_RPM_NAME $GXX_RPM_NAME $BINUTILS_RPM_NAME perl rpmdevtools"

is_new_objdump () {
    # return 0 if and only if the version >= 2.36
    local ver="$1" # expected: rpm -q --qf='%{VERSION}' <OBJ_DUMP_RPM>
    rpmdev-vercmp 0 "$ver" 0.el9 0 2.36 0.el9 # epoch and release here are just dummies
    local retval=$?
    if [[ $retval -eq 0 ]] || [[ $retval -eq 11 ]]; then
        return 0
    else
        return 1
    fi
}

rlJournalStart
    rlPhaseStartSetup
        rlRun "yum -y install --skip-broken $PACKAGES" 0-255 # best effort

        rlLogInfo "PACKAGES=$PACKAGES"
        rlLogInfo "COLLECTIONS=$COLLECTIONS"
        rlLogInfo "GCC=$GCC"
        rlLogInfo "SKIP_COLLECTION_METAPACKAGE_CHECK=$SKIP_COLLECTION_METAPACKAGE_CHECK"

        # We optionally need to skip checking for the presence of the metapackage
        # because that would pull in all the dependent toolset subrpms.  We do not
        # always want that, especially in CI.
        _COLLECTIONS="$COLLECTIONS"
        if ! test -z $SKIP_COLLECTION_METAPACKAGE_CHECK; then
            for c in $SKIP_COLLECTION_METAPACKAGE_CHECK; do
                 rlLogInfo "ignoring metapackage check for collection $c"
                 export COLLECTIONS=$(shopt -s extglob && echo ${COLLECTIONS//$c/})
            done
        fi
        rlLogInfo "(without skipped) COLLECTIONS=$COLLECTIONS"
        rlAssertRpm --all
        export COLLECTIONS="$_COLLECTIONS"

        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "cp *.pl *.cpp $TmpDir/"
        rlRun "pushd $TmpDir"

        # what binutils?
        rlRun "OBJDUMP_BIN=$(type -P objdump)"
        rlRun "OBJDUMP_RPM=$(rpm -qf $OBJDUMP_BIN)"
        rlRun "OBJDUMP_VER=$(rpm -q --qf='%{VERSION}' $OBJDUMP_RPM)"

        # check c++14 support
        echo "int main(void){return 0;}" | g++ -x c++ -std=c++14 - -o a.out
        if [ $? -eq 0 ]; then
            rlLog "c++14 standard is supported"
            export CXX14_SUPPORT="yes"
        else
            rlLogWarning "c++14 standard is NOT supporeted, however, the test still makes sense"
            export CXX14_SUPPORT="no"
        fi

        if is_new_objdump $OBJDUMP_VER; then
            rlLogInfo 'Newest binutils use "call" instead of "callq" on x86_64, changing out checking scripts...'
            for i in verify-*-dump.pl; do
                rlRun "sed -i'' 's|callq|call|' $i" # coarse but works for now...
            done
        fi
    rlPhaseEnd

    rlPhaseStartTest "C++11 test"
        rlRun "g++ -std=c++11 -g -o attrs11 attrs11.cpp"
        rlAssertExists "attrs11"
        rlRun "./attrs11 575 757 | grep 'Calling foo: 1'" 0 "Sample works #1"
        rlRun "./attrs11 575 757 | grep 'Calling bar: 1'" 0 "Sample works #2"
        rlRun "./attrs11 757 575 | grep 'Calling foo: 0'" 0 "Sample works #3"
        rlRun "./attrs11 757 575 | grep 'Calling bar: 0'" 0 "Sample works #4"
        rlRun "objdump -CSd attrs11 > attrs11.dump" 0 "Disassembling the binary"
        rlAssertExists "attrs11.dump"
        rlRun "VERBOSE=y ./verify-11-dump.pl < attrs11.dump" 0 "Verification of the objdump output" # bug reproduces here
    rlPhaseEnd

    if [ "$CXX14_SUPPORT" = "yes" ]; then
        rlPhaseStartTest "C++14 test"
            rlRun "g++ -std=c++14 -g -o attrs14 attrs14.cpp" 0 "Compiling a sample code"
            rlAssertExists "attrs14"
            rlRun "./attrs14 575 757 | grep 'Calling foo: 1'" 0 "Sample works #1"
            rlRun "./attrs14 575 757 | grep 'Calling bar: 1'" 0 "Sample works #2"
            rlRun "./attrs14 757 575 | grep 'Calling foo: 0'" 0 "Sample works #3"
            rlRun "./attrs14 757 575 | grep 'Calling bar: 0'" 0 "Sample works #4"
            rlRun "objdump -CSd attrs14 > attrs14.dump" 0 "Disassembling the binary"
            rlAssertExists "attrs14.dump"
            # FIXME:
            # This is commented out, since objdump is not able to demangle it
            #rlRun "./verify-14-dump.pl < attrs14.dump" 0 "Verification of the objdump output" # bug reproduces here
        rlPhaseEnd
    fi

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
