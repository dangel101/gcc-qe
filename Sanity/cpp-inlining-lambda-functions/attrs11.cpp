#include <iostream>

using namespace std;

int foo(int x, int y)
{
	auto lam = [](int a, int b) __attribute__ ((noinline)) { return a < b; };
	return lam(x, y);
}

int bar(int x, int y)
{
	auto lam = [](int a, int b) __attribute__ ((always_inline)) { return a < b; };
	return lam (x, y);
}

int main(int argc, char *argv[])
{
	int a, b;

	if (argc != 3)
		return 1;

	a = atoi(argv[1]);
	b = atoi(argv[2]);

	cout << "Calling foo: " << foo(a, b) << endl;
	cout << "Calling bar: " << bar(a, b) << endl;

	return 0;
}
