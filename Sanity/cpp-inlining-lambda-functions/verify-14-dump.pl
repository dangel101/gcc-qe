#!/usr/bin/perl

# init
$state = 0;
$line_cnt = 0;
$verbose = 1 if (defined $ENV{VERBOSE});
$debug = 1 if (defined $ENV{DEBUG});

# arch
$arch = `arch`;
if    ($arch =~ /^x86_64$/)  { $br = 'callq'; }
elsif ($arch =~ /^s390x$/)   { $br = 'jl'; }
elsif ($arch =~ /^ppc64$/)   { $br = 'bl'; }
elsif ($arch =~ /^ppc64le$/) { $br = 'bl'; }
elsif ($arch =~ /^aarch64$/) { $br = 'bl'; }
else { $br =''; }

# parse
while (<>)
{
	chomp;
	$line_cnt++;

	print "DEBUG: $line_cnt [ $state ]: $_\n" if ($debug);
	if ($state == 0)
	{
		$r = '^int foo\(int x, int y\)$';
		next unless /$r/; $state++;
	}
	elsif ($state == 1)
	{
		$r = '^\{$';
		next unless /$r/; $state++;
	}
	elsif ($state == 2)
	{
		$r = '^\s+auto lam = \[\]\(auto a, auto b\) __attribute__ \(\(noinline\)\) \{ return a < b; \};$';
		next unless /$r/; $state++;
	}
	elsif ($state == 3)
	{
		$r = $br . '\s+\w+\s+<\.?foo\(int, int\)::\{lambda\(int, int\)#1\}::operator\(\)\(int, int\) const';
		next unless /$r/; $state++;
	}
	elsif ($state == 4)
	{
		$r = '^\}$';
		next unless /$r/; $state++;
	}
	elsif ($state == 5)
	{
		$r = '^int bar\(int x, int y\)$';
		next unless /$r/; $state++;
	}
	elsif ($state == 6)
	{
		$r = 'auto lam = \[\]\(auto a, auto b\) __attribute__ \(\(always_inline\)\) \{ return a < b; \};';
		next unless /$r/; $state++;
	}
	elsif ($state == 7)
	{
		$r = '^\}$';
		# here must NOT be the function call!
		if (/<\.?foo\(int, int\)::\{lambda\(int, int\)#1\}::operator\(\)\(int, int\) const/)
		{
			$state = 99; next;
		}
		# here must NOT be the function call!
		if (/<\.?bar\(int, int\)::\{lambda\(int, int\)#1\}::operator\(\)\(int, int\) const/)
		{
			$state = 99; next;
		}
		next unless /$r/; $state++;
	}
	else
	{
		last;
	}
}

if ($state == 8)
{
	# everything is as expected --> PASS
	print "PASS\n" if ($verbose);
	exit (0);
}
elsif ($state == 99)
{
	# we found a call statement in always_inline --> FAIL+BUG
	$line_cnt--;
	print "FAIL/BUG: call statement found in bar() at line: $line_cnt\n" if ($verbose);
	exit (2);
}
else
{
	# the order of lines is different --> FAIL
	print "FAIL: something wrong at line $line_cnt\n" if ($verbose);
	print "Hint: I expected " . $r . "\n" if ($verbose);
	exit (1);
}
