summary: The test rebuilds gcc and checks the results.
description: |+
    This is an advanced wrapper for GCC testsuite. Apart from rebuilding
    gcc.src.rpm it can somehow automatically verify the results.

    There are two basic regression testing mechanisms:

      (1) Matching tests from the testsuite to known RHEL bugs, thus the
      logs show nicely per bug whether its test passed/failed/was not found.
      The matching rules are specified in bug_matching_table.txt and can be
      dependent on arch and release (RHEL6, RHEL7, Devtoolset). If a test
      that is expected to PASS somewhere fails or is missing, the wrapper
      asserts it. Thus it can be used as a bunch of regression tests for
      various RHEL/RHDTS bugs.

      (2) Comparison of the logs against some "goldens" if they are available
      (if not, this part is skipped). The "goldens" are stored in /mnt/qa
      space in the form of the *.sum files compressed by xz. They are there
      per architecture and release (RHEL6, RHEL7, Devtoolset). In case some
      problematic discrepacy is detected, the wrapper asserts that as well.
      Thus, in case there are matching "goldens", this wrapper is a pretty
      strong tool for GCC regression testing.

    In future, this wrapper should be used for all gcc-like friends, such
    as gcc-libraries and various compat-* things. Also, an experimental
    intelligent failure parser should be added in order to handle the fails
    in the suite and sort them out with a detection of the "more critical
    than others" failures.

enabled: true
link:
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=1468546
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=2095749
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=2095751
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=1996047
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=2109456
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=1552021
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=1416214
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=2080178
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=1463706
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=653729
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=2133794
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=1437220
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=1537828
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=1986854
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=1386922
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=2050089
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=2050090
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=1589680
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=2049712
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=2053418
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=1421107
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=1420723
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=1996085
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=2077493
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=1985207
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=1356346
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=1441338
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=1694013
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=1289022
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=1985463
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=1282755
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=1555397
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=1469384
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=1437641
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=1396298
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=1349067
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=2049611
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=1418446
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=2093997
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=1427412
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=1389276
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=1375711
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=1369183
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=1565536
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=2101887
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=1423460
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=1546728
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=1389801
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=2134379
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=1670129
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=1996017
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=1561204
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=2107126
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=1229690
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=1391102
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=2050559
tag:
  - 009203f6a293633355c6992185b6571e
  - rhel8-buildroot
  - simple
  - upstream_testsuite
contact: Vaclav Kadlcik <vkadlcik@redhat.com>
component:
  - gcc
test: ./runtest.sh
framework: beakerlib
require:
  - gcc
  - rpm-build
recommend:
  - yum-utils
  - xz
  - tar
  - python-sphinx
  - docbook5-style-xsl
  - texlive-collection-latex
  - dblatex
  - graphviz
  - texinfo-tex
  - sharutils
  - texinfo
  - texlive-collection-latex
  - gcc-gnat
  - libgnat
  - libgnat-devel
  - libart_lgpl-devel
  - alsa-lib-devel
  - bison
  - dejagnu
  - doxygen
  - elfutils-devel
  - elfutils-libelf-devel
  - flex
  - gdb
  - gettext
  - glibc-static
  - gmp-devel
  - isl-devel
  - libmpc-devel
  - mpfr-devel
  - python3-devel
  - python3-sphinx
  - zlib-devel
duration: 24h
extra-nitrate: TC#0547730
extra-summary: /tools/gcc/Sanity/upstream-suite2
extra-task: /tools/gcc/Sanity/upstream-suite2
