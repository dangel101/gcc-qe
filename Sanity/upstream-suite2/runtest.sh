#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Sanity/upstream-suite2
#   Description: The test rebuilds gcc and checks the results.
#   Author: Michael Petlan <mpetlan@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2017 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="gcc"

# THE DESIRED PACKAGE TO BE REBUILT MUST BE SPECIFIED HERE
BUILD_PKG=${BUILD_PKG:-`rpmquery --queryformat '%{NAME}' -f $(which gcc)`}
#' (this line is here to fix syntax highlighting)

# WHERE THE GOLDEN LOGS LIE
GOLDEN_PATH="/mnt/qa/scratch/michael/gcc_gold"

# PRESERVE LOGS
PRESERVE_LOGS=${PRESERVE_LOGS:-yes}


remove_toolset_from_path () {
    echo "$1" | tr : \\012 | grep -v -e devtoolset- -e gcc-toolset- | tr \\012 : | sed -e 's/:*$//'
}


rlJournalStart
    rlPhaseStartSetup
        export ARCH=`arch`
        if [[ $BUILD_PKG =~ devtoolset.* ]]; then
            export VERS="D"
        else
            export VERS=`rlGetDistroRelease`
        fi

        test -d /mnt/qa/scratch || rlMountQa

        # Collection gcc is not built using a collection environment
        rlLogInfo 'Important variables before removing DTS/GTS environment'
        rlLogInfo "PATH=$PATH"
        rlLogInfo "LD_LIBRARY_PATH=$LD_LIBRARY_PATH"
        rlLogInfo "MANPATH=$MANPATH"
        rlLogInfo "INFOPATH=$INFOPATH"
        rlLogInfo "PKG_CONFIG_PATH=$PKG_CONFIG_PATH"
        rlLogInfo "PCP_DIR=$PCP_DIR"
        PATH=$(remove_toolset_from_path "$PATH")
        LD_LIBRARY_PATH=$(remove_toolset_from_path "$LD_LIBRARY_PATH")
        MANPATH=$(remove_toolset_from_path "$MANPATH")
        INFOPATH=$(remove_toolset_from_path "$INFOPATH")
        PKG_CONFIG_PATH=$(remove_toolset_from_path "$PKG_CONFIG_PATH")
        PCP_DIR=$(remove_toolset_from_path "$PCP_DIR")
        rlLogInfo 'Important variables after removing DTS/GTS environment'
        rlLogInfo "PATH=$PATH"
        rlLogInfo "LD_LIBRARY_PATH=$LD_LIBRARY_PATH"
        rlLogInfo "MANPATH=$MANPATH"
        rlLogInfo "INFOPATH=$INFOPATH"
        rlLogInfo "PKG_CONFIG_PATH=$PKG_CONFIG_PATH"
        rlLogInfo "PCP_DIR=$PCP_DIR"

        rlRun "TmpDir=\$(TMPDIR=$HOME mktemp -d)"
        rlRun "cp compare_sums.pl bug_matching_table.txt unskip_tests.awk $TmpDir"
        rlRun "pushd $TmpDir"

        # get the sources
        rlFetchSrcForInstalled "$BUILD_PKG"
        rlRun "SRPM=`ls *.src.rpm`"

        # dependencies -- first the known ones, since gcc-build-deps are wrongly specified
        rlRun "yum -y install --skip-broken python-sphinx docbook5-style-xsl"
        rlRun "yum -y install --skip-broken texlive-collection-latex dblatex graphviz  texinfo-tex sharutils"
        rlRun "yum -y install --skip-broken texinfo gcc-gnat libgnat libgnat-devel libmpc-devel mpfr-devel"
        rlRun "yum -y install --skip-broken gmp-devel glibc-static zlib-devel dejagnu elfutils-libelf-devel"
        rlRun "yum -y install --skip-broken elfutils-devel rpm-build doxygen isl-devel python3-sphinx"

        # these packages were missing on s390x rhel-6 last time
        # (we don't assert here, this is just best-effort)
        yum -y install libart_lgpl-devel alsa-lib-devel cloog-ppl-devel ppl-devel

        # finally, try the builddep too
        rlRun "yum-builddep -y -t $SRPM || yum-builddep -y $SRPM" 0,1
    rlPhaseEnd

    rlPhaseStartTest
        # Some spec files skip %check on some architectures. Example:
        #   http://pkgs.devel.redhat.com/cgit/rpms/gcc/commit/?h=devtoolset-11.0-rhel-7&id=a1f3d32787d86975e69e52272d12cd0beae5c2bf
        # We need to revert that "skip" if present.
        rlRun "rpm --define '_topdir ${PWD}/my_rpmbuild' -Uvh $SRPM"
        rlRun "spec_dir=$(rpm --define '_topdir ${PWD}/my_rpmbuild' --eval=%_specdir)"
        rlRun "cp $spec_dir/gcc.spec $spec_dir/gcc.spec.orig"
        rlRun "awk -f unskip_tests.awk <$spec_dir/gcc.spec.orig >$spec_dir/gcc.spec"

        # rebuild gcc
        rlRun "rpmbuild --define '_topdir ${PWD}/my_rpmbuild' -bb $spec_dir/gcc.spec &>build.log"

        # print a little snippet of the build.log in case the build failed
        test $? -ne 0 && cat build.log | grep -v 'mockbuild does not exist' | tail -n 40

        # gather logs
        rlRun "mkdir testsuite_logs"
        for i in `find ${PWD}/my_rpmbuild/BUILD -name \*.sum`; do
            rlRun "cp $i testsuite_logs/"
            rlRun "cp ${i/.sum/.log} testsuite_logs/"
        done
    rlPhaseEnd

    rlPhaseStartTest "Regression test"
        # TODO Look into it but probably obsolete.
        grep -P -v '^#' bug_matching_table.txt | while read -r line; do
            SPLIT=($line)
            BUG=${SPLIT[0]}
            CHECK_ENV=${SPLIT[1]}
            CHECK_ARCHES=${SPLIT[2]}
            TC_STR=${SPLIT[3]}
            echo "$CHECK_ARCHES " | grep -q -P "all|$ARCH[,\s]"
            if [ $? -eq 0 ] && [[ $CHECK_ENV =~ .*$VERS.* ]]; then
                grep -P "$TC_STR" testsuite_logs/*.sum | tee tmp
                GREP_RESULT=${PIPESTATUS[0]}
                FAIL_NO=`awk -F':' '{print $2}' tmp | grep -P '^FAIL$' | wc -l`
                PASS_NO=`awk -F':' '{print $2}' tmp | grep -P '^PASS$' | wc -l`
                SKIP_NO=`awk -F':' '{print $2}' tmp | grep -P '^UNSUPPORTED$' | wc -l`
                XFAIL_NO=`awk -F':' '{print $2}' tmp | grep -P '^XFAIL$' | wc -l`
                XPASS_NO=`awk -F':' '{print $2}' tmp | grep -P '^XPASS$' | wc -l`
                if [ $PASS_NO -gt 0 ]; then
                    if [ $FAIL_NO -eq 0 ]; then
                        rlPass "Test for $BUG OK -- [ PASS=$PASS_NO  FAIL=$FAIL_NO ]"
                    else
                        rlLogWarning "Test for $BUG PASSed and FAILed -- [ PASS=$PASS_NO  FAIL=$FAIL_NO ]"
                    fi
                else
                    if [ $FAIL_NO -ne 0 ]; then
                        rlFail "Test for $BUG FAILed -- [ PASS=$PASS_NO  FAIL=$FAIL_NO ]"
                    else
                        rlLogWarning "Test for $BUG not found -- [ GREP_RESULT=$GREP_RESULT  SKIP_NO=$SKIP_NO ]"
                    fi
                fi
            fi
        done
    rlPhaseEnd

    export MY_GOLDEN_PATH="$GOLDEN_PATH/$VERS/$ARCH"
    rlPhaseStartTest "Comparison OLD : NEW"
        # TODO Look into it but probably obsolete.
        if [ -d $MY_GOLDEN_PATH ]; then
            for sumfile in testsuite_logs/*.sum; do
                sumf=`basename $sumfile`
                if [ -f $MY_GOLDEN_PATH/$sumf.xz ]; then
                    rlRun "./compare_sums.pl <(xzcat $MY_GOLDEN_PATH/$sumf.xz) $sumfile | tee temp" 0 "Comparing $MY_GOLDEN_PATH/$sumf $sumfile"
                    grep -q -e 'REGRESSION' temp && rlFail "REGRESSION(s) FOUND."
                    grep -q -e '-- FAIL --' temp && rlLogWarning "Some newly added tests failed."
                    grep -q -e '-- WARN --' temp && rlLog "There were some removed testcases."
                else
                    rlLog "Skipping $sumf (we do not have matching golden brick..."
                fi
            done
        else
            rlLog "THERE ARE NOT SUITABLE GOLDEN LOGS. SKIPPING THIS."
            rlLog "( I was searching in $MY_GOLDEN_PATH ...)"
        fi
    rlPhaseEnd

    # TODO compare (GCC_suite_compare_all --fails) against previous relevant build

    rlPhaseStartCleanup
        rlRun "( cd testsuite_logs ; chmod -x *.log ; tar c * | xz > ../logs__$ARCH.tar.xz )"
        rlRun "xz < build.log > build__$ARCH.log.xz"
        rlFileSubmit "logs__$ARCH.tar.xz"
        rlFileSubmit "build__$ARCH.log.xz"
        rlRun popd
        [[ "$PRESERVE_LOGS" = 'yes' ]] || rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
