#!/usr/bin/awk
/^%check/ {
	print;
	getline;
	print;
	getline;
	print;
	getline;
	if ($0 ~ "^#")
		getline;
	l1=$0;
	getline;
	l2=$0;
	getline;
	l3=$0;
	if ((l1 ~ "^%ifarch") && (l2 == "exit 0") && (l3 = "%endif")) {
	} else {
		print l1;
		print l2;
		print l3;
	}
	next;
}
{
	print;
}
