# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Makefile of /tools/gcc/Sanity/test-m32-m64-options
#   Description: Try -m32 and -m64 options.
#   Author: Marek Polacek <polacek@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2012 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

export TEST=/tools/gcc/Sanity/test-m32-m64-options
export TESTVERSION=1.0

BUILT_FILES=

FILES=$(METADATA) runtest.sh Makefile PURPOSE clear_cache.c hello.c hello.cpp  hello.f90 omphello.c quad.c thr-init-2.c tm.c cpp11.cpp lambda-template.C

.PHONY: all install download clean

run: $(FILES) build
	./runtest.sh

build: $(BUILT_FILES)
	test -x runtest.sh || chmod a+x runtest.sh

clean:
	rm -f *~ $(BUILT_FILES)


include /usr/share/rhts/lib/rhts-make.include

$(METADATA): Makefile
	@echo "Owner:           Marek Polacek <mpolacek@redhat.com>" > $(METADATA)
	@echo "Name:            $(TEST)" >> $(METADATA)
	@echo "TestVersion:     $(TESTVERSION)" >> $(METADATA)
	@echo "Path:            $(TEST_DIR)" >> $(METADATA)
	@echo "Description:     Try -m32 and -m64 options." >> $(METADATA)
	@echo "Type:            Sanity" >> $(METADATA)
	@echo "TestTime:        20m" >> $(METADATA)
	@echo "RunFor:          gcc" >> $(METADATA)
	@echo "Requires:        gcc gcc-c++ gcc-gfortran glibc-devel libgfortran libstdc++-devel libitm libgomp" >> $(METADATA)
	@echo "Requires:        glibc-devel.x86_64 glibc-devel.i686 glibc-devel.ppc64 glibc-devel.ppc glibc-devel.s390x glibc-devel.s390" >> $(METADATA)
	@echo "Requires:        libgfortran.x86_64 libgfortran.i686 libgfortran.ppc64 libgfortran.ppc libgfortran.s390x libgfortran.s390" >> $(METADATA)
	@echo "Requires:        libgfortran5.x86_64 libgfortran5.i686 libgfortran5.ppc64 libgfortran5.ppc libgfortran5.s390x libgfortran5.s390" >> $(METADATA)
	@echo "Requires:        libstdc++-devel.x86_64 libstdc++-devel.i686 libstdc++-devel.ppc64 libstdc++-devel.ppc libstdc++-devel.s390x libstdc++-devel.s390" >> $(METADATA)
	@echo "Requires:        libitm.x86_64 libitm.i686 libitm.ppc64 libitm.ppc libitm.s390x libitm.s390" >> $(METADATA)
	@echo "Requires:        libitm-devel.x86_64 libitm-devel.i686 libitm-devel.ppc64 libitm-devel.ppc libitm-devel.s390x libitm-devel.s390" >> $(METADATA)
	@echo "Requires:        libgomp.x86_64 libgomp.i686 libgomp.ppc64 libgomp.ppc libgomp.s390x libgomp.s390" >> $(METADATA)
	@echo "Requires:        libquadmath-devel.x86_64 libquadmath-devel.i686 libquadmath-devel.ppc64 libquadmath-devel.ppc libquadmath-devel.s390x libquadmath-devel.s390" >> $(METADATA)
	@echo "Priority:        Normal" >> $(METADATA)
	@echo "License:         GPLv2" >> $(METADATA)
	@echo "Confidential:    no" >> $(METADATA)
	@echo "Destructive:     no" >> $(METADATA)

	rhts-lint $(METADATA)
