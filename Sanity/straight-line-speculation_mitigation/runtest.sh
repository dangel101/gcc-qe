#!/usr/bin/env bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Sanity/straight-line-speculation_mitigation
#   Description: Straight Line Speculation (SLS) mitigation
#   Author: Vaclav Kadlcik <vkadlcik@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2022 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

# SLS testing is part of the upstream testsuite. However the suite never runs
# cleanly and although we do take a glance at the results, the process is prone
# to mistakes and this feature is too important at the moment not to make
# an explicit downstream test, at least at the basic sanity level.
#
# The test *.c files are taken from the upstream test suite.
#
# Notes on relevancy:
#   * x86_64 - only
#   * should work in any GCC 12+ (via upstream)
#   * should work in system GCC of RHEL 9.1+ (via upstream)
#   * should work in system GCC of RHEL 8.7+ (RHBZ#2108721)
# Suggested TCMS relevancy:
#   arch != x86_64: False
#   collection contains gcc-toolset-9, gcc-toolset-10, gcc-toolset-11, devtoolset-9, devtoolset-10, devtoolset-11: False
#   collection !defined && distro < rhel-8: False
#   collection !defined && distro = rhel-8 && distro < rhel-8.7: False
#   collection !defined && distro = rhel-9 && distro < rhel-9.1: False

GCC="${GCC:-$(type -P gcc)}"

GCC_RPM_NAME=$(rpm --qf '%{name}' -qf $GCC)
BINUTILS_RPM_NAME=${GCC_RPM_NAME%gcc}binutils

PACKAGES="$GCC_RPM_NAME $BINUTILS_RPM_NAME"

rlJournalStart
    rlPhaseStartSetup
        rlLogInfo "PACKAGES=$PACKAGES"
        rlLogInfo "COLLECTIONS=$COLLECTIONS"
        rlLogInfo "GCC=$GCC"
        rlLogInfo "SKIP_COLLECTION_METAPACKAGE_CHECK=$SKIP_COLLECTION_METAPACKAGE_CHECK"

        # We optionally need to skip checking for the presence of the metapackage
        # because that would pull in all the dependent toolset subrpms.  We do not
        # always want that, especially in CI.
        _COLLECTIONS="$COLLECTIONS"
        if ! test -z $SKIP_COLLECTION_METAPACKAGE_CHECK; then
            for c in $SKIP_COLLECTION_METAPACKAGE_CHECK; do
                rlLogInfo "ignoring metapackage check for collection $c"
                export COLLECTIONS=$(shopt -s extglob && echo ${COLLECTIONS//$c/})
            done
        fi
        rlLogInfo "(without skipped) COLLECTIONS=$COLLECTIONS"
        rlAssertRpm --all
        export COLLECTIONS="$_COLLECTIONS"

        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "cp files/* $TmpDir"
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    if [[ "$GCC_RPM_NAME" = devtoolset-12-gcc ]]; then
        # Faulty man page that slipped. Since it's just a man page, DTS-12/RHEL-6 is not
        # an officially supported build and DTS-12 is the last DTS we'll just ignore it.
        :
    else
        rlPhaseStartTest man
            rlRun 'man gcc >man.out'
            rlAssertGrep 'mindirect-branch-cs-prefix' man.out
            rlAssertGrep 'mharden-sls' man.out
            rlAssertGrep 'indirect-jmp' man.out
        rlPhaseEnd
    fi

    rlPhaseStartTest mindirect-branch-cs-prefix
        rlRun 'gcc -c -O2 -ffixed-rax -ffixed-rbx -ffixed-rcx -ffixed-rdx -ffixed-rdi -ffixed-rsi -mindirect-branch-cs-prefix -mindirect-branch=thunk-extern -fno-pic indirect-thunk-cs-prefix-1.c'
        rlRun 'objdump -d indirect-thunk-cs-prefix-1.o >indirect-thunk-cs-prefix-1.dasm'
        rlAssertGrep 'cs jmp' indirect-thunk-cs-prefix-1.dasm
    rlPhaseEnd

    rlPhaseStartTest int3
        rlRun 'gcc -c -O2 -fcf-protection -mharden-sls=indirect-jmp harden-sls-6.c'
        rlRun 'objdump -d harden-sls-6.o >harden-sls-6.dasm'
        rlRun 'tail -n 2 harden-sls-6.dasm | head -n 1 >harden-sls-6-last-but-one.dasm'
        rlRun 'tail -n 1 harden-sls-6.dasm             >harden-sls-6-last.dasm'
        rlAssertGrep '[[:space:]]jmp' harden-sls-6-last-but-one.dasm
        rlAssertGrep '[[:space:]]int3' harden-sls-6-last.dasm
        rlGetPhaseState || rlFileSubmit harden-sls-6.dasm harden-sls-6.dasm
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun popd
        rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
