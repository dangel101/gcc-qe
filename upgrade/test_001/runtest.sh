#!/usr/bin/env bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
. /usr/share/beakerlib/beakerlib.sh || exit 1

distribution_mcase__setup() {
    rlRun 'echo "int main(void) { return 0; }" >main.c'
    rlRun 'gcc main.c'
}

distribution_mcase__test() {
    rlRun a.out # after upgrade this checks compiled-before-upgrade binaries still work
    rlRun 'rm a.out'
    rlRun 'gcc main.c'
    rlRun a.out
}

distribution_mcase__cleanup() {
    rlRun 'rm -f main.c a.out'
}

rlJournalStart
    rlPhaseStartSetup
        rlRun "rlImport ControlFlow/mcase"
    rlPhaseEnd
    distribution_mcase__run
rlJournalEnd
