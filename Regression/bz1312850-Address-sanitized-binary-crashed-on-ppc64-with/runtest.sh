#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz1312850-Address-sanitized-binary-crashed-on-ppc64-with
#   Description: Test for BZ#1312850 (Address sanitized binary crashed on ppc64 with)
#   Author: Michael Petlan <mpetlan@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2016 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

# Relevant for GCC 4.8+. In practice, any currently supported GCC except the
# system one of RHEL-6. Further we have to limit the relevancy to only GCCs
# shipping libasan. Suggested TCMS relevancy:
#   distro < rhel-7 && collection !defined: False
#   distro < rhel-8 && collection !defined && arch != x86_64, ppc64: False

GCC=${GCC:-$(type -P gcc)}
GCC_RPM_NAME=$(rpm --qf '%{name}' -qf $GCC)

if [[ "$GCC_RPM_NAME" == *toolset* ]]; then
    TOOLSET=${GCC_RPM_NAME%-gcc}
else
    TOOLSET=''
fi

PACKAGES="$GCC_RPM_NAME"
if [[ -z "$TOOLSET" ]]; then
    PACKAGES+=" libasan"
else
    PACKAGES+=" ${TOOLSET}-libasan-devel" # libasanX of the toolset will come as a dep
fi

rlJournalStart
    rlPhaseStartSetup
        rpm -q $PACKAGES || rlRun "yum install -y --skip-broken $PACKAGES" 0-255

        # Workaround
        # ----------
        # On RHEL 7 and older with RHSCL repos enabled one can end up with
        # a RHSCL's libasan.rpm newer-than and incompatible-with system GCC.
        # Let's try to detect and fix the problem.
        if [[ -z "$TOOLSET" ]]; then
            PRI_ARCH=$(rlGetPrimaryArch)
            SEC_ARCH=$(rlGetSecondaryArch)
            VR=$(rpm -q --qf='%{VERSION}-%{RELEASE}' -f $GCC)
            if [[ $(rpm -q --qf='%{VERSION}-%{RELEASE}' libasan.$PRI_ARCH) != "$VR" ]]; then
                if tmp=$(mktemp -d) && pushd "$tmp"; then
                    rpm -q libasan.$PRI_ARCH        && rlRun "yumdownloader libasan-$VR.$PRI_ARCH"
                    rpm -q libasan-static.$PRI_ARCH && rlRun "yumdownloader libasan-static-$VR.$PRI_ARCH"
                    if [[ -n "$SEC_ARCH" ]]; then
                        rpm -q libasan.$SEC_ARCH        && rlRun "yumdownloader libasan-$VR.$SEC_ARCH"
                        rpm -q libasan-static.$SEC_ARCH && rlRun "yumdownloader libasan-static-$VR.$SEC_ARCH"
                    fi
                    rlRun 'rpm -Uvh --oldpackage *.rpm'
                    popd
                    rm -rf "$tmp"
                fi
            fi
        fi

        rlLogInfo "PACKAGES=$PACKAGES"
        rlLogInfo "COLLECTIONS=$COLLECTIONS"
        rlLogInfo "GCC=$GCC"
        rlLogInfo "SKIP_COLLECTION_METAPACKAGE_CHECK=$SKIP_COLLECTION_METAPACKAGE_CHECK"

        # We optionally need to skip checking for the presence of the metapackage
        # because that would pull in all the dependent toolset subrpms.  We do not
        # always want that, especially in CI.
        _COLLECTIONS="$COLLECTIONS"
        if ! test -z $SKIP_COLLECTION_METAPACKAGE_CHECK; then
            for c in $SKIP_COLLECTION_METAPACKAGE_CHECK; do
                rlLogInfo "ignoring metapackage check for collection $c"
                export COLLECTIONS=$(shopt -s extglob && echo ${COLLECTIONS//$c/})
            done
        fi
        rlLogInfo "(without skipped) COLLECTIONS=$COLLECTIONS"
        rlAssertRpm --all
        export COLLECTIONS="$_COLLECTIONS"

        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "pushd $TmpDir"
        rlRun 'echo "int main(int argc, char **argv) {  return 0; }"  >t.c'
        rlRun 'gcc -o t -fsanitize=address t.c'
    rlPhaseEnd

    rlPhaseStartTest
        rlRun 'ASAN_OPTIONS=verbosity=1 ./t'
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun popd
        rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
