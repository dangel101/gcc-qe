#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz694141-too-long-warning-for-inline-assembler-string
#   Description: Test for bz694141 (too-long warning for inline assembler string)
#   Author: Marek Polacek <mpolacek@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2011 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh
. /usr/lib/beakerlib/beakerlib.sh

PACKAGES=(gcc gcc-c++ systemtap systemtap-sdt-devel kernel-debuginfo kernel-devel kernel-headers)


rlJournalStart
	rlPhaseStartSetup
		# Ok, process dependencies.  We need quite a lot of thingz, especially
		# the kernel-debuginfo.
		# First, install some packages which are not so common.  We do this only
		# to prevent harmless FAILures.
		rlRun "yum install -y gcc-c++"
		rlRun "yum install -y systemtap"
		rlRun "yum install -y systemtap-testsuite" 0-255
		rlRun "yum install -y kernel-debuginfo kernel-devel kernel-headers"
		for p in "${PACKAGES[@]}"; do
			rlAssertRpm "$p" || yum -y install "$p"
		done; unset p
		rlRun "rpmquery kernel-headers-`uname -r`" 0 "kernel-headers match the running kernel"
		rlRun "rpmquery kernel-devel-`uname -r`" 0 "kernel-devel matches the running kernel"

		# check whether the kernel-debuginfo matches the running kernel
		rpm --qf " %{VERSION}-%{RELEASE}.%{ARCH} " -q kernel-debuginfo | grep " `uname -r` "
		if [ $? -eq 0 ]; then
			rlPass "OK, there is kernel-debuginfo that matches the running kernel."
		else
			rlDie "Unfortunately, kernel-debuginfo does not match the running kernel (`uname -r`)."
		fi

		rlRun "TmpDir=\$(mktemp -d)"
		rlRun "pushd $TmpDir"

		if ! rpm -q systemtap-testsuite &>/dev/null; then
			# RHEL 9+ doesn't ship systemtap-testsuite at all, let's get it outside of the repos
			rlRun "STAP_NVRA=$(rpm -q --qf='%{NVRA}' systemtap)"
			STAP_NVRA=${STAP_NVRA/systemtap-/systemtap-testsuite-}
			rlRpmDownload "$STAP_NVRA"
			rlRun "yum -y install $STAP_NVRA.rpm"
		fi
	rlPhaseEnd

	rlPhaseStartTest
		# We have systemtap-testsuite installed, thus we may cd into...
		rlRun "cd /usr/share/systemtap/testsuite/"

		# Now run the sdt.exp.  We don't use the rlRun since we don't care about
		# the result, only about output.
		rlLog "Running the testsuite"
		make RUNTESTFLAGS=systemtap.base/sdt.exp installcheck 

		# So, we should have the log file.
		rlAssertExists systemtap.log

		# It must not contain the compile failure.
		rlAssertNotGrep "FAIL: compiling sdt.c.*-pedantic uprobe" systemtap.log
	rlPhaseEnd

	rlPhaseStartCleanup
		rlRun "popd"
		rlRun "rm -r $TmpDir"
	rlPhaseEnd
rlJournalPrintText
rlJournalEnd
