program repro
  type small_stride
     character*40 long_string
     integer      small_pad
  end type small_stride
  type(small_stride), dimension (20), target :: unpleasant
  character*40, pointer, dimension(:):: c40pt
  integer i
  do i = 0,19
     unpleasant(i+1)%small_pad = i+1
     unpleasant(i+1)%long_string = char (ichar('0') + i) // '-hello'
  end do
  c40pt => unpleasant%long_string
  print *, c40pt  ! break-here
end program repro
