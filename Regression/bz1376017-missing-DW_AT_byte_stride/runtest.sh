#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz1376017-missing-DW_AT_byte_stride
#   Description: Test for BZ#1376017 (DWARF regression fortran Missing DW_AT_byte_stride)
#   Author: Michael Petlan <mpetlan@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2018 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

# Relevant for GCC 7+. In practice, it's system GCCs of RHEL 7+ and any
# currently supported toolset GCC. However there's an exception for
# system GCC of RHEL 8.0-8.2 where the test fails due to RHBZ#1655624.
# Suggested TCMS relevancy:
#   distro < rhel-7 && collection !defined: False
#   distro = rhel-8 && distro < rhel-8.3 && collection !defined: False # RHBZ#1655624

GCC=${GCC:-$(type -P gcc)}
GCC_RPM_NAME=$(rpm --qf '%{name}' -qf $GCC)
PACKAGES="$GCC_RPM_NAME ${GCC_RPM_NAME}-gfortran ${GCC_RPM_NAME%gcc}gdb ${GCC_RPM_NAME%gcc}binutils"

rlJournalStart
    rlPhaseStartSetup
        rpm -q $PACKAGES || rlRun "yum -y install --skip-broken $PACKAGES" 0-255 # best effort

        rlLogInfo "PACKAGES=$PACKAGES"
        rlLogInfo "COLLECTIONS=$COLLECTIONS"
        rlLogInfo "GCC=$GCC"
        rlLogInfo "SKIP_COLLECTION_METAPACKAGE_CHECK=$SKIP_COLLECTION_METAPACKAGE_CHECK"

        # We optionally need to skip checking for the presence of the metapackage
        # because that would pull in all the dependent toolset subrpms.  We do not
        # always want that, especially in CI.
        _COLLECTIONS="$COLLECTIONS"
        if ! test -z $SKIP_COLLECTION_METAPACKAGE_CHECK; then
            for c in $SKIP_COLLECTION_METAPACKAGE_CHECK; do
                rlLogInfo "ignoring metapackage check for collection $c"
                export COLLECTIONS=$(shopt -s extglob && echo ${COLLECTIONS//$c/})
            done
        fi
        rlLogInfo "(without skipped) COLLECTIONS=$COLLECTIONS"
        rlAssertRpm --all
        export COLLECTIONS="$_COLLECTIONS"

        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "cp dwarf-stridex.f90 $TmpDir"
        rlRun "pushd $TmpDir"
        rlRun 'gfortran -o dwarf-stridex dwarf-stridex.f90 -Wall -g'
        rlAssertExists dwarf-stridex
    rlPhaseEnd

    rlPhaseStartTest "Static test"
        rlRun 'readelf -wi dwarf-stridex >dwarfdump.log'
        rlAssertGrep '\s*<[\da-fA-F]+>\s*DW_AT_byte_stride' dwarfdump.log -P
    rlPhaseEnd

    rlPhaseStartTest "Dynamic test"
        rlRun 'gdb ./dwarf-stridex -batch -ex "b 14" -ex r -ex "p c40pt(2)" >out 2>err'
        # The following output is WRONG: $1 = '\001\000\000\000\061-hello', ' ' <repeats 29 times>
        # The regexp needs to be more generic, since on big endian, the order of the octal values differs
        rlAssertNotGrep '(\\0\d\d)+-hello' out -P
        rlAssertGrep "\\'1-hello" out -P
        rlAssertNotGrep 'Unhandled dwarf expression opcode 0xfd' err
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun popd
        rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
