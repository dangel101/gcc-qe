extern void abort (void);
long double v[144];

__attribute__((noinline, noclone)) void
bar (long double *x)
{
  if (x != &v[29])
    abort ();
}

__attribute__((noinline, noclone)) void
foo (long double *x, long y, long z)
{
  long double a, b, c;
  a = x[z * 4 + y * 3];
  b = x[z * 5 + y * 3];
  c = x[z * 5 + y * 4];
  x[y * 4] = a;
  bar (&x[z * 5 + y]);
  x[z * 5 + y * 5] = b + c;
}

int
main ()
{
  foo (v, 24, 1);
  return 0;
}
