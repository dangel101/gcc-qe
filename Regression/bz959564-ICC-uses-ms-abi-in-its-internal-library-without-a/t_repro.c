#include  <stdio.h>
#include  <execinfo.h>

void
do_backtrace (void)
{
  void  *pcs [ 64 ];
  int    max_pcs = sizeof ( pcs ) / sizeof ( pcs [ 0 ] );
  int    num_pcs = backtrace ( pcs, max_pcs );
  int    i;

  for ( i = 0; i < num_pcs; i ++ )
    printf ( " %2d: %p\n", i, pcs [ i ] );
} /* do_backtrace */

void
f ( int arg )
{
  if ( arg > 0 )
    {
      f ( arg - 1 );
    }
  else
    {
      do_backtrace ();
    }
} /* f */

int
__attribute__((ms_abi))
f_ms_abi ( int arg )
{
  if ( arg > 0 )
    {
      f_ms_abi ( arg - 1 );
    }
  else
    {
      do_backtrace ();
    }
} /* f_ms_abi */

int
main ( int argc, char *argv [] )
{

  printf ( "calling f\n" );
  f ( 6 );

  printf ( "calling f_ms_abi\n" );
  f_ms_abi ( 6 );

  return 0;
} /* main */
