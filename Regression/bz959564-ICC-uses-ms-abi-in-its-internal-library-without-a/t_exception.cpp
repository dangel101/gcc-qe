#include  <stdio.h>

struct E {
};


void
f ( int arg )
{
  if ( arg > 0 )
    {
      f ( arg - 1 );
    }
  else
    {
      throw E ();
    }
} /* f */

int
__attribute__((ms_abi))
f_ms_abi ( int arg )
{
  if ( arg > 0 )
    {
      f_ms_abi ( arg - 1 );
    }
  else
    {
      throw E ();
    }
} /* f_ms_abi */

int
main ( int argc, char *argv [] )
{

  printf ( "calling f\n" );
  try
    {
      f ( 6 );
    }
  catch ( ... )
    {
      printf ( "caught exception thrown by f\n" );
    }

  printf ( "calling f_ms_abi\n" );
  try
    {
      f_ms_abi ( 6 );
    }
  catch ( ... )
    {
      printf ( "caught exception thrown by f_ms_abi\n" );
    }

  return 0;
} /* main */
