extern void *memcpy (void *, const void *, unsigned long);
extern void scsi_eh_restore_cmnd (unsigned char *scmd);
void
cypress_atacb_passthrough (unsigned char *srb)
{
 unsigned char save_cmnd[16];
 memcpy (save_cmnd, srb, sizeof (save_cmnd));
 srb[7] = save_cmnd[6];
 srb[9] = save_cmnd[10];
 srb[11] = save_cmnd[13];
 srb[12] = save_cmnd[14];
 if (save_cmnd[1] & 0x01)
   {
     if (save_cmnd[3]
         || save_cmnd[5] || save_cmnd[7] || save_cmnd[9] || save_cmnd[11])
       return;
   }
 if (save_cmnd[2] & 0x20)
   scsi_eh_restore_cmnd (srb);
}
