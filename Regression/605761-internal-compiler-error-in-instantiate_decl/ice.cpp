#include <string>
#include <iostream>
using namespace std;

template< const char *_name_ >
class A {
public:
  string str;
  
  ~A() { cout << "~A()\n"; };
  A() : str(_name_) { cout << "A(): " << str << endl; };
};

template< class _t_ >
class B {
private:
  static const char sstr[];

public:
  typedef A< B< _t_ >::sstr > B_t;

  _t_ data;

  ~B() { cout << "~B()\n"; };
  B() : data(0) { cout << "B()\n"; };
};

template< class _t_ >
const char B< _t_ >::sstr[] = "test";


int main() {
  B<int>::B_t tmp;
};
