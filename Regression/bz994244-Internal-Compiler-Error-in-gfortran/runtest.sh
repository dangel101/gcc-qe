#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz994244-Internal-Compiler-Error-in-gfortran
#   Description: Test for BZ#994244 (Internal Compiler Error in gfortran)
#   Author: Dagmar Prokopova <dprokopo@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2013 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGES=(gcc gcc-gfortran libgfortran)

# Choose the compilers.
GCC=${GCC:-gcc}
GXX=${GXX:-g++}
GFORTRAN=${GFORTRAN:-gfortran}

rlJournalStart
	rlPhaseStartSetup
		for p in "${PACKAGES[@]}"; do
			rlCheckRpm "$p" || yum -y install "$p"
			rlAssertRpm "$p"
		done; unset p
		rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
		# We need the tarball as a reproducer.
		cp -v bt-all.f $TmpDir
		rlRun "pushd $TmpDir"

		# this test should not be run with gcc < 4.8
		GCC_VERSION="`gfortran --version | head -n 1 | awk '{print $4}'`"
		MIN_GCC_VERSION="4.8"
		OLDER="`echo -e \"$GCC_VERSION\n$MIN_GCC_VERSION\" | sort | head -n 1`"
	rlPhaseEnd

	rlPhaseStartTest
		if [[ "$OLDER" == "$MIN_GCC_VERSION" ]]; then
			# Should not ICE.
			[ "$(rlGetPrimaryArch)" != "s390x" ] && rlRun "$GFORTRAN -c -save-temps -O2 -g -fopenmp -fpic -mcmodel=large bt-all.f" 0
			if [[ $(uname -i) == "x86_64" ]]; then
			  rlRun "$GFORTRAN -c -save-temps -O2 -g -fopenmp -fpic -mcmodel=large -mtls-dialect=gnu2 bt-all.f" 0
			fi
		else
			rlPass "TEST SKIPPED: Your gcc is too old: $GCC_VERSION (should be at least $MIN_GCC_VERSION)"
		fi
	rlPhaseEnd

	rlPhaseStartCleanup
		rlRun "popd"
		rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
	rlPhaseEnd
rlJournalPrintText
rlJournalEnd
