#include <iostream>

using namespace std;

class Sequence {
public:
  int i;
  Sequence() {i = 42;}
  virtual void foo(void) {}
};

Sequence getSequence()
{
  Sequence data;
  cout << endl;
  return data;
}

int main()
{
	Sequence foo = getSequence();

	cout << foo.i << endl;
}
