struct A { int a; char b[]; };
union B { struct A a; char b[sizeof (struct A) + 31]; };
union B b __attribute__((aligned (__alignof__(union B)), section ("mysection"))) = { { 1, "123456789012345678901234567890" } };
extern char __start_mysection[], __stop_mysection[];

int
main ()
{
  __SIZE_TYPE__ start = (__SIZE_TYPE__) __start_mysection;
  __SIZE_TYPE__ stop = (__SIZE_TYPE__) __stop_mysection;
  if (sizeof (b) != (stop - start))
    __builtin_abort ();
  return 0;
}
