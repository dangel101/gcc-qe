#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz2140266-gcc-Incorrect-always-inline-diagnostic-in-LTO
#   Description: Test for BZ#2140266 (gcc Incorrect always_inline diagnostic in LTO mode)
#   Author: Vaclav Kadlcik <vkadlcik@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2022 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

# Notes on relevancy
#
# Relevant for GCC 10+ and hardware-specific. However upstream GCC-10 - GCC-12
# received the fixes relatively late and not all our downstream builds have
# them included.
#
# Suggested TCMS relevancy:
#   arch != ppc64le: False
#   collection !defined && distro < rhel-9: False
#   collection !defined && distro = rhel-9 && distro < rhel-9.2: False
#   collection defined && collection contains gcc-toolset-11, devtoolset-11, gcc-toolset-10, devtoolset-10: False

# Notes on implementation:
#
# The reproducer in RHBZ#2140266 (full Open Babel compilation) is not very
# suitable because it requires extensive dependencies, is slow and needs
# manual tuning (the code is quite old with narrowing conversions etc).
# Instead, I've opted to borrow reduced reproducers from the upstream ticket
# and related test cases from the upstream test suite. Please not that
# the fix and its testcases were merged to GCC 11 in a limited way only
# and using -mno-htm may be required.

GCC=${GCC:-$(type -P gcc)}
GCC_RPM_NAME=$(rpm --qf '%{name}' -qf $GCC)
PACKAGES="$GCC_RPM_NAME ${GCC_RPM_NAME}-c++ xz"

rlJournalStart
    rlPhaseStartSetup
        rlLogInfo "PACKAGES=$PACKAGES"
        rlLogInfo "COLLECTIONS=$COLLECTIONS"
        rlLogInfo "GCC=$GCC"
        rlLogInfo "SKIP_COLLECTION_METAPACKAGE_CHECK=$SKIP_COLLECTION_METAPACKAGE_CHECK"

        # We optionally need to skip checking for the presence of the metapackage
        # because that would pull in all the dependent toolset subrpms.  We do not
        # always want that, especially in CI.
        _COLLECTIONS="$COLLECTIONS"
        if ! test -z $SKIP_COLLECTION_METAPACKAGE_CHECK; then
            for c in $SKIP_COLLECTION_METAPACKAGE_CHECK; do
                rlLogInfo "ignoring metapackage check for collection $c"
                export COLLECTIONS=$(shopt -s extglob && echo ${COLLECTIONS//$c/})
            done
        fi
        rlLogInfo "(without skipped) COLLECTIONS=$COLLECTIONS"
        rlAssertRpm --all
        export COLLECTIONS="$_COLLECTIONS"

        rlRun "GCC_MAJOR=$(gcc -dumpversion)"
        [[ "$GCC_MAJOR" = 11 ]] && IS_11=true || IS_11=false
        rlLogInfo "IS_11=$IS_11"

        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "cp files/* $TmpDir"
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    rlPhaseStartTest upstream_comment_0
        rlRun 'unxz upstream_c0.ii.xz'
        if $IS_11; then
            rlRun 'g++ -c -O2 -flto -mcpu=power8 -mno-htm upstream_c0.ii'
        else
            rlRun 'g++ -c -O2 -flto -mcpu=power8 upstream_c0.ii'
        fi
    rlPhaseEnd

    rlPhaseStartTest upstream_comment_4
        if $IS_11; then
            rlRun 'g++ -c -O2 -flto -mcpu=power8 -mno-htm upstream_c4.ii'
        else
            rlRun 'g++ -c -O2 -flto -mcpu=power8 upstream_c4.ii'
        fi
    rlPhaseEnd

    # The following upstream tests weren't merged to GCC 11
    if ! $IS_11; then
        rlPhaseStartTest pr102059-1.c
            rlRun 'g++ -c -O2 -mcpu=power8 -Wno-attributes pr102059-1.c'
        rlPhaseEnd

        rlPhaseStartTest pr102059-2.c
            rlRun 'g++ -c -O2 -mcpu=power8 -mno-htm pr102059-2.c'
        rlPhaseEnd

        rlPhaseStartTest pr102059-3.c
            rlRun 'rm -f *.einline'
            rlRun 'g++ -c -O2 -mcpu=power8 -mno-power8-fusion -fdump-tree-einline-optimized pr102059-3.c'
            rlAssertGrep 'Inlining int foo(int*)/0' pr102059-3.c.*.einline -qF
        rlPhaseEnd

        rlPhaseStartTest pr102059-1_N.c
            rlRun 'gcc -O2 -mcpu=power8 -flto -Wno-attributes -mno-power8-fusion pr102059-1_?.c'
        rlPhaseEnd

        rlPhaseStartTest pr102059-2_N.c
            rlRun 'rm -f *.inline'
            rlRun 'gcc -O2 -mcpu=power8 -flto -Wno-attributes -mno-power8-fusion -fdump-ipa-inline pr102059-2_?.c'
            rlAssertNotGrep 'target specific option mismatch' *.inline
        rlPhaseEnd
    fi

    # This upstream test was merged to GCC 11 but with a twist
    rlPhaseStartTest pr102059-4.c
        if $IS_11; then
            rlRun 'sed -i"" "s/cpu=power8/cpu=power8,no-htm/" pr102059-4.c'
        fi
        rlRun 'g++ -c -O2 -mcpu=power10 pr102059-4.c'
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun popd
        rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
