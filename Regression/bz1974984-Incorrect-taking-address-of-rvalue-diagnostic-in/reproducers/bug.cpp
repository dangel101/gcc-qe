template <typename TYPE>
struct Enclosure { const TYPE& a;};

struct Obj {
    int get_a();
};

void callme(Enclosure<int> a) {
    (void)a;
}

void bar1() {
    Obj obj;
    // No diagnostic
    callme(Enclosure<decltype(obj.get_a())>{{obj.get_a()}});
}

template <typename TYPE>
void bar2(TYPE t) {
    Obj obj;
    // ERROR!
    callme(Enclosure<decltype(obj.get_a())>{{obj.get_a()}});
}
