#include <string.h>
#include <stdio.h>

static inline void
write_swapped_value(unsigned int *outp, unsigned int inval)
{
    ((char *)outp)[0] = ((char *)&inval)[3];
    ((char *)outp)[1] = ((char *)&inval)[2];
    ((char *)outp)[2] = ((char *)&inval)[1];
    ((char *)outp)[3] = ((char *)&inval)[0];
}

void
convert(const unsigned int *inp, unsigned int *outp, int n)
{
    int i;
    for (i = 0; i < n; ++i)
	write_swapped_value(&outp[i], inp[i] ^ 0x01010101);
}

unsigned int inbuf[3] = { 0x01020304, 0x05060708, 0x090a0b0c };
unsigned int outbuf[3] = { 0, 0, 0 };
unsigned int expected[3] = { 0x05020300, 0x09060704, 0x0d0a0b08 };

int
main(int argc, char **argv)
{
    int i;
    int ret;
    convert(inbuf, outbuf, 3);
    for (i = 0; i < 3; ++i)
	printf("%.8x ", outbuf[i]);
    if (memcmp(outbuf, expected, sizeof(outbuf))) {
	printf("- FAIL\n");
	ret=1;
    }
    else {
	printf("- OK\n");
	ret=0;
    }
    return ret;
}
