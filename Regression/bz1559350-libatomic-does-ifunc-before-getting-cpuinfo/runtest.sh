#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz1559350-libatomic-does-ifunc-before-getting-cpuinfo
#   Description: Runs the reproducer
#   Author: Michael Petlan <mpetlan@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2019 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

GCC=${GCC:-$(type -P gcc)}
GCC_RPM_NAME=$(rpm --qf '%{name}' -qf $GCC)
PACKAGES="$GCC_RPM_NAME tar xz"

if rlIsRHEL 6 7; then
    # No libatomic for system GCC in RHEL-6. Further, even if devtoolset
    # is being tested, its libatomic-devel just refers the system version
    # and the RHEL-7 one is known to have the bug present and not planned
    # to be fixed.
    exit 1
fi

if [[ "$GCC_RPM_NAME" == *toolset* ]]; then
    PACKAGES+=" ${GCC_RPM_NAME%-gcc}-libatomic-devel"
else
    PACKAGES+=' libatomic'
fi

rlJournalStart
    rlPhaseStartSetup
        # Work around incomplete toolset installs; best effort only
        rpm -q $PACKAGES || rlRun "yum -y install --skip-broken $PACKAGES" 0-255

        rlLogInfo "PACKAGES=$PACKAGES"
        rlLogInfo "COLLECTIONS=$COLLECTIONS"
        rlLogInfo "GCC=$GCC"
        rlLogInfo "SKIP_COLLECTION_METAPACKAGE_CHECK=$SKIP_COLLECTION_METAPACKAGE_CHECK"

        # We optionally need to skip checking for the presence of the metapackage
        # because that would pull in all the dependent toolset subrpms.  We do not
        # always want that, especially in CI.
        _COLLECTIONS="$COLLECTIONS"
        if ! test -z $SKIP_COLLECTION_METAPACKAGE_CHECK; then
            for c in $SKIP_COLLECTION_METAPACKAGE_CHECK; do
                rlLogInfo "ignoring metapackage check for collection $c"
                export COLLECTIONS=$(shopt -s extglob && echo ${COLLECTIONS//$c/})
            done
        fi
        rlLogInfo "(without skipped) COLLECTIONS=$COLLECTIONS"
        rlAssertRpm --all
        export COLLECTIONS="$_COLLECTIONS"

        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "cp reproducer.tar.xz $TmpDir"
        rlRun "pushd $TmpDir"
        rlRun 'tar xJf reproducer.tar.xz'
        rlRun 'gcc -c -o main.o main.c'
        rlRun 'gcc -Wl,-z,now -shared -fPIC -o lib.so lib.c -latomic'
        rlRun 'gcc -Wl,-rpath=. -o main main.o lib.so -lpthread -latomic'
        rlAssertExists lib.so
        rlAssertExists main
    rlPhaseEnd

    rlPhaseStartTest
        rlRun ./main
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun popd
        rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
