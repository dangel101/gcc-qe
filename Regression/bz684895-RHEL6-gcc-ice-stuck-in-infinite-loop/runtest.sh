#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz684895-RHEL6-gcc-ice-stuck-in-infinite-loop
#   Description: RHEL6-gcc-ice-stuck-in-infinite-loop
#   Author: Marek Polacek <mpolacek@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2011, 2012 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include rhts environment
. /usr/bin/rhts-environment.sh
. /usr/lib/beakerlib/beakerlib.sh

PACKAGES=(gcc)

# Choose the compiler.
GCC=${GCC:-gcc}

rlJournalStart
    rlPhaseStartSetup
        for p in "${PACKAGES[@]}"; do
            rlAssertRpm "$p"
        done; unset p
        # Implementation of free on RHEL-6.6 and older does not have the -h option
        free --help |& grep ' -h[, ]' &>/dev/null \
            && FREEOPT="-h" \
            || FREEOPT=""
        rlRun -l "free ${FREEOPT}" 0 "Check available memory"
        rlRun "TmpDir=\`mktemp -d\`" 0 "Creating tmp directory"
	# Prepare the reproducer.
	rlRun "cp -v pr48141.c  $TmpDir"
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    # We just compile the reproducer with various optimization levels.
    # XXX This might (and will) take some time.
    for opt in s $(seq 0 3); do
        rlPhaseStartTest "With -O$opt:"
	    # Without FRE (Full Redundancy Elimination).
            rlRun "$GCC -O${opt} -c -fno-tree-fre pr48141.c" || rlLog "$(dmesg | tail)"
            rlAssertExists "pr48141.o"
            rm -f ./pr48141.o
        rlPhaseEnd
    done

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
