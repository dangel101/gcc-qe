#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz605803-gcc-gets-an-internal-compiler-error-when-compiling-a-kernel-module
#   Description: gcc gets an internal compiler error when compiling a kernel module
#   Author: Michal Nowak <mnowak@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2010 Red Hat, Inc. All rights reserved.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 3 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include rhts environment
. /usr/bin/rhts-environment.sh
. /usr/lib/beakerlib/beakerlib.sh

# Notes on relevancy
#
# Unfortunately, this test can fail because of bugs in systemtap itself;
# it's similar to the situation of ../bz851467-*. It makes sense to exclude
# it from running where fixes to systemtap are no longer to be expected.
#
# Suggested TCMS relevancy:
#   collection contains gcc-toolset-10: False

PACKAGE="gcc"

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm $PACKAGE
        rlCheckRpm systemtap || rlRun "yum -y install systemtap"
        rlAssertRpm systemtap
        rlRun "TmpDir=\`mktemp -d\`" 0 "Creating tmp directory"
        cp skip.stp $TmpDir
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    rlPhaseStartTest
        # the only thing we care about is gcc survives, without ICE
        rlRun 'timeout 300 stap skip.stp -DMAXTRYLOCK=0 -DSTP_NO_OVERLOAD --disable-cache -vtug >skip.stp.log 2>&1' 0-255
        cat skip.stp.log
        rlAssertGrep "Pass .*: compiled C into" skip.stp.log
        rlAssertGrep "Pass .*: starting run" skip.stp.log
        rlAssertNotGrep "internal compiler error" skip.stp.log
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
