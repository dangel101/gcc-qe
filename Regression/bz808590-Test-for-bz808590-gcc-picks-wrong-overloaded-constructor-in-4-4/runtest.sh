#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz808590-Test-for-bz808590-gcc-picks-wrong-overloaded-constructor-in-4-4
#   Description: Test for bz808590 (gcc picks wrong overloaded constructor in 4.4)
#   Author: Dagmar Prokopova <dprokopo@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2012 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh
. /usr/share/beakerlib/beakerlib.sh

PACKAGES=(gcc gcc-c++)

# Choose the compiler.
GCC=${GCC:-gcc}
GXX=${GXX:-g++}

rlJournalStart
  rlPhaseStartSetup
    # Check for needed packages.
    for pack in "${PACKAGES[@]}"; do
      rlAssertRpm "$pack" || yum -y install "$pack"
    done; unset pack
    rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
    # Copy the file test.C into the directory.
    cp -v test.C $TmpDir
    rlRun "pushd $TmpDir"
  rlPhaseEnd

  rlPhaseStartTest
    # Compile the file.
    rlRun "$GXX test.C -o test" 0 "Compiling the file"
    # Run the compiled program; shouldn't abort.
    rlRun "./test" 0 "Running the program"
  rlPhaseEnd

  rlPhaseStartCleanup
    rlRun "popd"
    rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
  rlPhaseEnd
rlJournalPrintText
rlJournalEnd
