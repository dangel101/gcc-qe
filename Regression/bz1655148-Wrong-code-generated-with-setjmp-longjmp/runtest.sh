#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz1655148-Wrong-code-generated-with-setjmp-longjmp
#   Description: Test for BZ#1655148 (Wrong code generated with setjmp/longjmp)
#   Author: Michael Petlan <mpetlan@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2019 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="gcc"
GCC=${GCC:-gcc}

rlJournalStart
	rlPhaseStartSetup
		rlAssertRpm $PACKAGE
		rlLog "Using GCC: `rpmquery -f $(which $GCC)`"
		rlRun "gcc -O3 t.c -o t" 0 "Compiling the reproducer"
		rlAssertExists "t"
		rlRun "./t > out" 0 "Running the reproducer"
	rlPhaseEnd

	rlPhaseStartTest "BUG ASSERTIONS (three fails == bug)"
		rlAssertGrep "a contains a" out
		rlAssertNotGrep "This is bug" out
		rlAssertNotGrep "a contains bbb I am b" out
	rlPhaseEnd

	rlPhaseStartCleanup
		rlRun "rm -f t out" 0 "Removing temp files"
	rlPhaseEnd
rlJournalPrintText
rlJournalEnd
