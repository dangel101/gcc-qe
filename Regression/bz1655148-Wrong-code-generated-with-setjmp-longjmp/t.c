#include <stdio.h>
#include <setjmp.h>

jmp_buf jbuf;

void fun1(jmp_buf jb, void *a)
{
   printf("fun1: a = %p\n", a);
   longjmp(jb, 1);
}

void fun2(void *b, void *a)
{
   if ((size_t) b == (size_t) a)
     printf("**** This is bug: b and a should not be equal\n");
   printf("fun2: b = %p a = %p\n", b, a);
   printf(" a contains %s \n", (char *) a);
}

void fun()
{
   volatile char hp[100];
   char *volatile a = (char *) 0;

   if (setjmp(jbuf) == 0)
   {
     a = (char *) &hp[0];
     *a = 'a';
     fun1(jbuf, a);
   }
   else
   {
     char b[100] = "bbb I am b";

     if (a)
       fun2(&b[0], a);
   }
}


int main()
{
   fun();
   return 0;
}
