#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/g/Regression/bz875951-gcc-ar-and-friends-return-random-exit-codes
#   Description: Test for BZ#875951 (gcc-ar and friends return random exit codes)
#   Author: Dagmar Prokopova <dprokopo@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2013 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

# gcc >= 4.5 needed because of -flto. In practice those are:
#   - base gcc in rhel-7 and newer
#   - any supported collection gcc
# TCMS relevancy could look like:
#   distro < rhel-5: False
#   distro = rhel-6 && collection !defined: False

PACKAGES=(gcc gcc-c++ glibc)

# Choose the compilers.
GCC=${GCC:-gcc}
GXX=${GXX:-g++}


rlJournalStart
    rlPhaseStartSetup
        for pack in "${PACKAGES[@]}"; do
            rlAssertRpm "$pack" || yum -y install "$pack"
        done; unset pack
        rlLog "Using $GXX from $(rpmquery -f $(which $GXX))"

        GCC_DUMPVERSION=$(gcc -dumpversion)
        GCC_MAJOR_VERSION=${GCC_DUMPVERSION%%.*}
        [[ "$GCC_MAJOR_VERSION" -gt 10 ]] && ADDITIONAL_PARAMETERS='-std=c++14' || ADDITIONAL_PARAMETERS=''
        rlLogInfo "GCC_DUMPVERSION=$GCC_DUMPVERSION"
        rlLogInfo "GCC_MAJOR_VERSION=$GCC_MAJOR_VERSION"
        rlLogInfo "ADDITIONAL_PARAMETERS=$ADDITIONAL_PARAMETERS"

        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "cp -v lto.cpp $TmpDir"
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    rlPhaseStartTest
        # Write g++ version.
        $GXX --version
        # Compile the reproducer. Everything should be fine.
        rlRun "$GXX lto.cpp $ADDITIONAL_PARAMETERS -flto -fPIC -shared -O1" 0
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
