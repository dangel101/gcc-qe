#!/usr/bin/env bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz2116635-inconsistent-decription-of-Wmismatched-dealloc
#   Description: Test for BZ#2116635 (inconsistent man description of -Wmismatched-dealloc)
#   Author: Vaclav Kadlcik <vkadlcik@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2022 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

# Notes on relevancy
#
# -Wmismatched-dealloc appeared in GCC-11. That means:
#   * system GCC of RHEL-9+
#   * GTS/DTS-11+
# However RHBZ#2116635 is present in some our releases which are not likely
# to be fixed, namely DTS/GTS-11 and system RHEL 9.0 and 9.1. Therefore I'd
# suggest this TCMS relevancy:
#   collection !defined && distro < rhel-9: False
#   collection !defined && distro = rhel-9 && distro < rhel-9.2: False
#   collection contains gcc-toolset-10, gcc-toolset-11, devtoolset-10, devtoolset-11: False

GCC="${GCC:-$(type -P gcc)}"
GCC_RPM_NAME=$(rpm --qf '%{name}' -qf $GCC)

PACKAGES="$GCC_RPM_NAME"

rlJournalStart
    rlPhaseStartSetup
        rlLogInfo "PACKAGES=$PACKAGES"
        rlLogInfo "COLLECTIONS=$COLLECTIONS"
        rlLogInfo "GCC=$GCC"
        rlLogInfo "SKIP_COLLECTION_METAPACKAGE_CHECK=$SKIP_COLLECTION_METAPACKAGE_CHECK"

        # We optionally need to skip checking for the presence of the metapackage
        # because that would pull in all the dependent toolset subrpms.  We do not
        # always want that, especially in CI.
        _COLLECTIONS="$COLLECTIONS"
        if ! test -z $SKIP_COLLECTION_METAPACKAGE_CHECK; then
            for c in $SKIP_COLLECTION_METAPACKAGE_CHECK; do
                rlLogInfo "ignoring metapackage check for collection $c"
                export COLLECTIONS=$(shopt -s extglob && echo ${COLLECTIONS//$c/})
            done
        fi
        rlLogInfo "(without skipped) COLLECTIONS=$COLLECTIONS"
        rlAssertRpm --all
        export COLLECTIONS="$_COLLECTIONS"

        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "cp files/* $TmpDir"
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    # I haven't found an easy way to list warnings that are or are not enabled
    # by default. Best I have is to find the warning of interest in source
    # template gcc/c-family/c.opt and look if it has Init(1). However it's just
    # a guess work. For this particular warning we can try to compile a demo
    # "violation" based on an example at:
    #   https://gcc.gnu.org/onlinedocs/gcc-12.2.0/gcc/Warning-Options.html
    rlPhaseStartTest CHECK_BEHAVIOR

        # When explicitly required, it should be enabled. The assert assures
        # several things at once:
        # (1) our demo source is corrent
        # (2) the works
        # (3) we know how the warning is displayed.
        rlRun 'gcc -c -Wmismatched-dealloc demo.c &>optset.log'
        rlAssertGrep 'warning:.* \[-Wmismatched-dealloc\]' optset.log \
            || rlFileSubmit optset.log optset.log

        # The warning should be present when -Wall is given.
        rlRun 'gcc -c -Wall demo.c &>wall.log'
        rlAssertGrep 'warning:.* \[-Wmismatched-dealloc\]' wall.log \
            || rlFileSubmit wall.log wall.log

        # By default the warning should *not* be present.
        rlRun 'gcc -c demo.c &>noopt.log'
        rlAssertNotGrep 'warning:.* \[-Wmismatched-dealloc\]' noopt.log \
            || rlFileSubmit noopt.log noopt.log
    rlPhaseEnd

    # The catch of checking the manpage is checking the right one. I saw
    # DTS/GTS versions that put the toolset man path *after* the system one.
    # So let's not rely on correct MANPATH.
    rlPhaseStartTest CHECK_MAN
        rlRun "MANFILE=$(rpm -ql $GCC_RPM_NAME | grep /man/man1/gcc.1*)"
        rlAssertExists "$MANFILE"
        rlRun 'MANWIDTH=4096 man $MANFILE >man.txt'
        if rlIsRHEL 6; then
            rlRun 'col -b <man.txt >plaintext.man && mv plaintext.man man.txt'
        fi
        rlAssertGrep 'Option -Wmismatched-dealloc is included in -Wall\.' man.txt
        rlAssertNotGrep 'Wmismatched-dealloc.*is enabled by default' man.txt
        rlGetPhaseState || rlFileSubmit man.txt man.txt
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun popd
        rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
