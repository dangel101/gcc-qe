#include <stdio.h>
#include <stdlib.h>

void mydealloc (void*);

__attribute__ ((malloc (mydealloc, 1))) void*
myalloc(size_t);

void f(void)
{
  void *p = myalloc(32);
  printf("%p\n", p);
  free(p);   // warning: not a matching deallocator for myalloc
  mydealloc(p);   // ok
}
