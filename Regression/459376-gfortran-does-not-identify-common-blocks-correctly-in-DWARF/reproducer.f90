program common_test

   INTEGER*4            ix
   REAL*4               iy
   REAL*8               iz

   INTEGER*4            ix_x
   REAL*4               iy_y
   REAL*8               iz_z

   common /foo/ix,iy,iz
   common /fo_o/ix_x,iy_y,iz_z
!STOP: check that the commons has the right values in it

   ix = 1
   iy = 2.0
   iz = 3.0
!STOP: check that the commons has the right values in it

   ix_x = 11
   iy_y = 22.0
   iz_z = 33.0
!STOP: check that the commons has the right values in it
   ix = 0        ! Ensure there's somewhere to stop
end program common_test
