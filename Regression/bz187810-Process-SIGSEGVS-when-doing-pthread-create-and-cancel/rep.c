/* Copyright (c) Marek Polacek <mpolacek@redhat.com>, 2011.  */

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/select.h>


static void *
tf (void __attribute__ ((unused)) *arg)
{
  fd_set rfs;
  fd_set wfs;
  fd_set efs;

  while (1)
    {
      FD_ZERO (&rfs);
      FD_ZERO (&wfs);
      FD_ZERO (&efs);
      FD_SET (0, &wfs);

      int err = select (FD_SETSIZE, &rfs, &wfs, &efs, 0);
    }

  return NULL;
}


int
main (int argc, char *argv[])
{
  time_t cur;

  if (argc != 2)
    return 1;

  /* Get the run time.  */
  const long int limit = strtol (argv[1], NULL, 0);

  const time_t start = cur = time (NULL);

  while (cur - start < limit)
    {
      pthread_t th;

      int err = pthread_create (&th, NULL, tf, NULL);
      if (err != 0)
	{
	  printf ("create failed: %m\n");
	  return 1;
	}

      err = pthread_cancel (th);
      if (err != 0)
	{
	  printf ("cancel failed: %m\n");
	  return 1;
	}

      err = pthread_join (th, NULL);
      if (err != 0)
	{
	  printf ("join failed: %m\n");
	  return 1;
	}

      cur = time (NULL);
    }

  return 0;
}
