module module_test
   real :: a(2,2)
end module module_test

program test_pass
   use module_test, ONLY: a
   interface
      subroutine sub_test(a_sub,c_sub,c_size)
        real,target  :: a_sub(:,:)
        integer      :: c_size
        real         :: c_sub(c_size,c_size/2)
      end subroutine sub_test
   end interface
   a(1,1)=1.
   a(2,1)=2.
   a(1,2)=3.
   a(2,2)=4.
   call sub_test(a,a(:,2),2)
end program test_pass


subroutine sub_test(a_sub,c_sub,c_size)
   real,target  :: a_sub(:,:)
   integer      :: c_size
   real         :: c_sub(c_size,c_size/2)

   real,pointer :: b_sub(:,:)

   b_sub => a_sub
   call contained_sub

contains

   subroutine contained_sub
     print*,'a_sub',a_sub(:,:)
     print*,'b_sub',b_sub(:,:)
     print*,'c_sub',c_sub(:,:)
   end subroutine contained_sub

end subroutine sub_test


