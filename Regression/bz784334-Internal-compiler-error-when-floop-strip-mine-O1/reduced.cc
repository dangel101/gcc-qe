typedef unsigned int size_t;
__extension__ typedef int __ssize_t;
typedef __ssize_t ssize_t;
extern "C" void *memcpy (void *__restrict __dest,
       __const void *__restrict __src, size_t __n)
     throw () __attribute__ ((__nonnull__ (1, 2)));
extern void *malloc (size_t __size) throw () __attribute__ ((__malloc__)) ;
extern void free (void *__ptr) throw ();
extern double floor (double __x) throw () __attribute__ ((__const__));
extern double fmod (double __x, double __y) throw ();
extern double log (double __x) throw ();
extern int F;
class vect
{
 public:
 typedef double& reference;
 typedef ssize_t size_type;
 typedef double value_type;
 private:
 size_type Size;
 size_type Alloc;
 value_type *Elem;
 public:
 explicit vect( size_type sz );
 __attribute__((__always_inline__)) inline reference get( size_type i ) { return Elem[ i ]; }
 ~vect();
};
vect **failing(
 int dim,
 int npts,
 double start
)
{
 vect
   **Output;
 int i,
   j,
   m,
   r,
   iter,
   n1,
   n2,
   *Arr1,
   *Arr2,
   *Arr3,
   *Arr4,
   *Arr5,
   *Arr6,
   *Arr7;
 double end,
   mfloat,
   rfloat,
   *Arr8,
   *Arr9;
 end = start + ( npts - 1 );
 Output = (vect **) malloc( dim * sizeof( vect* ));
 for( i=0; i < dim; i++ )
  Output[ i ] = new vect( npts );
 r = F;
 rfloat = (double) r;
 n2 = 1 + (int) ( log( end ) / log( rfloat ));
 Arr1 = (int *) malloc( n2 * n2 * sizeof( int ));
 Arr2 = (int *) __builtin_alloca (n2 * sizeof( int ));
 Arr3 = (int *) __builtin_alloca (n2 * sizeof( int ));
 Arr4 = (int *) __builtin_alloca (n2 * sizeof( int ));
 Arr8 = (double *) __builtin_alloca (n2 * sizeof( double ));
 for( j = 0; j < n2; j++ )
  Arr1[j] = 1;
 Arr6 = Arr1;
 for( i = 1; i < n2; i++ )
 {
  Arr7 = Arr6 + n2;
  Arr7[i]= 1;
  for( j = i+1; j < n2; j++ )
   Arr7[j] = ( Arr7[j-1] + Arr6[j-1] ) % r;
  Arr6 = Arr7;
 }
 Arr8[0] = 1./rfloat;
 for( i = 1; i < n2; i++ )
  Arr8[ i ] = Arr8[ i-1 ] / rfloat;
 mfloat = start;
 n1 = 0;
 while( mfloat > 0. )
 {
  Arr2[ n1++ ] = (int) fmod( mfloat, rfloat );
  mfloat = floor( mfloat / rfloat );
 }
 for( iter = 0; start <= end; start++, iter++ )
 {
  memcpy( Arr3, Arr2, n1 * sizeof( int ));
  for( m = 0;; )
  {
   Arr9 = &Output[m]->get( iter );
   *Arr9 = 0;
   for( j = 0; j < n1; j++ )
    *Arr9 += Arr8[j] * Arr3[j];
   if( ++m == dim )
    break;
   Arr7 = Arr1;
   for( j = 0; j < n1; j++ )
   {
    Arr4[j] = 0;
    for( i = j; i < n1; i++ )
     Arr4[j] += Arr7[i] * Arr3[i];
    Arr4[j] %= r;
    Arr7 += n2;
   }
   Arr5 = Arr3;
   Arr3 = Arr4;
   Arr4 = Arr5;
  }
  for( m = 0; m < n1; m++ )
  {
   Arr2[ m ]++;
   if( Arr2[ m ] == r )
    Arr2[ m ] = 0;
   else
    break;
  }
  if( m == n1 )
   Arr2[ n1++ ] = 1;
 }
 free( Arr1 );
 return Output;
}
