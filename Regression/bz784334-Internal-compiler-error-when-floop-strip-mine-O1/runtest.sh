#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz784334-Internal-compiler-error-when-floop-strip-mine-O1
#   Description: Test for bz784334 (Internal compiler error when -floop-strip-mine -O1)
#   Author: Marek Polacek <polacek@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2012 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# PR debug/46885
# * tree-ssa-loop-manip.c (canonicalize_loop_ivs): Use gsi_last_bb
# instead of gsi_last_nondebug_bb if bump_in_latch.

# Include Beaker environment
. /usr/bin/rhts-environment.sh
. /usr/lib/beakerlib/beakerlib.sh

# ============================================================================
# Notes on relevancy
# ============================================================================
#
# The isl library, which is a prerequisite for the -f... switches below,
# is missing or disabled in RHEL9+ gcc and in RHEL6 DTS10+ gcc. See the
# respective gcc.spec files. Some discussion is in
#   https://bugzilla.redhat.com/show_bug.cgi?id=1975890
# as well.
#
# How to set relecancy in TCMS:
#   [relevancy]
#   distro = rhel-9: False
#   distro = rhel-6 && collection defined: False

PACKAGES=(gcc gcc-c++)

# Choose the compilers.
GCC=${GCC:-gcc}
GXX=${GXX:-g++}

rlJournalStart
  rlPhaseStartSetup
    # Check for needed packages.
    for p in "${PACKAGES[@]}"; do
      rlAssertRpm "$p" || yum -y install "$p"
    done; unset p
    rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
    # We need the reproducer.
    cp -v pr46885.c reduced.cc $TmpDir
    rlRun "pushd $TmpDir"
  rlPhaseEnd

  rlPhaseStartTest
    # Compile the original reproducer.  Has to compile.
    rlRun "$GXX -c -g -O1 -floop-strip-mine reduced.cc"
    # And now even the testsuite-ish reproducer.
    rlRun "$GCC -c -O -ftree-parallelize-loops=4 -fcompare-debug -fno-tree-dominator-opts -funswitch-loops pr46885.c"
  rlPhaseEnd

  rlPhaseStartCleanup
    rlRun "popd"
    rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
  rlPhaseEnd
rlJournalPrintText
rlJournalEnd
