#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/gcc/bz199604-Openssl-compiled-with-latest-gcc-fails-sha512-test
#   Description: Test checks if sha512 test case from OpenSSL works as expected.
#   Author: Michal Nowak <mnowak@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2010, 2012 Red Hat, Inc. All rights reserved.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 3 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include rhts environment
. /usr/bin/rhts-environment.sh
. /usr/lib/beakerlib/beakerlib.sh

PACKAGE="gcc"

# Choose the compiler.
GCC=${GCC:-gcc}

# This is intended to run only on s390x/ppc64!

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm $PACKAGE
        rlRun "TmpDir=\`mktemp -d\`" 0 "Creating tmp directory"
	cp -v testcasesha512.c $TmpDir
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    # -fschedule-insns isn't supported on x86-64 nor i686 (and, shouldn't be used
    # anywhere with -O0).  Only GCC 4.5 has some -fschedule-insns support for
    # x86-64/i686 with register pressure sensitive scheduling.
    for opt in s $(seq 3); do
	rlPhaseStartTest opt-O$opt
	    rlRun "$GCC -O$opt -fschedule-insns -fregmove testcasesha512.c -o testcasesha512-opt-O$opt"
	    rlRun "./testcasesha512-opt-O$opt"
	rlPhaseEnd
    done

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
