#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz1610457-gcc-bug-when-static-linking-with-boost-and-lto
#   Description: Test for BZ#1610457 (gcc bug when static linking with boost and lto)
#   Author: Michael Petlan <mpetlan@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2018 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

GXX=${GXX:-g++}
PACKAGE=`rpmquery -f $(which $GXX) | grep $(arch)`

rlJournalStart
	rlPhaseStartSetup
		rlAssertRpm $PACKAGE
		rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
		ORIG_DIR=`pwd`
		rlRun "pushd $TmpDir"
		rlRun "tar x < $ORIG_DIR/reduced_test.tar" 0 "Unpack the reproducer"
		# check c++17 support
		echo "int main(void){return 0;}" | $GXX -x c++ -std=c++17 - -o a1.out
		if [ $? -eq 0 ]; then
			rlLog "c++17 standard is supported"
			export CXX17_SUPPORT="yes"
			rlRun "$GXX -O2 -c value_semantic.ii" 0 "Prepare the reproducer"
			rlAssertExists "value_semantic.o"
		else
			rlLogWarning "c++17 standard is NOT supporeted, skipping"
			export CXX17_SUPPORT="no"
		fi
	rlPhaseEnd

	if [ "$CXX17_SUPPORT" = "yes" ]; then
		rlPhaseStartTest
			rlRun "$GXX -w -Wl,--no-demangle lines.ii options.ii -O2 -std=c++17 -flto value_semantic.o &>BUILD_LOG" 0-255 "Build reproducer"
			cat BUILD_LOG # debug purposes
			rlLog "THE FOLLOWING LINE IS THE BUG REPRODUCTION ASSERT:"
			rlAssertNotGrep "_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED1Ev" BUILD_LOG  # BUG ASSERTION HERE
		rlPhaseEnd
	fi

	rlPhaseStartCleanup
		rlRun "popd"
		rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
	rlPhaseEnd
rlJournalPrintText
rlJournalEnd
