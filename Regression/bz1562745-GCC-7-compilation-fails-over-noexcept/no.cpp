#include <type_traits>
#include <iostream>
#include <atomic>
#include <stdlib.h>
#include <dlfcn.h>

template <typename Return, typename... Args>
static Return (*rtld_next(Return (*f)(Args...), const char* symbolVersion = nullptr))(Args...)
{
    Dl_info info;
    dladdr(reinterpret_cast<void*>(f), &info);
    auto symbolName = info.dli_sname;

    // Clear any existing error
    dlerror();

    auto realFunction = reinterpret_cast<decltype(f)>(
		symbolVersion ? dlvsym(RTLD_NEXT, symbolName, symbolVersion)
		: dlsym(RTLD_NEXT, symbolName));

    return realFunction;
}

template <typename F, F* Func, typename... Args>
static typename std::result_of<F*(Args...)>::type call_next(Args&&... args)
{
    static std::atomic<F*> real{};

    if (!real.load(std::memory_order_relaxed))
		real.store(rtld_next(Func));

    return real.load(std::memory_order_relaxed)(std::forward<Args>(args)...);
}


void* malloc(size_t size)
{
	return call_next<decltype(malloc), malloc>(size);
}

void* operator new(std::size_t size)
{
	return call_next < decltype(malloc), ::operator new>(size);
}

int main()
{
}
