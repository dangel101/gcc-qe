#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz851464-Systemtap-fails-to-probe-sunrpc-related-kernel-functions
#   Description: Test for BZ#851464 ([FJ7.0 Bug] apparent debuginfo quality regression)
#   Author: Dagmar Prokopova <dprokopo@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2014 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

# Notes on relevancy
#
# Unfortunately, this test can fail because of bugs in systemtap itself;
# it's similar to the situation of ../bz851467-*. It makes sense to exclude
# it from running where fixes to systemtap are no longer to be expected.
#
# Suggested TCMS relevancy:
#   arch = arm, armhfp, i386, ia64, ppc, ppc64, ppc64le, s390: False # inherited
#   collection contains gcc-toolset-10: False

# Requires either kernel-3.10.0-11.el7 and older or systemtap
# containing commit 39e21dd.
# See https://sourceware.org/bugzilla/show_bug.cgi?id=16542.

PACKAGES=(gcc systemtap kernel-debuginfo)

rlJournalStart
    rlPhaseStartSetup
        for p in "${PACKAGES[@]}"; do
            rlAssertRpm "$p" || yum -y install "$p"
        done; unset p
        rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
        # We need the reproducers.
        cp -v sunrpc.clnt.bind_new_program.stp $TmpDir
        cp -v sunrpc.clnt.call_sync.stp $TmpDir
        rlRun "pushd $TmpDir"
        rlLog "======== running stap-prep ========"
        stap-prep
        rlLog "======== stap-prep finished with exit code $? ========"
    rlPhaseEnd

    rlPhaseStartTest
        rlRun "stap -p4 -v sunrpc.clnt.bind_new_program.stp" 0
        rlRun "stap -p4 -v sunrpc.clnt.call_sync.stp" 0
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
