inline int *foo() {
  static thread_local int i = 42;
  return &i;
}
