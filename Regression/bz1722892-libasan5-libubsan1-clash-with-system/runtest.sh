#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz1722892-libasan5-libubsan1-clash-with-system
#   Description: Test for BZ#1722892 (libasan5/libubsan1 clash with system)
#   Author: Alexandra Hájková <ahajkova@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2019 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

# This is relevant for devtoolset and gcc-toolset GCCs only. If they provide
# some lib<whatever> RPM, that RPM must not clash (*) with a corresponding RPM
# of system GCC's (RHBZ#1722892). Moreover, that RPM must not leak to repos
# other than RHSCL (RHEL-7 and older) or AppStream (RHEL-8+) (RHBZ#1729402).
#
# (*) not clash:
#       - It their names are different then their files must not have an
#         intersection so the RPMs can be installed in parallel.
#         Example:
#           - RHEL7 has libasan with /usr/lib64/libasan.so.0* files
#           - DTS11 has libasan6 with /usr/lib64/libasan.so.6*
#           Both the RPMs can live together.
#       - If they have the same name then the toolset RPM must be a "sane"
#         upgrade of the base RPM. In practice, we discuss just lib* RPMs
#         consisting of just shared libraries so a "good enough" check
#         would be a perfect name match of the included files' names.
#         Example:
#           - RHEL7 has libtsan with /usr/lib64/libtsan.so.0* files
#           - DTS11 has libtsan with /usr/lib64/libtsan.so.0* files
#           The filenames in these two RPMs perfectly match, we can assume
#           that DTS11's libtsan-11.2.1-9.el7 is a natural upgrade to RHEL7's
#           libtsan-4.8.5-44.el7.
#
# Still, the test can run into troubles when run for an older toolset
# version than already present in the repos. E.g. both gcc-toolset-10 and -11
# have libasan6, where GTS-11 libasan6 is a natural upgrade to GTS-10 one.
# However if you run the test for GTS-10 it will complain about GTS-11
# libasan6, not knowing it comes from a later GTS so it's actually OK.
# I don't see an easy way out while keeping the test "simple enough"...
# Remains as a TODO.
#
# Suggested TCMS relevancy:
#   collection !defined: False

# Global
#   variables
#     ARCH  ... architecture
#     VT RT ... RPM version and tag of the toolset GCC being tested
#     VS RS ... RPM version and tag of the base GCC
#   files
#     rpmlist-t.txt        ... list of Brew RPM files of the toolset GCC being tested
#     rpmlist-s.txt        ... list of Brew RPM files of the base GCC
#     files-s.txt          ... sorted list of all the files of all the RPMs of the base GCC
#     non_toolset_rpms.txt ... RPMs available in repos where toolset GCCs shouldn't be present

GCC="${GCC:-$(type -P gcc)}"

check_clash () {
    local full_path_t="$1" # full path in Brew to a toolset RPM
    local full_path_s      # full path in Brew to the respective base RPM, if any

    local name; name=$(rpm -q --qf="%{NAME}" -p "$full_path_t") || rlFail 'Cannot get RPM name'

    # Clashes on directories would be false positives. This will allows us
    # to detect directories easily and exclude them.
    rlAssertRpm "$name" "$VT" "$RT" "$ARCH"

    # base RPM of the same name, if any
    full_path_s=$(grep -F "/${name}-${VS}-${RS}.${ARCH}" rpmlist-s.txt)
    [[ $? -ge 2 ]] && rlFail 'unexpected exit value from grep'

    if [[ -n "$full_path_s" ]]; then

        # Base GCC has an RPM of the same name. We should check that both the
        # toolset and base RPMs contain the same filenames, ie. the toolset RPM
        # is a "shared-library" upgrade to the base RPM.
        rlLogInfo "Homonymous, should match: $full_path_t $full_path_s"
        rlRun "rpm -qlp $full_path_t >toolset-unsorted.list"
        rlRun "rpm -qlp $full_path_s >base-unsorted.list"
        rlRun 'sort <toolset-unsorted.list >toolset-sorted.list'
        rlRun 'sort <base-unsorted.list >base-sorted.list'
        rlAssertNotDiffer toolset-sorted.list base-sorted.list

    else

        # Base GCC has no RPM of the same name. We should check that the toolset
        # RPM can be installed in parallel with whole base GCC.
        rlLogInfo "No homonym in base for $full_path_t, should not match any base file"
        rlRun "rpm -qlp $full_path_t >toolset-unsorted.list"
        rlRun 'sort <toolset-unsorted.list >toolset-sorted.list'
        rlRun 'comm -1 -2 toolset-sorted.list files-s.txt >files_match.txt'
        local shared_line no_match_found=true
        while read shared_line; do
            if ! [[ -d "$shared_line" ]]; then
                rlFail "Clash on $shared_line"
                no_match_found=false
            fi
        done <files_match.txt
        $no_match_found && rlPass 'No match found'
    fi
}

check_leak () {
    # Tricky. We may be testing a build that hasn't been considered for the
    # composes yet so we shouldn't check for the specific N-V-R, not even N-V.
    # Looking for just N isn't good either, becase N could be a false positive
    # delivered by base GCC. Using just the major version part of N (let's call
    # it M) and checking for "N-M." looks promising - base GCC and toolset GCC
    # always have different M (getting a newer M is the whole point of toolset
    # GCCs.
    #
    # Still, we cannot trust this test completely. It will work only when the
    # composes already have some "pre-spin" included and the pre-spin is
    # "similar enough" to the GCC being tested. In other words, we rely on
    # building several respins of GCCs, their content becoming more and more
    # stable stable and a reasonably fresh respin is already present in the
    # composes.  It's very problematic assumption in theory but in practice,
    # it's how it really works. Moreover, we should check the channels/repos
    # when adding builds to errata so this test is basically a double-check
    # anyway.

    local full_path_t="$1" # full path in Brew to a toolset RPM
    local N; N=$(rpm -q --qf="%{NAME}" -p "$full_path_t") || rlFail 'Cannot get RPM name'
    local M=${VT%%.*}
    local line
    rlAssertNotGrep "|${N}-${M}." non_toolset_rpms.txt -qF
    grep -F "|${N}-${M}." non_toolset_rpms.txt | while read line; do
        rlLogError "$line looks like a leak"
    done
}

rlJournalStart
    rlPhaseStartSetup
        rlMountRedhat
        rlRun "ARCH=$(rlGetPrimaryArch)"

        # GTS/DTS GCC under tests
        rlLogInfo "GCC=$GCC"
        rlRun "NT=$(rpm -q --qf='%{NAME}'    -f $GCC)"
        rlRun "VT=$(rpm -q --qf='%{VERSION}' -f $GCC)"
        rlRun "RT=$(rpm -q --qf='%{RELEASE}' -f $GCC)"
        rlRun "RPM_DIR_T=/mnt/redhat/brewroot/packages/$NT/$VT/$RT/$ARCH"
        if [[ "$NT" = gcc ]]; then
            rlDie 'Test not applicable to system GCC'
        fi

        # System GCC
        rpm -q gcc &>/dev/null || rlRun 'yum -y install gcc'
        rlAssertRpm gcc
        rlRun "NS=$(rpm -q --qf='%{NAME}'    gcc)"
        rlRun "VS=$(rpm -q --qf='%{VERSION}' gcc)"
        rlRun "RS=$(rpm -q --qf='%{RELEASE}' gcc)"
        rlRun "RPM_DIR_S=/mnt/redhat/brewroot/packages/$NS/$VS/$RS/$ARCH"

        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "pushd $TmpDir"

        # Listings we'll use later
        rlRun "ls $RPM_DIR_T/*.rpm >rpmlist-t.txt" # RPMs of toolset GCC being tested
        rlRun "ls $RPM_DIR_S/*.rpm >rpmlist-s.txt" # RPMs of base GCC
        rlRun "rpm -qlp $RPM_DIR_S/*.rpm >files-s-unsorted.txt"
        rlRun 'sort <files-s-unsorted.txt >files-s.txt' # base GCC's files, sorted
        rlRun 'yum repolist all >repolist.txt'
        touch non_toolset_rpms.txt # RPMs available in repos where toolset GCCs shouldn't be present
        while read line; do
            [[ "$line" == *\ enabled* ]] || continue # ignore headers, diagnostics and disabled repos
            repo=${line%% *}
            repo=${repo#!} # there can be a leading exclamation mark to signal stale data
            case "$repo" in
                *devtoolset*|*rhscl*|*RHSCL*|*AppStream*|*appstream*)
                    # such repos are expected to have toolset GCC RPMs
                    ;;
                brew-build-repo-*)
                    # created by /distribution/install/brew-build, they can have the toolset GCC being tested
                    ;;
                *)
                    rlRun "yum repolist -v $repo >repoinfo.txt"
                    if grep -qi 'baseurl.*:.*rhscl'; then
                        : # likely some Collections-related repo (e.g. a RHSCL compose added by bkr --repo=...)
                        : # therefore expected to have toolset GCC RPMs
                    else
                        rlRun "repoquery --disablerepo='*' --enablerepo='$repo' --qf='%{NAME}-%{VERSION}-%{RELEASE}' -a >repocontent.txt"
                        rlRun "sed 's|^|$repo\||' <repocontent.txt >>non_toolset_rpms.txt"
                    fi
                    rm -f repoinfo.txt
                    ;;
            esac
        done <repolist.txt
    rlPhaseEnd

    rlPhaseStartTest 'potential clashers'
        while read i; do
            rlLogDebug "Checking $i ..."
            b=${i##*/}
            if [[ "$b" == *debuginfo* ]]; then
                rlLogDebug "Skipping debuginfo RPM $b"
                continue
            fi
            if [[ "$b" == devtoolset-* ]] || [[ "$b" == gcc-toolset-* ]]; then
                rlLogDebug "Skipping 'honest' toolset RPM $b"
                continue
            fi
            rlRun "echo '$b' >>potential_clashers.txt"
        done <rpmlist-t.txt
    rlPhaseEnd

    while read i; do
        rlPhaseStartTest "No clash for $i"
            check_clash "$RPM_DIR_T/$i"
        rlPhaseEnd
    done <potential_clashers.txt

    while read i; do
        rlPhaseStartTest "No repo leak for $i"
            check_leak "$RPM_DIR_T/$i"
        rlPhaseEnd
    done <potential_clashers.txt

    rlPhaseStartCleanup
        rlFileSubmit repolist.txt repolist.txt
        rlFileSubmit non_toolset_rpms.txt non_toolset_rpms.txt
        rlRun "popd"
        rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
