#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz1076690-Python-Exception-type-exceptions-IndexError
#   Description: Test for BZ#1076690 (Python Exception <type 'exceptions.IndexError'>)
#   Author: Michael Petlan <mpetlan@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2016 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

GCC="${GCC:-$(type -P gcc)}"
GCC_RPM_NAME=$(rpm --qf '%{name}' -qf $GCC)
ARCH=$(rlGetPrimaryArch)
PACKAGES="$GCC_RPM_NAME.$ARCH ${GCC_RPM_NAME}-c++.$ARCH ${GCC_RPM_NAME%gcc}gdb.$ARCH"

rlJournalStart
    rlPhaseStartSetup
        rlLogInfo "PACKAGES=$PACKAGES"
        rlLogInfo "COLLECTIONS=$COLLECTIONS"
        rlLogInfo "GCC=$GCC"
        rlLogInfo "SKIP_COLLECTION_METAPACKAGE_CHECK=$SKIP_COLLECTION_METAPACKAGE_CHECK"

        # We optionally need to skip checking for the presence of the metapackage
        # because that would pull in all the dependent toolset subrpms.  We do not
        # always want that, especially in CI.
        _COLLECTIONS="$COLLECTIONS"
        if ! test -z $SKIP_COLLECTION_METAPACKAGE_CHECK; then
            for c in $SKIP_COLLECTION_METAPACKAGE_CHECK; do
                rlLogInfo "ignoring metapackage check for collection $c"
                export COLLECTIONS=$(shopt -s extglob && echo ${COLLECTIONS//$c/})
            done
        fi
        rlLogInfo "(without skipped) COLLECTIONS=$COLLECTIONS"
        rlAssertRpm --all
        export COLLECTIONS="$_COLLECTIONS"

        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "cp reproducer.cpp $TmpDir"
        rlRun "pushd $TmpDir"
        rlRun 'g++ -o reproducer -g reproducer.cpp'
    rlPhaseEnd

    rlPhaseStartTest
        rlRun 'gdb -batch -q -ex "b foo" -ex "set python print-stack full" -ex run -ex bt reproducer 2>err >out'

        # the stdout should be sane (check gdb did its job)
        rlAssertGrep "Breakpoint" out
        rlAssertGrep "foo" out
        rlAssertGrep "return" out
        rlAssertGrep "element" out
        rlAssertGrep "main" out
        rlAssertGrep "std::map" out

        # there should be no errors in the stderr log
        rlAssertNotGrep "IndexError" err
        rlAssertNotGrep "Traceback" err
        rlAssertNotGrep "std::vector" err
        rlAssertNotGrep "self.val.type" err
        rlAssertNotGrep "finished_results" err
        rlAssertNotGrep "capacity" err
    rlPhaseEnd

    rlPhaseStartCleanup
        rlFileSubmit out out
        rlFileSubmit err err
        rlRun popd
        rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
