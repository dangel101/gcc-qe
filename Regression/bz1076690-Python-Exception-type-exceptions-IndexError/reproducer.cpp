#include <map>

typedef std::map<int, void*> M;

M::size_type foo (M &m)
{
    return m.size ();
}

int main(void)
{
    M m;
    m[1] = (void *) 1;
    foo(m);
}
