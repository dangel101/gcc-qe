#include <pthread.h>
#include <future>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

static void* foo(void *arg)
{
	promise<void> *prom = (promise<void>*)arg;
	prom->set_value();
	return 0;
}

int main()
{
	promise<void> prom;
	future<void> fut = prom.get_future();

	pthread_t tid;
	if (pthread_create(&tid, 0, foo, &prom) != 0)
	{
		perror("pthread_create failed:");
		exit(0);
	}

	fut.get();

	pthread_join(tid, 0);
	return 0;
}
