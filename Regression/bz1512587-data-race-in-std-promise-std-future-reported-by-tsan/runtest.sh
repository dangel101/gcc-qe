#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz1512587-data-race-in-std-promise-std-future-reported-by-tsan
#   Description: Compiles reproducer with TSAN and checks output
#   Author: Michael Petlan <mpetlan@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2018 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

# Relevancy is complicated because libtsan's availability is complicated.
# On top of that, the test case uses features not available in older GCCs.
# Suggested TCMS relevancy:
#   collection !defined && distro = rhel-6: False # no libtsan
#   collection !defined && distro = rhel-7: False # no libtsan or missing features
#   collection !defined && distro = rhel-8 && arch = s390x: False # no libtsan
#   collection !defined && distro = rhel-9 && arch = s390x: False # no libtsan
#   collection defined && arch = s390x && collection contains gcc-toolset-10, gcc-toolset-11, devtoolset-10, devtoolset-11: False # no libtsan
#   collection defined && arch = s390x && collection contains devtoolset-12: False # RHBZ#2119088 WONTFIX

GCC=${GCC:-$(type -P gcc)}
GCC_RPM_NAME=$(rpm --qf '%{name}' -qf $GCC)
PACKAGES="$GCC_RPM_NAME ${GCC_RPM_NAME}-c++"

if [[ "$GCC_RPM_NAME" == *toolset* ]]; then
    TOOLSET=${GCC_RPM_NAME%-gcc}
else
    TOOLSET=''
fi

ARCH=$(rlGetPrimaryArch)

if [[ -z "$TOOLSET" ]]; then
    # system libtsan

    if rlIsRHEL 6; then
        exit 1 # not system libtsan; should be excluded by relevancy
    elif rlIsRHEL 7; then
        if [[ "$ARCH" = x86_64 ]]; then
            PACKAGES+=' libtsan libtsan-static'
            TEST_STATIC=true
            TEST_DYNAMIC=true
        else
            exit 1 # not system libtsan; should be excluded by relevancy
        fi
    elif rlIsRHEL 8 9; then
        if [[ "$ARCH" != s390x ]]; then
            PACKAGES+=' libtsan'
            TEST_STATIC=false
            TEST_DYNAMIC=true
        else
            exit 1 # not system libtsan; should be excluded by relevancy
        fi
    else
        # We may need more RHEL-, CentOS- or Fedora-specific rules.
        # For now let's expect anything there.
        PACKAGES+=' libtsan libtsan-static'
        TEST_STATIC=true
        TEST_DYNAMIC=true
    fi

else
    # toolset libtsan

    # No GTS/DTS 10-11 libtsan on s390x; should be excluded by relevancy
    if [[ "$TOOLSET" == *-1[01] ]] && [[ "$ARCH" = s390x ]]; then
        exit 1
    fi

    # Both static and dynamic variants are always available, although
    # the dynamic variant may come from the system
    TEST_STATIC=true
    TEST_DYNAMIC=true

    # Static variant comes with the toolset
    PACKAGES+=" ${TOOLSET}-libtsan-devel"

    # Dynamic variant comes either with the toolset or with the system. Its name
    # is pretty "complicated". Depends on the toolset and OS versions.
    if [[ "$TOOLSET" == gcc-toolset-1[01] ]]; then
        PACKAGES+=' libtsan' # system one
    elif [[ "$TOOLSET" == devtoolset-1[01] ]]; then
        PACKAGES+=' libtsan' # toolset one
    elif [[ "$TOOLSET" == *toolset-12 ]]; then
        PACKAGES+=' libtsan2' # here it's always from the toolset
    else
        # TODO: Explicitly specify the RPM name for each new toolset. I don't
        # see any other way to make this code "defensive".
        rlJournalStart; rlPhaseStartTest; rlFail "$TOOLSET not supported"; rlPhaseEnd; rlJournalPrintText; rlJournalEnd
    fi
fi

rlJournalStart
    rlPhaseStartSetup
        if [[ "$TOOLSET" == devtoolset-* ]]; then
            # For various reasons, DTS<12 on RHEL<8 is prone to incomplete installs
            # regardless my metadata efforts. Let's use this as a workaround:
            rpm -q libtsan-static && rlRun 'rpm -e libtsan-static' # not wanted and breaking install deps
            rpm -q $PACKAGES || rlRun "yum -y install --skip-broken $PACKAGES" 0-255 # best effort
        fi

        rlLogInfo "PACKAGES=$PACKAGES"
        rlLogInfo "COLLECTIONS=$COLLECTIONS"
        rlLogInfo "GCC=$GCC"
        rlLogInfo "SKIP_COLLECTION_METAPACKAGE_CHECK=$SKIP_COLLECTION_METAPACKAGE_CHECK"

        # We optionally need to skip checking for the presence of the metapackage
        # because that would pull in all the dependent toolset subrpms.  We do not
        # always want that, especially in CI.
        _COLLECTIONS="$COLLECTIONS"
        if ! test -z $SKIP_COLLECTION_METAPACKAGE_CHECK; then
            for c in $SKIP_COLLECTION_METAPACKAGE_CHECK; do
                rlLogInfo "ignoring metapackage check for collection $c"
                export COLLECTIONS=$(shopt -s extglob && echo ${COLLECTIONS//$c/})
            done
        fi
        rlLogInfo "(without skipped) COLLECTIONS=$COLLECTIONS"
        rlAssertRpm --all
        export COLLECTIONS="$_COLLECTIONS"

        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "cp racy.c $TmpDir"
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    if $TEST_STATIC; then
        rlPhaseStartTest STATIC
            rlRun 'g++ -std=c++11 -fsanitize=thread -static-libtsan -g -o racy racy.c -pthread'
            rlRun './racy 2>err'
            [[ $? -eq 66 ]] && rlFail "BUG REPRODUCED (exitcode == 66)"
            rlAssertNotDiffer err /dev/null
        rlPhaseEnd
    fi

    if $TEST_DYNAMIC; then
        rlPhaseStartTest DYNAMIC
            rlRun 'g++ -std=c++11 -fsanitize=thread -g -o racy racy.c -pthread'
            rlRun './racy 2>err'
            [[ $? -eq 66 ]] && rlFail "BUG REPRODUCED (exitcode == 66)"
            rlAssertNotDiffer err /dev/null
        rlPhaseEnd
    fi

    rlPhaseStartCleanup
        rlRun popd
        rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
