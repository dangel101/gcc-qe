#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz1722881-Update-Fortran-patches-for-9-1
#   Description: Test for BZ#1722881 (Update Fortran patches for 9.1)
#   Author: Alexandra Hájková <ahajkova@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2019 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

# Relevant for GCC>8. Suggested TCMS relevncy:
#   distro < rhel-9 && collection !defined: False

TESTPROG="sample"

rlJournalStart
    rlPhaseStartSetup
        GCC_MAJOR_VERSION=$(gcc -dumpversion)
        GCC_MAJOR_VERSION=${GCC_MAJOR_VERSION%%.*}
        rlLogInfo "GCC_MAJOR_VERSION=$GCC_MAJOR_VERSION"
        [[ -z "$GCC_MAJOR_VERSION" ]] && rlFail 'Unknown GCC version'
        if [[ "$GCC_MAJOR_VERSION" -lt 10 ]]; then
            # In downstream, we introduced this option:
            FDECCMP_OPT='-fdec-comparisons'
        else
            # When it was being upstreamed to GCC 10, the respective feature
            # landed as part of more general option '-fdec'. See:
            # * Thread starting with
            #   https://gcc.gnu.org/legacy-ml/gcc-patches/2019-11/msg02082.html
            # * https://gcc.gnu.org/gcc-10/changes.html ("Hollerith constants")
            # * https://gcc.gnu.org/onlinedocs/gfortran/Hollerith-constants-support.html
            FDECCMP_OPT='-fdec'
        fi
        if [[ "$GCC_MAJOR_VERSION" -lt 12 ]]; then
            FDEC_ADD_MIS_IND_OPT_IF_ANY='-fdec-add-missing-indexes'
        else
            # Not needed anymore, see https://bugzilla.redhat.com/show_bug.cgi?id=2095769#c3
            FDEC_ADD_MIS_IND_OPT_IF_ANY=''
        fi
        rlLogInfo "FDECCMP_OPT=$FDECCMP_OPT"

        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "cp ${TESTPROG}.f90 $TmpDir"
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    rlPhaseStartTest
        rlRun "gfortran $FDEC_ADD_MIS_IND_OPT_IF_ANY -fdec-format-defaults -fdec-blank-format-item $FDECCMP_OPT -g ${TESTPROG}.f90 &> gfortran.out"
        rlAssertNotGrep "unrecognized .*‘-fdec-add-missing-indexes’" gfortran.out
        rlAssertNotGrep "unrecognized .*‘-fdec-format-defaults’" gfortran.out
        rlAssertNotGrep "unrecognized .*‘-fdec-blank-format-item’" gfortran.out
        rlAssertNotGrep "unrecognized .*‘$FDECCMP_OPT’" gfortran.out
        rlGetPhaseState || rlFileSubmit gfortran.out gfortran.out
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
