program foo
        implicit none
        common /bobcom/ bob(2)
!$omp threadprivate (/bobcom/)

        integer i
        real*8 bob

        do i=1,2
        write(*,*) i
        bob(i)=0.0d0
        enddo

        end program


