#include <iostream>
#include <string>
#include <ctime>
#include <sstream>

using namespace std;

locale SetCppLocale()
{
        locale loc;
        locale temp(std::locale::classic(), "en_US.UTF-8", std::locale::collate );
        locale oldLoc = std::locale::global(temp);

        return oldLoc;
}

string DoDateTimeString()
{
        time_t utcTime;
        time(&utcTime);
        struct tm *time = localtime(&utcTime);
        char buffer[300];
        strftime(buffer, 300, "%c", time);
        return string(buffer);
}

int main()
{
        string time = DoDateTimeString();
        cout << "Time in default locale: " << time << endl;

        locale oldLoc = SetCppLocale();
        cout << "Old locale name: " << oldLoc.name() << endl;
        locale newLoc;
        cout << "New locale name: " << newLoc.name() << endl;

        time = DoDateTimeString();
        cout << "Time in hybrid locale: " << time << endl;

        return 0;
}


