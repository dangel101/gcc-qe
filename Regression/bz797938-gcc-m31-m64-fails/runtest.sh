#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz797938-gcc-m31-m64-fails
#   Description: Test for bz797938 (gcc -m31 -m64 fails)
#   Author: Marek Polacek <polacek@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2012 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh
. /usr/lib/beakerlib/beakerlib.sh

# echo "int main () {return 0;}" | gcc -x c /dev/stdin -m31 -m64
# cc1: error: unrecognized command line option "-mesa-mzarch"

PACKAGES=(gcc)

# Choose the compiler.
GCC=${GCC:-gcc}

rlJournalStart
  rlPhaseStartSetup
    for p in "${PACKAGES[@]}"; do
      rlAssertRpm "$p" || yum -y install "$p"
    done; unset p
    rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
    rlRun "pushd $TmpDir"
  rlPhaseEnd

if [[ $(uname -m) == "s390x" ]]; then
  rlPhaseStartTest
    rlRun "echo \"int main () {return 0;}\" | $GCC -x c /dev/stdin -m31 -m64 &> i"
    rlAssertNotGrep "unrecognized command line option" i
  rlPhaseEnd
fi

  rlPhaseStartCleanup
    rlRun "popd"
    rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
  rlPhaseEnd
rlJournalPrintText
rlJournalEnd
