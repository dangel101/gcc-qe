#!/bin/bash

# Copyright (c) 2008, 2012 Red Hat, Inc. All rights reserved.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# 
# Author: Michal Nowak <mnowak@redhat.com>
#	  Marek Polacek <polacek@redhat.com>

# Include rhts environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

GCC="${GCC:-$(type -P gcc)}"
GCC_RPM_NAME=$(rpm --qf '%{name}' -qf $GCC)
PACKAGES="$GCC_RPM_NAME ${GCC_RPM_NAME}-c++"

rlJournalStart
    rlPhaseStartSetup
        rlLogInfo "PACKAGES=$PACKAGES"
        rlLogInfo "COLLECTIONS=$COLLECTIONS"
        rlLogInfo "GCC=$GCC"
        rlLogInfo "SKIP_COLLECTION_METAPACKAGE_CHECK=$SKIP_COLLECTION_METAPACKAGE_CHECK"

        # We optionally need to skip checking for the presence of the metapackage
        # because that would pull in all the dependent toolset subrpms.  We do not
        # always want that, especially in CI.
        _COLLECTIONS="$COLLECTIONS"
        if ! test -z $SKIP_COLLECTION_METAPACKAGE_CHECK; then
            for c in $SKIP_COLLECTION_METAPACKAGE_CHECK; do
                rlLogInfo "ignoring metapackage check for collection $c"
                export COLLECTIONS=$(shopt -s extglob && echo ${COLLECTIONS//$c/})
            done
        fi
        rlLogInfo "(without skipped) COLLECTIONS=$COLLECTIONS"
        rlAssertRpm --all
        export COLLECTIONS="$_COLLECTIONS"

        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "cp ansidecl.h  config.h regex.c  reproducer.c wmemchr.c  xregex2.h  xregex.h $TmpDir"
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    # C++ code part
    rlPhaseStartTest '[reproducer.c] Compile the bogus test case'
        rlRun 'g++ -D__CORRECT_ISO_CPP_PROTO -Wall -W -pedantic -Wcast-qual reproducer.c -o reproducer' 1
    rlPhaseEnd

    # bug in wmemchr() -- string.h
    # https://bugzilla.redhat.com/show_bug.cgi?id=458862#c24
    rlPhaseStartTest '[wmemchr.c] Compile the test case'
        rlRun 'g++ wmemchr.c -o wmemchr -D__CORRECT_ISO_CPP_PROTO'
    rlPhaseEnd

    # C code part. Note: Workarounds used.
    # - *.h and regex.* files "stolen" from binutils-2.18.50.0.9-7.fc10
    # - there might be problems with the pregenerated (i386) `config.h' file
    #   but it can be easily avoided by omiting -DHAVE_CONFIG_H
    # - without -D__CORRECT_ISO_CPP_PROTO
    rlPhaseStartTest '[regex.c] Compile the test case'
        rlRun 'gcc -c regex.c -o regex.o -I. -DHAVE_CONFIG_H'
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun popd
        rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
