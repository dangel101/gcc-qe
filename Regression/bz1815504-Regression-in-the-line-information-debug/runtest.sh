#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz1815504-Regression-in-the-line-information-debug
#   Description: Test for BZ#1815504 (Regression in the line information debug)
#   Author: Alexandra Hájková <ahajkova@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2021 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

rlJournalStart
    rlPhaseStartSetup
        rlRun "VALGRIND_BIN=$(type -P valgrind)"

        # Make sure we use the primary architecture valgrind
        rlRun "VALGRIND_RPM_NAME=$(rpm -qf --qf='%{NAME}\n' $VALGRIND_BIN | head -n 1)"
        rlRun "ARCH=$(rlGetPrimaryArch)"
        if rpm -q $VALGRIND_RPM_NAME.$ARCH &>/dev/null; then
            rlRun "yum -y reinstall $VALGRIND_RPM_NAME.$ARCH" 0-255
        else
            rlRun "yum -y install $VALGRIND_RPM_NAME.$ARCH"
        fi
        rlLogInfo "valgrind RPM is $(rpm -qf $VALGRIND_BIN)"

        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    rlPhaseStartTest
        rlRun 'valgrind -q date &> valgrind.log'
        rlAssertNotGrep "Can't handle inlined call info entry with line number" valgrind.log
        rlGetPhaseState || rlFileSubmit valgrind.log valgrind.log
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
