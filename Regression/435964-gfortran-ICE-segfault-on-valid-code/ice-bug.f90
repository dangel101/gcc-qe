MODULE modone 
      INTEGER varone
      END MODULE modone

      MODULE modtwo
      INTEGER,DIMENSION(:),ALLOCATABLE :: vartwo 
      END MODULE modtwo

      MODULE modthree
      USE modone,       ONLY : varone
      CONTAINS
      SUBROUTINE subone(varthree)
      REAL :: varthree(varone)
      END SUBROUTINE subone
      END MODULE modthree


      MODULE my_module
      USE modone, ONLY : varone
      USE modthree,ONLY : subone
      USE modtwo,    ONLY : vartwo
      CONTAINS
      SUBROUTINE my_sub
      END SUBROUTINE my_sub
      END MODULE my_module

