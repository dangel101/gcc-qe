#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz1104812-libgcc-unwind-c-c-never-uses-Unwind-GetIPInfo
#   Description: Test for BZ#1104812 (libgcc unwind-c.c never uses _Unwind_GetIPInfo)
#   Author: Michael Petlan <mpetlan@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2018 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

GCC="${GCC:-$(type -P gcc)}"
GCC_RPM_NAME=$(rpm --qf '%{name}' -qf $GCC)

PRI_ARCH=$(rlGetPrimaryArch)
SEC_ARCH=$(rlGetSecondaryArch)

# libgcc.ppc as a secondary architecture library is a special case,
# the test doesn't really apply => exclude it
if [[ -n "$SEC_ARCH" ]] && [[ "$SEC_ARCH" != ppc ]]; then
    LIBGCC_RPMS="libgcc.$PRI_ARCH libgcc.$SEC_ARCH"
else
    LIBGCC_RPMS="libgcc.$PRI_ARCH"
fi

PACKAGES="$GCC_RPM_NAME.$PRI_ARCH ${GCC_RPM_NAME%gcc}binutils $LIBGCC_RPMS"

rlJournalStart
    rlPhaseStartSetup
        rlLogInfo "PACKAGES=$PACKAGES"
        rlLogInfo "COLLECTIONS=$COLLECTIONS"
        rlLogInfo "GCC=$GCC"
        rlLogInfo "SKIP_COLLECTION_METAPACKAGE_CHECK=$SKIP_COLLECTION_METAPACKAGE_CHECK"

        # We optionally need to skip checking for the presence of the metapackage
        # because that would pull in all the dependent toolset subrpms.  We do not
        # always want that, especially in CI.
        _COLLECTIONS="$COLLECTIONS"
        if ! test -z $SKIP_COLLECTION_METAPACKAGE_CHECK; then
            for c in $SKIP_COLLECTION_METAPACKAGE_CHECK; do
                rlLogInfo "ignoring metapackage check for collection $c"
                export COLLECTIONS=$(shopt -s extglob && echo ${COLLECTIONS//$c/})
            done
        fi
        rlLogInfo "(without skipped) COLLECTIONS=$COLLECTIONS"
        rlAssertRpm --all
        export COLLECTIONS="$_COLLECTIONS"

        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    for lib in $LIBGCC_RPMS; do

        rlPhaseStartTest $lib

            rlRun "rpm -ql $lib >rpm_files.txt"
            rlRun 'grep "s\.so\.[0-9]*$" <rpm_files.txt >so_files.txt'
            for file in $(<so_files.txt); do
                rlRun "objdump --disassemble --reloc $file >objdump.txt"
                rlRun 'grep _Unwind_GetIP <objdump.txt >_Unwind_GetIP.txt'
                rlRun 'grep plt <_Unwind_GetIP.txt >plt.txt'
                rlAssertGrep _Unwind_GetIPInfo plt.txt
                rlAssertNotGrep _Unwind_GetIP@ plt.txt
            done

        rlPhaseEnd

    done

    rlPhaseStartCleanup
        rlRun popd
        rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
