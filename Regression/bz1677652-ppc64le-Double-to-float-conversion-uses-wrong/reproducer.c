void
f (double d, char *target)
{
  float f = d;
  __builtin_memcpy (target, &f, sizeof (f));
}

int main(void)
{
    double d;
    char target;

    f(d, &target);

    return 0;
}
