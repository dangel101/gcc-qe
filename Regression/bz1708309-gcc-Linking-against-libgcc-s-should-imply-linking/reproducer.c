static int
implementation_avx2 (void)
{
  return 1;
}

static int
implementation (void)
{
  return 0;
}

static __typeof__ (implementation) * resolver (void) __asm__ ("resolver");

static __typeof__ (implementation) *
resolver (void)
{
  __builtin_cpu_init ();
  if (__builtin_cpu_supports ("avx2"))
    return implementation_avx2;
  else
    return implementation;
}

int magic (void) __attribute__ ((ifunc ("resolver")));
