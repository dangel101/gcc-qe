#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz804963-Enable-build-id-ld-flag-by-default-in-DTS-v1-0
#   Description: Test for bz804963 (Enable --build-id ld flag by default in DTS v1.0)
#   Author: Marek Polacek <polacek@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2012 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh
. /usr/lib/beakerlib/beakerlib.sh

PACKAGES=(gcc binutils grep)

# Choose the compiler.
GCC=${GCC:-gcc}

# This is for RHEL5 only and toolset GCC only!
# We test whether --build-id flag is enabled by default
# when DTS gcc is used on RHEL5.  This includes an ELF section
# (.note.gnu.build-id).

rlJournalStart
  rlPhaseStartSetup
    # Check for needed packages.
    for p in "${PACKAGES[@]}"; do
      rlAssertRpm "$p" || yum -y install "$p"
    done; unset p
    rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
    rlRun "pushd $TmpDir"
  rlPhaseEnd

if [[ $($GCC -dumpversion) > "4.7" ]]; then
  rlPhaseStartTest
    # Ok, let's hope we're using the toolset gcc.
    # So, no scl enable devtoolset-1.0 'bash' necessary.
    rlRun "$GCC -xc - -ox <<< \"int main(void) { }\""
    rlAssertExists "x"
    rlRun "readelf -t x | grep build-id"
    rlRun "$GCC -xc - -oy -Wl,--build-id <<< \"int main(void) { }\""
    rlAssertExists "y"
    rlRun "readelf -t y | grep build-id"
  rlPhaseEnd
fi

  rlPhaseStartCleanup
    rlRun "popd"
    rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
  rlPhaseEnd
rlJournalPrintText
rlJournalEnd
