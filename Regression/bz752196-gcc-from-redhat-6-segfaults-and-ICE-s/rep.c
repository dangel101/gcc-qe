extern long *fun_1(void);

static void *fun_2(long *p)
{
    union {
        struct {
            unsigned pos1 : 6;
            unsigned pos2 : 26;
        };
        int v;
    } u;

    u.v = 0;
    if (!(p[u.pos2] & (1UL << u.pos1))) {
        fun_3();
        p[u.pos2] |= 1UL << u.pos1;
    }
}

void *fun_4(void *set)
{
    long *en;

    for (en = fun_1();;) {
        fun_2(en);
    }
}
