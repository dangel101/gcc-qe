#!/bin/bash
#
# [idea]    Author: Petr Muller <pmuller@redhat.com>
# [rewrite] Author: Michal Nowak <mnowak@redhat.com>
# [another rewrite] Marek Polacek <polacek@redhat.com>

# Include Beaker environment
. /usr/bin/rhts-environment.sh
. /usr/lib/beakerlib/beakerlib.sh

PACKAGES=(gcc binutils)

# Choose the compiler.
GCC=${GCC:-gcc}

rlJournalStart
    rlPhaseStartSetup
        for p in "${PACKAGES[@]}"; do
            rlAssertRpm "$p"
        done; unset p
        rlRun "TmpDir=\`mktemp -d\`" 0 "Creating tmp directory"
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    rlPhaseStartTest
	# Bug 147296: incorrect RWE stack marking in libgcc
	echo '.section .note.GNU-stack,"",@progbits' | as -o a.o -
	echo '.section .note.GNU-stack,"",@progbits' | as -o b.o -
	ld -r -o c.o [ab].o
	# no-X : good
	#    X : bad 
	readelf -WS [abc].o | grep .note.GNU-stack
	rlRun "readelf -WS [abc].o | grep .note.GNU-stack | grep -q \"X \"" 1 "no problem with libgcc"
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
