__get_cpuid_max (unsigned int __ext, unsigned int *__sig) {
  unsigned __edx;
  __cpuid (0, 0, 0, 0, __edx);
}
int __get_cpuid (unsigned int __level, unsigned int *__eax, unsigned int *__ebx, unsigned int *__ecx, unsigned int *__edx) {
fail: return 0;
}
typedef int IV __attribute__((vector_size(16)));
typedef union {
  int s;
  IV v;
} U;
static U i[2], b, c;
void check0(void) {
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  4, 0, 0, 0});
  (memcmp (&b, &c, sizeof(c)) == 0) || (__builtin_trap (), 0);
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  5, 0, 0, 0});
  (memcmp (&b, &c, sizeof(c)) == 0) || (__builtin_trap (), 0);
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  6, 0, 0, 0});
  (memcmp (&b, &c, sizeof(c)) == 0) || (__builtin_trap (), 0);
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  7, 0, 0, 0});
  (memcmp (&b, &c, sizeof(c)) == 0) || (__builtin_trap (), 0);
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  4, 1, 0, 0});
  (memcmp (&b, &c, sizeof(c)) == 0) || (__builtin_trap (), 0);
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  5, 1, 0, 0});
  (memcmp (&b, &c, sizeof(c)) == 0) || (__builtin_trap (), 0);
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  6, 1, 0, 0});
  (memcmp (&b, &c, sizeof(c)) == 0) || (__builtin_trap (), 0);
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  7, 1, 0, 0});
  (memcmp (&b, &c, sizeof(c)) == 0) || (__builtin_trap (), 0);
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  0, 4, 0, 0});
  (memcmp (&b, &c, sizeof(c)) == 0) || (__builtin_trap (), 0);
}
void check2(void) {
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  0, 0, 4, 0});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  1, 0, 4, 0});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  4, 0, 4, 0});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  5, 0, 4, 0});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  0, 1, 4, 0});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  1, 1, 4, 0});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  4, 1, 4, 0});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  5, 1, 4, 0});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  0, 0, 5, 0});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  1, 0, 5, 0});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  4, 0, 5, 0});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  5, 0, 5, 0});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  0, 1, 5, 0});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  1, 1, 5, 0});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  4, 1, 5, 0});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  5, 1, 5, 0});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  0, 4, 5, 0});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  1, 4, 5, 0});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  4, 4, 5, 0});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  5, 4, 5, 0});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  0, 5, 5, 0});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  1, 5, 5, 0});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  4, 5, 5, 0});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  5, 5, 5, 0});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  0, 0, 6, 0});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  1, 0, 6, 0});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  6, 0, 6, 0});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  7, 0, 6, 0});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  0, 1, 6, 0});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  1, 1, 6, 0});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  6, 1, 6, 0});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  7, 1, 6, 0});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  0, 6, 6, 0});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  1, 6, 6, 0});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  6, 6, 6, 0});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  7, 6, 6, 0});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  0, 7, 6, 0});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  1, 7, 6, 0});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  6, 7, 6, 0});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  7, 7, 6, 0});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  0, 0, 7, 0});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  1, 0, 7, 0});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  6, 0, 7, 0});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  7, 0, 7, 0});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  0, 1, 7, 0});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  1, 1, 7, 0});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  6, 1, 7, 0});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  7, 1, 7, 0});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  0, 6, 7, 0});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  1, 6, 7, 0});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  6, 6, 7, 0});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  4, 3, 4, 4});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  5, 3, 4, 4});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  0, 4, 4, 4});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  1, 4, 4, 4});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  2, 4, 4, 4});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  3, 4, 4, 4});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  0, 5, 4, 4});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  1, 5, 4, 4});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  2, 5, 4, 4});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  3, 5, 4, 4});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  4, 0, 5, 4});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  5, 0, 5, 4});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  4, 1, 5, 4});
  b.v = __builtin_shuffle (i[0].v, i[1].v, (IV){  5, 1, 5, 4});
}

