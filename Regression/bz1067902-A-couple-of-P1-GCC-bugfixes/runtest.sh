#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz1067902-A-couple-of-P1-GCC-bugfixes
#   Description: Test for BZ#1067902 (A couple of P1 GCC bugfixes)
#   Author: Dagmar Prokopova <dprokopo@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2014 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

# Relevant for GCC 4.8+. Suggested TCMS relevancy:
#   distro < rhel-7 && collection !defined: False

GCC=${GCC:-$(type -P gcc)}
GCC_RPM_NAME=$(rpm --qf '%{name}' -qf $GCC)

[[ "$GCC_RPM_NAME" == *toolset* ]] && TOOLSET=${GCC_RPM_NAME%-gcc} || TOOLSET=''

PRI_ARCH=$(rlGetPrimaryArch)
SEC_ARCH=$(rlGetSecondaryArch)

# State applicable -m<bits> switches
SWITCHES='-m64 -m32'
case "$PRI_ARCH" in
    ppc64le) # we never had 32 support there
        SWITCHES=-m64
        ;;
    aarch64)
        # Not only we never had 32-bit support there, GCC on this architecture
        # doesn't accept the -m64 switch either.
        SWITCHES=''
        ;;
    ppc64|s390x) # 32-bit support present only in system GCC of RHEL <8
        if [[ -z "$TOOLSET" ]] && rlIsRHEL '<8'; then
            if [[ "$PRI_ARCH" != s390x ]]; then
                SWITCHES='-m64 -m32'
            else
                SWITCHES='-m64 -m31'
            fi
        else
            SWITCHES=-m64
            SEC_ARCH='' # Needed in case of RHEL <=7 and DTS
        fi
        ;;
esac

# State required packages
PACKAGES="$GCC_RPM_NAME"
for arch in $PRI_ARCH $SEC_ARCH; do
    PACKAGES+=" glibc-devel.$arch"
done

rlJournalStart
    rlPhaseStartSetup
        rlLogInfo "PACKAGES=$PACKAGES"
        rlLogInfo "COLLECTIONS=$COLLECTIONS"
        rlLogInfo "GCC=$GCC"
        rlLogInfo "SKIP_COLLECTION_METAPACKAGE_CHECK=$SKIP_COLLECTION_METAPACKAGE_CHECK"

        # We optionally need to skip checking for the presence of the metapackage
        # because that would pull in all the dependent toolset subrpms.  We do not
        # always want that, especially in CI.
        _COLLECTIONS="$COLLECTIONS"
        if ! test -z $SKIP_COLLECTION_METAPACKAGE_CHECK; then
            for c in $SKIP_COLLECTION_METAPACKAGE_CHECK; do
                rlLogInfo "ignoring metapackage check for collection $c"
                export COLLECTIONS=$(shopt -s extglob && echo ${COLLECTIONS//$c/})
            done
        fi
        rlLogInfo "(without skipped) COLLECTIONS=$COLLECTIONS"
        rlAssertRpm --all
        export COLLECTIONS="$_COLLECTIONS"

        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "cp test?.c $TmpDir"
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    rlPhaseStartTest

        # test1.c applies to any architecture
        for switch in $SWITCHES; do
           rlRun "gcc -O0 $switch -c test1.c"
        done

        # test2.c is x86_64-specific
        if [[ "$PRI_ARCH" = x86_64 ]]; then
            for switch in $SWITCHES; do
                rlRun "gcc -O0 $switch -c test2.c"
            done
        fi

    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun popd
        rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
