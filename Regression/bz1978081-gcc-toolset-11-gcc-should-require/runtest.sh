#!/usr/bin/env bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz1978081-gcc-toolset-11-gcc-should-require
#   Description: Test for BZ#1978081 (gcc-toolset-11-gcc should require)
#   Author: Vaclav Kadlcik <vkadlcik@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2021 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

# The test as is implemented should be relevant for any GCC. However when
# being run on RHEL-7 with system GCC and binutils, the attempt to remove
# binutils fails because systemd, which is protected, depends on it.
# Since system GCC and binutils of RHEL-7 aren't under active development
# any more, let just disable the test there.
#
# Suggested TCMS relevancy:
#   distro < rhel-8 && collection !defined: False

GCC="${GCC:-$(type -P gcc)}"
PACKAGE=$(rpm --qf '%{name}' -qf $GCC)
PACKAGES="$PACKAGE"

rlJournalStart
    rlPhaseStartSetup
        rlImport distribution/RpmSnapshot || rlFail rlImport

        rlLogInfo "PACKAGES=$PACKAGES"
        rlLogInfo "COLLECTIONS=$COLLECTIONS"
        rlLogInfo "GCC=$GCC"
        rlLogInfo "SKIP_COLLECTION_METAPACKAGE_CHECK=$SKIP_COLLECTION_METAPACKAGE_CHECK"

        # We optionally need to skip checking for the presence of the metapackage
        # because that would pull in all the dependent toolset subrpms.  We do not
        # always want that, especially in CI.
        _COLLECTIONS="$COLLECTIONS"
        if ! test -z $SKIP_COLLECTION_METAPACKAGE_CHECK; then
            for c in $SKIP_COLLECTION_METAPACKAGE_CHECK; do
                rlLogInfo "ignoring metapackage check for collection $c"
                export COLLECTIONS=$(shopt -s extglob && echo ${COLLECTIONS//$c/})
            done
        fi
        rlLogInfo "(without skipped) COLLECTIONS=$COLLECTIONS"
        rlAssertRpm --all
        export COLLECTIONS="$_COLLECTIONS"

        RpmSnapshotCreate

        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    rlPhaseStartTest
        BINUTILS_PACKAGE=${PACKAGE%gcc}binutils

        # Let's try to have gcc without the corresponding binutils. Multiple
        # ways to do it, the easiest one is removing binutils and then adding
        # gcc, IHMO. We may end up with or without the binutils. We don't care.
        rlRun "yum -y erase $BINUTILS_PACKAGE"
        rpm -q $PACKAGE &>/dev/null || rlRun "yum -y install $PACKAGE"
        for i in $(rpm -qa --qf='%{NAME}\n' | grep -e '^binutils$' -e '.*toolset.*-binutils$'); do
            rlLogInfo "Present: $(rpm -q $i)"
        done
        rlLogInfo "Path of as: $(type -P as)"

        # Regardless the binutils, gcc should be able to compile. Let's see.
        rlRun 'echo >p.c'
        rlRun 'gcc -g -c p.c &>gcc.out'
        rlAssertNotGrep 'as: unrecognized option.*gdwarf' gcc.out
        rlGetPhaseState || rlFileSubmit gcc.out gcc.out
    rlPhaseEnd

    rlPhaseStartCleanup
        RpmSnapshotRevert
        RpmSnapshotDiscard

        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
