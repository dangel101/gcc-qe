struct S
{
  void *a;
  unsigned int b;
};

extern struct S *c;

void
foo (struct S **x, int y)
{
  while (y)
    {
      c = x[--y];
      if (c)
        ++c->b;
    }
}
