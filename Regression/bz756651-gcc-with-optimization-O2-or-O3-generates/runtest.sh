#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz756651-gcc-with-optimization-O2-or-O3-generates
#   Description: Test for bz756651 (gcc with optimization -O2 or -O3 generates)
#   Author: Marek Polacek <polacek@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2012 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# This is fixed in version gcc-4.4.6-4.el6.
# 	PR rtl-optimization/44858
#	* combine.c (try_combine): If recog_for_combine added CLOBBERs to
#	newi2pat, make sure they don't affect newpat.
#	PR rtl-optimization/45695
#	* combine.c (try_combine): When splitting a two set pattern,
#	make sure the pattern which will be put into i2 doesn't use REGs
#	or MEMs set by insns in between i2 and i3.

# Include Beaker environment
. /usr/bin/rhts-environment.sh
. /usr/lib/beakerlib/beakerlib.sh

PACKAGES=(gcc)

# Choose the compiler.
GCC=${GCC:-gcc}

rlJournalStart
  rlPhaseStartSetup
    # Check for needed packages.
    for p in "${PACKAGES[@]}"; do
      rlAssertRpm "$p" || yum -y install "$p"
    done; unset p
    rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
    # We need the reproducer.
    cp -v rep.c pr44858.c pr45695.c $TmpDir
    rlRun "pushd $TmpDir"
   rlPhaseEnd

  rlPhaseStartTest
    # We have three reproducers.  The fail happened with -O2 and -O3, but
    # we compile all reproducers with all optimization levels (well, not
    # with -Ofast, as it is gcc >= 4.6.
    for opt in 0 1 2 3 s; do
      # Check rep.c.
      rlRun "$GCC rep.c -O$opt -o rep_$opt"
      rlRun "./rep_$opt"
      # Check pr44858.c.
      rlRun "$GCC pr44858.c -O$opt -o pr44858_$opt"
      rlRun "./pr44858_$opt"
      # Check pr45695.c.
      rlRun "$GCC pr45695.c -O$opt -o pr45695_$opt"
      rlRun "./pr45695_$opt"
    done
  rlPhaseEnd

  rlPhaseStartCleanup
    rlRun "popd"
    rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
  rlPhaseEnd
rlJournalPrintText
rlJournalEnd
