/* Derived from testcase in #756651.  --polacek  */

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
char *mRealTree = NULL;
char *mBlob = NULL;

enum DataType { kTREE=1,    
                kBLOB=2 };

int GetDataTypes() 
{
   int res = 0;
   if (mRealTree) res |= kTREE;
   if (mBlob) res |= kBLOB;
   return res;
}

bool IsTree()  { return (GetDataTypes() & kTREE) != 0; }
bool IsBlob()  { return (GetDataTypes() & kBLOB) != 0; }
bool IsNone()  { return (GetDataTypes() == 0); }

int main(int argc, const char **argv)
{
  if (IsBlob())
    return 1;
  else if (IsTree())
     return 1;
  else
    return 0;
}
