#include <rfb/rfb.h>
#define COUNT_OFFSETS 4

static void
rfbTranslateWithRGBTables24to24 (char *table, rfbPixelFormat *in,
                                  rfbPixelFormat *out,
                                  char *iptr, char *optr,
                                  int bytesBetweenInputLines,
                                  int width, int height)
{
    uint8_t *ip = NULL;
    uint8_t *op = NULL;
    uint8_t *opLineEnd;
    uint8_t *redTable = (uint8_t *)table;
    uint8_t *greenTable = redTable + 3*(in->redMax + 1);
    uint8_t *blueTable = greenTable + 3*(in->greenMax + 1);
    uint32_t outValue,inValue;
    int shift = rfbEndianTest?0:8;

    while (height > 0) {
        opLineEnd = op+3*width;

        while (op < opLineEnd) {
            inValue = ((*(uint32_t *)ip)>>shift)&0x00ffffff;
            outValue = (redTable[(inValue >> in->redShift) & in->redMax] |
                       greenTable[(inValue >> in->greenShift) & in->greenMax] |
                       blueTable[(inValue >> in->blueShift) & in->blueMax]);
            memcpy(op,&outValue,3);
        }
    }
}

void
foo(rfbClientPtr cl)
{
    cl->translateFn = rfbTranslateWithRGBTables24to24;
}
