      program recurse
      call cleanstack(42, 42)
      call recursive(42,0)
      call cleanstack(42, 42)
      call recursive(0,5)
      call cleanstack(42, 42)
      call recursive(0,5)
      end

      subroutine recursive(setup, count)
      integer*4 setup
      integer*4 count
      integer*4 setupcopy
      integer*4 countcopy
      integer*4 something
      automatic countcopy,something
      equivalence (countcopy, something)
      countcopy = count

      if (setup .gt. 0) setupcopy = setup
      print *, " > setup = ", setupcopy, " count = ", countcopy
      if (count .gt. 0) call recursive(setupcopy - 1, count - 1)
      print *, " < setup = ", setupcopy, " count = ", countcopy
      end

      subroutine cleanstack(dummy1, dummy2)
      integer*4 dummy1
      integer*4 dummy2
      integer*4 dummy3
      integer*4 dummy4

      dummy3 = dummy1
      dummy4 = dummy2

      print *, " = clean ", dummy1, "  ", dummy2
      end
