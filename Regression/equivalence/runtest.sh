#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/equivalence
#   Description: Compiles a Bloomberg's reproducer
#   Author: Michael Petlan <mpetlan@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2018 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

# Notes on relevancy
#
# This test came from Bloomberg and tests a feature that, IIUC, is present
# only in GCCs they explicitely required to have it. It's
#   * devtoolset-7 GCCs and newer (now it's all currently supported)
#   * all gcc-toolset GCCs
#   * base GCC of RHEL-9+
#
# Further, test needs an appropriate libgfortran* library but in some cases
# it's not available. For example, newer devtoolset GCCs need (base)
# libgfortran5 that is not provided on RHEL-7 below 7.5 and RHEL-6 below 6.10.
#
# Suggested TCMS relevancy:
#   collection !defined && distro < rhel-9: False
#   collection defined && distro = rhel-7 && distro < rhel-7.5: False
#   collection defined && distro = rhel-6 && distro < rhel-6.10: False

PACKAGE="gcc"
GFORTRAN=${GFORTRAN:-gfortran}

rlJournalStart
	rlPhaseStartSetup
		rlAssertRpm $PACKAGE
		rlAssertRpm $PACKAGE-gfortran
		rlLog "Using $GFORTRAN from package: `rpmquery -f $(which $GFORTRAN)`"
		DUMPVERSION=$(gcc -dumpversion)
		rlLogInfo "DUMPVERSION=$DUMPVERSION"
	rlPhaseEnd

	rlPhaseStartTest
		if rlTestVersion $DUMPVERSION '<' 7; then
			rlLogError "Too old gcc version=$DUMPVERSION to test"
		elif rlTestVersion $DUMPVERSION '<' 9; then
			rlRun "$GFORTRAN eq.f -std=extra-legacy -fdec-static -frecursive -fno-automatic -o eq"
		elif rlTestVersion $DUMPVERSION '<' 12; then
			rlRun "$GFORTRAN eq.f -fdec-sequence -fdec-static -frecursive -fno-automatic -o eq"
		else
			# See https://bugzilla.redhat.com/show_bug.cgi?id=2095769#c3
			rlRun "$GFORTRAN eq.f -fdec-static -frecursive -fno-automatic -o eq"
		fi
		rlAssertExists "./eq"
		rlRun "./eq > eq.out" 0 "Running the testcase"
		cat eq.out
		rlAssertNotDiffer "eq.out" "eq.golden"
	rlPhaseEnd

	rlPhaseStartCleanup
		rlRun "rm -f eq eq.out" 0 "Removing temp files"
	rlPhaseEnd
rlJournalPrintText
rlJournalEnd
