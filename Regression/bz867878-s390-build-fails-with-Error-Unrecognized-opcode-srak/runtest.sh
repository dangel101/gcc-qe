#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz867878-s390-build-fails-with-Error-Unrecognized-opcode-srak
#   Description: Test for BZ#867878 (s390 build fails with Error Unrecognized opcode)
#   Author: Dagmar Prokopova <dprokopo@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2012 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh
. /usr/share/beakerlib/beakerlib.sh

PACKAGES=(gcc gcc-c++)

# Choose the compilers.
GCC=${GCC:-gcc}
GXX=${GXX:-g++}

rlJournalStart
  rlPhaseStartSetup
    # Check for needed packages.
    for pack in "${PACKAGES[@]}"; do
      rlAssertRpm "$pack" || yum -y install "$pack"
    done; unset pack
    rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
    # We need the reproducer.
    cp -v "test.c" $TmpDir
    rlRun "pushd $TmpDir"
  rlPhaseEnd

  rlPhaseStartTest
    # Compile the original reproducer.  It should compile successfully in both cases.
    rlRun "$GCC -c -O2 -march=z9-109 test.c -o test1.o"
    rlRun "$GCC -c -O2 -march=z196 test.c -o test2.o"
  rlPhaseEnd

  rlPhaseStartCleanup
    rlRun "popd"
    rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
  rlPhaseEnd
rlJournalPrintText
rlJournalEnd

