int bar (char);

void
foo (char *x, unsigned int y)
{
  unsigned int i;
  for (i = 0; i < y; i++)
    if (bar (x[i]) || ((1 << (x[i] & 0x7f)) & 0x2314))
      break;
}

void main(){}
