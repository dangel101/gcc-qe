/*
  File: pkgPoint.cc
  Creation date: May 1997
  Author: Xiaojun Zhu
  Notes:
  
  Copyright (C) 1997
  Cadence Design Systems Inc.  All Rights Reserved.
*/



#include "pkgPoint.hh"

double pkgPoint::x() const{
  return _x;
}

double pkgPoint::y() const{
  return _y;
}

double pkgPoint::z() const{
  return _z;
}

// constructor
pkgPoint::pkgPoint(const pkgPoint& p) {
  _x = p.x();
  _y = p.y();
  _z = p.z();
}

pkgPoint::pkgPoint(double xp, double yp, double zp) {
  _x = xp;
  _y = yp;
  _z = zp;
}

pkgPoint::pkgPoint() {
  _x = 0;
  _y = 0;
  _z = 0;
}

pkgPoint::~pkgPoint() {
};

void pkgPoint::SetX(double xs) {
  _x = xs;
}

void pkgPoint::SetY(double ys) {
  _y = ys;
}

void pkgPoint::SetZ(double zs) {
  _z = zs;
}

/**********************************************************/
// REVISIT: These operators need further check
pkgPoint& pkgPoint::operator += (const pkgPoint& p) {
  _x += p.x();
  _y += p.y();
  _z += p.z();
  return *this;
}

pkgPoint& pkgPoint::operator -= (const pkgPoint& p) {
  _x -= p.x();
  _y -= p.y();
  _z -= p.z();
  return *this;
}

pkgPoint& pkgPoint::operator *= (const double c) {
  _x *= c;
  _y *= c;
  _z *= c;
  return *this;
}

/**********************************************************/

pkgPoint& pkgPoint::operator = (const pkgPoint& p) {
  this->SetX(p.x());
  this->SetY(p.y());
  this->SetZ(p.z());
  return *this;
}

pkgPoint operator + (const pkgPoint& p1, const pkgPoint& p2) {
  return pkgPoint(p1.x()+p2.x(), p1.y()+p2.y(), p1.z()+p2.z());
}

pkgPoint operator - (const pkgPoint& p1, const pkgPoint& p2) {
  return pkgPoint(p1.x()-p2.x(), p1.y()-p2.y(), p1.z()-p2.z());
}

pkgPoint  cross(const pkgPoint& p1, const pkgPoint& p2) {
  double x = p1.y()*p2.z() - p1.z()*p2.y();
  double y = p1.z()*p2.x() - p1.x()*p2.z();
  double z = p1.x()*p2.y() - p1.y()*p2.x();
  return pkgPoint(x, y, z);
}

pkgPoint operator * (const pkgPoint& p, const double c) {
  return pkgPoint(p.x()*c, p.y()*c, p.z()*c);
}

double pkgPoint::Mag() {
  return sqrt(_x*_x + _y*_y + _z*_z);
}


// This function converts the local coordinates to the 
// global coordinates using the two reference points and the 
// package side information
void  pntL2G(pkgPoint *pntNormal, pkgPoint *pntRef, pkgPoint *pnt) {
  double x, y, z;
  SIDE pkgSide;
  if(pntNormal->y() == -1)
    pkgSide = LOWER;
  else if(pntNormal->y() == 1) 
    pkgSide = UPPER;
  else if(pntNormal->x() == -1)
    pkgSide = LEFT;
  else if(pntNormal->x() == 1)
    pkgSide = RIGHT;

  switch (pkgSide) {
  case LOWER:
    x = pntRef->x() + pnt->x();
    y = pntRef->y() + pnt->y();
    z = pnt->z();
    break;
  case UPPER:
    x = pntRef->x() - pnt->x();
    y = pntRef->y() - pnt->y();
    z = pnt->z();
    break;
  case LEFT:
    x = pntRef->x() + pnt->y();
    y = pntRef->y() - pnt->x();
    z = pnt->z();
    break;
  case RIGHT:
    x = pntRef->x() - pnt->y();
    y = pntRef->y() + pnt->x();
    z = pnt->z();
    break;
  default:
    fprintf(stderr, "unknow option\n");
    break;
  }
  
  pnt->SetX(x);
  pnt->SetY(y);
  pnt->SetZ(z);
}
