#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/g++/bz548384-m32-O2-ICE-in-emit_swap_insn-at-reg-stack-c-827
#   Description: -m32 -O2 ICE in emit_swap_insn, at reg-stack.c:827
#   Author: Michal Nowak <mnowak@redhat.com>
#	    Marek Polacek <polacek@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2010, 2012 Red Hat, Inc. All rights reserved.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 3 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include rhts environment
. /usr/bin/rhts-environment.sh
. /usr/lib/beakerlib/beakerlib.sh

PACKAGES=(gcc gcc-c++)
TEST_32BIT=${TEST_32BIT:-no}

# Choose the compiler.
GXX=${GXX:-g++}

rlJournalStart
    rlPhaseStartSetup
        for p in "${PACKAGES[@]}"; do
            rlAssertRpm "$p"
        done; unset p
        rlRun "TmpDir=\`mktemp -d\`" 0 "Creating tmp directory"
	cp pkgPoint.hh pkgPoint.cc $TmpDir
        rlRun "pushd $TmpDir"
	arch=$(uname -i)
	if [ "$TEST_32BIT" = "yes" ]; then
		# for possible 64/32/31-bit archs; otherwise we use defaults
		if [ $arch = "x86_64" ]; then
		  bits="-m32 -m64"
		elif [ $arch = "s390x" ]; then
		  bits="-m31 -m64"
		elif [ $arch = "ppc64" ]; then
		  bits="-m32 -m64"
		elif [ $arch = "i386" -o $arch = "i686" ]; then
		  bits="-m32"
		else
		  bits="-m64"
		fi
	else
		bits="-g" # this is just a dummy switch
	fi

	rlLog "This system: arch=$arch bits='$bits'"
    rlPhaseEnd

    for opt in s $(seq 0 3); do
      for bit in $bits; do
        rlPhaseStartTest "gpp=$gpp opt=$opt bits=$bit"
            rlRun "$GXX -O${opt} ${bit} pkgPoint.cc -c"
            rlAssertExists "pkgPoint.o"
            rm -f ./pkgPoint.o
        rlPhaseEnd
      done
      unset bit
    done

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
