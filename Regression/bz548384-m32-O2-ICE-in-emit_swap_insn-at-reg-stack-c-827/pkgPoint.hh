/*
  File: pkgPoint.H
  Creation date: May 1997
  Author: Xiaojun Zhu
  Notes:
  
  Copyright (C) 1997
  Cadence Design Systems Inc.  All Rights Reserved.
*/



#ifndef PKG_POINT_H
#define PKG_POINT_H
#include <math.h>
#include <stdio.h>

enum SIDE {LOWER, UPPER, LEFT, RIGHT};
class pkgPoint {
private:
  double _x;
  double _y;
  double _z;
public:
  double x() const;
  double y() const;
  double z() const;
  void SetX(double xs);
  void SetY(double ys);
  void SetZ(double zs);
  pkgPoint();
  pkgPoint(double, double, double);
  pkgPoint(const pkgPoint& pnt);
  ~pkgPoint();
  
  pkgPoint& operator += (const pkgPoint& p);
  pkgPoint& operator -= (const pkgPoint& p);
  pkgPoint& operator *= (const double c);
  pkgPoint& operator = (const pkgPoint& p);
  friend pkgPoint operator + (const pkgPoint& p1, const pkgPoint& p2);
  friend pkgPoint operator - (const pkgPoint& p1, const pkgPoint& p2);
  friend pkgPoint operator * (const pkgPoint& p1, const double c);
  friend pkgPoint cross(const pkgPoint& p1, const pkgPoint& p2);
  double Mag();
  friend void pntL2G(pkgPoint *pntNormal, pkgPoint *pntRef, pkgPoint *pnt);
};

#endif

  
  
