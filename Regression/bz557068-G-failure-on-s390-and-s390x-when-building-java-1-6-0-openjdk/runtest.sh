#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz557068-G-failure-on-s390-and-s390x-when-building-java-1-6-0-openjdk
#   Description: In the testcase of this PR, a value equivalence between a REG and a
#   Author: Michal Nowak <mnowak@redhat.com>
#	    Marek Polacek <polacek@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2010, 2012 Red Hat, Inc. All rights reserved.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 3 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# S390x only.

# Include rhts environment
. /usr/bin/rhts-environment.sh
. /usr/lib/beakerlib/beakerlib.sh

PACKAGES=(gcc gcc-c++)

# Choose the compiler.
GXX=${GXX:-g++}


rlJournalStart
    rlPhaseStartSetup
        for p in "${PACKAGES[@]}"; do
            rlAssertRpm "$p"
        done; unset p

        GCC_DUMPVERSION=$(gcc -dumpversion)
        GCC_MAJOR_VERSION=${GCC_DUMPVERSION%%.*}
        [[ "$GCC_MAJOR_VERSION" -gt 10 ]] && ADDITIONAL_PARAMETERS='-std=c++14' || ADDITIONAL_PARAMETERS=''
        rlLogInfo "GCC_DUMPVERSION=$GCC_DUMPVERSION"
        rlLogInfo "GCC_MAJOR_VERSION=$GCC_MAJOR_VERSION"
        rlLogInfo "ADDITIONAL_PARAMETERS=$ADDITIONAL_PARAMETERS"

        rlRun "TmpDir=\$(mktemp -d)"
        cp *.ii $TmpDir
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    rlPhaseStartTest
        rlRun "$GXX $ADDITIONAL_PARAMETERS -save-temps -DLINUX -D_GNU_SOURCE -DCC_INTERP -DZERO -DS390 -DZERO_LIBARCH=\"s390x\" -DPRODUCT -I. -I../generated/adfiles -I../generated/jvmtifiles -I/builddir/build/BUILD/icedtea6-1.6/openjdk-ecj/hotspot/src/share/vm/asm -I/builddir/build/BUILD/icedtea6-1.6/openjdk-ecj/hotspot/src/share/vm/c1 -I/builddir/build/BUILD/icedtea6-1.6/openjdk-ecj/hotspot/src/share/vm/ci -I/builddir/build/BUILD/icedtea6-1.6/openjdk-ecj/hotspot/src/share/vm/classfile -I/builddir/build/BUILD/icedtea6-1.6/openjdk-ecj/hotspot/src/share/vm/code -I/builddir/build/BUILD/icedtea6-1.6/openjdk-ecj/hotspot/src/share/vm/compiler -I/builddir/build/BUILD/icedtea6-1.6/openjdk-ecj/hotspot/src/share/vm/gc_implementation -I/builddir/build/BUILD/icedtea6-1.6/openjdk-ecj/hotspot/src/share/vm/gc_implementation/shared -I/builddir/build/BUILD/icedtea6-1.6/openjdk-ecj/hotspot/src/share/vm/gc_implementation/concurrentMarkSweep -I/builddir/build/BUILD/icedtea6-1.6/openjdk-ecj/hotspot/src/share/vm/gc_implementation/g1 -I/builddir/build/BUILD/icedtea6-1.6/openjdk-ecj/hotspot/src/share/vm/gc_implementation/parNew -I/builddir/build/BUILD/icedtea6-1.6/openjdk-ecj/hotspot/src/share/vm/gc_implementation/parallelScavenge -I/builddir/build/BUILD/icedtea6-1.6/openjdk-ecj/hotspot/src/share/vm/gc_interface -I/builddir/build/BUILD/icedtea6-1.6/openjdk-ecj/hotspot/src/share/vm/interpreter -I/builddir/build/BUILD/icedtea6-1.6/openjdk-ecj/hotspot/src/share/vm/memory -I/builddir/build/BUILD/icedtea6-1.6/openjdk-ecj/hotspot/src/share/vm/oops -I/builddir/build/BUILD/icedtea6-1.6/openjdk-ecj/hotspot/src/share/vm/prims -I/builddir/build/BUILD/icedtea6-1.6/openjdk-ecj/hotspot/src/share/vm/runtime -I/builddir/build/BUILD/icedtea6-1.6/openjdk-ecj/hotspot/src/share/vm/services -I/builddir/build/BUILD/icedtea6-1.6/openjdk-ecj/hotspot/src/share/vm/shark -I/builddir/build/BUILD/icedtea6-1.6/openjdk-ecj/hotspot/src/share/vm/utilities -I/builddir/build/BUILD/icedtea6-1.6/openjdk-ecj/hotspot/src/cpu/zero/vm -I/builddir/build/BUILD/icedtea6-1.6/openjdk-ecj/hotspot/src/os/linux/vm -I/builddir/build/BUILD/icedtea6-1.6/openjdk-ecj/hotspot/src/os_cpu/linux_zero/vm -I../generated -DHOTSPOT_RELEASE_VERSION="\"14.0-b16\"" -DHOTSPOT_BUILD_TARGET="\"product\"" -DHOTSPOT_BUILD_USER="\"mockbuild\"" -DHOTSPOT_LIB_ARCH=\"s390x\" -DJRE_RELEASE_VERSION="\"1.6.0_0-b16\"" -DHOTSPOT_VM_DISTRO="\"OpenJDK\"" -I/usr/lib64/libffi-3.0.5/include   -fPIC -fno-rtti -fno-exceptions -D_REENTRANT -fcheck-new -g -m64 -O2 -D_LP64=1  -Wpointer-arith -Wsign-compare    -c -o systemDictionary.o -fpermissive systemDictionary.ii" 0 "Creating the foo test file"
        rlAssertExists "systemDictionary.o"
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
