#include <regex>
#include <cassert>
#include <cstdio>

static bool error_range_thrown(const char *pat)
{
    bool result = false;
    try {
        std::regex re(pat);
    } catch (const std::regex_error &ex) {
        puts("regex_error");
        fflush(stdout);
        result = (ex.code() == std::regex_constants::error_range);
    }   
    return result;
}

int main(int, char**)
{
    assert(error_range_thrown("([\\w-a])"));
}
