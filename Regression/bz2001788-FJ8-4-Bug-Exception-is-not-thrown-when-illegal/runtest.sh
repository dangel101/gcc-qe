#!/usr/bin/env bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz2001788-FJ8-4-Bug-Exception-is-not-thrown-when-illegal
#   Description: Test for BZ#2001788 ([FJ8.4 Bug] Exception is not thrown when illegal)
#   Author: Vaclav Kadlcik <vkadlcik@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2022 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Notes on relevancy
#
# The bug was fixed upstream for GCC 12 and backported to 11.4 (but
# later than our last GTS/DTS 11 release and RHEL 9.0 release).
# Internally, we had to backport it to system GCC of RHEL 8.7. My
# initial expectations, translated to TCMS, are:
#   collection !defined && distro < rhel-8: False
#   collection !defined && distro < rhel-8.7: False
#   collection !defined && distro = rhel-9.0: False
#   collection contains gcc-toolset-9, gcc-toolset-10, gcc-toolset-11, devtoolset-9, devtoolset-10, devtoolset-11: False

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

GCC="${GCC:-$(type -P gcc)}"

GCC_RPM_NAME=$(rpm --qf '%{name}' -qf $GCC)
GCCCPP_RPM_NAME=${GCC_RPM_NAME}-c++

PACKAGES="$GCC_RPM_NAME $GCCCPP_RPM_NAME"

rlJournalStart
    rlPhaseStartSetup
        rlLogInfo "PACKAGES=$PACKAGES"
        rlLogInfo "COLLECTIONS=$COLLECTIONS"
        rlLogInfo "SKIP_COLLECTION_METAPACKAGE_CHECK=$SKIP_COLLECTION_METAPACKAGE_CHECK"

        # We optionally need to skip checking for the presence of the metapackage
        # because that would pull in all the dependent toolset subrpms.  We do not
        # always want that, especially in CI.
        _COLLECTIONS="$COLLECTIONS"
        if ! test -z $SKIP_COLLECTION_METAPACKAGE_CHECK; then
            for c in $SKIP_COLLECTION_METAPACKAGE_CHECK; do
                rlLogInfo "ignoring metapackage check for collection $c"
                export COLLECTIONS=$(shopt -s extglob && echo ${COLLECTIONS//$c/})
            done
        fi
        rlLogInfo "(without skipped) COLLECTIONS=$COLLECTIONS"
        rlAssertRpm --all
        export COLLECTIONS="$_COLLECTIONS"

        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "cp files/* $TmpDir"
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    rlPhaseStartTest
        rlRun 'g++ test.cpp'
        rlRun './a.out >stdout.log 2>stderr.log'
        rlAssertNotDiffer stdout.log expected_output.txt
        rlAssertNotDiffer stderr.log /dev/null
        if ! rlGetPhaseState; then
            rlFileSubmit stdout.log stdout.log
            rlFileSubmit stderr.log stderr.log
        fi
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun popd
        rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
