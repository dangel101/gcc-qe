MODULE mmm
 integer*1 www
 integer xxx
 integer yyy
 real zzz
contains
 subroutine init
   xxx = 1
   yyy = 2
   zzz = 0.4
   www = 5
!STOP: check that the module has the right values in it
 end subroutine init
END MODULE mmm

subroutine sub1
 USE mmm

!STOP: check that the xxx=1, yyy=2, zzz=0.4 and www=1 have the correct 
!values
 print *,'xxx = ',xxx
 print *,'yyy = ',yyy
 print *,'zzz = ',zzz
 print *,'www = ',www
 print *

 www = 1
 xxx = yyy

!STOP: check that the xxx=2, yyy=2, zzz=0.4 and www=1 have the correct 
!values
 print *,'xxx = ',xxx
 print *,'yyy = ',yyy
 print *,'zzz = ',zzz
 print *,'www = ',www
 print *

 call sub2

end subroutine sub1

subroutine sub2

 USE mmm, ONLY : yyy=>zzz,zzz=>yyy
 real www

!STOP: check that the
!      xxx=N/A,
!      yyy=0.4 (in this subroutine, yyy is the local name for zzz),
!      zzz=2 (in this subroutine, zzz is the local name for yyy) and
!      www=N/A (just a local name) have the correct values
 print *,'xxx = ',xxx
 print *,'yyy = ',yyy
 print *,'zzz = ',zzz
 print *,'www = ',www
 print *

 zzz = 111
 yyy = 999

 www = 222

!STOP: check that the
!      xxx=N/A,
!      yyy=999 (in this subroutine, yyy is the local name for zzz),
!      zzz=111 (in this subroutine, zzz is the local name for yyy) and
!      www=222 (just a local name) have the correct values
 print *,'xxx = ',xxx
 print *,'yyy = ',yyy
 print *,'zzz = ',zzz
 print *,'www = ',www
 print *

end subroutine sub2

program main

 use mmm

 call init
 call sub1

!STOP: check that the xxx=2, yyy=111, zzz=999 and www=1 have the correct 
!values
 print *,'xxx = ',xxx
 print *,'yyy = ',yyy
 print *,'zzz = ',zzz
 print *,'www = ',www
 print *

end program main
