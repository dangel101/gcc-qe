subroutine bar1
  USE pr29635_1
  varx = 6
end subroutine bar1
subroutine bar2
  USE pr29635_1, only : varz
  varz = 7
end subroutine bar2
subroutine bar3
  USE pr29635_1, a=>varx, b=>varx, c=>varv, d=>typex
  type (d) :: h
  a = 8
  c = 7.0
end subroutine bar3
subroutine bar4
  USE pr29635_1, only: a=>varz, b=>varz, c=>varv, fini=>init
  b = 9
  c = 7.5
  call fini ()
end subroutine bar4
