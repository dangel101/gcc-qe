MODULE pr29635_1
  integer :: varx, vary, varz
  real :: varv, varw
  common /foo/ varx, varw, vary
  type typex
    real :: fldx
  end type
  type typey
    integer :: fldy
  end type
  type(typex) :: varu  
contains
  subroutine init
    varx = 1
    vary = 2
    varz = 3
    varv = 4.5
    varw = 5.5
    varu%fldx = 6.5
  end subroutine init
END MODULE pr29635_1
subroutine foo1
  USE pr29635_1
  varx = 6
end subroutine foo1
subroutine foo2
  USE pr29635_1, only : varz
  varz = 7
end subroutine foo2
subroutine foo3
  USE pr29635_1, a=>varx, b=>varx, c=>varv, d=>typex
  type (d) :: h
  a = 8
  c = 7.0
end subroutine foo3
subroutine foo4
  USE pr29635_1, only: a=>varz, b=>varz, c=>varv
  b = 9
  c = 7.5
end subroutine foo4
