#include <stdio.h>

extern void abort ();

unsigned long long xh = 1ull;

int
main ()
{
  unsigned long long yh = 0xffffffffull;
  unsigned long long z = xh * yh;
  unsigned long long i = 1ull * yh;

  printf ("%llx\n", i);
  printf ("%llx\n", z);
  if (z != i) {
    abort ();
  }
  else {
    exit(0);
  }
}

