module rh746405
  integer, dimension(:), pointer :: var
contains
  subroutine foo (x)
    integer :: x(40)
    x(1) = 1
  contains
    subroutine bar (y)
      double precision, dimension(:) :: y
    end subroutine
  end subroutine
end module
