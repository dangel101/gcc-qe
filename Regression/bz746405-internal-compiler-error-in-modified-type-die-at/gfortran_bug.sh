#! /bin/bash -e
file='dwarf2out'
line='8495'
dir="gfortran_bug_${file}_${line}"
#
cat << EOF
This shell script demonstrates a bug in 
     GNU Fortran (GCC) 4.1.2 20080704 (Red Hat 4.1.2-50)
When using that version of gfortran, the following error is generated:
"bug.f:7: internal compiler error: in modified_type_die, at $file.c:$line"
This occurs on a system with the following /etc/redhat-release:
     Red Hat Enterprise Linux Server release 5.5 (Tikanga)

EOF
if [ ! -d $dir ]
then
	echo "mkdir $dir"
	mkdir $dir
fi
echo "cd $dir"
cd $dir
#
echo 'create bug.f'
cat << EOF > bug.f
      MODULE MUMPS_STATIC_MAPPING
      integer,dimension(:),pointer:: cv_info
      contains
      subroutine MUMPS_369(info)
      integer:: info(40)
      info(1) = 1
      return
      CONTAINS
      subroutine MUMPS_413(memused)
      DOUBLE PRECISION,dimension(:)::memused
      return
      end subroutine MUMPS_413
      end subroutine MUMPS_369
      END MODULE MUMPS_STATIC_MAPPING
EOF
#
GFORTRAN=${GFORTRAN:-gfortran}
echo "$GFORTRAN -g -c bug.f -o bug.o"
echo
${GFORTRAN} -g -c bug.f -o bug.o
