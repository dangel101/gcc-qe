#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz746405-internal-compiler-error-in-modified-type-die-at
#   Description: Test for bz746405 (internal compiler error in modified_type_die, at)
#   Author: Marek Polacek <mpolacek@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2011, 2012 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh
. /usr/lib/beakerlib/beakerlib.sh

PACKAGES=(gcc gcc-gfortran)

# Choose the compiler.
export GFORTRAN=${GFORTRAN:-gfortran}

rlJournalStart
    rlPhaseStartSetup
# don't assert anything under devtoolset
if type gcc | grep -q -v devtoolset
then
	for p in "${PACKAGES[@]}"; do
	    rlCheckRpm "$p" || yum -y install "$p"
	    rlAssertRpm "$p"
	done; unset p
fi
        rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
	# We need the reproducer.
	cp -v gfortran_bug.sh rh746405.f90 $TmpDir
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    rlPhaseStartTest
    	# Run the reproducer script.
	rlRun "bash ./gfortran_bug.sh"

	# Additionaly, run the testcase from gcc testsuite.
    	rlRun "$GFORTRAN -c -g rh746405.f90"
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
