#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz1487434-GCC-does-not-emit-proper-alignment-directives-for-toc-section
#   Description: Test for BZ#1487434 (GCC does not emit proper alignment directives for TOC section)
#   Author: Michael Petlan <mpetlan@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2017 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

GCC=${GCC:-$(type -P gcc)}
GCC_RPM_NAME=$(rpm --qf '%{name}' -qf $GCC)
PACKAGES="$GCC_RPM_NAME ${GCC_RPM_NAME%gcc}binutils kernel-headers kernel-devel bzip2 make"

rlJournalStart
    rlPhaseStartSetup
        rlLogInfo "PACKAGES=$PACKAGES"
        rlLogInfo "COLLECTIONS=$COLLECTIONS"
        rlLogInfo "GCC=$GCC"
        rlLogInfo "SKIP_COLLECTION_METAPACKAGE_CHECK=$SKIP_COLLECTION_METAPACKAGE_CHECK"

        # We optionally need to skip checking for the presence of the metapackage
        # because that would pull in all the dependent toolset subrpms.  We do not
        # always want that, especially in CI.
        _COLLECTIONS="$COLLECTIONS"
        if ! test -z $SKIP_COLLECTION_METAPACKAGE_CHECK; then
            for c in $SKIP_COLLECTION_METAPACKAGE_CHECK; do
                rlLogInfo "ignoring metapackage check for collection $c"
                export COLLECTIONS=$(shopt -s extglob && echo ${COLLECTIONS//$c/})
            done
        fi
        rlLogInfo "(without skipped) COLLECTIONS=$COLLECTIONS"
        rlAssertRpm --all
        export COLLECTIONS="$_COLLECTIONS"

        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "cp toctest_062117a.tar.bz2 $TmpDir"
        rlRun "pushd $TmpDir"
        rlRun 'tar xjf toctest_062117a.tar.bz2'
        rlRun 'pushd toctest'

        # This is necessary, since ARCH variable affects every kernel-related builds;
        # within Beaker tests however, ARCH is preset but to a *different* content
        # than kernel builds need.
        unset ARCH

        rlIsRHEL '>=9' && rlRun "echo 'MODULE_LICENSE(\"GPL\");' >>toctest.c"
        rlRun 'make &>make-debug.log'
        rlAssertExists toctest.ko
        rlAssertExists toctest.o
    rlPhaseEnd

    for obj in o ko; do

        rlPhaseStartTest "toctest.$obj"
            rlRun "objdump -x toctest.$obj | head -n 50 | grep -C5 '\.toc' >toctest.$obj.dump.snip"
            rlAssertGrep '\s\.toc[\s\da-f]+\s+2\*\*[38]' toctest.$obj.dump.snip -qP
            rlAssertNotGrep '\s\.toc[\s\da-f]+\s+2\*\*0' toctest.$obj.dump.snip -qP
        rlPhaseEnd

    done

    rlPhaseStartCleanup
        if ! rlGetTestState; then
            rlFileSubmit make-debug.log make-debug.log
            for i in *.snip; do
                rlFileSubmit "$i" "$i"
            done
        fi
        rlRun popd
        rlRun popd
        rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
