struct CATWBRealInterval {
  double _lo;
  double _hi;
};

// interval constants
CATWBRealInterval CATWBReal_TrigInv = {CATWBReal_TrigInv._lo = -1.0, CATWBReal_TrigInv._hi = 1.0};
