void
foo (void)
{
  register long s __asm ("r13");
  register long t __asm ("r14");
  register long u __asm ("r15");
  asm volatile (".globl __HERE__; __HERE__ : xorq %%r12, %%r12" : : : "r12");
  asm volatile ("xorq %0, %0" : "=r" (s));
  asm volatile ("xorq %0, %0" : "=r" (t));
  asm volatile ("xorq %0, %0" : "=r" (u));
}
