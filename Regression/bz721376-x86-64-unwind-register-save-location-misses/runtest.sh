#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz721376-x86-64-unwind-register-save-location-misses
#   Description: Test for bz721376 (x86-64 unwind register save location misses)
#   Author: Marek Polacek <mpolacek@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2011 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh
. /usr/lib/beakerlib/beakerlib.sh

PACKAGES=(gcc binutils perl) 

# Choose the compiler.
GCC=${GCC:-gcc}

# This is for x86_64 only!


# Set the variabile UNDER_DTS on non-empty string, when run under devtoolset
if $( echo `which $GCC` | grep -qE '/opt/rh/' ); then
  UNDER_DTS="true"
  # Set the actual version of DTS
  DTS=`which gcc | awk 'BEGIN { FS="/" } { print $4 }'`
fi

# Get the version of GCC
if [ ! -z ${UNDER_DTS} ]; then
  VERSION=$(rpmquery $DTS-gcc --queryformat=%{VERSION})
else
  VERSION=$(rpmquery $GCC --queryformat=%{VERSION})
fi

rlJournalStart
    rlPhaseStartSetup
        for p in "${PACKAGES[@]}"; do
            rlAssertRpm "$p" || yum -y install "$p"
        done; unset p
        rlLog "GCC = $GCC"
        rlLog "Installed within `rpmquery -f $(which $GCC)`"
        rlRun "TmpDir=\`mktemp -d\`" 0 "Creating tmp directory"
	# We need the reproducer.
	rlRun "cp -v pr48597.c $TmpDir"
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    rlPhaseStartTest
    	# Compile the reproducer.
	rlRun "$GCC -O2 -c pr48597.c" 0 "Compiling the reproducer"

	# The output of objdump -dr should be exactly the same for old/new gcc.
	# But we don't test it here.

	# Get the address of `__HERE__'.
	rlRun "here_addr=\`readelf -Ws pr48597.o | grep __HERE__ | gawk '{print \$2}'\`" 0 "Getting the address of __HERE__"
        # Beware, it must be a hex number.
        here_addr="0x$here_addr"

	# Now dump the .eh_frame.
	rlRun "readelf -wf pr48597.o > eh_frame" 0 "Getting the .eh_frame info"

	# DW_CFA_advance_loc should point at the start of inline asm,
	# since inline asm can clobber registers.
	# Described in another words, DW_CFA_offset directives should be _before_
	# the xor insns.
	# The fix is to create a FDI even for inline asm.


        #for every of four registers, find the Y
        for num in "12" "13" "14" "15"; do
            rlRun "y=`grep -B 3 "r$num" eh_frame | gawk '
            /^  DW_CFA_advance_loc:/ {
            print \$4
        }'`" 0 "Find Y in DW_CFA_advance_loc: X to Y"

          # Y must be <= than address of __HERE__.  But beware, these are hex
          # numbers!
          y="0x$y"

          # This here is the whole point of all the shenanigans.
          rlRun "perl -e 'if ($y > $here_addr) { exit 1; }'" 0 "Y must be <= than the address of __HERE__"
      done
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
