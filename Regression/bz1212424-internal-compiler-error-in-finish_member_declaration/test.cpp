// taken from https://gcc.gnu.org/bugzilla/show_bug.cgi?id=50800#c9
// c++ -std=c++11 -c test.cpp
// --> internal compiler error = FAIL (bug reproduced)
// --> warning/pass = PASS

#include <type_traits>
template <typename L> typename std::remove_reference<L>::type foo(L &&lhs, int rhs);
template <typename T> T d(T i)
{
  enum { Size = sizeof(int) };
  return foo(i, Size);
}
template <typename T> struct A {
  typedef int TA __attribute__((__may_alias__));
};
int main()
{
  typedef A<int> V;
  typedef typename std::remove_reference<typename V::TA &>::type T;
  return d(1);
}
