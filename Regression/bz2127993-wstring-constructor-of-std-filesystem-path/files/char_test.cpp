#include <type_traits>
#include <filesystem>
#include <locale>
#include <iostream>
#include <cassert>
#include <string>

namespace fs = std::filesystem;

int main()
{
#ifdef __GNUC_WIDE_EXECUTION_CHARSET_NAME
    std::cout << __GNUC_EXECUTION_CHARSET_NAME << std::endl;
    std::cout << __GNUC_WIDE_EXECUTION_CHARSET_NAME << std::endl;
#endif

    std::locale::global(std::locale("en_US.UTF-8"));
    //std::locale::global(std::locale("zh_CN.utf8"));
    const wchar_t *const wstr = L"Dir1/\u732B.txt";
    constexpr int mbstr_siz = 16;
    char mbstr[mbstr_siz]{};
    auto len = std::wcstombs(mbstr, wstr, mbstr_siz);
    mbstr[len] = '\0';
    std::cout << mbstr << std::endl;

    std::cout << "Build from char multibyte string" << std::endl;
    fs::path p0 = mbstr;
    std::cout << "Build from wchar_t string" << std::endl;
    fs::path p1 = wstr;
    return 0;
}
