__attribute__((noinline, noclone)) int
foo (int a, int b)
{
  if (a)
    return -3;
  asm volatile goto ("bts $1, %0; jc %l[lab]" : : "m" (b) : "memory" : lab);
  return 0;
lab:
  return 0;
}

int
main ()
{
  if (foo (1, 0) != -3 || foo (0, 3) != 0 || foo (0, 0) != 0)
    __builtin_abort ();
  return 0;
}
