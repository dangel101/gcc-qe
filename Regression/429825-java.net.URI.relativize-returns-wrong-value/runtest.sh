#!/bin/bash

# Copyright (c) 2008, 2012 Red Hat, Inc. All rights reserved.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# 
# Author: Michal Nowak <mnowak@redhat.com>
# 	  Marek Polacek <polacek@redhat.com>

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

# Relevenant for system GCC of RHEL-6 only

PACKAGES='gcc libgcj'

rlJournalStart
    rlPhaseStartSetup
        rlLogInfo "PACKAGES=$PACKAGES"
        rlAssertRpm --all

        if [[ -e /usr/lib/jvm/java-1.4.2-gcj/bin/javac ]]; then
            rlRun JAVACTESTPATH=/usr/lib/jvm/java-1.4.2-gcj/bin/javac
        elif [[ -e /usr/lib/jvm/java-1.5.0-gcj-1.5.0.0/bin/javac ]]; then
            rlRun JAVACTESTPATH=/usr/lib/jvm/java-1.5.0-gcj/bin/javac
        fi

        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "cp Test.java $TmpDir"
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    rlPhaseStartTest 'Interpreting/compiling via java'
        rlRun "/usr/sbin/alternatives --set javac $JAVACTESTPATH" 0 'Set GCJ javac binary as default'
        rlRun 'ls -l /etc/alternatives/javac | grep gcj' 0 'GCJ is set as a javac bytecoder'
        rlRun 'javac Test.java'
        rlRun 'gij Test'
        rlRun 'gij Test | grep "file:/home/eclipse/runtime-New_configuration/simple_spu/simple_spu.c"'
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun popd
        rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
