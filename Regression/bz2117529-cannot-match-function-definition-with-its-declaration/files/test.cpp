template<typename OBJ>
struct Test
{
  template<int VALUE>
  constexpr OBJ const& obj() const noexcept;

  template<int VALUE>
  constexpr auto get() const noexcept -> decltype(obj<VALUE>().get());
};


template<typename OBJ>
template<int VALUE>
constexpr auto Test<OBJ>::get() const noexcept -> decltype(obj<VALUE>().get())
{
  return obj<VALUE>().get();
}


int main() { return 0; }
