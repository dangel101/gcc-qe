typedef long int64_t;
void vp9_encode_sby_pass1();
typedef struct {
  int64_t coded_error;
  int64_t sr_coded_error;
  int64_t frame_noise_energy;
  int64_t intra_error;
} FIRSTPASS_DATA;
typedef struct {
  FIRSTPASS_DATA fp_data;
} TileDataEnc;
void vp9_first_pass_encode_tile_mb_row(int td, FIRSTPASS_DATA *fp_acc_data,
                                       TileDataEnc *tile_data) {
  vp9_encode_sby_pass1(td);
  FIRSTPASS_DATA __trans_tmp_1 = *fp_acc_data;
  TileDataEnc *this_tile = tile_data;
  this_tile->fp_data.coded_error += this_tile->fp_data.sr_coded_error +=
      __trans_tmp_1.sr_coded_error;
  this_tile->fp_data.frame_noise_energy += __trans_tmp_1.frame_noise_energy;
  this_tile->fp_data.intra_error += __trans_tmp_1.intra_error;
}
void launch_enc_workers();
void first_pass_worker_hook();
void vp9_encode_fp_row_mt() { launch_enc_workers(first_pass_worker_hook); }
void vp9_first_pass() { vp9_encode_fp_row_mt(); }
