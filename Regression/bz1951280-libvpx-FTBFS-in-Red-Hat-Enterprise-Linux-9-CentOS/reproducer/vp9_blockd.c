extern char num_4x4_blocks_high_lookup, num_4x4_blocks_wide_lookup;
typedef struct {
  char tx_size;
} MODE_INFO;
struct macroblockd_plane {
  int subsampling_y;
};
typedef struct {
  struct macroblockd_plane plane[3];
  MODE_INFO *mi;
  int mb_to_right_edge;
} MACROBLOCKD;
typedef void foreach_transformed_block_visitor();
int vp9_foreach_transformed_block_in_plane_i;
foreach_transformed_block_visitor vp9_foreach_transformed_block_in_plane_visit;

#pragma GCC visibility push(internal)
void vp9_foreach_transformed_block_in_plane(MACROBLOCKD *xd) {
  struct macroblockd_plane pd = xd->plane[0];
  MODE_INFO mi = xd->mi[0];
  char tx_size = mi.tx_size, plane_bsize = pd.subsampling_y;
  int num_4x4_w = num_4x4_blocks_wide_lookup,
      num_4x4_h = num_4x4_blocks_high_lookup, r, c,
      max_blocks_wide = num_4x4_w + xd->mb_to_right_edge,
      max_blocks_high = num_4x4_h, extra_step = max_blocks_wide >> 1;
  for (r = 0; r < max_blocks_high; r += tx_size) {
    for (c = 0; c < max_blocks_wide; c += 1 << tx_size)
      vp9_foreach_transformed_block_in_plane_visit(plane_bsize);
    vp9_foreach_transformed_block_in_plane_i += extra_step;
  }
}
void vp9_encode_sby_pass1(MACROBLOCKD *x) { vp9_foreach_transformed_block_in_plane(x); }
#pragma GCC visibility pop

struct vpx_codec_iface {
  int i;
  int get_glob_hdrs;
  void (*fp)();
};
void vp9_first_pass ();
void vp9_get_compressed_data() { vp9_first_pass(); }
void encoder_encode() {
  vp9_get_compressed_data();
}

struct vpx_codec_iface vpx_codec_vp9_cx_algo = {1, 0, encoder_encode };

void first_pass_worker_hook()
{
  vp9_first_pass_encode_tile_mb_row();
}
