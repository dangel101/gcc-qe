#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz1652929-ppc64le-correct-strcmp-inlined-code
#   Description: Test for BZ#1652929 (Backport ppc64le str[n]cmp inlined code)
#   Author: Michael Petlan <mpetlan@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2019 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

GCC=${GCC:-gcc}

# ppc64le-specific
#
# This test has a wild history of relevancy. It was created for GCC 8 of
# RHEL 8.0 (RHBZ#1652929) but killed in RHEL 8.1 by RHBZ#1666977. Then it
# became relevant for any GCC>9. However, only when power8 code is
# generated which is ithe default of any GCC on any RHEL<9. Since RHEL-9,
# GCCs need to explicitly set -mcpu=power8 (or maybe we can just drop
# the test).
#
# To wrap all that up for currently supported RHELs and toolsets on ppc64le,
# the following GCCs are applicable:
#   * any toolset GCC on RHEL<9
#   * any GCC on RHEL 9+
# Suggested TCMS relevancy:
#   arch != ppc64le: False
#   distro < rhel-9 && collection !defined: False

rlJournalStart
	rlPhaseStartSetup
		rlIsRHEL '<9' && USE_P8='' || USE_P8='-mcpu=power8'
		rlRun -l "rpmquery -f `which $GCC`"

		rlRun "TmpDir=\$(mktemp -d)"
		rlRun "cp files/* $TmpDir"
		rlRun "pushd $TmpDir"

		rlRun "$GCC $USE_P8 -o test -O2 test.c -g"
		rlRun "$GCC $USE_P8 -o test.S -O2 test.c -S"
		rlAssertExists "test"
		rlAssertExists "test.S"
		rlRun "./test" 0 "Running the example code"
		rlRun "objdump -d -S test > test.dump"
	rlPhaseEnd

	rlPhaseStartTest "BUG ASSERTIONS"
		rlAssertGrep "lxvd2x" test.dump
		rlAssertGrep "lxvd2x" test.S
		rlAssertGrep "vcmpequb" test.dump
		rlAssertGrep "vcmpequb" test.S
	rlPhaseEnd

	rlPhaseStartCleanup
		rlRun 'popd'
		rlRun "rm -r $TmpDir"
	rlPhaseEnd
rlJournalPrintText
rlJournalEnd
