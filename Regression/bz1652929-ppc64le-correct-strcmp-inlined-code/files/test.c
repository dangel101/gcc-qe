#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define SIZE 65536

int main(int argc, char *argv[])
{
	char *s = malloc(sizeof(char) * SIZE);
	char *u = malloc(sizeof(char) * SIZE);
	FILE *f1 = fopen("good.dump", "r");
	FILE *f2 = fopen("bad.dump", "r");
	fgets(s, SIZE - 1, f1);
	fgets(u, SIZE - 1, f2);
	fclose(f1);
	fclose(f2);
	int a = strcmp(s, u);
	printf("Comparison of %s and %s: %i\n", s, u, a);
	free(s);
	free(u);
	return 0;
}
