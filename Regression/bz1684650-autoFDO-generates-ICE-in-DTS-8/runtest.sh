#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz1684650-autoFDO-generates-ICE-in-DTS-8
#   Description: Test for BZ#1684650 (autoFDO generates ICE in DTS 8)
#   Author: Michael Petlan <mpetlan@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2019 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

# Relevant for GCC 8+, archtecture-specific.
# Suggested TCMS relevancy:
#   arch != x86_64, ppc64le, aarch64: False
#   distro < rhel-8 && collection !defined: False

GCC="${GCC:-$(type -P gcc)}"
PACKAGE=$(rpm --qf '%{name}' -qf $GCC)
PACKAGES="${PACKAGE} ${PACKAGE}-c++"

rlJournalStart
	rlPhaseStartSetup
		for i in $PACKAGES; do
			rlAssertRpm $i
		done
		rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
		rlRun "cp repro.tar.xz $TmpDir/"
		rlRun "pushd $TmpDir"
		rlRun "xzcat repro.tar.xz | tar x" 0 "Unpacking the reproducer"
		CC1PLUS=`rpmquery -l ${PACKAGE}-c++.$(arch) | grep cc1plus`
		rlLog "Using $CC1PLUS from `rpmquery -f $CC1PLUS`"
	rlPhaseEnd

	rlPhaseStartTest
		rlRun "$CC1PLUS -quiet -g -O2 -fauto-profile=ams-Find_Performance_UT1.gcov utilityBASE.ii" 0 "Compiling the reproducer [BUG REPRODUCTION]"
		rlAssertExists "utilityBASE.s"
		# Check the utilityBASE.s sanity -- it should be big enough (usually, it
		# is around 500 kB, but let's set the threshold to 100, since it should
		# filter out the wrong cases, when utilityBASE.s is about 4 kB
		rlAssertGreater "utilityBASE.s should be big enough" `ls -s -k utilityBASE.s | cut -d' ' -f1` 100
	rlPhaseEnd

	rlPhaseStartCleanup
		rlRun "popd"
		rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
	rlPhaseEnd
rlJournalPrintText
rlJournalEnd
