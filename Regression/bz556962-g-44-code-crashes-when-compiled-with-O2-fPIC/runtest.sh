#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc44/Regression/bz556962-g-44-code-crashes-when-compiled-with-O2-fPIC
#   Description: Test for bz556962 (g++44 code crashes when compiled with -O2 -fPIC)
#   Author: Marek Polacek <polacek@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2012 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh
. /usr/lib/beakerlib/beakerlib.sh

# Fixed in gcc44-4.4.7-1.el5.

# Choose the compiler.
GCC=${GCC:-gcc}
GXX=${GXX:-g++}

rlJournalStart
  rlPhaseStartSetup
    # Check for needed packages.
    # We need gcc44, but in newer RHEL's this may not be true.
    rlAssertRpm $GCC
    rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
    # Copy reproducers.
    cp -v enum2.C test_long.cc $TmpDir
    rlRun "pushd $TmpDir"
  rlPhaseEnd

  rlPhaseStartTest
    # Compile and run two testcases.
    rlRun "$GXX -fPIC -O2 test_long.cc -o test_long"
    rlAssertExists "./test_long"
    rlRun "./test_long"

    # ...and the other one from GCC testsuite.
    rlRun "$GXX -O2 -fPIC enum2.C -o enum2"
    rlAssertExists "./enum2"
    rlRun "./enum2"
  rlPhaseEnd

  rlPhaseStartCleanup
    rlRun "popd"
    rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
  rlPhaseEnd
rlJournalPrintText
rlJournalEnd
