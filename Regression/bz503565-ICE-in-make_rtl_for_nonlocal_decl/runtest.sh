#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2010, 2012 Red Hat, Inc. All rights reserved.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 3 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include rhts environment
. /usr/bin/rhts-environment.sh
. /usr/lib/beakerlib/beakerlib.sh

PACKAGE="gcc"

# RHEL5 and i?86 only.

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm $PACKAGE
        rlRun "TmpDir=\`mktemp -d\`" 0 "Creating tmp directory"
	cp *.out $TmpDir
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    rlPhaseStartTest
        rlRun "/usr/libexec/gcc/i386-redhat-linux/\`gcc -dumpversion\`/cc1plus -quiet -I../../include -I../../include/libtorrent -I/usr/local/include/boost-1_39 -I/usr/include/openssl -I/usr/include/python2.4 -D_GNU_SOURCE -D_REENTRANT -DNDEBUG -D_GNU_SOURCE -DTORRENT_USE_OPENSSL -DTORRENT_LINKING_SHARED -DHAVE_SSL -DBOOST_MULTI_INDEX_DISABLE_SERIALIZATION -D_FORTIFY_SOURCE=2  -quiet -dumpbase extensions.cpp -m32 -march=i386 -mtune=generic -auxbase-strip build/temp.linux-i686-2.4/src/extensions.o -g -O2 -Wall -fno-strict-aliasing -fexceptions -fstack-protector -fasynchronous-unwind-tables -fPIC --param ssp-buffer-size=4 -o - -frandom-seed=0 ccuAqAlB.out" 0 "Compile ccuAqAlB.out"
        rlRun "/usr/libexec/gcc/i386-redhat-linux/\`gcc -dumpversion\`/cc1plus -quiet -I/ngs_storage/linux_zuul.3.0/ngs_source_vob/zengine -D_GNU_SOURCE -D_DEBUG -D_CONSOLE -quiet -dumpbase -mtune=generic -auxbase-strip /ngs_storage/buildforgeprojects/NGS_Linux_Build/NGS_LINUX_BUILD_241/obj/debug/zenstringtest/./zenstringtest.o -g -O0 -Wall -Wno-unused-function -fPIC -fno-strict-aliasing -o - -frandom-seed=0 cciRFBUK.out" 0 "Compile cciRFBUK.out"
        rlRun "/usr/libexec/gcc/i386-redhat-linux/\`gcc -dumpversion\`/cc1plus -quiet -I/home/ric/MIDB/include/python2.6 -I/home/ric/MIDB/lib/python2.6/site-packages/numpy/core/include -I/home/ric/boost/include -I/home/ric/MIDB/rdkit_070415/Code -D_GNU_SOURCE -Drdchem_EXPORTS -DNDEBUG -quiet -dumpbase -mtune=generic -auxbase-strip CMakeFiles/rdchem.dir/EditableMol.cpp.o -O3 -Wno-deprecated -Wno-unused-function -fno-strict-aliasing -fPIC -o - -frandom-seed=0 ccqsYSQx.out" 0 "Compile ccqsYSQx.out"
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
