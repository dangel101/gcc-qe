#!/usr/bin/env bash
while true; do
    ./T_simple-2
    retval=$?
    [[ $retval -eq 0 ]] && continue
    [[ $retval -eq 139 ]] && touch SEGFAULTED.file
    echo "./T_simple-2 exited with exit value $retval"
    exit 0
done
