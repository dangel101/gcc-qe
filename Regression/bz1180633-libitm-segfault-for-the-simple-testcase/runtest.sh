#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz1180633-libitm-segfault-for-the-simple-testcase
#   Description: Test for BZ#1180633 (libitm segfault for the simple testcase)
#   Author: Michael Petlan <mpetlan@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2016 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

# Relevant for GCC 4.8+. In practice, any currently supported GCC except the
# system one of RHEL-6.
# Suggested TCMS relevancy:
#   distro < rhel-7 && collection !defined: False

GCC=${GCC:-$(type -P gcc)}
GCC_RPM_NAME=$(rpm --qf '%{name}' -qf $GCC)
PACKAGES="$GCC_RPM_NAME"

if [[ "$GCC_RPM_NAME" == *toolset* ]]; then
    TOOLSET=${GCC_RPM_NAME%-gcc}
    PACKAGES+=" ${TOOLSET}-libitm-devel"
else
    PACKAGES+=" libitm"
    if ! rlIsRHEL 6; then
        PACKAGES+=" libitm-devel"
    fi
fi

# we'll give the bug 2 minutes to reproduce
TIMEOUT=120

rlJournalStart
    rlPhaseStartSetup
        rpm -q $PACKAGES || rlRun "yum -y install --skip-broken $PACKAGES" 0-255 # best effort

        rlLogInfo "PACKAGES=$PACKAGES"
        rlLogInfo "COLLECTIONS=$COLLECTIONS"
        rlLogInfo "GCC=$GCC"
        rlLogInfo "SKIP_COLLECTION_METAPACKAGE_CHECK=$SKIP_COLLECTION_METAPACKAGE_CHECK"

        # We optionally need to skip checking for the presence of the metapackage
        # because that would pull in all the dependent toolset subrpms.  We do not
        # always want that, especially in CI.
        _COLLECTIONS="$COLLECTIONS"
        if ! test -z $SKIP_COLLECTION_METAPACKAGE_CHECK; then
            for c in $SKIP_COLLECTION_METAPACKAGE_CHECK; do
                rlLogInfo "ignoring metapackage check for collection $c"
                export COLLECTIONS=$(shopt -s extglob && echo ${COLLECTIONS//$c/})
            done
        fi
        rlLogInfo "(without skipped) COLLECTIONS=$COLLECTIONS"
        rlAssertRpm --all
        export COLLECTIONS="$_COLLECTIONS"

        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "cp -p simple-2.c wrapper.sh $TmpDir"
        rlRun "pushd $TmpDir"

        LD_SHOW_AUXV=1 /bin/true | grep AT_PLATFORM | grep -i power8
        if [[ $? -eq 0 ]]; then
            rlLogInfo 'THIS MACHINE IS POWER8: HERE WE CAN CHECK THE BUGFIX'
        else
            rlLogInfo 'THIS MACHINE IS NOT POWER8: CANNOT REALLY CHECK THE BUGFIX'
        fi
        rlRun 'gcc -g simple-2.c -O2 -o T_simple-2 -fgnu-tm -lpthread'
        rlAssertExists T_simple-2
        rlRun ./T_simple-2
    rlPhaseEnd

    rlPhaseStartTest
        rlWatchdog ./wrapper.sh $TIMEOUT
        if [[ $? -eq 0 ]]; then
            rlFail 'wrapper.sh finished sooner than expected'
        fi
        if [[ -e SEGFAULTED.file ]]; then
            rlFail "T_simple-2 segfaulted"
        fi
        rlGetPhaseState && rlPass 'No segfault detected'
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun popd
        rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
