#include <g3log/g3log.hpp>

class Element
{
  public:
    Element();
    std::uint64_t data_;
};
using ElementArray = std::array<Element, 256>;

class ElementManager
{
  public:
    ElementManager();
  private:
    ElementArray array_;
};

static ElementArray makeArray()
{
  LOG(INFO) << "here";
  ElementArray foo;
  return foo;
}

ElementManager::ElementManager() : array_(makeArray())
{
  LOG(INFO) << "here";
}
