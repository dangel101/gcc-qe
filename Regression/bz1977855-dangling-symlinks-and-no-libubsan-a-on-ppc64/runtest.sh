#!/usr/bin/env bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz1977855-dangling-symlinks-and-no-libubsan-a-on-ppc64
#   Description: Test for BZ#1977855 (dangling symlinks and no libubsan.a on ppc64)
#   Author: Vaclav Kadlcik <vkadlcik@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2021 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

GCC="${GCC:-$(type -P gcc)}"
PACKAGE=$(rpm --qf '%{name}' -qf $GCC)
PACKAGES="${PACKAGE}"

rlJournalStart
    rlPhaseStartSetup
        rlLogInfo "PACKAGE=$PACKAGE"
        rlLogInfo "PACKAGES=$PACKAGES"
        rlLogInfo "COLLECTIONS=$COLLECTIONS"
        rlLogInfo "GCC=$GCC"
        rlLogInfo "SKIP_COLLECTION_METAPACKAGE_CHECK=$SKIP_COLLECTION_METAPACKAGE_CHECK"

        # We optionally need to skip checking for the presence of the metapackage
        # because that would pull in all the dependent toolset subrpms.  We do not
        # always want that, especially in CI.
        _COLLECTIONS="$COLLECTIONS"
        if ! test -z $SKIP_COLLECTION_METAPACKAGE_CHECK; then
            for c in $SKIP_COLLECTION_METAPACKAGE_CHECK; do
                rlLogInfo "ignoring metapackage check for collection $c"
                export COLLECTIONS=$(shopt -s extglob && echo ${COLLECTIONS//$c/})
            done
        fi
        rlLogInfo "(without skipped) COLLECTIONS=$COLLECTIONS"
        rlAssertRpm --all
        export COLLECTIONS="$_COLLECTIONS"

        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "pushd $TmpDir"

        rlRun "MY_BUILD=$(rpm -q --qf='%{NAME}-%{VERSION}-%{RELEASE}' $PACKAGE)"
        rlRun "MY_SRPM=$(rpm -q --qf='%{SOURCERPM}' $PACKAGE)"

        # Have the full build installed
        # Quick and very very dirty hack, should be done properly one day..
        if ! rpm -q distribution-distribution-install-brew-build &>/dev/null; then
            rlRun 'yum -y --disablerepo="*" --enablerepo=beaker-tasks install distribution-distribution-install-brew-build'
        fi
        rlRun "( cd /mnt/tests/distribution/install/brew-build && sed -e 's|rlJournal.*||' -e 's|rlPhase.*||' <runtest.sh >runtest2.sh )"
        rlRun "( cd /mnt/tests/distribution/install/brew-build && chmod 755 ./runtest2.sh )"
        rlRun "( cd /mnt/tests/distribution/install/brew-build && BUILDS=$MY_BUILD METHOD=install ./runtest2.sh )"

        # Find all RPMs of the build
        rpm -qa | while read i; do
            if [[ $(rpm -q --qf='%{SOURCERPM}' $i) = "$MY_SRPM" ]]; then
                rlRun "echo $i >>rpms.txt"
            fi
        done
        rlAssertGrep . rpms.txt # there has to be something

        # Find all potential directories of the RPMs
        for i in $(<rpms.txt); do
            rpm -ql $i | while read j; do
                if [[ -d "$j" ]] || [[ -L "$j" ]]; then
                    rlRun "echo '$j' >>dirs1.txt"
                fi
            done
        done
        rlRun 'sort -u <dirs1.txt >dirs2.txt'
        rlAssertGrep . dirs2.txt # there has to be something
    rlPhaseEnd

    rlPhaseStartTest
        while read i; do
            rlRun "find -L '$i' -type l >>dangling.txt 2>>errors.txt"
        done <dirs2.txt

        while read i; do
            [[ "$i" == /usr/lib/.build-id/* ]] || continue
            [[ "$i" == */32/lib* ]] || continue
            rlFail "dangling link: $i"
        done <dangling.txt

        while read i; do
            rlFail "erroneous link: $i" # e.g. self-referring symlinks ("Too many levels of symbolic links")
        done <errors.txt
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
