void log (const char *s);
void
UpdateExchParameter ()
{
  static const char *_FunctionTracePoint_name = __PRETTY_FUNCTION__;
  class _FunctionTracePoint
  {
  public:
    _FunctionTracePoint ()
    {
      log ( _FunctionTracePoint_name);
    }
  };
  _FunctionTracePoint l;
}
