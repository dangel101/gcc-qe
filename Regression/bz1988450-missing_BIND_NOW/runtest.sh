#!/usr/bin/env bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz1988450-missing_BIND_NOW
#   Description: Test for BZ#1988450 (annocheck reports that libgomp.so.1.0.0,)
#   Author: Vaclav Kadlcik <vkadlcik@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2021 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

GCC="${GCC:-$(type -P gcc)}"
PACKAGE=$(rpm --qf '%{name}' -qf $GCC)
PACKAGES="${PACKAGE} ${PACKAGE}-c++ ${PACKAGE%gcc}libstdc++-devel ${PACKAGE%gcc}annobin-annocheck"

rlJournalStart
    rlPhaseStartSetup
        rlMountRedhat

        rlLogInfo "PACKAGES=$PACKAGES"
        rlLogInfo "COLLECTIONS=$COLLECTIONS"
        rlLogInfo "GCC=$GCC"
        rlLogInfo "SKIP_COLLECTION_METAPACKAGE_CHECK=$SKIP_COLLECTION_METAPACKAGE_CHECK"

        for i in $PACKAGES; do
            rpm -q $i &>/dev/null || rlRun "yum -y install $i"
        done

        # Have some elfutils. Preferably of the same toolset but the package
        # isn't always included so let's have the system version as a fallback.
        rlRun "yum install -y --skip-broken ${PACKAGE%gcc}elfutils elfutils"

        # We optionally need to skip checking for the presence of the metapackage
        # because that would pull in all the dependent toolset subrpms.  We do not
        # always want that, especially in CI.
        _COLLECTIONS="$COLLECTIONS"
        if ! test -z $SKIP_COLLECTION_METAPACKAGE_CHECK; then
            for c in $SKIP_COLLECTION_METAPACKAGE_CHECK; do
                rlLogInfo "ignoring metapackage check for collection $c"
                export COLLECTIONS=$(shopt -s extglob && echo ${COLLECTIONS//$c/})
            done
        fi
        rlLogInfo "(without skipped) COLLECTIONS=$COLLECTIONS"
        rlAssertRpm --all
        export COLLECTIONS="$_COLLECTIONS"

        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "pushd $TmpDir"

        # DTS/GTS often don't bring their own libraries but use the base instances (wrapped
        # by DTS/GTS linker scripts). Let's use "brute force", i.e. look everywhere. In the
        # worst case we'll check a bit more libraries than necessary but there's no harm
        # in that.
        rlRun "find -L /usr/lib* -type f -name 'libgomp.so*'    >candicates1.txt"
        rlRun "find -L /usr/lib* -type f -name 'libstdc++.so*' >>candicates1.txt"
        rlRun "find -L /usr/lib* -type f -name 'libgcc_s.so*'  >>candicates1.txt"
        rlAssertGrep '/usr/lib64/libgomp.so'   candicates1.txt -F
        rlAssertGrep '/usr/lib64/libstdc++.so' candicates1.txt -F
        rlAssertGrep '/usr/lib64/libgcc_s.so'  candicates1.txt -F
        if [[ "$GCC" == /opt/rh/* ]]; then
            COLL_DIR=$(echo "$GCC" | cut -d/ -f1-4)
            rlAssertExists "$COLL_DIR"
            rlLogInfo "GCC Collection directory COLL_DIR=$COLL_DIR"
            rlRun "find -L $COLL_DIR -type f -name 'libgomp.so*'   >>candicates1.txt"
            rlRun "find -L $COLL_DIR -type f -name 'libstdc++.so*' >>candicates1.txt"
            rlRun "find -L $COLL_DIR -type f -name 'libgcc_s.so*'  >>candicates1.txt"
            rlAssertGrep "$COLL_DIR/.*/libgomp\.so"   candicates1.txt
            rlAssertGrep "$COLL_DIR/.*/libstdc++\.so" candicates1.txt
            rlAssertGrep "$COLL_DIR/.*/libgcc_s\.so"  candicates1.txt
        fi

        # Not all *.so* files are binaries
        for i in $(<candicates1.txt); do
            if file -L "$i" | grep -q 'ASCII .*text'; then
                rlLogInfo "$i will be skipped as a text file (likely a ld script)"
            elif [[ "$i" == *-gdb.py? ]]; then
                rlLogInfo "$i will be skipped as a Python file"
            elif [[ "$i" == *.debug ]]; then
                rlLogInfo "$i will be skipped as a debug file"
            else
                rlRun "echo '$i' >>candicates2.txt"
            fi
        done

        # Let's eliminate duplicates hidden by various symlinks
        for i in $(<candicates2.txt); do
            rlRun "realpath '$i' >>realpaths.txt"
        done
        rlRun "sort -u <realpaths.txt >to_check.txt"

        rlAssertGrep '/libgomp'   to_check.txt -F
        rlAssertGrep '/libstdc++' to_check.txt -F
        rlAssertGrep '/libgcc_s'  to_check.txt -F
        FILES=$(<to_check.txt)
        for i in $FILES; do
            rlLogInfo "Will be checked: $i"
        done
    rlPhaseEnd

    for i in $FILES; do
        rlPhaseStartTest eu-readelf-$i
            rlRun -s "eu-readelf -d $i"
            rlAssertGrep '\bBIND_NOW\b' "$rlRun_LOG" -P \
                || rlFileSubmit "$rlRun_LOG" eu-readelf-d${i//\//_}.out
            rm -f "$rlRun_LOG"
        rlPhaseEnd
    done

    for i in $FILES; do
        rlPhaseStartTest annocheck-$i
            rlRun -s "annocheck $i" \
                || rlFileSubmit "$rlRun_LOG" annocheck_${i//\//_}.out
            rm -f "$rlRun_LOG"
        rlPhaseEnd
    done

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
