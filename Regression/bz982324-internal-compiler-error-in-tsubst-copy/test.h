#include <functional>

class A {
  typedef std::function<int()> Func;
  Func f_;
public:
  template<class T = Func> A(const T& f = []() { return 0; }) : f_(f) {}
};

int main(int argc, char* argv[]) {
  A a;
}
