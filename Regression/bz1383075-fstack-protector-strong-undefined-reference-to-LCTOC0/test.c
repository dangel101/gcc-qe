void
foo (void)
{
  unsigned long x = 0;
  asm volatile ("" : : "r" (&x) : "memory");
}

int
main ()
{
  foo ();
  return 0;
}
