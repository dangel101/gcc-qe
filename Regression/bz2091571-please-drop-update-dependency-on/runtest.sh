#!/usr/bin/env bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz2091571-please-drop-update-dependency-on
#   Description: Do not depend on older collections
#   Author: Vaclav Kadlcik <vkadlcik@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2022 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

# Relevant for Collection GCCs only. Suggested TCMS relevancy:
#   collection !defined: False

GCC="${GCC:-$(type -P gcc)}"

GCC_RPM_NAME=$(rpm --qf '%{name}' -qf $GCC)

PACKAGES="$GCC_RPM_NAME"

rlJournalStart
    rlPhaseStartSetup
        rlMountRedhat

        rlRun "yum -y install --skip-broken $PACKAGES" 0-255 # best effort

        rlLogInfo "PACKAGES=$PACKAGES"
        rlLogInfo "COLLECTIONS=$COLLECTIONS"
        rlLogInfo "GCC=$GCC"
        rlLogInfo "SKIP_COLLECTION_METAPACKAGE_CHECK=$SKIP_COLLECTION_METAPACKAGE_CHECK"

        # We optionally need to skip checking for the presence of the metapackage
        # because that would pull in all the dependent toolset subrpms.  We do not
        # always want that, especially in CI.
        _COLLECTIONS="$COLLECTIONS"
        if ! test -z $SKIP_COLLECTION_METAPACKAGE_CHECK; then
            for c in $SKIP_COLLECTION_METAPACKAGE_CHECK; do
                rlLogInfo "ignoring metapackage check for collection $c"
                export COLLECTIONS=$(shopt -s extglob && echo ${COLLECTIONS//$c/})
            done
        fi
        rlLogInfo "(without skipped) COLLECTIONS=$COLLECTIONS"
        rlAssertRpm --all
        export COLLECTIONS="$_COLLECTIONS"

        rlRun "OUR_NVR_SLASHED=$(rpm -q --qf='%{NAME}/%{VERSION}/%{RELEASE}' $GCC_RPM_NAME)"
        rlRun "RPMS_DIR=/mnt/redhat/brewroot/packages/$OUR_NVR_SLASHED"
        rlRun "ONLY_ACCEPTED_COLLECTION=${GCC_RPM_NAME%-gcc}"

        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    rlPhaseStartTest
        rlRun "ARCH=$(rlGetPrimaryArch)"
        rlAssertExists "$RPMS_DIR/$ARCH"

        for a in "$ARCH" $(rlGetSecondaryArch) noarch; do
            [[ -e $RPMS_DIR/$a ]] || continue # rlGetSecondaryArch and noarch may not exist

            rlRun "/bin/ls $RPMS_DIR/$a/*.rpm >rpms.txt"

            while read i; do

                just_basename=${i##*/}
                just_basename=${just_basename%.rpm}

                rlRun "rpm -q --requires -p $i >$just_basename"

                while read req; do
                    if [[ "$req" == *${ONLY_ACCEPTED_COLLECTION}* ]]; then
                        continue # This is OK, deps on "our" collection are expected
                    fi
                    if [[ "$req" == *devtoolset-* ]] || [[ "$req" == *gcc-toolset-* ]]; then
                        rlFail "$just_basename depends on an unexpected collection package: $req"
                    fi
                done <$just_basename

            done <rpms.txt
        done
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
