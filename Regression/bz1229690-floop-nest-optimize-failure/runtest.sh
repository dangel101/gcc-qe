#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz1229690-floop-nest-optimize-failure
#   Description: Test for BZ#1229690 (-floop-nest-optimize failure)
#   Author: Michael Petlan <mpetlan@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2016 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

# ============================================================================
# Notes on relevancy
# ============================================================================
#
# The isl library, which is a prerequisite for -floop-nest-optimize,
# is missing or disabled in RHEL9+ gcc and in RHEL6 DTS10+ gcc. See the
# respective gcc.spec files. Some discussion is in
#   https://bugzilla.redhat.com/show_bug.cgi?id=1975890
# as well.
#
# Further, -floop-nest-optimize itself is too new for system gcc in RHEL7 so
# the test is not applicable.
#
# Further, system gcc in RHEL7 is known to fail. (According to mpetlan's
# investigation, the upstream was fixed in 6.2.1 so any downstream based
# on the older versions is expected to fail.)
#
# In summary:
#   * Not relevant for RHEL6 (not applicable to system gcc, not applicable
#     to devtoolset-10 and newer)
#   * On RHEL7, relevant for DTS gcc only
#   * On RHEL8, relevant for every gcc available
#   * Not relevant for RHEL9+
#
# How to set relecancy in TCMS:
#   [relevancy]
#   distro != rhel-7, rhel-8: False
#   distro = rhel-7 && collection !defined: False

GCC="${GCC:-$(type -P gcc)}"
GCC_RPM_NAME=$(rpm --qf '%{name}' -qf $GCC)
PACKAGES="$GCC_RPM_NAME"

rlJournalStart
    rlPhaseStartSetup
        rlLogInfo "PACKAGES=$PACKAGES"
        rlLogInfo "COLLECTIONS=$COLLECTIONS"
        rlLogInfo "GCC=$GCC"
        rlLogInfo "SKIP_COLLECTION_METAPACKAGE_CHECK=$SKIP_COLLECTION_METAPACKAGE_CHECK"

        # We optionally need to skip checking for the presence of the metapackage
        # because that would pull in all the dependent toolset subrpms.  We do not
        # always want that, especially in CI.
        _COLLECTIONS="$COLLECTIONS"
        if ! test -z $SKIP_COLLECTION_METAPACKAGE_CHECK; then
            for c in $SKIP_COLLECTION_METAPACKAGE_CHECK; do
                rlLogInfo "ignoring metapackage check for collection $c"
                export COLLECTIONS=$(shopt -s extglob && echo ${COLLECTIONS//$c/})
            done
        fi
        rlLogInfo "(without skipped) COLLECTIONS=$COLLECTIONS"
        rlAssertRpm --all
        export COLLECTIONS="$_COLLECTIONS"

        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "cp the_Bad.c $TmpDir"
        rlRun "pushd $TmpDir"
        rlRun 'gcc -O3 -floop-nest-optimize -o the_Bad the_Bad.c'
        rlAssertExists the_Bad
    rlPhaseEnd

    rlPhaseStartTest
        rlRun ./the_Bad
        test $? -eq 139 && rlFail "BUG REPRODUCED"
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun popd
        rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
