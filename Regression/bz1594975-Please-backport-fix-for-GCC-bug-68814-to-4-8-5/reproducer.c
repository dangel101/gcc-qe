#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

static uint64_t __attribute__((noinline))
expand_value(uint64_t value)
{

 value &= 0xffffffff;
 value |= value << 32;

 return value;
}

int main(int argc, char **argv)
{

 if (argc > 1) {
   uint64_t v = strtoull(argv[1], NULL, 0);
   printf("%llx -> %llx\n", v, expand_value(v));
 }

 return 0;
}
