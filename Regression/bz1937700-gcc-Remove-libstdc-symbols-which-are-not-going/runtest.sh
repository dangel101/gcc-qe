#!/usr/bin/env bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz1937700-gcc-Remove-libstdc-symbols-which-are-not-going
#   Description: Test for BZ#1937700 (gcc Remove libstdc++ symbols which are not going)
#   Author: Vaclav Kadlcik <vkadlcik@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2021 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

# Note: Relevant for GCC>10. Suggested TCMS relevancy:
#   distro < rhel-9 && collection !defined: False
#   collection contains gcc-toolset-9, gcc-toolset-10, devtoolset-9, devtoolset-10: False

SYMBOLS='_ZNSt9once_flag11_M_activateEv _ZNSt9once_flag9_M_finishEb'

rlJournalStart
    rlPhaseStartSetup
        GCC="${GCC:-$(type -P gcc)}"
        GCC_PACKAGE=$(rpm --qf '%{name}' -qf $GCC)
        GXX_PACKAGE=${GCC_PACKAGE}-c++
        LIB_DEVEL_PACKAGE=${GCC_PACKAGE%gcc}libstdc++-devel
        ARCH_PRIMARY=$(rlGetPrimaryArch)
        ARCH_SECONDARY=$(rlGetSecondaryArch)
        if [[ "$GCC_PACKAGE" =~ toolset- ]] && [[ "$ARCH_PRIMARY" != x86_64 ]]; then
            ARCH_SECONDARY=''
        fi
        PACKAGES="${GXX_PACKAGE}.${ARCH_PRIMARY} ${LIB_DEVEL_PACKAGE}.${ARCH_PRIMARY}"
        if [[ -n "$ARCH_SECONDARY" ]]; then
            PACKAGES+=" ${LIB_DEVEL_PACKAGE}.${ARCH_SECONDARY}"
        fi

        rlLogInfo "GCC=$GCC"
        rlLogInfo "PACKAGES=$PACKAGES"
        rlLogInfo "COLLECTIONS=$COLLECTIONS"

        for i in $PACKAGES; do
            rpm -q $i &>/dev/null || rlRun "yum -y install $i"
            rlAssertRpm $i
        done

        L64="$(g++ --print-file-name libstdc++.so)"
        [[ -z "$L64" ]] && rlFail 'Primary libstdc++.so not found'
        LIBRARIES="$L64"
        if [[ -n "$ARCH_SECONDARY" ]]; then
            L32="$(g++ -m32 --print-file-name libstdc++.so)"
            [[ -z "$L32" ]] && rlFail 'Secondary libstdc++.so not found'
            LIBRARIES+=" $L32"
        fi

        rlLogInfo "libstdc++.so libraries: $LIBRARIES"

        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    rlPhaseStartTest IS_RELEVANT_FOR_PRESENT_GCC
        DUMPVERSION=$(gcc -dumpversion)
        rlLogInfo "DUMPVERSION=$DUMPVERSION"
        [[ -z "$DUMPVERSION" ]] && rlFail 'Unknown GCC version'
        [[ $(rlCmpVersion "$DUMPVERSION" 11) = '<' ]] && rlFail "This test isn't relevant for GCC $DUMPVERSION (it needs GCC 11+)"
    rlPhaseEnd

    rlPhaseStartTest SYMBOLS_NOT_PRESENT
        for j in $LIBRARIES; do
            rlLogInfo "Checking file $j ..."
            if file -L $j | grep -q 'ASCII .*text'; then
                rlLogInfo "Skipping $j as a text file" # better: should be an ld script
            else
                IS_OK=true
                rlRun "objdump --wide --dynamic-syms $j >objdump.out"
                for s in $SYMBOLS; do
                    rlAssertNotGrep $s objdump.out || IS_OK=false
                done
                if ! $IS_OK; then
                    rlFileSubmit objdump.out "${j//\//_}-dynamic_symbols.txt"
                fi
            fi
        done
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
