#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz559186-Redirection-of-built-in-ffs-does-not-work-on-s390x
#   Description: tests Redirection of built-in ffs does not work on s390x
#   Author: Michal Nowak <mnowak@redhat.com>
#	    Marek Polacek <polacek@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2010, 2012 Red Hat, Inc. All rights reserved.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 3 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include rhts environment
. /usr/bin/rhts-environment.sh
. /usr/lib/beakerlib/beakerlib.sh

PACKAGES=(gcc binutils)

# Choose the compiler.
GCC=${GCC:-gcc}

rlJournalStart
    rlPhaseStartSetup
        for p in "${PACKAGES[@]}"; do
            rlAssertRpm "$p"
        done; unset p
        rlRun "TmpDir=\`mktemp -d\`" 0 "Creating tmp directory"
	cp ffsll.c $TmpDir/ffsll.c
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    rlPhaseStartTest 31-bit
	rlRun "$GCC -O2 -c ffsll.c -m31"
        rlAssertExists "ffsll.o"
	rlRun "nm -u ffsll.o | grep 'U ffs'" 1 "[32-bit] No undefined symbol 'U ffs'"
    rlPhaseEnd

    rlPhaseStartTest 64-bit
	rlRun "$GCC -O2 -c ffsll.c -m64"
        rlAssertExists "ffsll.o"
	rlRun "nm -u ffsll.o | grep 'U ffs'" 1 "[64-bit] No undefined symbol 'U ffs'"
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
