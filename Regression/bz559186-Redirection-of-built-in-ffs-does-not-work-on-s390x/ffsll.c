extern int ffs (int __i) __attribute__ ((__nothrow__)) __attribute__((__const__));
extern __typeof (ffs) ffs __asm__ ("" "__GI_ffs") __attribute__ ((visibility("hidden")));
int ffsll (long long int i)
{
  unsigned long long int x = i & -i;

  if (x <= 0xffffffff)
    return ffs (i);
  else
    return 32 + ffs (i >> 32);
}

