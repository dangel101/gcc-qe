#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz1666977-Possible-Power9-DD2-1-performance-exposure-with
#   Description: Test for BZ#1666977 (Possible Power9 DD2.1 performance exposure with)
#   Author: Alexandra Hájková <ahajkova@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2019 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

# Relevant for GCC 8,9,10 only (see RHBZ#1971531 for more), ppc64le-specific.
# Suggested TCMS relevancy:
#   arch != ppc64le: False
#   distro < rhel-8 && collection !defined: False # GCC < 8
#   distro > rhel-8: False # GCC > 10, regardless system/toolset
#   collection defined && collection !contains gcc-toolset-9, gcc-toolset-10, devtoolset-9, devtoolset-10: False # GCC > 10

PACKAGE="gcc"
TESTPROG="test"

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm $PACKAGE
        rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
        rlRun "cp ${TESTPROG}.c $TmpDir"
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    rlPhaseStartTest
        rlRun "gcc ${TESTPROG}.c -o $TESTPROG"
        rlRun " objdump -d $TESTPROG > log"
        rlAssertNotGrep 'lxvd2x' log
    rlPhaseEnd


    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
