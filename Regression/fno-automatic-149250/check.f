      real delta
      REAL*8 DPARMAT,DCOVMAT
      REAL*8 DPARCAL,DCOVCAL
      DIMENSION DPARMAT(5),DCOVMAT(5,5)
      DIMENSION DPARCAL(5),DCOVCAL(5,5)
      
      do i=1,5
         dparmat(i)=cos(i*2.)
      do j=1,5
         dcovmat(i,j)=sin(i*2.+j*2.)
      enddo
      enddo    

      delta=1.4

      call FDKCOVCAL6(DELTA,DPARMAT,DCOVMAT,DPARCAL,DCOVCAL)
      print *,DPARCAL
      print *,DCOVCAL
      stop
      end
