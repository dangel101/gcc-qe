      SUBROUTINE FDKCOVCAL6(DELTA,DPARMAT,DCOVMAT,DPARCAL,DCOVCAL)
      IMPLICIT NONE
      INTEGER I,J,K
      REAL DELTA
      REAL*8 DPARMAT,DCOVMAT
      REAL*8 DPARCAL,DCOVCAL
      DIMENSION DPARMAT(5),DCOVMAT(5,5)
      DIMENSION DPARCAL(5),DCOVCAL(5,5)
      REAL*8 DFT,DF,DS
      DIMENSION DFT(5,5),DF(5,5)
      REAL FTMAT
      DIMENSION FTMAT(5,5)
      DATA FTMAT/1.,0.,0.,0.,0.,
     +           0.,1.,0.,0.,0.,
     +           0.,0.,1.,0.,0.,
     +           0.,0.,0.,1.,0.,
     +           0.,0.,0.,0.,1./
      IF(DELTA.EQ.0.)THEN
      DO I=1,5
      DPARCAL(I)=DPARMAT(I)
      DO J=1,5
      DCOVCAL(I,J)=DCOVMAT(I,J)
      ENDDO
      ENDDO
      ELSE
      DPARCAL(1)=DPARMAT(1)+DELTA*DPARMAT(2)
      DPARCAL(2)=DPARMAT(2)
      DPARCAL(3)=DPARMAT(3)+DELTA*DPARMAT(4)
      DPARCAL(4)=DPARMAT(4)
      DPARCAL(5)=DPARMAT(5)
      DO I=1,5
      DO J=1,5
      DFT(I,J)=FTMAT(I,J)
      ENDDO
      ENDDO
      DFT(2,1)=DELTA
      DFT(4,3)=DELTA
      DO I=1,5
      DO J=1,5
      DS=0.D0
      DO K=1,5
      DS=DS+DCOVMAT(I,K)*DFT(K,J)
      ENDDO
      DF(I,J)=DS
      ENDDO
      ENDDO
      DO I=1,5
      DO J=1,5
      DS=0.D0
      DO K=1,5
      DS=DS+DFT(K,I)*DF(K,J)
      ENDDO
      DCOVCAL(I,J)=DS
      ENDDO
      ENDDO     
      ENDIF
      RETURN
      END       
