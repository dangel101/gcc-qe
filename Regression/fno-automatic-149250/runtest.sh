#!/bin/bash

# Copyright (c) 2006, 2009 Red Hat, Inc. All rights reserved. This copyrighted material 
# is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General
# Public License v.2.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# Author: Arjan van de Ven <arjanv@redhat.com>
# Maintainer: Michal Nowak <mnowak@redhat.com>

# Include rhts environment
. /usr/bin/rhts-environment.sh
. /usr/lib/beakerlib/beakerlib.sh

PACKAGE="gcc"

rlJournalStart
    rlPhaseStartSetup Setup
        rlAssertRpm $PACKAGE
        rlShowPackageVersion $PACKAGE
        rlShowRunningKernel

	# -01 optimalised build
	rlRun "g77 -O1 -m32 -mcpu=i486 -fno-automatic -fno-second-underscore -fugly-logint -Wno-globals -c check.f" 0 "[-01] g77: Create object file from check.f"
	rlRun "g77 -O1 -m32 -mcpu=i486 -fno-automatic -fno-second-underscore -fugly-logint -Wno-globals -c fdkcovcal6.f" 0 "[-01] g77: Create object file from fdkcovcal6.f"
	rlRun "g77 -m32 check.o fdkcovcal6.o -o 01file" 0 "[-01] g77: Create binary file from .f files"

	# -02 optimalised build
	rlRun "g77 -O2 -m32 -mcpu=i486 -fno-automatic -fno-second-underscore -fugly-logint -Wno-globals -c check.f" 0 "[-02] g77: Create object file from check.f"
	rlRun "g77 -O2 -m32 -mcpu=i486 -fno-automatic -fno-second-underscore -fugly-logint -Wno-globals -c fdkcovcal6.f" 0 "[-02] g77: Create object file from fdkcovcal6.f"
	rlRun "g77 -m32 check.o fdkcovcal6.o -o 02file" 0 "[-02] g77: Create binary file from .f files"
    rlPhaseEnd

    rlPhaseStartTest Testing
	if [ -x ./01file ]; then ./01file > 01output; fi
	O1_SIZE=$(wc -c 01output | awk '{ print $1 }')
	if [ -x ./02file ]; then ./02file > 02output; fi
	O2_SIZE=$(wc -c 02output | awk '{ print $1 }')
	rlRun "if [ \"$O1_SIZE\" = "0" ] || [ \"$O2_SIZE\" = "0" ] || [ -z \"$O1_SIZE\" ]; then false; else true; fi" 0 "Both binary files have the same functionality/output"

	diff 01output 02output > output.diff
	DIFF_RETURN=$?
	rlRun "if [ \"$DIFF_RETURN\" -eq 0 ] && [ \"$O1_SIZE\" -ne 0 ]; then true; else cat output.diff; false; fi" 0 "Diff of 01output and 02output is void"
    rlPhaseEnd

    rlPhaseStartCleanup Cleanup
        rlBundleLogs "gcc-outputs" 01output 02output output.diff
        rlJournalPrintText > $OUTPUTFILE
        rm -rf 01file 02file output.diff 01output 02output *.o
    rlPhaseEnd
rlJournalPrintText

