#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Sanity/Check-PPC64-packages-on-small-stack
#   Description: tests PPC64 packages on small stack
#   Author: Michal Nowak <mnowak@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2010 Red Hat, Inc. All rights reserved.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 3 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include rhts environment
. /usr/bin/rhts-environment.sh
. /usr/lib/beakerlib/beakerlib.sh

PACKAGE="gcc"

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm $PACKAGE
        rlRun "TmpDir=\`mktemp -d\`" 0 "Creating tmp directory"
        rlRun "pushd $TmpDir"
        mkdir affdir 2> /dev/null
        rlMountRedhat
        TREE="$(grep 'nfs .* --dir=' /root/anaconda-ks.cfg | head -n1 | sed 's/nfs .* --dir=\/pub\/rhel/\/mnt/redhat')"
        echo "TREE: $TREE"
        PACKAGES64="$(find $TREE -name '*.ppc64.rpm' | grep -v debuginfo)"
        echo "PACKAGES64=$(echo $PACKAGES64 | wc -l)"
        mkdir tmp
    rlPhaseEnd

    rlPhaseStartTest
        for p in $PACKAGES64; do
                pushd tmp 2>/dev/null
                rpm2cpio $p | cpio -id 2>/dev/null
                for i in $(find . -type f); do
                        case `file $i 2>/dev/null` in *shared\ object*) readelf -Ws $i | grep -q __tls_get_addr && echo $p $i && cp $i ~/affdir/ && objdump -dr $i 2>/dev/null | grep 'stdu .*r1,.*r1)\| bl ' | grep -v blr | awk '/stdu/{val=gensub(/^.*r1,-([0-9]*)\(r1\).*$/,"\\1","",$0); if (strtonum(val) < 112) print $0 }'; EC=$?; echo; rlAssertEquals "$(basename $p) $i stack >= 112" "${EC}" "1" ;;
                        esac
                done
                popd 2>/dev/null
                rm -rf tmp/*
        done 2>&1 | grep -v '^ *$' | tee /tmp/ppc64tls
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
