extern int v;

void
foo (int x)
{
  try
  {
    int i;
    unsigned long long z = x;
  #pragma omp parallel if (z >= v)
    {
    #pragma omp for
      for (i = 0; i < z; ++i)
        ;
    }
  }
  catch (...)
  {
  }
}
