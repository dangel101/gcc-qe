#!/usr/bin/env bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz2130392-FJ8-6-Bug-REG-operator-of-std-normal_distribution
#   Description: Test for BZ#2130392 ([FJ8.6 Bug] [REG] ">>" operator of std::normal_distribution doesn't work properly)
#   Author: Vaclav Kadlcik <vkadlcik@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2022 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

# Notes on relevancy
#
# Not relevant for system GCC of RHEL-7 and older (old compiler). Relevant
# for the rest of supported compilers but some of them are buggy and unlikely
# to be fixed:
#   * GTS/DTS 10 and 11
#   * system GCC of RHEL 8.4 - 8.6 and RHEL 9.0
# At the moment system GCC of RHEL-8.7 and 9.1 are buggy as well but I expect
# they will be fixed.
#
# Suggested TCMS relevancy:
#   collection !defined && distro < rhel-8: False # old compiler
#   collection !defined && distro = rhel-8 && distro > rhel-8.2 && distro < rhel-8.7: False # buggy
#   collection !defined && distro = rhel-8 && distro > rhel-8.2 && distro < rhel-8.7: False # buggy
#   collection !defined && distro = rhel-9 && distro < rhel-9.1: False # buggy
#   collection contains gcc-toolset-10, gcc-toolset-11, devtoolset-10, devtoolset-11: False # buggy

GCC="${GCC:-$(type -P gcc)}"
GCC_RPM_NAME=$(rpm --qf '%{name}' -qf $GCC)

PACKAGES="$GCC_RPM_NAME ${GCC_RPM_NAME}-c++"

rlJournalStart
    rlPhaseStartSetup
        rlLogInfo "PACKAGES=$PACKAGES"
        rlLogInfo "COLLECTIONS=$COLLECTIONS"
        rlLogInfo "GCC=$GCC"
        rlLogInfo "SKIP_COLLECTION_METAPACKAGE_CHECK=$SKIP_COLLECTION_METAPACKAGE_CHECK"

        # We optionally need to skip checking for the presence of the metapackage
        # because that would pull in all the dependent toolset subrpms.  We do not
        # always want that, especially in CI.
        _COLLECTIONS="$COLLECTIONS"
        if ! test -z $SKIP_COLLECTION_METAPACKAGE_CHECK; then
            for c in $SKIP_COLLECTION_METAPACKAGE_CHECK; do
                rlLogInfo "ignoring metapackage check for collection $c"
                export COLLECTIONS=$(shopt -s extglob && echo ${COLLECTIONS//$c/})
            done
        fi
        rlLogInfo "(without skipped) COLLECTIONS=$COLLECTIONS"
        rlAssertRpm --all
        export COLLECTIONS="$_COLLECTIONS"

        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "cp files/* $TmpDir"
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    rlPhaseStartTest
        rlRun 'g++ test.cpp'
        rlRun './a.out >stdout.log 2>stderr.log'
        rlAssertNotDiffer stdout.log expected_stdout.txt
        rlAssertNotDiffer stderr.log /dev/null
        if ! rlGetPhaseState; then
            rlFileSubmit stdout.log stdout.log
            rlFileSubmit stderr.log stderr.log
        fi
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun popd
        rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
