#include <random>
#include <sstream>
#include <cstdio>

int main()
{
	std::normal_distribution<> d1(7, 5);
	std::stringstream ss;

	ss << d1;

	std::normal_distribution<> d2;
	ss >> d2;

	if (d1 == d2) puts("OK");
	else puts("NG");

	return 0;
}
