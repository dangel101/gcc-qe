#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/gfortran/bz533183-common-block-thread-private-variables-out-of-scope-inside-OpenMP-parallel-region
#   Description: common block thread private variables out of scope inside OpenMP parallel region
#   Author: Michal Nowak <mnowak@redhat.com>
#	    Marek Polacek <polacek@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2010, 2012 Red Hat, Inc. All rights reserved.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 3 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include rhts environment
. /usr/bin/rhts-environment.sh
. /usr/lib/beakerlib/beakerlib.sh

PACKAGES=(gcc gcc-gfortran glibc gdb)

# Choose the compiler.
GFORTRAN=${GFORTRAN:-gfortran}

rlJournalStart
    rlPhaseStartSetup
        for p in "${PACKAGES[@]}"; do
            rlAssertRpm "$p"
        done; unset p
        rlRun "TmpDir=\`mktemp -d\`" 0 "Creating tmp directory"
	# We need the reproducer.
	cp tpcommon-init.f tpcommon-init.batch $TmpDir
        rlRun "pushd $TmpDir"
    rlPhaseEnd

      for opt in s $(seq 0 3); do
        rlPhaseStartTest
            rlRun "$GFORTRAN -fopenmp -g -O$opt -o tpcommon-init tpcommon-init.f"
            rlAssertExists "tpcommon-init"
            rlRun "./tpcommon-init"
	    rlRun "gdb -batch -x tpcommon-init.batch ./tpcommon-init > /dev/null 2> stderr.gdb"
	    cat stderr.gdb
	    rlAssertNotGrep "No symbol \"istart\" in current context." stderr.gdb
        rlPhaseEnd
      done

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
