! "Parallel Programming in OpenMP" ISBN 1-55860-671-8
! Copyright 2001 by Academic Press
! Example 4.7 (pp 104-105) with slight modifications

! AIX:
!   xlf90    -qsmp=noopt -g -O0 -o tpcommon_64_xlf90_9.1.0.3-qsmp=noopt-g-O0 \
!     tpcommon.f
!
! Linux x86:
!   pgf95    -mp         -g -O0 -o tpcommon_pgf95_6.2-3-mp-g-O0              \
!     tpcommon.f
!   gfortran -fopenmp    -g -O0 -o tpcommon_gfortran tpcommon.f
!   ifort    -openmp     -g -O0 -o tpcommon_ifort_10.0.013-openmp-g-O0       \
!     tpcommon.f
!   pathf90  -mp         -g -O0 -o tpcommon_pathf90_2.5-mp-g-O0              \
!     tpcommon.f
!   setenv PSC_OMP_STACK_SIZE 100000

      program correct
      use omp_lib
      integer iarray(100), N, nthreads, iam, chunk, istart, iend
      common /bounds/ istart, iend
!$omp threadprivate(/bounds/)

      N = 100
      istart = 1
!$omp parallel private(nthreads, iam, chunk)

      ! Compute the subset of iterations executed by each thread
      nthreads = omp_get_num_threads()
      iam = omp_get_thread_num()
      chunk = (N + nthreads - 1)/nthreads
      istart = iam * chunk + 1
      iend = min((iam + 1) * chunk, N)
      print *, nthreads, iam, chunk, istart, iend

      call work(iarray)
!$omp end parallel
      print *
      print *, iarray
      PRINT *, "Hello, World."
      end

      subroutine work(iarray)

      ! Subroutine to operate on a thread's portion of the array "iarray"
      integer iarray(100), istart, iend, i
      common /bounds/ istart, iend
!$omp threadprivate(/bounds/)

      do i = istart, iend
          iarray(i) = i * i
      enddo
      return
      end
