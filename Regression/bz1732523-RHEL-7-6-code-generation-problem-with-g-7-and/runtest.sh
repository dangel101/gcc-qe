#!/usr/bin/env bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz1732523-RHEL-7-6-code-generation-problem-with-g-7-and
#   Description: Test for BZ#1732523 (RHEL 7.6 - code generation problem with g++ 7 and)
#   Author: Vaclav Kadlcik <vkadlcik@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2021 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

# RHBZ#1732523 was explicitly fixed in downstream GCC 11 but the test
# passes with some older GCCs as well. The set of expected passes in
# currently supported GCCs is as follows:
#   - system GCC of:
#     - RHEL-7
#     - RHEL-8.5+
#     - RHEL-9 and newer
#   - toolsets 11 and newer
# Suggested TCMS relevancy:
#   distro < rhel-7 && collection !defined: False
#   distro = rhel-8 && distro < rhel-8.5 && collection !defined: False
#   collection contains devtoolset-9, gcc-toolset-9, devtoolset-10, gcc-toolset-10: False

GCC="${GCC:-$(type -P gcc)}"
PACKAGE=$(rpm --qf '%{name}' -qf $GCC)
PACKAGES="${PACKAGE} ${PACKAGE}-c++"

rlJournalStart
    rlPhaseStartSetup
        rlLogInfo "PACKAGES=$PACKAGES"
        rlLogInfo "COLLECTIONS=$COLLECTIONS"
        rlLogInfo "GCC=$GCC"
        rlLogInfo "SKIP_COLLECTION_METAPACKAGE_CHECK=$SKIP_COLLECTION_METAPACKAGE_CHECK"

        # We optionally need to skip checking for the presence of the metapackage
        # because that would pull in all the dependent toolset subrpms.  We do not
        # always want that, especially in CI.
        _COLLECTIONS="$COLLECTIONS"
        if ! test -z $SKIP_COLLECTION_METAPACKAGE_CHECK; then
            for c in $SKIP_COLLECTION_METAPACKAGE_CHECK; do
                rlLogInfo "ignoring metapackage check for collection $c"
                export COLLECTIONS=$(shopt -s extglob && echo ${COLLECTIONS//$c/})
            done
        fi

        rlLogInfo "(without skipped) COLLECTIONS=$COLLECTIONS"

        for i in $PACKAGES; do
            rpm -q $i &>/dev/null || rlRun "yum -y install $i"
        done
        rlAssertRpm --all

        export COLLECTIONS="$_COLLECTIONS"

        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "cp reproducers/* $TmpDir"
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    if [[ $(arch) = x86_64 ]]; then

        rlPhaseStartTest LLL
            rlRun 'g++ -Wno-deprecated -Wno-write-strings -Wno-int-to-pointer-cast -g -fno-inline -fno-omit-frame-pointer -fno-builtin -std=gnu++11 -fpermissive -fPIC -fsanitize=address -Wdelete-non-virtual-dtor -Wreturn-type -Wconversion-null -Wuninitialized -Wdiv-by-zero -Wenum-compare -Woverflow -Wpointer-arith -Wno-narrowing -Wno-main -Wno-deprecated-declarations -Wno-attributes -Wno-maybe-uninitialized -c LLL.cpp &>LLL.out'
            rlAssertNotGrep 'is already defined' LLL.out
            rlFileSubmit LLL.out LLL.out
        rlPhaseEnd

    fi

    rlPhaseStartTest reduced
        rlRun 'g++ -c reduced.cpp &>reduced.out'
        rlAssertNotGrep 'is already defined' reduced.out
        rlFileSubmit reduced.out reduced.out
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
