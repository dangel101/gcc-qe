#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz722587-RFE-gdb-accessibility-of-unused-template-methods
#   Description: Test for bz722587 ([RFE] gdb accessibility of unused template methods)
#   Author: Marek Polacek <mpolacek@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2011, 2012 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh
. /usr/lib/beakerlib/beakerlib.sh

PACKAGES=(gcc gcc-c++ glibc binutils)

# Choose the compiler.
GXX=${GXX:-g++}

rlJournalStart
    rlPhaseStartSetup
        for p in "${PACKAGES[@]}"; do
            rpm -q "$p" &>/dev/null || yum -y install "$p"
            rlAssertRpm "$p"
        done; unset p
        rlRun "TmpDir=\`mktemp -d\`" 0 "Creating tmp directory"
        # We need the reproducer.
        rlRun "cp -v attr-used-1.C $TmpDir"
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    rlPhaseStartTest
	# So, first get an assembly.  Here we need to see two labels: _ZN1AIiE1fEv
	# and _ZN1AIiE1tE.
	rlRun "$GXX -S attr-used-1.C"
	rlAssertExists attr-used-1.s
	rlAssertGrep "^_ZN1AIiE1fEv" attr-used-1.s
	rlAssertGrep "^_ZN1AIiE1tE" attr-used-1.s

	# Use -Dused=.  Now we shouldn't see the labels.
	rlRun "$GXX -S -Dused= attr-used-1.C"
	rlAssertExists attr-used-1.s
	rlAssertNotGrep "^_ZN1AIiE1fEv" attr-used-1.s
	rlAssertNotGrep "^_ZN1AIiE1tE" attr-used-1.s
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
