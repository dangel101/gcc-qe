#!/usr/bin/perl

$found_faulty_add = 0;
$found_faulty_offset_usage = 0;
$faulty_reg = "";

while (<>)
{
	chomp;
	if (($reg) = /add\s+([a-z][0-9]+),\s*\1,\s*\1/)
	{
#		print "prooser (reg: $reg): $_\n";
		$found_faulty_add++;
		$faulty_reg = $reg;
		next;
	}
	if (/ldrh\s+.*\[$faulty_reg\]/)
	{
		$found_faulty_offset_usage++;
		$faulty_reg = "";
		next;
	}
}

if ($found_faulty_add)
{
	print "FAULTY ADD FOUND: $found_faulty_add times\n";
	if ($found_faulty_offset_usage)
	{
		print "FAULTY OFFSET USED: $found_faulty_offset_usage times\n";
		exit $found_faulty_offset_usage;
	}
	else
	{
		print "BUT SEEMS NOT BEING USED\n";
		exit 99;
	}
}
