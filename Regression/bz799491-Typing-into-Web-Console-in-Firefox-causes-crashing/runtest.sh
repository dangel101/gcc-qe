#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz799491-Typing-into-Web-Console-in-Firefox-causes-crashing
#   Description: Test for bz799491 (Typing into Web Console in Firefox causes crashing)
#   Author: Marek Polacek <polacek@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2012 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh
. /usr/lib/beakerlib/beakerlib.sh

# See http://gcc.gnu.org/bugzilla/show_bug.cgi?id=52430
# The problem is that ipcp_need_redirect_p does not redirect back the
# problematic call because n_cloning_candidates == 0.

PACKAGES=(gcc gcc-c++ tar binutils)

# Choose the compiler.
GXX=${GXX:-g++}

rlJournalStart
    rlPhaseStartSetup
        for p in "${PACKAGES[@]}"; do
            rlAssertRpm "$p" || yum -y install "$p"
        done; unset p

        GCC_DUMPVERSION=$(gcc -dumpversion)
        GCC_MAJOR_VERSION=${GCC_DUMPVERSION%%.*}
        [[ "$GCC_MAJOR_VERSION" -gt 10 ]] && ADDITIONAL_PARAMETERS='-std=c++14' || ADDITIONAL_PARAMETERS=''
        rlLogInfo "GCC_DUMPVERSION=$GCC_DUMPVERSION"
        rlLogInfo "GCC_MAJOR_VERSION=$GCC_MAJOR_VERSION"
        rlLogInfo "ADDITIONAL_PARAMETERS=$ADDITIONAL_PARAMETERS"

        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "cp -v rep.xz $TmpDir"
        rlRun "pushd $TmpDir"
        rlRun "tar xJvf rep.xz"
    rlPhaseEnd

    rlPhaseStartTest
        rlRun "$GXX $ADDITIONAL_PARAMETERS -c -fPIC -fno-rtti -pedantic -fno-exceptions -fstack-protector --param ssp-buffer-size=4 -m64 -mtune=generic -fpermissive -fno-exceptions -fno-strict-aliasing -fshort-wchar -ffunction-sections -fdata-sections -Os -freorder-blocks -fomit-frame-pointer -fpreprocessed dombindings.ii"
        rlAssertExists "dombindings.o"
        rlRun "nm dombindings.o > nm.out"
        # There shouldn't be e.g.
        # 0000000000000000 t _ZN15nsBaseHashtableI16nsDepCharHashKeyP8JSObjectS2_E4InitEj.clone.2
        # 0000000000000000 t _ZN15xpcObjectHelperC2EP11nsISupportsS1_P14nsWrapperCacheb.clone.1
        # 0000000000000000 t _ZN2js6VectorIlLm8ENS_15TempAllocPolicyEE20calculateNewCapacityEmmRm.clone.3
        rlAssertGrep "W _ZN15xpcObjectHelperC1EP11nsISupportsS1_P14nsWrapperCacheb$" "nm.out"
        rlAssertGrep "W _ZN15xpcObjectHelperC2EP11nsISupportsS1_P14nsWrapperCacheb$" "nm.out"
        rlAssertGrep "W _ZN2js6VectorIlLm8ENS_15TempAllocPolicyEE20calculateNewCapacityEmmRm$" "nm.out"
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
