#!/usr/bin/env bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz2022588-gcc-Linking-against-libgcc-s-should-imply-linking
#   Description: Test for BZ#2022588 (gcc Linking against libgcc_s should imply linking)
#   Author: Vaclav Kadlcik <vkadlcik@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2021 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

# Expected to work on non-s390x architectures with
#   * base GCC RHEL 8.6+
#   * any gcc-toolset
# Suggested TCMS relevancy:
#   arch = s390x: False
#   distro < rhel-8: False
#   distro = rhel-8 && distro < rhel-8.6 && collection !defined: False

GCC="${GCC:-$(type -P gcc)}"

GCC_RPM_NAME=$(rpm --qf '%{name}' -qf $GCC)
GCCCPP_RPM_NAME=${GCC_RPM_NAME}-c++

PACKAGES="$GCC_RPM_NAME $GCCCPP_RPM_NAME"

rlJournalStart
    rlPhaseStartSetup
        rlRun "yum -y install --skip-broken $PACKAGES" 0-255 # best effort

        rlLogInfo "PACKAGES=$PACKAGES"
        rlLogInfo "COLLECTIONS=$COLLECTIONS"
        rlLogInfo "GCC=$GCC"
        rlLogInfo "SKIP_COLLECTION_METAPACKAGE_CHECK=$SKIP_COLLECTION_METAPACKAGE_CHECK"

        # We optionally need to skip checking for the presence of the metapackage
        # because that would pull in all the dependent toolset subrpms.  We do not
        # always want that, especially in CI.
        _COLLECTIONS="$COLLECTIONS"
        if ! test -z $SKIP_COLLECTION_METAPACKAGE_CHECK; then
            for c in $SKIP_COLLECTION_METAPACKAGE_CHECK; do
                rlLogInfo "ignoring metapackage check for collection $c"
                export COLLECTIONS=$(shopt -s extglob && echo ${COLLECTIONS//$c/})
            done
        fi
        rlLogInfo "(without skipped) COLLECTIONS=$COLLECTIONS"
        rlAssertRpm --all
        export COLLECTIONS="$_COLLECTIONS"

        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "pushd $TmpDir"

        rlRun "rpm -ql $GCC_RPM_NAME >gcc.list"
    rlPhaseEnd

    rlPhaseStartTest relevant_architecture
        rlRun "ARCH=$(arch)"
        if [[ "$ARCH" == s390* ]]; then
            rlFail "This test is not expected to run on $ARCH, exclude it by e.g. TCMS Relevancy"
            rlFail "See libgcc_s.so creation in gcc.spec if in doubt"
        fi
    rlPhaseEnd

    rlPhaseStartTest imply_libgcc
        LIBGCC_S_SO_LIST=$(grep '/libgcc_s\.so$' gcc.list)
        if [[ -z "$LIBGCC_S_SO_LIST" ]]; then
            rlFail "No libgcc_s.so found"
        else
            for i in $LIBGCC_S_SO_LIST; do
                rlAssertGrep 'GNU ld script' $i
                rlAssertGrep '^GROUP .* libgcc.a ' $i
            done
        fi
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
