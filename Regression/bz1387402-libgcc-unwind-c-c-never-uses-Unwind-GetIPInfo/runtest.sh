#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz1387402-libgcc-unwind-c-c-never-uses-Unwind-GetIPInfo
#   Description: Test for BZ#1387402 (libgcc unwind-c.c never uses _Unwind_GetIPInfo)
#   Author: Michael Petlan <mpetlan@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2017 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

GCC=${GCC:-$(type -P gcc)}
GCC_RPM_NAME=$(rpm --qf '%{name}' -qf $GCC)
PACKAGES="$GCC_RPM_NAME ${GCC_RPM_NAME%gcc}binutils"

PRI_ARCH=$(rlGetPrimaryArch)
SEC_ARCH=$(rlGetSecondaryArch)
if rlIsRHEL '>7' && [[ "$PRI_ARCH" != x86_64 ]]; then
    SEC_ARCH=''
fi

LIBGCC_RPMS=''
for arch in $PRI_ARCH $SEC_ARCH; do
    LIBGCC_RPMS+=" libgcc.$arch"
done

PACKAGES+="$LIBGCC_RPMS"

rlJournalStart
    rlPhaseStartSetup
        rlLogInfo "PACKAGES=$PACKAGES"
        rlLogInfo "COLLECTIONS=$COLLECTIONS"
        rlLogInfo "GCC=$GCC"
        rlLogInfo "SKIP_COLLECTION_METAPACKAGE_CHECK=$SKIP_COLLECTION_METAPACKAGE_CHECK"

        # We optionally need to skip checking for the presence of the metapackage
        # because that would pull in all the dependent toolset subrpms.  We do not
        # always want that, especially in CI.
        _COLLECTIONS="$COLLECTIONS"
        if ! test -z $SKIP_COLLECTION_METAPACKAGE_CHECK; then
            for c in $SKIP_COLLECTION_METAPACKAGE_CHECK; do
                rlLogInfo "ignoring metapackage check for collection $c"
                export COLLECTIONS=$(shopt -s extglob && echo ${COLLECTIONS//$c/})
            done
        fi
        rlLogInfo "(without skipped) COLLECTIONS=$COLLECTIONS"
        rlAssertRpm --all
        export COLLECTIONS="$_COLLECTIONS"

        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "cp extract_function.pl $TmpDir"
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    for i in $LIBGCC_RPMS; do

        rlPhaseStartTest $i

            rlRun "rpm -ql $i >files.txt"
            rlRun 'grep "\.so\.[\.0-9]*" files.txt >candidates.txt'
            count=0

            for file in $(<candidates.txt); do

                # disassemble the object file and find the desired function, if this fails,
                # the function is not there and the test tests nothing!
                rlRun "objdump -d --reloc $file | ./extract_function.pl __gcc_personality_v0 >libgcc.dump"

                # check whether it contains the desired symbol reference
                # (this actually verifies the bug)
                rlAssertGrep _Unwind_GetIPInfo libgcc.dump

                (( count++ ))
            done

            if [[ "$count" -eq 0 ]]; then
                rlFail "No .so.* found in $i"
            else
                rlPass "At least one .so.* of $i tested"
            fi

        rlPhaseEnd

    done

    rlPhaseStartCleanup
        rlRun popd
        rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
