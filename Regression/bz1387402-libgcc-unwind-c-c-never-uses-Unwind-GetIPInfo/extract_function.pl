#!/usr/bin/perl

$func = shift;
$in = 0;

while (<>)
{
	if ($in)
	{
		last if /^[\da-fA-F]+/;
		print;
	}
	else
	{
		if (/<$func[>@]/)
		{
			print;
			$in = 1;
		}
	}
}

die "FUNCTION NOT FOUND\n" unless $in;
