#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz745004-Global-variable-nested-by-another-dso-shows-up
#   Description: Test for bz745004 (Global variable nested by another dso shows up)
#   Author: Marek Polacek <mpolacek@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2011 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh
. /usr/lib/beakerlib/beakerlib.sh

PACKAGES=(gcc gdb) 

# Choose the binaries.
GCC=${GCC:-gcc}
GDB=${GDB:-gdb}

rlJournalStart
    rlPhaseStartSetup
        for p in "${PACKAGES[@]}"; do
            rlAssertRpm "$p" || yum -y install "$p"
        done; unset p
        rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
	# We need the reproducers.
	cp -v cmds test.c test_main.c $TmpDir
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    rlPhaseStartTest
    	rlRun "cp -f test.c test2.c"
	rlRun "$GCC -g -fPIC -DEXPORT_FUNC=C_Main -o testC.o -c test.c"
	rlRun "$GCC -g -fPIC -DEXPORT_FUNC=B_Main -o testB.o -c test2.c"
	rlRun "$GCC -g -shared -o libB.so testB.o"
	rlRun "$GCC -g -shared -o libC.so testC.o"
	rlRun "$GCC -fPIC -g -o test_main.o  -c test_main.c"
	rlRun "$GCC -L$PWD -g  -o test_main test_main.o -lB -lC"

	rlRun "( LD_LIBRARY_PATH=. $GDB -q test_main -x cmds ) &> out"
	rlAssertGrep "\$1 = 42" out
	rlLog "$(<out)"
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
