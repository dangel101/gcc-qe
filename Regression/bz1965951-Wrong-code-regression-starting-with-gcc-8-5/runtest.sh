#!/usr/bin/env bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz1965951-Wrong-code-regression-starting-with-gcc-8-5
#   Description: Test for BZ#1965951 (Wrong-code regression starting with gcc 8.5)
#   Author: Vaclav Kadlcik <vkadlcik@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2021 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

# Should pass in all system GCCs back to RHEL-7. As for toolsets, GCC 10 was
# downstreamed before the upstream bug
#   https://gcc.gnu.org/bugzilla/show_bug.cgi?id=95719
# got the GCC 10 backport. Hence just the toolsets 11+ are relevant.
# Suggested TCMS relevancy:
#   distro < rhel-7 && collection !defined: False
#   collection contains gcc-toolset-9, gcc-toolset-10, devtoolset-9, devtoolset-10: False

GCC="${GCC:-$(type -P gcc)}"
PACKAGE=$(rpm --qf '%{name}\n' -qf $GCC | head -1)
PACKAGES="${PACKAGE} ${PACKAGE}-c++"

rlJournalStart
    rlPhaseStartSetup
        rlLogInfo "PACKAGES=$PACKAGES"
        rlLogInfo "COLLECTIONS=$COLLECTIONS"
        rlRun "dnf -y install $PACKAGES" 0-255
        for i in $PACKAGES; do
            rlAssertRpm $i
        done
        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "cp reproducer.cc $TmpDir"
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    rlPhaseStartTest
        rlRun "g++ -o reproducer reproducer.cc"
        rlRun "./reproducer"
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
