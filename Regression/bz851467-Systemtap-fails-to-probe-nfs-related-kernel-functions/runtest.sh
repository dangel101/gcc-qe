#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz851467-Systemtap-fails-to-probe-nfs-related-kernel-functions
#   Description: Test for BZ#851467 ([FJ7.0 Bug] apparent debuginfo quality regression,)
#   Author: Dagmar Prokopova <dprokopo@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2014 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

# Notes on relevancy
#
# The test, unfortunately fails quite often because of bugs in systemtap
# itself, e.g. RHBZ#2044385, RHBZ#1997192, RHBZ#1973751. It makes sense
# to exclude it from running where fixes to systemtap are no longer to be
# expected.
#
# Suggested TCMS relevancy:
#   collection contains devtoolset-10: False

PACKAGES=(gcc systemtap kernel-debuginfo)

rlJournalStart
    rlPhaseStartSetup
        for p in "${PACKAGES[@]}"; do
            rpm -q "$p" &>/dev/null || yum -y install "$p"
            rlAssertRpm "$p"
        done; unset p
        rlLog "RUNNING KERNEL: `uname -r` (should match the kernel-debuginfo version)"
        rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
        # We need the reproducers.
        cp -v nfs.proc.read_done.stp $TmpDir
        cp -v nfs.proc.write_done.stp $TmpDir
        rlRun "pushd $TmpDir"
        rlRun "stap-prep" 0-255
    rlPhaseEnd

    rlPhaseStartTest
        extra_switches=""
        arch | grep -q 'ppc64le' && extra_switches="-P"
        rlRun "stap $extra_switches -p4 -v nfs.proc.read_done.stp" 0
        rlRun "stap $extra_switches -p4 -v nfs.proc.write_done.stp" 0
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
