#include <iostream>
#include <list>
#include <algorithm>
#include <functional>
#include <string>
#include <map>

class Data
{
public:
    Data(int a=0);
    ~Data();
    Data& operator=(const Data& other);
    Data(const Data& other);
    int getData()const;
private:
    int data_;
    std::map<std::string, std::string> mapData_;
};

class A
{
public:
    A(){}
    virtual ~A(){}
    virtual Data returnData()
    {
         std::cout << "Get Data" << std::endl;
         return 5;
    }
private:
    A(const A& other);
    A& operator=(const A& other);
};


class B : public A
{
public:
    B(): A(){}
    ~B(){}
    Data returnData()
    {
        std::cout << "B: " << std::endl;
        return 10;
    }
private:
    B(const B& other);
    B& operator=(const B& other);
};

std::list<B*> createDataList(int size)
{
    std::list<B*> dataList;
    for (int i = 0 ; i < size ; ++i)
    {
        dataList.push_back(new B() );
    } 
    return dataList;
}

int main(int, char**)
{
    std::list<B*> dataList = createDataList(100);

    std::for_each(dataList.begin(), dataList.end(),
                  std::mem_fun(&B::returnData) );
    std::cout << "End" << std::endl;    
    return 0;
}


Data::Data(int a)
:data_(a)
,mapData_()
{
}

Data::~Data()
{
}

Data& Data::operator=(const Data& other)
{
    data_ = other.data_;
    mapData_ = other.mapData_;
    return *this;
}
Data::Data(const Data& other)
: data_(other.data_)
, mapData_(other.mapData_)
{
}

int Data::getData() const
{
  return data_;
}

