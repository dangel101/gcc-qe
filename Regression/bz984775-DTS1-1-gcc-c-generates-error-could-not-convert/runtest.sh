#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz984775-DTS1-1-gcc-c-generates-error-could-not-convert
#   Description: Test for BZ#984775 (DTS1.1 gcc-c++ generates error could not convert)
#   Author: Dagmar Prokopova <dprokopo@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2013 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGES=(gcc gcc-c++)

GXX=${GXX:-c++}

rlJournalStart
    rlPhaseStartSetup
        for p in "${PACKAGES[@]}"; do
            rlAssertRpm "$p" || yum -y install "$p"
        done; unset p 
        rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
        rlLog "GXX = $GXX"
        rlLog "Installed within `rpmquery -f $(which $GXX)`"
        # We need the reproducer.
        cp -v funcptrtmplarg.C $TmpDir
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    rlPhaseStartTest
        rlRun "$GXX funcptrtmplarg.C -o it.o" 0
        rlRun "$GXX -O0 funcptrtmplarg.C -o it.o" 0
        rlRun "$GXX -O1 funcptrtmplarg.C -o it.o" 0
        rlRun "$GXX -O2 funcptrtmplarg.C -o it.o" 0
        rlRun "$GXX -O3 funcptrtmplarg.C -o it.o" 0
        rlRun "$GXX -Os funcptrtmplarg.C -o it.o" 0
        echo "int main(void){return 0;}" | $GXX -Ofast -x c++ - -o a.out
        test $? -eq 0 && rlRun "$GXX -Ofast funcptrtmplarg.C -o it.o" 0
        echo "int main(void){return 0;}" | $GXX -Og -x c++ - -o a.out
        test $? -eq 0 && rlRun "$GXX -Og -g funcptrtmplarg.C -o it.o" 0
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
