#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/ppc64le-gcc-internal-settings
#   Description: The test checks whether the gcc has been configured properly.
#   Author: Michael Petlan <mpetlan@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2016 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="gcc"

rlJournalStart
	rlPhaseStartSetup
		rlAssertRpm $PACKAGE
		if rlIsRHEL '<9'; then
			POWER_TO_OPTIMIZE_TO=power8 # RHBZ#1213268
		else
			POWER_TO_OPTIMIZE_TO=power9 # RHBZ#1870028; related: RHBZ#1876584, RHBZ#1876436
		fi
		rlLogInfo "POWER_TO_OPTIMIZE_TO=$POWER_TO_OPTIMIZE_TO"
		echo "int main(void){return 0;}" > t.c
		rlAssertExists "t.c"
		rlRun "gcc -v -o t_be.o -mbig  t.c 2> LOG" 0,1,2 "Trying to build t.c (should fail)"
		cat LOG
	rlPhaseEnd

	rlPhaseStartTest "optimize gcc to $POWER_TO_OPTIMIZE_TO"
		rlAssertGrep "with-cpu-64=$POWER_TO_OPTIMIZE_TO" LOG
		rlAssertGrep "with-tune-64=$POWER_TO_OPTIMIZE_TO" LOG
	rlPhaseEnd

	rlPhaseStartTest "bz1296211 - do not use --oformat elf32-powerpc"
		rlAssertNotGrep "elf32-powerpc" LOG
	rlPhaseEnd

	rlPhaseStartCleanup
		rlRun "rm -f t.c LOG" 0 "Removing tmp files"
	rlPhaseEnd
rlJournalPrintText
rlJournalEnd
