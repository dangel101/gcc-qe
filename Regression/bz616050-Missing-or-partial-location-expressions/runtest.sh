#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/gcc/bz616050-Missing-or-partial-location-expressions
#   Description: Missing or partial location expressions
#   Author: Michal Nowak <mnowak@redhat.com>
#    	    Marek Polacek <polacek@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2010, 2012 Red Hat, Inc. All rights reserved.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 3 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function runtest {
    rlPhaseStartTest "compiler:$compiler opt:$opt pr:$pr"
	rlRun "$compiler -O${opt} -g ${pr}.c -o ${pr}" 0 "$compiler $opt $pr"
	gdb -batch -x ${pr}.batch -q $pr | tee ${pr}.gdb
	if [ "$pr" = "pr1" ]; then
		rlAssertGrep '$1 = 0x8078' ${pr}.gdb
		rlAssertGrep '$2 = 0xffff8078' ${pr}.gdb
	elif [ "$pr" = "pr2" ]; then
		rlAssertGrep '$1 = 14' ${pr}.gdb
	fi
    rlPhaseEnd
}

# Include rhts environment
. /usr/bin/rhts-environment.sh
. /usr/lib/beakerlib/beakerlib.sh

PACKAGES=(gcc gdb gcc-c++)

# Choose the compiler.
GCC=${GCC:-gcc}
GXX=${GXX:-g++}


rlJournalStart
    rlPhaseStartSetup
        for p in "${PACKAGES[@]}"; do
            rlAssertRpm "$p"
        done; unset p
        rlRun "TmpDir=\`mktemp -d\`" 0 "Creating tmp directory"
	cp pr1.{c,batch} pr2.{c,batch} $TmpDir
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    arch="$(uname -i)"
    compilers="$GCC $GXX"

    for compiler in $compilers; do
      if [ -x /usr/bin/$compiler ]; then
	for opt in s $(seq 0 3); do
		for pr in pr1 pr2; do
		    if [ "${pr}" = "pr1" ]; then
		        if [ "${arch}" = "i386" ] || [ "${arch}" = "i686" ] || [ "${arch}" = "x86_64" ]; then
  			    runtest
			fi
		    else
		        runtest
		    fi
		done
	done
      fi
    done

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
