/* Make sure we have debug info for "problem".  */
/* { dg-do compile } */
/* { dg-options "-O0 -g2" } */

extern void f (int *);

class A
{
public:
 A(int i);
};

A::A(int i)
{
 int *problem = new int(i);
 f (problem);
}

/* { dg-final { scan-assembler "problem" } } */
