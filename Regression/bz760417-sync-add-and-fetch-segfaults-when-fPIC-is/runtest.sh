#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz760417-sync-add-and-fetch-segfaults-when-fPIC-is
#   Description: Test for bz760417 (__sync_add_and_fetch segfaults when -fPIC is)
#   Author: Marek Polacek <polacek@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2012 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh
. /usr/lib/beakerlib/beakerlib.sh

# http://gcc.gnu.org/bugzilla/show_bug.cgi?id=39431

# The problem is that the memory_operand in the insns also needs registers, and
# as the insn before RA has (mem:DI (plus:SI (reg:SI reg1) (reg:SI reg2))), it
# needs
# 2 registers, not just one or zero.  And that is already one too much.  I'd say
# we need to handle only a subset of valid memory_operand operands in these 2
# patterns, those that need zero or one register.

# The code generated from compiler intrinsic __sync_add_and_fetch (on a 64bit
# value) is erroneous when the -fPIC switch is enabled.

# It seems that when the -fPIC switch is enabled, instruction CMPXCHG8B
# references register EBX as a base pointer for the memory operand, whereas EBX
# must contain part of the value to be incremented.

# i?86 only.
# Fixed in gcc-4.1.2-54.el5.

PACKAGES=(gcc grep)

# Choose the compiler.
GCC=${GCC:-gcc}

rlJournalStart
  rlPhaseStartSetup
    for p in "${PACKAGES[@]}"; do
      rlAssertRpm "$p" || yum -y install "$p"
    done; unset p
    rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
    # We will need the reproducer.
    rlRun "cp -v test.c $TmpDir"
    rlRun "pushd $TmpDir"
  rlPhaseEnd

if [[ $(uname -m) == "i686" ]]; then
  rlPhaseStartTest
    # Get path to the cc1.
    cc1_bin=`$GCC -print-prog-name=cc1`
    rlRun "$cc1_bin -E -quiet -v test.c -m32 -march=i686 -fPIC -fworking-directory -fpch-preprocess -o test.i"
    rlAssertExists "test.i"
    rlRun "$cc1_bin -fpreprocessed test.i -quiet -dumpbase test.c -m32 -march=i686 -auxbase main -g -version -fPIC -o test.s"
    rlAssertExists "test.s"

    # We don't want to see %ebx here.
    rlAssertNotGrep "cmpxchg8b.*counter@GOTOFF(%ebx)" test.s
  rlPhaseEnd
fi

  rlPhaseStartCleanup
    rlRun "popd"
    rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
  rlPhaseEnd
rlJournalPrintText
rlJournalEnd
