static unsigned long long int counter = 0;

int main(int argc, char* argv[]) {
   return __sync_add_and_fetch(&counter, 1);
}
