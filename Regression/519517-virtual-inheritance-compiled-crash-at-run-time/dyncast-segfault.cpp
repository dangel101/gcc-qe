#include <iostream>

using namespace std;

///////////////

// fwd. declaration
class my_object;

// global variable
my_object* my_module_ptr = 0;

///////////////

class my_object //: public my_object_base 
{
public:
  my_object() { cout << "in my_object ctor" << endl;}
  virtual ~my_object(); 
};

///////////////

class my_module : public my_object {
public:
  my_module() { 
    cout << "in my_module ctor, setting up ptr" << endl; 
    my_module_ptr = this;
  }
  ~my_module() { cout << "in my_module dtor" << endl;}
};

my_object::~my_object() {
  cout << "in my_object dtor" << endl;
  cout << "before DCASTing to my_module*" << endl;
  my_module* m = dynamic_cast<my_module*>(my_module_ptr);
  cout << "after DCASTing to my_module*" << endl;
}

///////////////

class my_interface {
public:
  my_interface() { cout << "in my_interface ctor" << endl;}
  ~my_interface() { cout << "in my_interface dtor" << endl;}
};

class myif : public virtual my_interface 
{
public:
  myif() { cout << "in myif ctor" << endl;}
  ~myif() { cout << "in myif dtor" << endl;}
};

///////////////

template <typename DATA = int>
class synchro_slave_base :
  public virtual myif
{
public:
  synchro_slave_base() { cout << "in synchro_slave_base ctor" << endl; }
  ~synchro_slave_base() { cout << "in synchro_slave_base dtor" << endl; }

protected:
  my_object o;
};

template <typename DATA>
class synchro_target : 
  public my_module, 
  public synchro_slave_base<DATA>,
  public synchro_slave_base<> 
{
public:
  synchro_target() { cout << "in synchro_target ctor" << endl;}
  ~synchro_target() { cout << "in synchro_target dtor" << endl; }
};

//////////

int main(int, char**) {
  synchro_target<bool> t;
  return 0;
}

