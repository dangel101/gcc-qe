/* Compile with -mcpu=power6 or -mavoid-indexed-addresses */

long long
b64 (long long *p)
{
  return __builtin_bswap64 (*p);
}

int
b32 (int *p)
{
  return __builtin_bswap32 (*p);
}

void main(){}

