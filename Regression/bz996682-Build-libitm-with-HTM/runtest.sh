#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc-libraries/Regression/bz996682-Build-libitm-with-HTM
#   Description: Test for BZ#996682 (Build libitm with HTM)
#   Author: Dagmar Prokopova <dprokopo@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2013 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

# libitm SO ships in system GCC only. While GTS/DTS ship their *-libitm-devel
# RPMs, they have a static build and an *.so file which is just a linker script
# pointing to the system SO. Therefore we can test just system GCC's libitm.
#
# Of the supported systems, we have exclude RHEL-6, where system binutils is
# too old to be used for testing. In theory we could install some devtoolset
# binutils but creating souch a workaround isn't worth the hassle, RHEL-6
# won't see invasive GCC update, if any at all.
#
# Lastlu, the feature being tested is PC-specific.
#
# Suggested TCMS relevancy:
#   distro = rhel-6: False
#   arch != x86_64: False
#   collection defined: False
#
# As for 32-bit and 64-bit builds on the remaining applicable RHELs:
# * RHEL-7
#   * only x86_64, ppc64 and s390x have both
# * RHEL-8+
#   * only x86_64 has both

GCC=${GCC:-$(type -P gcc)}
GCC_RPM_NAME=$(rpm --qf '%{name}' -qf $GCC)
PACKAGES="$GCC_RPM_NAME ${GCC_RPM_NAME%gcc}binutils"

PRI_ARCH=$(rlGetPrimaryArch)
SEC_ARCH=$(rlGetSecondaryArch)
if rlIsRHEL '>7' && [[ "$PRI_ARCH" != x86_64 ]]; then
    SEC_ARCH=''
fi

for arch in $PRI_ARCH $SEC_ARCH; do
    PACKAGES+=" libitm.$arch"
done

rlJournalStart
    rlPhaseStartSetup
        rlLogInfo "PACKAGES=$PACKAGES"
        rlLogInfo "COLLECTIONS=$COLLECTIONS"
        rlLogInfo "GCC=$GCC"
        rlLogInfo "SKIP_COLLECTION_METAPACKAGE_CHECK=$SKIP_COLLECTION_METAPACKAGE_CHECK"

        # We optionally need to skip checking for the presence of the metapackage
        # because that would pull in all the dependent toolset subrpms.  We do not
        # always want that, especially in CI.
        _COLLECTIONS="$COLLECTIONS"
        if ! test -z $SKIP_COLLECTION_METAPACKAGE_CHECK; then
            for c in $SKIP_COLLECTION_METAPACKAGE_CHECK; do
                rlLogInfo "ignoring metapackage check for collection $c"
                export COLLECTIONS=$(shopt -s extglob && echo ${COLLECTIONS//$c/})
            done
        fi
        rlLogInfo "(without skipped) COLLECTIONS=$COLLECTIONS"
        rlAssertRpm --all
        export COLLECTIONS="$_COLLECTIONS"

        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    for arch in $PRI_ARCH $SEC_ARCH; do

        rlPhaseStartTest libitm.$arch

            rlRun "rpm -ql libitm.$arch >files.txt"
            rlRun 'grep "/libitm\.so\.[\.0-9]*" files.txt >candidates.txt'
            count=0
            for file in $(<candidates.txt); do
                rlRun "objdump -dr $file >objdump.txt"
                rlAssertGrep xbegin objdump.txt
                rlAssertGrep xend   objdump.txt
                (( count++ ))
            done
            if [[ "$count" -eq 0 ]]; then
                rlFail "No libitm.so.* found in libitm.$arch"
            else
                rlPass "At least one libitm.so.* of libitm.$arch tested"
            fi

        rlPhaseEnd

    done

    rlPhaseStartCleanup
        rlRun popd
        rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
