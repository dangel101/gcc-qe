#include <boost/intrusive/list.hpp>

template< typename T >
class ListElement : public boost::intrusive::list_base_hook<>
{
public:
    boost::intrusive::list_member_hook<> _memberHook;
    typedef boost::intrusive::member_hook< ListElement, boost::intrusive::list_member_hook<>, &ListElement::_memberHook > MemberOption;

    boost::intrusive::list_member_hook<> _unconfirmedListMemberHook;
    typedef boost::intrusive::member_hook< ListElement, boost::intrusive::list_member_hook<>, &ListElement::_unconfirmedListMemberHook > UnconfirmedMemberOption;
    typedef boost::intrusive::list< ListElement, UnconfirmedMemberOption > UnconfirmedListT;

    ListElement(int parm)
	: memb(parm)
    {
    }

    int memb;
};

struct Tyoe {};

int main()
{
    typedef ListElement<Tyoe> ListElementT;

    ListElementT o1(10);
    ListElementT::UnconfirmedListT l2;

    l2.push_back(o1);

    ListElementT::UnconfirmedListT::const_iterator it = l2.begin();
    while ( it != l2.end() )
    {
	asm volatile ("" :: "r" (it->memb));
	++it;
    }
}

