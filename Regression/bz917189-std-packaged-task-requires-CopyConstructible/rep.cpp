#include <future>

struct S
{
  S() = default;
  S(S&&) = default;
  void operator()() { }
};

std::packaged_task<void ()> pt{ S{} };
