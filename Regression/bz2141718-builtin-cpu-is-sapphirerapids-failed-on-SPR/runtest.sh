#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz2141718-builtin-cpu-is-sapphirerapids-failed-on-SPR
#   Description: Test for BZ#2141718 (__builtin_cpu_is ("sapphirerapids") failed on SPR)
#   Author: Vaclav Kadlcik <vkadlcik@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2022 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

# Relevancy: GCC >=12 (though DTS-12.0/RHEL-6 may remain unfixed; we'll see.)
# Suggested TCMS relevancy:
#   collection !defined && distro < rhel-10: False
#   collection contains gcc-toolset-11, devtoolset-11, gcc-toolset-10, devtoolset-10: False

GCC="${GCC:-$(type -P gcc)}"
GCC_RPM_NAME=$(rpm --qf '%{name}' -qf $GCC)

PACKAGES="${GCC_RPM_NAME} yum-utils rpm-build"

rlJournalStart
    rlPhaseStartSetup
        rlRun "yum -y install --skip-broken $PACKAGES" 0-255 # best effort

        rlLogInfo "PACKAGES=$PACKAGES"
        rlLogInfo "COLLECTIONS=$COLLECTIONS"
        rlLogInfo "GCC=$GCC"
        rlLogInfo "SKIP_COLLECTION_METAPACKAGE_CHECK=$SKIP_COLLECTION_METAPACKAGE_CHECK"

        # We optionally need to skip checking for the presence of the metapackage
        # because that would pull in all the dependent toolset subrpms.  We do not
        # always want that, especially in CI.
        _COLLECTIONS="$COLLECTIONS"
        if ! test -z $SKIP_COLLECTION_METAPACKAGE_CHECK; then
            for c in $SKIP_COLLECTION_METAPACKAGE_CHECK; do
                rlLogInfo "ignoring metapackage check for collection $c"
                export COLLECTIONS=$(shopt -s extglob && echo ${COLLECTIONS//$c/})
            done
        fi
        rlLogInfo "(without skipped) COLLECTIONS=$COLLECTIONS"
        rlAssertRpm --all
        export COLLECTIONS="$_COLLECTIONS"

        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "pushd $TmpDir"

        rlFetchSrcForInstalled $GCC_RPM_NAME || yumdownloader --source $GCC_RPM_NAME
        rlRun 'ls *.src.rpm'
        rlRun 'yum-builddep -y --skip-unavailable *.src.rpm' 0-255 # best effort only

        rlRun 'mkdir rpmbuild'
        rlRun "rpm --define='_topdir $PWD/rpmbuild' -i *.src.rpm"
        rlRun "rpmbuild --define='_topdir $PWD/rpmbuild' --nodeps -bp rpmbuild/SPECS/*.spec"

        rlRun 'cp rpmbuild/BUILD/*/gcc/config/i386/driver-i386.cc .'
        rlRun 'cp rpmbuild/BUILD/*/gcc/config/i386/i386.h .'
    rlPhaseEnd

    rlPhaseStartTest driver-i386.cc
        # extract the relevant part
        rlRun 'awk "/case PROCESSOR_PENTIUMPRO/,/case PROCESSOR_PENTIUM4/" <driver-i386.cc >pentium_pro_1.txt'

        # are there the individual pieces?
        for i in FEATURE_AVX FEATURE_TSXLDTRK tigerlake sapphirerapids; do
            rlAssertGrep $i pentium_pro_1.txt
        done

        # make a one-line, single-spaced file from it for easy grepping
        rlRun 'tr -d \\012 <pentium_pro_1.txt >pentium_pro_2.txt'
        rlRun 'sed "s|[[:space:]][[:space:]]*| |g" <pentium_pro_2.txt >pentium_pro_3.txt'

        # the test itself
        rlAssertGrep 'if (has_feature (FEATURE_AVX512VP2INTERSECT)) cpu = "tigerlake"; /* Assume Sapphire Rapids. */ else if (has_feature (FEATURE_TSXLDTRK)) cpu = "sapphirerapids"; /* Assume Cooper Lake */ else if' pentium_pro_3.txt -Fq
    rlPhaseEnd

    rlPhaseStartTest i386.h
        # extract the relevant part
        rlRun 'awk "/constexpr wide_int_bitmask PTA_SAPPHIRERAPIDS/,/;/" <i386.h >pta_sapphirerapids_1.txt'

        # looks reasonable?
        for i in PTA_MOVDIR64B PTA_AVX512BF16; do
            rlAssertGrep $i pta_sapphirerapids_1.txt
        done

        # make a one-line, single-spaced file from it for easy grepping
        rlRun 'tr -d \\012 <pta_sapphirerapids_1.txt >pta_sapphirerapids_2.txt'
        rlRun 'sed "s|[[:space:]][[:space:]]*| |g" <pta_sapphirerapids_2.txt >pta_sapphirerapids_3.txt'

        # the test itself
        rlAssertNotGrep PTA_AVX512VP2INTERSECT pta_sapphirerapids_3.txt -Fq
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun popd
        rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
