#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/g/Regression/bz916175-symlinks-to-libasan-libtsan-libitm-and-libatomic
#   Description: Test for BZ#916175 (symlinks to libasan, libtsan, libitm and libatomic)
#   Author: Dagmar Prokopova <dprokopo@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2013 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

# This is for gcc >= 4.7 only.

PACKAGES=(gcc libitm libatomic)
TO_BE_TESTED=(libitm libatomic)

rlJournalStart
    rlPhaseStartSetup
        # Check needed packages.
        for p in "${PACKAGES[@]}"; do
            rlCheckRpm "$p" || yum -y install "$p"
        done; unset p

        # Check if libtsan is installed on x86_64 and add it into the list.
        if [[ $(uname -i) = "x86_64" ]] && rlIsRHEL '>=7'; then
           rlCheckRpm libtsan || yum -y install libtsan
           rlAssertRpm libtsan
           TO_BE_TESTED+=('libtsan')
        fi

        # Check if libasan is installed on all except s390x an add it into the list.
        ARCH=`uname -i`
        if rlIsRHEL '>=7' && [[ "$ARCH" != "s390x" && "$ARCH" != "aarch64" && "$ARCH" != "ppc64le" ]]; then
           yum -y install libasan
           rlCheckRpm libasan || yum -y install libasan
           rlAssertRpm libasan
           TO_BE_TESTED+=('libasan')
        fi

        # Check if libasan2 is installed (DTS-4)
        which gcc | grep -q devtoolset-4
        if [ $? -eq 0 ]; then
           rlCheckRpm libasan2 || yum -y install libasan2
           rlAssertRpm libasan2
           TO_BE_TESTED+=('libasan') # this is on purpose 'libasan', not a typo!
        fi

        # Check if libasan3 is installed (DTS-6)
        which gcc | grep -q devtoolset-6
        if [ $? -eq 0 ]; then
           rlCheckRpm libasan3 || yum -y install libasan3
           rlAssertRpm libasan3
           TO_BE_TESTED+=('libasan') # this is on purpose 'libasan', not a typo!
        fi

        # Check if libasan4 is installed (DTS-7)
        which gcc | grep -q devtoolset-7
        if [ $? -eq 0 ]; then
           rlCheckRpm libasan4 || yum -y install libasan4
           rlAssertRpm libasan4
           TO_BE_TESTED+=('libasan') # this is on purpose 'libasan', not a typo!
        fi

        # Set the right directory according to architecture.
        if [[ $(uname -i) = "i386" ]]; then
           LIB="lib"
        else
           LIB="lib64"
        fi


        rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    rlPhaseStartTest
        for COMPONENT in "${TO_BE_TESTED[@]}"; do
          for DSO in `ls /usr/$LIB | grep $COMPONENT`; do
            rlLog "Check $DSO"
            rlRun "rpm -qf /usr/$LIB/$DSO | grep -v \"is not owned by any package\"" 0
          done; unset DSO
        done; unset COMPONENT
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
