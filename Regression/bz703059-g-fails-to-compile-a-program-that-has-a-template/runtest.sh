#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz703059-g-fails-to-compile-a-program-that-has-a-template
#   Description: Try to compile a program that has a template parameter with a boolean expression involving a templated static constant
#   Author: Marek Polacek <mpolacek@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2011, 2012 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include rhts environment
. /usr/bin/rhts-environment.sh
. /usr/lib/beakerlib/beakerlib.sh

PACKAGES=(gcc gcc-c++)

# Choose the compiler.
GXX=${GXX:-g++}

rlJournalStart
	rlPhaseStartSetup
		for p in "${PACKAGES[@]}"; do
			rlAssertRpm "$p" || yum -y install "$p"
		done; unset p
		rlLog "GXX = $GXX"
		rlLog "Installed within `rpmquery -f $(which $GXX)`"
		rlRun "TmpDir=\`mktemp -d\`" 0 "Creating tmp directory"
		rlRun "cp -v f.cc $TmpDir"
		rlRun "pushd $TmpDir"
	rlPhaseEnd

	rlPhaseStartTest
		# Just try to compile the reproducer.
		rlRun "$GXX -c f.cc" 0 "Compiling the reproducer"
		rlAssertExists "f.o"
		rlRun "file f.o | grep ELF" 0 "Basic obj file sanity check"
	rlPhaseEnd

	rlPhaseStartCleanup
		rlRun "popd"
		rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
	rlPhaseEnd
rlJournalPrintText
rlJournalEnd
