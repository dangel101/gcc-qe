#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

void *thr(void *arg)
{
	int pid = fork();
	if(pid < 0)
	{
		fprintf(stderr, "fork crashed\n");
		return;
	}

	if(pid == 0)
		fprintf(stderr, "A child here!\n");
	else
		fprintf(stderr, "Oh, I am dad now! I have a baby %i now!\n", pid);
	return;
}

pthread_t th1, th2;
int main(void)
{
	int rv1, rv2;

	rv1 = pthread_create(&th1, NULL, thr, NULL);
	rv2 = pthread_create(&th2, NULL, thr, NULL);

	if(rv1)
		fprintf(stderr, "pthread_create 1 crashed (%i)\n", rv1);
	else
	{
		rv1 = pthread_join(th1, NULL);
		if(rv1) fprintf(stderr, "pthread_join 1 crashed (%i)\n", rv1);
	}
	
	if(rv2)
		fprintf(stderr, "pthread_create 2 crashed (%i)\n", rv2);
	{
		rv2 = pthread_join(th2, NULL);
		if(rv2) fprintf(stderr, "pthread_join 2 crashed (%i)\n", rv2);
	}

	return 0;
}
