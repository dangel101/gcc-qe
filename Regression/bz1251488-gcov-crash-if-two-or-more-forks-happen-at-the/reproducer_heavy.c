#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

#define THDS 15

void *thr(void *arg)
{
	int pid = fork();

	fprintf(stderr, "running...\n");
	if(pid < 0)
	{
		fprintf(stderr, "fork crashed\n");
		return;
	}

	if(pid == 0)
		fprintf(stderr, "A child here!\n");
	else
		fprintf(stderr, "Oh, I am dad now! I have a baby %i now!\n", pid);
	return;
}

pthread_t thds[THDS];

int main(void)
{
	int i, rv[THDS];

	for(i = 0; i < THDS; i++)
	{
		fprintf(stderr, "launch %i\n", i);
		rv[i] = pthread_create(&thds[i], NULL, thr, NULL);
		if(rv[i])
			fprintf(stderr, "pthread_create %i crashed (%i)\n", i, rv[i]);
	}

	for(i = 0; i < THDS; i++)
		if(!rv[i])
		{
			rv[i] = pthread_join(thds[i], NULL);
			if(rv[i])
				fprintf(stderr, "pthread_join %i crashed (%i)\n", i, rv[i]);
		}
	return 0;
}
