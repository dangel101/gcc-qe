#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz820281-ICE-in-loop-optimization
#   Description: Test for BZ#820281 (ICE in loop optimization)
#   Author: Dagmar Prokopova <dprokopo@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2012 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh
. /usr/share/beakerlib/beakerlib.sh

# ============================================================================
# Notes on relevancy
# ============================================================================
#
# The isl library, which is a prerequisite for the -f... switches below,
# is missing or disabled in RHEL9+ gcc and in RHEL6 DTS10+ gcc. See the
# respective gcc.spec files. Some discussion is in
#   https://bugzilla.redhat.com/show_bug.cgi?id=1975890
# as well.
#
# How to set relecancy in TCMS:
#   [relevancy]
#   distro = rhel-9: False
#   distro = rhel-6 && collection defined: False

PACKAGES=(gcc gcc-c++)

# Choose the compilers.
GCC=${GCC:-gcc}
GXX=${GXX:-g++}

rlJournalStart
  rlPhaseStartSetup
    # Check for needed packages.
    for pack in "${PACKAGES[@]}"; do
      rlAssertRpm "$pack" || yum -y install "$pack"
    done; unset pack
    rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
    # We need the reproducer.
    cp -v rep1.cc $TmpDir
    rlRun "pushd $TmpDir"
  rlPhaseEnd

  rlPhaseStartTest
    # If not on i386 set the optional flag -m64.
    if [[ $(uname -i) != i386 && $(arch) != aarch64 ]]; then
      M64='-m64'
    fi

    # Compile the original reproducer.  No Segmentation fault should appear.
    rlRun "$GCC -fPIC -g3 -O3 -floop-strip-mine ${M64} -c rep1.cc"
  rlPhaseEnd

  rlPhaseStartCleanup
    rlRun "popd"
    rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
  rlPhaseEnd
rlJournalPrintText
rlJournalEnd
