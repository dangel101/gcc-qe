#include <deque>
typedef long unsigned int size_type;
typedef long int ssize_type;


struct TYPE1;
typedef TYPE1 *TYPE2;

extern TYPE2 VAR1;
extern TYPE2 VAR2;

struct TYPE3;

class TYPE4
{
};

class TYPE5
{
public:
  inline TYPE5 operator++ (int);
private:
  TYPE5 (TYPE4 *, bool) {}
};
class TYPE6:public TYPE5
{
public:
  inline bool operator== (const TYPE6 &) const
  {
    return true;
  }
  inline bool operator!= (const TYPE6 &) const
  {
    return false;
  }
};
inline bool
operator== (const TYPE6 &, const TYPE5 &)
{
  return false;
}

inline bool
operator!= (const TYPE6 &Lhs, const TYPE5 &Rhs)
{
  return !(Lhs == Rhs);
}

struct TYPE7;

union TYPE8
{
  double MEMB1;
  const void *MEMB2;
};

struct TYPE3
{
  friend struct TYPE7;
  TYPE8 MEMB3;

  // XXX If this function is not inlined the compiler won't crash
  inline const void *FUNC1 () const
  {
    return MEMB3.MEMB2;
  }
  inline void FUNC2 (TYPE2, const void *)
  {
  }

  // XXX If this function is not inlined the compiler won't crash
  inline void FUNC3 (TYPE2, double PAR)
  {
    MEMB3.MEMB1 = PAR;
  }
};

struct TYPE9;
struct TYPE11 : public TYPE3
{
  friend struct TYPE7;
public:
  inline TYPE7 operator[] (TYPE11 const &);
  inline TYPE9 FUNC4 (TYPE11 const &) const;
};

struct TYPE10
{
  explicit TYPE10 (TYPE11 const &);
};
struct TYPE9
{
  explicit TYPE9 (TYPE11 const &);
  TYPE11 release ();
  TYPE9 (TYPE10 const &);
  operator  TYPE10 ();
private:
    friend struct TYPE7;
};

struct TYPE7
{
  friend struct TYPE11;
  TYPE7 operator[] (ssize_type) const;
private:
    TYPE7 (TYPE11 *, TYPE11 const &);
};

inline TYPE7
TYPE11::operator[] (TYPE11 const &subscript)
{
  return TYPE7 (this, subscript);
}

inline TYPE9
TYPE11::FUNC4 (TYPE11 const &) const
{
  TYPE11 LOC1;
  return TYPE9 (LOC1);
}

template < class CONTAINER_TYPE, class VALUE_TYPE >
__attribute__ ((__always_inline__))
inline void FUNC5 (CONTAINER_TYPE & Container, const VALUE_TYPE & Value);

inline TYPE5
TYPE5::operator++ (int)
{
  return TYPE5 (0, false);
}

class TYPE12
{
public:
  typedef TYPE11 value_type;
  template < class VALUE_TYPE >
  __attribute__ ((__always_inline__)) inline void push_back (VALUE_TYPE
							     const &Value)
  {
    FUNC5 (*this, Value);
  }
  __attribute__ ((__always_inline__)) inline void push_back (value_type const
							     &Value)
  {
    if (m_Size < 100 && !(Value.FUNC1 () == this))
      {
	m_Size++;
      }
  }
  int m_Size;
};

typedef std::deque<TYPE11> DEQUE;

class TYPE13;

template <class T>
struct TYPE14
{
  static void FUNC7 (TYPE11 &PAR1, TYPE2 PAR2, T *PAR3)
  {
    PAR1.FUNC2 (PAR2, PAR3);
  }
};

template<class T>
struct TYPE15
{
  static void FUNC7 (TYPE11 &PAR1, TYPE2 PAR2, const T &PAR3)
  {
    PAR1.FUNC3 (PAR2, static_cast<double>(PAR3));
  }
};

template<class T>
struct TYPE16
{
  typedef int MEMB4;
};

template<class T>
struct TYPE17
{
  static TYPE2 FUNC6 (const T *) { return VAR2; }
  typedef typename TYPE16<T>::MEMB4 MEMB4;
};

template <>
struct TYPE16<short>
{
  typedef TYPE15<short> MEMB4;
};

template <>
struct TYPE16<DEQUE*>
{
  typedef TYPE14<DEQUE> MEMB4;
};
template <>
inline TYPE2 TYPE17<DEQUE*>::FUNC6 (DEQUE *const *)
{
  return VAR1;
}

template< class T>
inline void FUNC8 (TYPE11 &PAR1, const T &PAR2)
{
  TYPE17<T>::MEMB4::FUNC7 (PAR1, TYPE17<T>::FUNC6 (&PAR2), PAR2);
}

template<class T1, class T2>
__attribute__ ((__always_inline__))
inline void FUNC5 (T1 &PAR1, const T2 &PAR2)
{
  typename T1::value_type LOC1;
  FUNC8 (LOC1, PAR2);
  PAR1.push_back (LOC1);
}

void problem (void)
{
  TYPE12 LOC1;
  LOC1.push_back (static_cast<short>(0));
  DEQUE LOC2;
  LOC1.push_back (&LOC2);
}
