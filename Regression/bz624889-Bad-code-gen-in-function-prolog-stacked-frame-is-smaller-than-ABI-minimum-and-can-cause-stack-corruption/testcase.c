#include <dlfcn.h>
#include <stdlib.h>
#include <stdio.h>

int
main (void)
{
  void *h = dlopen ("./libb.so", RTLD_LAZY);
  if (h == NULL)
    return 1;
  void (*fn) (void) = dlsym (h, "foo");
  if (fn == NULL)
    return 2;
  fprintf (stderr, "calling foo\n");
  fn ();
  return 0;
}

