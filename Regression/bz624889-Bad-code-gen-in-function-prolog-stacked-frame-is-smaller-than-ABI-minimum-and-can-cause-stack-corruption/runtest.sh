#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/gcc/bz624889-Bad-code-gen-in-function-prolog-stacked-frame-is-smaller-than-ABI-minimum-and-can-cause-stack-corruption
#   Description: tests Bad code gen in function prolog; stacked frame is smaller than ABI minimum and can cause stack corruption
#   Author: Michal Nowak <mnowak@redhat.com>
#	    Marek Polacek <polacek@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2010, 2012 Red Hat, Inc. All rights reserved.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 3 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include rhts environment
. /usr/bin/rhts-environment.sh
. /usr/lib/beakerlib/beakerlib.sh

PACKAGES=(gcc binutils gawk)

# Choose the compiler.
GCC=${GCC:-gcc}

rlJournalStart
    rlPhaseStartSetup
        for p in "${PACKAGES[@]}"; do
            rlAssertRpm "$p"
        done; unset p
        rlRun "TmpDir=\`mktemp -d\`" 0 "Creating tmp directory"
	cp global.c libb.c testcase.c $TmpDir
        rlRun "pushd $TmpDir"
	rlRun "$GCC -shared -O2 -fpic -o libb.so libb.c -g"
	rlRun "$GCC -O2 -g -o testcase testcase.c -ldl"
	rlRun "$GCC -O2 -g -o global global.c -fPIC -c"
	rlRun "./testcase" 0 "Run test case"
	if [ "`arch`" == "ppc64le" ]; then
		SF_SIZE=32
	else
		SF_SIZE=112
	fi
    rlPhaseEnd

    rlPhaseStartTest
	rlRun "if [ -z \"$(objdump -dr libb.so 2>/dev/null | grep 'stdu .*r1,.*r1)\| bl ' | grep -v blr | awk '/stdu/{val=gensub(/^.*r1,-([0-9]*)\(r1\).*$/,"\\1","",$0); if (strtonum(val) < $SF_SIZE) print $0 }')\" ]; then true; else false; fi" 0 "libb.so: No small stacks"
	rlRun "if [ -z \"$(objdump -dr global 2>/dev/null | grep 'stdu .*r1,.*r1)\| bl ' | grep -v blr | awk '/stdu/{val=gensub(/^.*r1,-([0-9]*)\(r1\).*$/,"\\1","",$0); if (strtonum(val) < $SF_SIZE) print $0 }')\" ]; then true; else false; fi" 0 "global: No small stacks"
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
