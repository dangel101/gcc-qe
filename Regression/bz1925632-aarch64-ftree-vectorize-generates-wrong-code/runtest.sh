#!/usr/bin/env bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz1925632-aarch64-ftree-vectorize-generates-wrong-code
#   Description: Test for BZ#1925632 (aarch64 -ftree-vectorize generates wrong code with)
#   Author: Vaclav Kadlcik <vkadlcik@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2021 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

# Notes on relevancy
#
# Should pass for system GCC of RHEL 8.5+ and any currently supported toolset.
# Suggested TCMS relevancy:
#   distro < rhel-8 && collection !defined: False
#   distro < rhel-8.5 && collection !defined: False

GCC="${GCC:-$(type -P gcc)}"
PACKAGE=$(rpm --qf '%{name}' -qf $GCC)
PACKAGES="${PACKAGE} ${PACKAGE}-c++"

rlJournalStart
    rlPhaseStartSetup
        rlLogInfo "PACKAGES=$PACKAGES"
        rlLogInfo "COLLECTIONS=$COLLECTIONS"
        rlLogInfo "GCC=$GCC"
        rlLogInfo "SKIP_COLLECTION_METAPACKAGE_CHECK=$SKIP_COLLECTION_METAPACKAGE_CHECK"

        rlRun "yum -y install --skip-broken $PACKAGES libtsan-static" 0-255 # best effort

        # We optionally need to skip checking for the presence of the metapackage
        # because that would pull in all the dependent toolset subrpms.  We do not
        # always want that, especially in CI.
        _COLLECTIONS="$COLLECTIONS"
        if ! test -z $SKIP_COLLECTION_METAPACKAGE_CHECK; then
            for c in $SKIP_COLLECTION_METAPACKAGE_CHECK; do
                rlLogInfo "ignoring metapackage check for collection $c"
                export COLLECTIONS=$(shopt -s extglob && echo ${COLLECTIONS//$c/})
            done
        fi
        rlLogInfo "(without skipped) COLLECTIONS=$COLLECTIONS"
        rlAssertRpm --all
        export COLLECTIONS="$_COLLECTIONS"

        ARCH_PRIMARY=$(rlGetPrimaryArch)
        rlLogInfo "ARCH_PRIMARY=$ARCH_PRIMARY"

        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "cp files/* $TmpDir"
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    rlPhaseStartTest UPSTREAM_98949
         rlRun 'g++ -O3 test.cc'
         rlRun './a.out'
         rlRun 'g++ -O3 -fno-tree-vectorize test.cc'
         rlRun './a.out'
    rlPhaseEnd

    if [[ "$ARCH_PRIMARY" = aarch64 ]]; then
        rlPhaseStartTest UPSTREAM_98949_march
             rlRun 'g++ -O3 -march=armv8-a test.cc'
             rlRun './a.out'
             rlRun 'g++ -O3 -fno-tree-vectorize -march=armv8-a test.cc'
             rlRun './a.out'
        rlPhaseEnd
    fi

    rlPhaseStartTest UPSTREAM_97236_1
        rlRun 'gcc -O3 97236_1.c'
         rlRun './a.out'
    rlPhaseEnd

    rlPhaseStartTest UPSTREAM_97236_2
        rlRun 'gcc -O3 97236_2.c'
         rlRun './a.out'
    rlPhaseEnd

    rlPhaseStartTest UPSTREAM_97236_test
        rlRun 'gcc -O3 upstream_test.c'
         rlRun './a.out'
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
