#include <cassert>
#include <cstdint>

int main(void) {
  uint64_t a[64], b[64];
  for (int i = 0; i < 64; ++i) {
    a[i] = 1;
    b[i] = 2;
  }
  a[63] = b[63];  // only last element is the same

  uint8_t bitmap[8];  // holds 64 bits, bit_i = 1 if (a[i] == b[i]) else 0, i = 0 ~ 63
  int index = 0;  // index to a[], b[]
  for (int byte = 0; byte < 8; ++byte) {
    uint8_t out_results[8]; // holds 8 comparison results temporarily
    for (int bit = 0; bit < 8; ++bit) {
      out_results[bit] = a[index] == b[index];
      ++index;
    }
    bitmap[byte] = (out_results[0] | out_results[1] << 1 | out_results[2] << 2 |
                    out_results[3] << 3 | out_results[4] << 4 | out_results[5] << 5 |
                    out_results[6] << 6 | out_results[7] << 7);
  }

  // last bitmap should be non-zero, fired on gcc-9.3 aarch64 -O3
  assert(bitmap[7] != 0);
  return 0;
}
