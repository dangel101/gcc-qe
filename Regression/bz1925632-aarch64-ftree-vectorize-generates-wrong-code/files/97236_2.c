typedef unsigned char __uint8_t;
typedef __uint8_t uint8_t;
typedef struct plane_t {
  uint8_t *p_pixels;
  int i_lines;
  int i_pitch;
  int i_pixel_pitch;
  int i_visible_lines;
  int i_visible_pitch;
}
plane_t;

struct picture_t {
  plane_t p[5];
  int i_planes;
};

typedef struct picture_t picture_t;

void picture_Clone(picture_t *picture, picture_t *res) {
  for (int i = 0; i < picture->i_planes; i++) {
    res->p[i].p_pixels = picture->p[i].p_pixels;
    res->p[i].i_lines = picture->p[i].i_lines;
    res->p[i].i_pitch = picture->p[i].i_pitch;
  }
}
picture_t aaa, bbb;

#define N 4
uint8_t pixels[100] = {1, 1, 1, 1, 1, 1, 1, 1};
uint8_t c;

int
main() {
  aaa.i_planes = N;
  for (unsigned i = 0; i < N; i++)
    aaa.p[i].p_pixels = pixels;

  picture_Clone (&aaa, &bbb);

  for (unsigned i = 0; i < N; i++)
    c += bbb.p[i].p_pixels[0];

  if (c != 4)
    __builtin_abort ();
  return 0;
}
