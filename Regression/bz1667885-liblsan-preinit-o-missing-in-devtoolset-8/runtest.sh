#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz1667885-liblsan-preinit-o-missing-in-devtoolset-8
#   Description: Test for BZ#1667885 (liblsan_preinit.o missing in devtoolset-8)
#   Author: Michael Petlan <mpetlan@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2019 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

# Relevant for GCC 8+, architecture-specific.
# Suggested TCMS relevancy:
#   distro < rhel-8 && collection !defined: False
#   arch = s390x && collection !defined && distro <= rhel-9: False
#   arch = s390x && collection contains gcc-toolset-9, gcc-toolset-10, gcc-toolset-11, devtoolset-9, devtoolset-10, devtoolset-11: False

GCC="${GCC:-$(type -P gcc)}"
PACKAGE=$(rpm --qf '%{name}' -qf $GCC)
PACKAGES="${PACKAGE} ${PACKAGE}-c++"

rlJournalStart
	rlPhaseStartSetup
		for i in $PACKAGES; do
			rlAssertRpm $i
		done
		rlRun "rpmquery -a | grep liblsan" 0 "We need liblsan and either liblsan-devel or liblsan-static"
	rlPhaseEnd

	rlPhaseStartTest
		rlRun "c++ 2D_vector.cpp -fsanitize=leak -fno-omit-frame-pointer -o 2D_vector" 0 "Compiling something [BUG REPRODUCTION]"
		rlAssertExists "2D_vector"
		rlRun "./2D_vector > tmp.log" 0 "Running the reproducer"
		rlAssertGrep "-1  6" tmp.log
	rlPhaseEnd

	rlPhaseStartCleanup
		rlRun "rm -f 2D_vector tmp.log" 0 "Removing temp files"
	rlPhaseEnd
rlJournalPrintText
rlJournalEnd
