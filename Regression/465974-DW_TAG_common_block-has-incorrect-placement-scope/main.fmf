summary: Fortran DW_TAG_common_block has incorrect placement/scope
description: |+
    Problem: gcc-4.3.2 / gfortran-4.3.2 (in the forthcoming tech-preview for
    RHEL5.3) appears to also have the "DW_TAG_common_block has incorrect
    placement/scope" issues listed in the following gcc upstream bug:

    http://gcc.gnu.org/bugzilla/show_bug.cgi?id=37738

    What gfortran is doing currently is:

    (1) Merges all common blocks into a single one.
    (2) Places this single common block into the first subroutine where it was
    used.

enabled: true
link:
  - relates: https://bugzilla.redhat.com/show_bug.cgi?id=465974
tag:
  - 009203f6a293633355c6992185b6571e
  - CI-Tier-1
  - DTS8-INTEROP-CI
  - DTS9-INTEROP-CI
  - Tier1
  - dts_smoke
  - dts_stable
  - mfranc_stable
  - tier1_mfranc
tier: '1'
contact: mcermak@redhat.com
component:
  - gcc
test: ./runtest.sh
framework: beakerlib
require:
  - gcc
  - gcc-gfortran
  - binutils
duration: 5m
extra-nitrate: TC#0062268
extra-summary: /tools/gcc/g77/465974-DW_TAG_common_block-has-incorrect-placement-scope
extra-task: /tools/gcc/g77/465974-DW_TAG_common_block-has-incorrect-placement-scope
