#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz578382-PrintServiceLookup-lookupPrintServices-fails
#   Description: PrintServiceLookup.lookupPrintServices() fails to list printers
#   Author: Michal Nowak <mnowak@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2010 Red Hat, Inc. All rights reserved.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 3 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include rhts environment
. /usr/bin/rhts-environment.sh
. /usr/lib/beakerlib/beakerlib.sh

PACKAGE="gcc"

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm $PACKAGE
        rlRun "TmpDir=\`mktemp -d\`" 0 "Creating tmp directory"
	cp printers.conf listprintersOne.java listprintersTwo.java $TmpDir
        rlRun "pushd $TmpDir"
	rlFileBackup /etc/cups/printers.conf
	rlServiceStop cups
	cp -v printers.conf /etc/cups/printers.conf
	rlServiceStart cups
    rlPhaseEnd

    rlPhaseStartTest listprintersOne
	rlRun "gcj -o listprintersOne --main=listprintersOne ./listprintersOne.java" 0 "Compile listprintersOne"
	./listprintersOne > listprintersOne.out 2>&1
	cat listprintersOne.out
	rlAssertNotGrep "found 0 services" listprintersOne.out
	rlAssertGrep "bubblejet" listprintersOne.out
    rlPhaseEnd

    rlPhaseStartTest listprintersTwo
	rlRun "gcj -o listprintersTwo --main=listprintersTwo ./listprintersTwo.java" 0 "Compile listprintersTwo"
	./listprintersTwo > listprintersTwo.out 2>&1
	cat listprintersTwo.out
	rlAssertNotGrep "found 0 services" listprintersTwo.out
	rlAssertGrep "bubblejet" listprintersTwo.out
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
	rlServiceStop cups
	rlFileRestore
	rlServiceStart cups
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
