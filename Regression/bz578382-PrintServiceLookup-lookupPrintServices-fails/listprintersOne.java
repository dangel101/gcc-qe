import javax.print.attribute.standard.PrinterIsAcceptingJobs;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;

class listprintersOne {
	public static void main(String[] argv) {
		PrintService ss[] = PrintServiceLookup.lookupPrintServices(null,null);
		System.out.println("found " + ss.length + " services");
		for(int i = 0; i < ss.length; i++) {
			javax.print.attribute.Attribute att[] = ss[i].getAttributes().toArray();
			System.out.println("found " + att.length + " attributes for " + ss[i].getName());
			for(int j = 0; j < att.length; j++)
				System.out.println(ss[i].getName() + "/" + att[j].getName() + (att[j].equals(PrinterIsAcceptingJobs.ACCEPTING_JOBS) ? "" : "not") + " accepting jobs");
		}
	}
}

