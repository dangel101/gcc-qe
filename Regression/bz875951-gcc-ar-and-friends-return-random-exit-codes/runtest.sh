#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/g/Regression/bz875951-gcc-ar-and-friends-return-random-exit-codes
#   Description: Test for BZ#875951 (gcc-ar and friends return random exit codes)
#   Author: Dagmar Prokopova <dprokopo@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2013 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

# XXX This test case is for gcc >= 4.8 only.



rlJournalStart
    rlPhaseStartSetup
        # Set the directory name according to the architecture.
        if [ "$(uname -i)" == "i386" ]; then
          DIR='/usr/lib'
        else
          DIR='/usr/lib64'
        fi

        # Set the name of the library. There is no libc.a on rhel6.
        if rlIsRHEL 5; then
          TARGET='libc.a'
        else
          TARGET='libg.a'
        fi

        GCC_VERSION="`gcc --version | head -n 1 | awk '{print $3}'`"
        MIN_GCC_VERSION="4.8"
        OLDER="`echo -e \"$GCC_VERSION\n$MIN_GCC_VERSION\" | sort | head -n 1`"
    rlPhaseEnd

    rlPhaseStartTest
        if [[ "$OLDER" == "$MIN_GCC_VERSION" ]]; then
            # Use gcc-ar few times to know if it returns zero exit code.
            rlRun "gcc-ar t $DIR/$TARGET > /dev/null" 0
            rlRun "gcc-ar t $DIR/$TARGET > /dev/null" 0
            sleep 1
            rlRun "gcc-ar t $DIR/$TARGET > /dev/null" 0
            sleep 1
            rlRun "gcc-ar t $DIR/$TARGET > /dev/null" 0
        else
            rlPass "TEST SKIPPED: Your gcc is too old: $GCC_VERSION (should be at least $MIN_GCC_VERSION)"
        fi
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
