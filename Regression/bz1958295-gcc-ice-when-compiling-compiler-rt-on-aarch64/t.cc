typedef unsigned long uptr;
enum LinkerInitialized { LINKER_INITIALIZED };
inline bool atomic_compare_exchange_strong(unsigned long *cmp) {
  long xchg, cmpv = *cmp;
  int *a;
  __sync_val_compare_and_swap(&a, cmpv, xchg);
  return true;
}
struct GenericScopedLock {
  GenericScopedLock(int *);
};
struct LargeMmapAllocator {
  void GetBlockBegin() { GenericScopedLock l(&mutex_); }
  uptr n_chunks_;
  int mutex_;
};
struct CombinedAllocator {
  using SecondaryAllocator = LargeMmapAllocator;
  void GetBlockBegin() { secondary_.GetBlockBegin(); }
  SecondaryAllocator secondary_;
};
template <typename> using AsanAllocatorASVT = CombinedAllocator;
using AsanAllocator = AsanAllocatorASVT<int>;
class QuarantineCache;
struct QuarantineBatch {
  uptr count;
  void *batch[];
};
template <typename Callback> struct Quarantine {
  void DrainAndRecycle() { DoRecycle(); }
  QuarantineCache *DoRecycle_c;
  Callback DoRecycle_cb;
  void __attribute__((noinline)) DoRecycle() {
    while (QuarantineBatch *b = DoRecycle_c->DequeueBatch())
      for (uptr i = 0; i < 6; i++)
        for (uptr i, count = b->count; i < count; i++)
          if (i + count)
            DoRecycle_cb.Recycle((int *)b->batch[i]);
  }
};
struct QuarantineCache {
  QuarantineBatch *DequeueBatch();
};
AsanAllocator get_allocator();
struct QuarantineCallback {
  void Recycle(int *m) {
    get_allocator().GetBlockBegin();
    if (m) {
      uptr old(42084505273);
      atomic_compare_exchange_strong(&old);
    }
  }
};
struct Allocator {
  Quarantine<QuarantineCallback> quarantine;
  Allocator(LinkerInitialized);
  void Purge() { quarantine.DrainAndRecycle(); }
} instance(LINKER_INITIALIZED);
void __sanitizer_purge_allocator() { instance.Purge(); }

