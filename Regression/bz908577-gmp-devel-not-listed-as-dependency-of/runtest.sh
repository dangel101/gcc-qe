#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/g/Regression/bz908577-gmp-devel-not-listed-as-dependency-of
#   Description: Test for BZ#908577 (gmp-devel not listed as dependency of)
#   Author: Dagmar Prokopova <dprokopo@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2013 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

# Notes on relevancy:
#
# On RHEL6, this was never relevant for system gcc and stopped being relevant
# for DTS-10 gcc and newer (see commit 505b97e in devtoolset-10.0-rhel-6 gcc).
# On RHHEL7+, it's relevant for any gcc.

PACKAGES=(gcc gcc-c++ tar)

# Set the variabile UNDER_DTS on non-empty string, when run under devtoolset
if $( echo `which gcc` | grep -qE '/opt/rh/' ); then
  UNDER_DTS="true"
  # Set the actual version of DTS
  DTS=`which gcc | awk 'BEGIN { FS="/" } { print $4 }'`
fi

rlJournalStart
    rlPhaseStartSetup
        for p in "${PACKAGES[@]}"; do
            rpm -q "$p" || yum -y install "$p"
            rlAssertRpm "$p"
        done; unset p

        # Set the name of the package.
        if [ ! -z ${UNDER_DTS} ]; then
          gpd="$DTS-gcc-plugin-devel"
        else
          gpd="gcc-plugin-devel"
        fi

        # Check if gcc-plugin-devel is installed.
        rpm -q $gpd || yum install -y $gpd
        rlAssertRpm $gpd
        # Check if gmp-devel is installed.
        rlAssertRpm gmp-devel
    rlPhaseEnd

    rlPhaseStartTest
        # Check that gmp-devel is listed as dependency of gcc-plugin-devel.
        rlRun "rpm -qR $gpd | grep -q gmp-devel" 0 "gmp-devel is listed as dependency of gcc-plugin-devel"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
