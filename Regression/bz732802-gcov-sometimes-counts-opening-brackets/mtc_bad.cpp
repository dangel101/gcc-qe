#include <iostream>

struct mtcClass
{
  mtcClass() { mTest(); mThrow(); }

  void mTest() throw( int ) 
  {
  } // Problem here

  void mThrow()
  throw( int ) 
  { // Problem here
    throw( 1 );
  }
};

int main()
{
  try
  {
    mtcClass mtc;
  }
  catch(...)
  {
    std::cout<<"Done."<<std::endl;
  }
  
  return 0;
}
