#include <iostream>

struct mtcClass
{
  mtcClass() { mTest(); mThrow(); }

  void mTest() throw( int ) { // Like this is ok
  }

  //void mThrow() throw(int) // With the exception specification exhibits the problem
  void mThrow()              // Without seems fine
  {
    throw( 1 );
  }
};

int main()
{
  try
  {
    mtcClass mtc;
  }
  catch(...)
  {
    std::cout<<"Done."<<std::endl;
  }
  
  return 0;
}
