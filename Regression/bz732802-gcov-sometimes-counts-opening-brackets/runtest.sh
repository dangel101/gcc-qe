#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz732802-gcov-sometimes-counts-opening-brackets
#   Description: Test for bz732802 (gcov sometimes counts opening brackets)
#   Author: Marek Polacek <mpolacek@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2011 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh
. /usr/lib/beakerlib/beakerlib.sh

PACKAGES=(gcc gcc-c++)

# Choose the compiler.
GXX=${GCC:-g++}

rlJournalStart
    rlPhaseStartSetup
        for p in "${PACKAGES[@]}"; do
            rlAssertRpm "$p" || yum -y install "$p"
        done; unset p
        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "cp -v Makefile_rep mtc_bad.cpp mtc_bad.cpp.gcov.golden  $TmpDir"
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    rlPhaseStartTest
        # Create the .gcov file.
        rlRun "CXX=$GXX make -f Makefile_rep clean && make -f Makefile_rep bad"
        rlAssertExists mtc_bad.cpp.gcov

        # Delete first three lines of the output file.
        rlRun "sed -i '1,3d' mtc_bad.cpp.gcov"

        # Now check relevant parts of the output file against a golden image.
        # Those parts are marked with special comments made in the source.
        rlRun 'grep -C1 "// Problem here" mtc_bad.cpp.gcov >actual'
        rlRun 'grep -C1 "// Problem here" mtc_bad.cpp.gcov.golden >expected'
        rlAssertNotDiffer actual expected || diff expected actual
        if [ $? -ne 0 ]; then
            # for debugging purpose...
            echo "================== DIFF =================="
            diff -u mtc_bad.cpp.gcov.golden mtc_bad.cpp.gcov
            echo "=========================================="
            sdiff -b -s mtc_bad.cpp.gcov.golden mtc_bad.cpp.gcov | while read line; do
                [[ "$line" =~ '{' ]] && rlLogError "$line"
            done
        fi
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
