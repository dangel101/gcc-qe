#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz832967-C-11-ABI-breakage
#   Description: Test for bz832967 (C++11 ABI breakage)
#   Author: Marek Polacek <polacek@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2012 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh
. /usr/lib/beakerlib/beakerlib.sh

PACKAGES=(gcc gcc-c++ tar)

# Choose the compiler.
GXX=${GXX:-g++}

# Relevant for GCC 4.7+ (or 4.8+?). In practice, it's any supported toolset
# GCC and system GCCs of RHEL 7+.
# Suggested TCMS relevancy:
#   distro < rhel-7 && collection !defined: False

# See upstream bugs/discussions:
# * http://gcc.gnu.org/bugzilla/show_bug.cgi?id=53646
# * http://gcc.gnu.org/ml/gcc/2012-05/msg00409.html
# * http://gcc.gnu.org/bugzilla/show_bug.cgi?id=53657

# Recently we found some abi change when -std=c++11 is enabled.  It seems
# gcc 4.7 caller expects 16-byte value to be returned on the stack in c++11
# mode.  But if not in c++11 mode, or in older compiler, they are returned
# in registers. 

# "Returning a std::pair by value from function causes problems if the caller
# and callee don't use the same -std setting."

# We have a few reproducers here, collected from upstream BZ/ML.

rlJournalStart
  rlPhaseStartSetup
    # Check for needed packages.
    for p in "${PACKAGES[@]}"; do
      rlAssertRpm "$p" || yum -y install "$p"
    done; unset p
    rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
    # Copy what we need.
    cp -v mmatz.tar.bz2 jon.C test1.cpp test2.cpp test.sh out.ok $TmpDir
    rlRun "pushd $TmpDir"
  rlPhaseEnd

  # Try Micha's test.
  # http://gcc.gnu.org/bugzilla/show_bug.cgi?id=53646
  rlPhaseStartTest "Michael Matz's test"
    # Unpack sources.
    rlRun "tar jxvf mmatz.tar.bz2"
    rlRun "pushd cxxabi-incompat"
    # Compile it all.
    rlRun "make CXX=$GXX"
    # Now this shouldn't segfault.
    rlRun "./app"
    rlRun "./app2"
    rlRun "popd"
  rlPhaseEnd

  # Now Jon's.
  # http://gcc.gnu.org/bugzilla/show_bug.cgi?id=53657.
  rlPhaseStartTest "Jon's test"
    # Just try to compile it.  Clang accepts that.
    rlRun "$GXX -O2 -std=c++11 jon.C"
  rlPhaseEnd

  # Now from the ML.
  # http://gcc.gnu.org/ml/gcc/2012-05/msg00409.html
  rlPhaseStartTest "ML test"
    rlRun ":> out"
    export GXX
    rlRun "bash test.sh &> out"
    echo ===========
    cat out
    echo ===========
    rlAssertNotDiffer out out.ok
  rlPhaseEnd

  rlPhaseStartCleanup
    rlRun "popd"
    rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
  rlPhaseEnd
rlJournalPrintText
rlJournalEnd
