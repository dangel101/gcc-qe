#!/bin/sh
CC="$GXX -O"
$CC -std=c++03 -o test_cxx03.o -c test1.cpp
$CC -std=c++0x -o test_cxx11.o -c test1.cpp
$CC -std=c++03 -o test2_cxx03.o -c test2.cpp
$CC -std=c++0x -o test2_cxx11.o -c test2.cpp

$CC -o test test_cxx03.o test2_cxx03.o; ./test
$CC -o test test_cxx11.o test2_cxx11.o; ./test
$CC -o test test_cxx03.o test2_cxx11.o; ./test
$CC -o test test_cxx11.o test2_cxx03.o; ./test
