#include <utility>
#include <stdio.h>

std::pair<int, int> execute();

int main() {
    std::pair<int, int> result = execute();
    printf("%d %d\n", result.first, result.second);
}
