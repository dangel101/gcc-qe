#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz1282755-htm-builtins-not-compiler-barriers-on-powerpc
#   Description: The test checks, whether the 'add' is always in a transaction
#   Author: Michael Petlan <mpetlan@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2016 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

GCC="${GCC:-$(type -P gcc)}"
GCC_RPM_NAME=$(rpm --qf '%{name}' -qf $GCC)
PACKAGES="$GCC_RPM_NAME perl"

rlJournalStart
    rlPhaseStartSetup
        rlLogInfo "PACKAGES=$PACKAGES"
        rlLogInfo "COLLECTIONS=$COLLECTIONS"
        rlLogInfo "GCC=$GCC"
        rlLogInfo "SKIP_COLLECTION_METAPACKAGE_CHECK=$SKIP_COLLECTION_METAPACKAGE_CHECK"

        # We optionally need to skip checking for the presence of the metapackage
        # because that would pull in all the dependent toolset subrpms.  We do not
        # always want that, especially in CI.
        _COLLECTIONS="$COLLECTIONS"
        if ! test -z $SKIP_COLLECTION_METAPACKAGE_CHECK; then
            for c in $SKIP_COLLECTION_METAPACKAGE_CHECK; do
                rlLogInfo "ignoring metapackage check for collection $c"
                export COLLECTIONS=$(shopt -s extglob && echo ${COLLECTIONS//$c/})
            done
        fi
        rlLogInfo "(without skipped) COLLECTIONS=$COLLECTIONS"
        rlAssertRpm --all
        export COLLECTIONS="$_COLLECTIONS"

        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "cp barrier?.c $TmpDir"
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    for opt in -O0 -O1 -O2 -O3 -Os; do
        for tc in barrier1 barrier2; do

            rlPhaseStartTest "$tc with $opt"
                rlRun "gcc -S $opt -mcpu=power8 $tc.c"
                rlAssertExists "$tc.s"
                perl -ne 'print $1 if /(tbegin\.|add|tabort\.|tend\.)/' < $tc.s | \
                perl -ne 'BEGIN{$rv=1;} $rv=0 if /tbegin\..*add.*tend\./;END{exit $rv;}'
                if [ $? -eq 0 ]; then
                    rlPass "Yes, 'add' lies in between 'tbegin.' and 'tend.'"
                else
                    if [ "$opt" = "-O1" ]; then
                        perl -ne 'print $1 if /(\.L[34]:|tbegin\.|add|tabort\.|tend\.|b\s\.L[34])/' < $tc.s | \
                        perl -ne 'BEGIN{$rv=1;} $rv=0 if /\.L3:.*tend\..*\.L4:.*tbegin\..*add.*b\s\.L3/;END{exit $rv;}'
                        if [ $? -eq 0 ]; then
                            rlPass "Yes, 'add' lies in between 'tbegin.' and 'tend.' (with branching)"
                        else
                            rlFail "HEY, 'add' lies outside of 'tbegin.' and 'tend.' (not even branching detection helped)"
                            rlFileSubmit $tc.s "${tc}${opt}.s"
                        fi
                    else
                        rlFail "HEY, 'add' lies outside of 'tbegin.' and 'tend.'"
                        rlFileSubmit $tc.s "${tc}${opt}.s"
                    fi
                fi
            rlPhaseEnd

        done
    done

    rlPhaseStartTest "IBM testcase"
        rlRun 'touch empty.c'
        rlRun 'gcc -mhtm -E -dD empty.c | grep __TM_FENCE__'
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun popd
        rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
