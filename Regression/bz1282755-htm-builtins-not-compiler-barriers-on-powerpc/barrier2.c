long
foo (long dest, long *src0, long *src1, long tries)
{
  long i;
  for (i = 0; i < tries; i++)
    {
      __builtin_tbegin (0);
      dest = *src0 + *src1;
      if (dest == 13)
	__builtin_tabort(0);
      __builtin_tend (0);
    }
  return dest;
}
