struct T
{
        unsigned int a : 8;
        unsigned int b : 8;
};

void foo (struct T *);
void baz (void);

        void
bar (void)
{
        struct T s, t = { 0, 0 };
        int v;

        for (v = 0; v < 10; v++)
        {
                foo (&s);
                if (s.a != t.a || s.b != t.b)
                        baz ();
                t = s;
        }
}

