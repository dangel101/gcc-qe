#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz675132-M-option-not-available-in-gcc-gfortran-4-4
#   Description: Test for bz675132 (-M option not available in gcc-gfortran-4.4)
#   Author: Miroslav Franc <mfranc@redhat.com>
#	    Marek Polacek <polacek@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2011, 2012 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include rhts environment
. /usr/bin/rhts-environment.sh
. /usr/lib/beakerlib/beakerlib.sh

# The test needs an appropriate libgfortran* library but in some cases it's not
# available. For example, newer devtoolset GCCs need (base) libgfortran5 that
# is not provided on RHEL-7 below 7.5 and RHEL-6 below 6.10.
#
# Suggested TCMS relevancy:
#   collection defined && distro = rhel-7 && distro < rhel-7.5: False
#   collection defined && distro = rhel-6 && distro < rhel-6.10: False

GCC="${GCC:-$(type -P gcc)}"
PACKAGE=$(rpm --qf '%{name}' -qf $GCC)
PACKAGES="${PACKAGE} ${PACKAGE}-gfortran"

rlJournalStart
    rlPhaseStartSetup
        rlLogInfo "PACKAGES=$PACKAGES"
        rlLogInfo "COLLECTIONS=$COLLECTIONS"
        rlLogInfo "GCC=$GCC"
        rlLogInfo "SKIP_COLLECTION_METAPACKAGE_CHECK=$SKIP_COLLECTION_METAPACKAGE_CHECK"

        # We optionally need to skip checking for the presence of the metapackage
        # because that would pull in all the dependent toolset subrpms.  We do not
        # always want that, especially in CI.
        _COLLECTIONS="$COLLECTIONS"
        if ! test -z $SKIP_COLLECTION_METAPACKAGE_CHECK; then
            for c in $SKIP_COLLECTION_METAPACKAGE_CHECK; do
                rlLogInfo "ignoring metapackage check for collection $c"
                export COLLECTIONS=$(shopt -s extglob && echo ${COLLECTIONS//$c/})
            done
        fi
        rlLogInfo "(without skipped) COLLECTIONS=$COLLECTIONS"
        for i in $PACKAGES; do
            rpm -q $i || rlRun "yum -y install $i"
        done
        rlAssertRpm --all
        export COLLECTIONS="$_COLLECTIONS"

        rlRun "TmpDir=\`mktemp -d\`" 0 "Creating tmp directory"
        rlRun "cp mtest.F mtest.h $TmpDir"
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    rlPhaseStartTest "-M option should be available in gcc-gfortran"
        rlRun "$GCC -M mtest.F 2> elog > log"
        rlRun "test $(wc -c <elog) -eq 0" 0 "elog is empty - no warnings"
        rlLog "$(<elog)"
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
