#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz821901-internal-compiler-error-in-convert-to-eh-region-ranges
#   Description: Test for BZ#821901 (internal compiler error in)
#   Author: Dagmar Prokopova <dprokopo@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2012 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh
. /usr/share/beakerlib/beakerlib.sh

# Specific to x86_64. Not relevant for GCC 11+ (some of the options used
# in this test case are not present in newer GCC versions.
# Suggested TCMS relevancy:
#   arch != x86_64: False
#   distro > rhel-8: False
#   collection !contains devtoolset-9, devtoolset-10, gcc-toolset-9, gcc-toolset-10: False

PACKAGES=(gcc gcc-c++)

# Choose the compiler.
GCC=${GCC:-gcc}
GXX=${GXX:-g++}

rlJournalStart
  rlPhaseStartSetup
    # Check for needed packages.
    for pack in "${PACKAGES[@]}"; do
      rlAssertRpm "$pack" || yum -y install "$pack"
    done; unset pack
    rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
    # We need the reproducers.
    cp -v "EnumBase.ii" $TmpDir
    cp -v "EnumBase.gcda" $TmpDir

    cc1plus=`rpmquery -l $(rpmquery -f $(which $GXX) | grep $(arch)) | grep cc1plus`
    rlRun "pushd $TmpDir"
  rlPhaseEnd

  rlPhaseStartTest
    # Compile the original reproducer. It should compile successfully.
    rlRun "$cc1plus -fpreprocessed EnumBase.ii -quiet -dumpbase EnumBase.C -mtune=generic -auxbase-strip ./EnumBase.o -g -O3 -Wreturn-type -version -freorder-blocks-and-partition -ffunction-sections -fdata-sections -fprofile-use -fprofile-dir=`pwd` -ftemplate-depth-256 --param max-inline-insns-auto=1000 --param max-grow-copy-bb-insns=80 --param large-function-growth=500 --param inline-unit-growth=500 --param large-stack-frame=1024 -o EnumBase.s"
  rlPhaseEnd

  rlPhaseStartCleanup
    rlRun "popd"
    rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
  rlPhaseEnd
rlJournalPrintText
rlJournalEnd
