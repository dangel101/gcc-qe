#!/usr/bin/env bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz2028798-gcc-reload-failures-on-x86-64-after-Systemtap-4-6
#   Description: Test for BZ#2028798 (gcc reload failures on x86-64 after Systemtap 4.6)
#   Author: Vaclav Kadlcik <vkadlcik@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2021 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

# Expected to work on
#   * base GCC
#     * up to and including RHEL 7
#     * RHEL 8.6+
#   * any devtoolset
#   * gcc-toolset-10 and newer
# Suggested TCMS relevancy:
#   collection !defined && distro = rhel-8 && distro < rhel-8.6: False
#   collection contains gcc-toolset-9: False

GCC="${GCC:-$(type -P gcc)}"

GCC_RPM_NAME=$(rpm --qf '%{name}' -qf $GCC)
GCCCPP_RPM_NAME=${GCC_RPM_NAME}-c++

PACKAGES="$GCC_RPM_NAME $GCCCPP_RPM_NAME"

rlJournalStart
    rlPhaseStartSetup
        rlRun "yum -y install --skip-broken $PACKAGES" 0-255 # best effort

        rlLogInfo "PACKAGES=$PACKAGES"
        rlLogInfo "COLLECTIONS=$COLLECTIONS"
        rlLogInfo "GCC=$GCC"
        rlLogInfo "SKIP_COLLECTION_METAPACKAGE_CHECK=$SKIP_COLLECTION_METAPACKAGE_CHECK"

        # We optionally need to skip checking for the presence of the metapackage
        # because that would pull in all the dependent toolset subrpms.  We do not
        # always want that, especially in CI.
        _COLLECTIONS="$COLLECTIONS"
        if ! test -z $SKIP_COLLECTION_METAPACKAGE_CHECK; then
            for c in $SKIP_COLLECTION_METAPACKAGE_CHECK; do
                rlLogInfo "ignoring metapackage check for collection $c"
                export COLLECTIONS=$(shopt -s extglob && echo ${COLLECTIONS//$c/})
            done
        fi
        rlLogInfo "(without skipped) COLLECTIONS=$COLLECTIONS"
        rlAssertRpm --all
        export COLLECTIONS="$_COLLECTIONS"

        RUN_FULL_TEST_CASE=true
        if rlIsRHEL '<8'; then
            rlRun 'RUN_FULL_TEST_CASE=false'
        fi

        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "cp files/* $TmpDir"
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    rlPhaseStartTest reduced_testcase.c
        rlRun -tl 'gcc -c -O2 reduced_testcase.c'
    rlPhaseEnd

    if $RUN_FULL_TEST_CASE; then
        rlPhaseStartTest bad-dl-load.i
            rlRun -tl 'gcc -c -O2 -fstack-clash-protection bad-dl-load.i'
        rlPhaseEnd
    fi

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
