struct B { int a; };
int c, d;
int bar (void);
void baz (void);

void
foo (struct B *f)
{
  int *g = (int *) (__INTPTR_TYPE__) bar ();
lab:
  __asm__("" ::"norfxy"(g));
  baz ();
  int k[d];
  for (; f < (struct B *) &f; ++f)
    switch (f->a)
    case 1:
      if (__builtin_expect (c, 0))
        goto lab;
}
