#include <bitset>
#include <iostream>

bool test_bit (size_t pos);

int main ()
{
	test_bit(0);

	/* With DTS 7 this pulls in libstdc++_nonshared.a(system_error48.o), which
	 * gives the sso string functions hidden visibility, overriding the default
	 * visibility declared in sso_string.o.
	 */
	return bool(std::cout);
}
