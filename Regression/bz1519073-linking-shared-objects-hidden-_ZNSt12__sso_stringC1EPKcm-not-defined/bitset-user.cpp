#include <bitset>

static std::bitset<1> bs;

bool test_bit (size_t pos)
{
	/* With DTS 7 this pulls in libstdc++_nonshared.a(sso_string.o), which
	 * gives the sso string stuff default visibility.
	 */
	return bs.test(pos);
}
