#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz1519073-linking-shared-objects-hidden-_ZNSt12__sso_stringC1EPKcm-not-defined
#   Description: This test runs the reproducer from the BZ.
#   Author: Michael Petlan <mpetlan@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2018 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

GCC=${GCC:-$(type -P gcc)}
GCC_RPM_NAME=$(rpm --qf '%{name}' -qf $GCC)
PACKAGES="$GCC_RPM_NAME ${GCC_RPM_NAME}-c++ ${GCC_RPM_NAME%gcc}binutils"

rlJournalStart
    rlPhaseStartSetup
        rlLogInfo "PACKAGES=$PACKAGES"
        rlLogInfo "COLLECTIONS=$COLLECTIONS"
        rlLogInfo "GCC=$GCC"
        rlLogInfo "SKIP_COLLECTION_METAPACKAGE_CHECK=$SKIP_COLLECTION_METAPACKAGE_CHECK"

        # We optionally need to skip checking for the presence of the metapackage
        # because that would pull in all the dependent toolset subrpms.  We do not
        # always want that, especially in CI.
        _COLLECTIONS="$COLLECTIONS"
        if ! test -z $SKIP_COLLECTION_METAPACKAGE_CHECK; then
            for c in $SKIP_COLLECTION_METAPACKAGE_CHECK; do
                rlLogInfo "ignoring metapackage check for collection $c"
                export COLLECTIONS=$(shopt -s extglob && echo ${COLLECTIONS//$c/})
            done
        fi
        rlLogInfo "(without skipped) COLLECTIONS=$COLLECTIONS"
        rlAssertRpm --all
        export COLLECTIONS="$_COLLECTIONS"

        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "cp *.cpp $TmpDir"
        rlRun "pushd $TmpDir"

        # detect c++14 support
        echo 'int main(void){return 0;}' | g++ -x c++ -std=c++14 - -o a.out
        if [[ $? -eq 0 ]]; then
            stds='c++11 c++14'
        else
            stds='c++11'
        fi
        rlLogInfo "Standards to be tested: $stds"

        # detect linkers to test (bfd and gold if available)
        linkers="`rpmquery -l $(rpmquery -f $(which ld)) | grep /usr/bin/ld. | cut -d. -f2`"
        rlLogInfo "Linkers to be tested: $linkers"
    rlPhaseEnd

    for std in $stds; do
        for linker in $linkers; do

            rlPhaseStartTest "Testing -std=$std with $linker"
                rlRun "g++ -fuse-ld=$linker -std=$std -shared -fPIC -o bitset-user-$linker-$std.so bitset-user.cpp"
                rlRun "g++ -fuse-ld=$linker -std=$std main.cpp bitset-user-$linker-$std.so -o binary-$linker-$std"
                rlAssertExists "binary-$linker-$std"
                rlRun "LD_LIBRARY_PATH=. ./binary-$linker-$std" 1
            rlPhaseEnd

        done
    done

    rlPhaseStartCleanup
        rlRun popd
        rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
