. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="gcc"

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm $PACKAGE
        rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
        rlRun "cp greenlet.tar.gz $TmpDir"
        rlRun "pushd $TmpDir"
        tar xf greenlet.tar.gz
        cd greenlet
    rlPhaseEnd

    rlPhaseStartTest
        rlRun "./setup.py build &> log"
        rlAssertNotGrep "PIC register clobbered" log
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
