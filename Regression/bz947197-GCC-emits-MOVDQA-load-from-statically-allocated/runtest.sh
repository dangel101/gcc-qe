#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz947197-GCC-emits-MOVDQA-load-from-statically-allocated
#   Description: Test for BZ#947197 (GCC emits MOVDQA load from statically allocated)
#   Author: Dagmar Prokopova <dprokopo@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2013 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

# This was fixed in gcc-4.8.1-2.

PACKAGES=(gcc gcc-c++)

# Choose the compilers.
GCC=${GCC:-gcc}
GXX=${GXX:-g++}
GFORTRAN=${GFORTRAN:-gfortran}

rlJournalStart
    rlPhaseStartSetup
        for p in "${PACKAGES[@]}"; do
            rlAssertRpm "$p" || yum -y install "$p"
        done; unset p

				rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
        # We need the reproducer.
        cp -v preprocessed.cpp $TmpDir
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    rlPhaseStartTest
        rlRun "$GXX preprocessed.cpp -O3 -S -std=c++11" 0 "Compiling the reproducer."
        rlAssertNotGrep "xmm.*_ZZN5boost16exception_detail27get_static_exception_objectINS0_14bad_exception_EEENS_13except" preprocessed.s 
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
