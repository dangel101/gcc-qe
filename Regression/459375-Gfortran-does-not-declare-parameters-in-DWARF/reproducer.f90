! Test for PR 3319
       PROGRAM nonva
       implicit none

       character (len=*), parameter :: file_version =                    &
      &  'Id: main.F90,v 1.1.1.1 2000/02/18 22:53:45 lally Exp '
       integer:: dbg
       character*4:: IV,niv
       character*1,parameter:: esc=char(27)
       character (len=60) :: file
       IV  = esc//'&dB'         ! inverse video on hp-terminals
       niv = esc//'&d@'         ! end inverse video
       file= file_version
       ! Break on this function
       call breakhere1


       call moreparams

       END PROGRAM nonva

       subroutine breakhere1
       end subroutine breakhere1

       subroutine breakhere2
       end subroutine breakhere2
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Define lots of different kinds of parameters.
! Note that we have carefully picked the values here because we
! check the bit patterns in the regression tests.
!
       subroutine moreparams

       type posn
         real :: x,y
         integer :: z
       end type posn

       integer (kind=1), parameter :: i1 = 127_1
       integer (kind=2), parameter :: i2 = 32767_2
       integer (kind=4), parameter :: i3 = 2147483647_4
       integer (kind=4), parameter :: i4 = -2147483647_4-1_4
       integer (kind=8), parameter :: i5 = 4294967296_8
       integer (kind=8), parameter :: i6 = 9223372036854775807_8

       !
       ! These are not used, but we want to be able to compare
       ! variables with constants in the stabs and dwarf.
       !
       integer (kind=1) :: vi1 = 127_1
       integer (kind=2) :: vi2 = 32767_2
       integer (kind=4) :: vi3 = 2147483647_4
       integer (kind=4) :: vi4 = -2147483647_4-1_4
       integer (kind=8) :: vi5 = 4294967296_8
       integer (kind=8) :: vi6 = 9223372036854775807_8

       logical (kind=1), parameter :: l1 = .true.
       logical (kind=2), parameter :: l2 = .false.
       logical (kind=4), parameter :: l3 = .true.
       logical (kind=8), parameter :: l4 = .false.

       real (kind=4),  parameter :: r1 = 1.0
       real (kind=4),  parameter :: r2 = -1.0
       real (kind=8),  parameter :: r3 = 2.0
       real (kind=8),  parameter :: r4 = -2.0
! real (kind=16) not supported everywhere
!      real (kind=16), parameter :: r5 = 4.0
!      real (kind=16), parameter :: r6 = -4.0

       character (len=*), parameter :: s1 = 'Vanilla string constant'
       character (len=*), parameter :: s2 =                              &
      &  'Embedded <'//char(34)//'> double quote'
       character (len=*), parameter :: s3 =                              &
      &  'Embedded <'//char(92)//'> backslash'
       character (len=*), parameter :: s4 =                              &
      &  'Embedded <'//char(10)//'> newline'
       character, parameter :: nul = char(0)  ! NB: An embedded NUL
       character (len=5), parameter :: s5 =                              &
      &  'N'//nul//'U'//nul//'L'
       character (len=*), parameter :: s6 =                              &
      &  'Embedded <'//char(39)//'> single quote'
       character (len=*), parameter :: s7 =                              &
      &  'Embedded <'//char(34)//char(39)//'> double/single quote'
       character (len=*), parameter :: s8 =                              &
      &  char(34)
       character (len=*), parameter :: s9 =                              &
      &  char(39)
       character (len=*), parameter :: s10 =                             &
      &  char(92)

       character, parameter :: c1 = 'A'
       character, parameter :: c2 = char(0)  ! NB: An embedded NUL
       character, parameter :: c3 = char(1)  ! CTRL-A
       character, parameter :: c4 = char(34) ! double quote
       character, parameter :: c5 = char(39) ! single quote
       character, parameter :: c6 = char(92) ! backslash

       complex (kind=4),  parameter :: cx1    = (1,2)
       complex (kind=4)             :: cxvar1 = cx1
       integer (kind=4), dimension(2) ::cxintvar1
       equivalence (cx1var,cxintvar1)
       complex (kind=4),  parameter :: cx2    = (-1,-2)
       complex (kind=4)             :: cxvar2 = cx2
       integer (kind=4), dimension(2) ::cxintvar2
       equivalence (cx2var,cxintvar2)
       complex (kind=8),  parameter :: cx3    = (4,8)
       complex (kind=8)             :: cxvar3 = (4,8)
       integer (kind=8), dimension(2) ::cxintvar3
       equivalence (cx3var,cxintvar3)
       complex (kind=8),  parameter :: cx4    = (-4,-8)
       complex (kind=8)             :: cxvar4 = (-4,-8)
       integer (kind=8), dimension(2) ::cxintvar4
       equivalence (cx4var,cxintvar4)
! complex (kind=16) not supported everywhere
!      complex (kind=16), parameter :: cx5 = (16,32)
!      complex (kind=16), parameter :: cx6 = (-16,-32)

       type(posn), parameter :: p1 = posn( 10.0, 20.0,  30)
       type(posn), parameter :: p2 = posn(-10.0, 20.0, -30)

       real,    dimension(3), parameter :: a1 = (/ 1.1, 2.2, 3.3 /)
       integer, dimension(3), parameter :: a2 = (/ 1,   2,   3 /)

       call breakhere2
       print *, i1, i2, i3, i4, i5, i6
       print *, l1, l2, l3, l4
       print *, r1, r2, r3, r4
!      print *, r5, r6
       print *, s1, s2, s3, s4
       print *, c1, c2, c3
       print *, cx1, cx2, cx3, cx4
!      print *, cx5, cx6
       print *, p1, p2
       print *, a1, a2

       end
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
