typedef __attribute__((altivec(vector__))) unsigned int V;
void foo (V *p)
{
    register V a = {4,4,4,4};
    register V b = a << 1;
    b = b << 1;
    *p = b;
}

int main(void)
{
    V *p;

    foo(p);
    return;
}
