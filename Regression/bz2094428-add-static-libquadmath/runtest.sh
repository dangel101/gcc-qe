#!/usr/bin/env bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz2094428-add-static-libquadmath
#   Description: Test for BZ#2094428 (Add -static-libquadmath)
#   Author: Vaclav Kadlcik <vkadlcik@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2022 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

GCC="${GCC:-$(type -P gcc)}"
GCC_RPM_NAME=$(rpm --qf '%{name}' -qf $GCC)

PACKAGES="$GCC_RPM_NAME ${GCC_RPM_NAME}-gfortran"

# Notes on relevancy
#
# Relevant only when static builds of libquadmath and libgfortran are
# available (see the "PACKAGES+=" command below for details). Further,
# the feature at hand is implemented not everywhere where the static
# libraries are available. At the moment the respective upstream patch
# is present in the GCC 13 branch, not merged to 11 nor 12. Red Hat
# plans to backport to GTS/DTS 11 and 12. No plan to backport to base
# GCC 11 of RHEL-9.
#
# Suggested TCMS relevancy (could be simplified but this is easier to follow)
#   collection !defined && distro < rhel-7: False                            # no libquadmath
#   collection !defined && distro = rhel-7 && arch != x86_64: False          # no libquadmath
#   collection !defined && distro > rhel-7 && arch != x86_64, ppc64le: False # no libquadmath
#   collection !defined && distro < rhel-10: False                           # not backported
#   collection  defined && distro = rhel-7 && arch = s390x: False            # no libquadmath
#   collection  defined && distro > rhel-7 && arch != x86_64, ppc64le: False # no libquadmath
#   collection contains gcc-toolset-10, devtoolset-10: False                 # not backported

# Define where and what static libquadmath is expected. Where static libquadmath
# is available, static libgfortran is available as well. (Note: not the other way
# around.)
if [[ "$GCC_RPM_NAME" = gcc ]]; then
    # base GCC: static libquadmath is a separate RPM: libquadmath-static
    if rlIsRHEL '<7'; then
        : # libquadmath not there
    elif rlIsRHEL 7 && [[ "$PRI_ARCH" != x86_64 ]]; then
        : # libquadmath not there
    elif rlIsRHEL '>7' && [[ "$PRI_ARCH" != x86_64 ]] && [[ "$PRI_ARCH" != ppc64le ]]; then
        : # libquadmath not there
    else
        PACKAGES+=" libquadmath-static libgfortran-static"
    fi
else
    # toolset GCC: static libquadmath is part of toolset's libquadmath-devel, if present
    if rlIsRHEL 7 && [[ "$PRI_ARCH" = s390x ]]; then
        : # libquadmath not there
    elif rlIsRHEL '>7' && [[ "$PRI_ARCH" != x86_64 ]] && [[ "$PRI_ARCH" != ppc64le ]]; then
        : # libquadmath not there
    else
        PACKAGES+=" ${GCC_RPM_NAME%%gcc}libquadmath-devel ${GCC_RPM_NAME}-gfortran"
    fi
fi

inspect_binary () {
    rlRun './demo.exe >out.txt 2>err.txt'
    rlAssertGrep 3.14 out.txt -qF || cat out.txt
    rlAssertNotDiffer err.txt /dev/null || cat err.txt
    rlRun 'ldd demo.exe >ldd.out'
}

rlJournalStart
    rlPhaseStartSetup
        rlLogInfo "PACKAGES=$PACKAGES"
        rlLogInfo "COLLECTIONS=$COLLECTIONS"
        rlLogInfo "GCC=$GCC"
        rlLogInfo "SKIP_COLLECTION_METAPACKAGE_CHECK=$SKIP_COLLECTION_METAPACKAGE_CHECK"

        # We optionally need to skip checking for the presence of the metapackage
        # because that would pull in all the dependent toolset subrpms.  We do not
        # always want that, especially in CI.
        _COLLECTIONS="$COLLECTIONS"
        if ! test -z $SKIP_COLLECTION_METAPACKAGE_CHECK; then
            for c in $SKIP_COLLECTION_METAPACKAGE_CHECK; do
                rlLogInfo "ignoring metapackage check for collection $c"
                export COLLECTIONS=$(shopt -s extglob && echo ${COLLECTIONS//$c/})
            done
        fi
        rlLogInfo "(without skipped) COLLECTIONS=$COLLECTIONS"
        rlAssertRpm --all
        export COLLECTIONS="$_COLLECTIONS"

        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "cp files/* $TmpDir"
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    rlPhaseStartTest NORMAL_LINKING_IS_DYNAMIC
        rm -f demo.exe
        rlRun 'gfortran -o demo.exe demo.f90'
        inspect_binary
        rlAssertGrep libgfortran.so ldd.out -qF || cat ldd.out
        rlAssertGrep libquadmath.so ldd.out -qF || cat ldd.out
    rlPhaseEnd

    rlPhaseStartTest STATIC_LIBGFORTRAN
        rm -f demo.exe
        rlRun 'gfortran -static-libgfortran -o demo.exe demo.f90'
        inspect_binary
        rlAssertNotGrep libgfortran.so ldd.out -qF || cat ldd.out

        # libquadmath.so may or may not be in ldd.out, depending if
        # libquadmath was needed at all. Let's enforce it and check it gets
        # linked dynamically.
        rm -f demo.exe
        rlRun 'gfortran -static-libgfortran -lquadmath -o demo.exe demo.f90'
        inspect_binary
        rlAssertNotGrep libgfortran.so ldd.out -qF || cat ldd.out
        rlAssertGrep    libquadmath.so ldd.out -qF || cat ldd.out
    rlPhaseEnd

    # Combining dynamic libgfortran and static libquadmath doesn't make sense
    # because libgfortran.so refers/requires libquadmath.so. Even if we use
    # -static-libquadmath, we'll end up with dynamically linked libquadmath.
    # There's nothing to test.

    rlPhaseStartTest BOTH_STATIC
        rm -f demo.exe
        rlRun 'gfortran -static-libgfortran -static-libquadmath -o demo.exe demo.f90'
        inspect_binary
        rlAssertNotGrep libgfortran.so ldd.out -qF || cat ldd.out
        rlAssertNotGrep libquadmath.so ldd.out -qF || cat ldd.out

        # The order of the -static-* options should not matter
        rm -f demo.exe
        rlRun 'gfortran -static-libquadmath -static-libgfortran -o demo.exe demo.f90'
        inspect_binary
        rlAssertNotGrep libgfortran.so ldd.out -qF || cat ldd.out
        rlAssertNotGrep libquadmath.so ldd.out -qF || cat ldd.out
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun popd
        rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
