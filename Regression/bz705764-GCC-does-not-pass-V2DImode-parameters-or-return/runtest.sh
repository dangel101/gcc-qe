#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz705764-GCC-does-not-pass-V2DImode-parameters-or-return
#   Description: Test pass V2DImode parameters or return values correctly with respect to the ABI
#   Author: Marek Polacek <mpolacek@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2011, 2012 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include rhts environment
. /usr/bin/rhts-environment.sh
. /usr/lib/beakerlib/beakerlib.sh

PACKAGES=(gcc gawk)

# Choose the compiler.
GCC=${GCC:-gcc}

declare -i do_it

rlJournalStart
    rlPhaseStartSetup
        for p in "${PACKAGES[@]}"; do
            rlAssertRpm "$p"
        done; unset p
        rlRun "TmpDir=\`mktemp -d\`" 0 "Creating tmp directory"
	# Get the reproducer.
	rlRun "cp -v v2di-bug.c $TmpDir" 0
if [[ $(uname -m) = "ppc64" ]]; then
	# Try to use the -mtune=power7 switch.
	gcc -mtune=power7 -xc - <<< "int main(){}"
	do_it=$?
fi
        rlRun "pushd $TmpDir"
    rlPhaseEnd

    rlPhaseStartTest
if [[ $(uname -m) = "ppc64" ]] && [[ $do_it -eq 0 ]]; then
	# Compile the reproducer.
	rlRun "$GCC -O2 -mcpu=power7 -S -m64 v2di-bug.c" 0 "Compiling the reproducer"
	
	# Now check the assembly output.
	rlRun "test -s v2di-bug.s" 0 "Assembly does exist"
	rlRun "cat v2di-bug.s | perl -ne 'BEGIN { \$ff=0; } if(\$ff){if(/^\s*\./){next;} if(/^\s*xxlor\s+34,35,35/){exit(0);}else{exit(1);}}\$ff=1 if /^\.L\.foo:/;'" 0 "Checking the assembly"
	test $? -ne 0 && cat v2di-bug.s
fi
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
