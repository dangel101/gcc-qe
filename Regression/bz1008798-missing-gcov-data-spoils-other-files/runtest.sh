#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz1008798-missing-gcov-data-spoils-other-files
#   Description: Test for BZ#1008798 (missing gcov data spoils other files)
#   Author: Miroslav Franc <mfranc@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2014 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

GCC="${GCC:-$(type -P gcc)}"
GCC_RPM_NAME=$(rpm --qf '%{name}' -qf $GCC)
PACKAGES="$GCC_RPM_NAME"

rlJournalStart
    rlPhaseStartSetup
        rlLogInfo "PACKAGES=$PACKAGES"
        rlLogInfo "COLLECTIONS=$COLLECTIONS"
        rlLogInfo "GCC=$GCC"
        rlLogInfo "SKIP_COLLECTION_METAPACKAGE_CHECK=$SKIP_COLLECTION_METAPACKAGE_CHECK"

        # We optionally need to skip checking for the presence of the metapackage
        # because that would pull in all the dependent toolset subrpms.  We do not
        # always want that, especially in CI.
        _COLLECTIONS="$COLLECTIONS"
        if ! test -z $SKIP_COLLECTION_METAPACKAGE_CHECK; then
            for c in $SKIP_COLLECTION_METAPACKAGE_CHECK; do
                rlLogInfo "ignoring metapackage check for collection $c"
                export COLLECTIONS=$(shopt -s extglob && echo ${COLLECTIONS//$c/})
            done
        fi
        rlLogInfo "(without skipped) COLLECTIONS=$COLLECTIONS"
        rlAssertRpm --all
        export COLLECTIONS="$_COLLECTIONS"

        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "pushd $TmpDir"
        rlRun 'echo -e "int main()\n{\n    return 0;\n}" >gcov_test.c'
    rlPhaseEnd

    rlPhaseStartTest
        rlRun 'gcc gcov_test.c -o gcov_test -fprofile-arcs -ftest-coverage &>gcc.log'
        rlAssertExists ./gcov_test

        rlRun 'cmp -s gcc.log /dev/null' || rlLogError "$(<gcc.log)"

        rlAssertExists ./gcov_test.gcno
        rlRun ./gcov_test
        rlAssertExists ./gcov_test.gcda

        rlRun 'gcov *.c &>gcov-1.log'
        rlAssertGrep 'Lines executed:100.00% of 2' gcov-1.log -qF
        rlAssertGrep '1:\s+\d:int\smain' gcov_test.c.gcov -qP
        rlAssertGrep '1:\s+\d:\s*return\s0;' gcov_test.c.gcov -qP

        rlRun 'rm gcov_test.c.gcov'

        rlRun 'touch unused.c'
        rlRun 'gcov *.c &>gcov-2.log' 0,1
        rlAssertGrep 'Lines executed:100.00% of 2' gcov-2.log -qF
        rlAssertGrep '1:\s+1:int\smain' gcov_test.c.gcov -qP
        rlAssertGrep '1:\s+\d:\s*return\s0;' gcov_test.c.gcov -qP
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun popd
        rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
