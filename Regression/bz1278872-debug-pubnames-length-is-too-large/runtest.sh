#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/Regression/bz1278872-debug-pubnames-length-is-too-large
#   Description: Test for BZ#1278872 (.debug_pubnames length is too large)
#   Author: Michael Petlan <mpetlan@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2016 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

# Relevant for GCC 4.8+. In practice, any currently supported GCC except the
# system one of RHEL-6.
# Suggested TCMS relevancy:
#   distro = rhel-6 && collection !defined: False

GCC="${GCC:-$(type -P gcc)}"
GCC_RPM_NAME=$(rpm --qf '%{name}' -qf $GCC)
PACKAGES="$GCC_RPM_NAME ${GCC_RPM_NAME%gcc}binutils"

rlJournalStart
    rlPhaseStartSetup
        rlLogInfo "PACKAGES=$PACKAGES"
        rlLogInfo "COLLECTIONS=$COLLECTIONS"
        rlLogInfo "GCC=$GCC"
        rlLogInfo "SKIP_COLLECTION_METAPACKAGE_CHECK=$SKIP_COLLECTION_METAPACKAGE_CHECK"

        # We optionally need to skip checking for the presence of the metapackage
        # because that would pull in all the dependent toolset subrpms.  We do not
        # always want that, especially in CI.
        _COLLECTIONS="$COLLECTIONS"
        if ! test -z $SKIP_COLLECTION_METAPACKAGE_CHECK; then
            for c in $SKIP_COLLECTION_METAPACKAGE_CHECK; do
                rlLogInfo "ignoring metapackage check for collection $c"
                export COLLECTIONS=$(shopt -s extglob && echo ${COLLECTIONS//$c/})
            done
        fi
        rlLogInfo "(without skipped) COLLECTIONS=$COLLECTIONS"
        rlAssertRpm --all
        export COLLECTIONS="$_COLLECTIONS"

        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "cp progtest.tar.gz $TmpDir"
        rlRun "pushd $TmpDir"
        rlRun 'tar xzf progtest.tar.gz'
        rlRun make
    rlPhaseEnd

    rlPhaseStartTest

        # the program object file should contain only the symbols from the program itself
        rlRun 'readelf -wp progtest.o >readelf_1.log'
        rlAssertGrep '\s\smain' readelf_1.log -P
        rlAssertGrep '\s\svariable' readelf_1.log -P
        rlAssertNotGrep '\s\si' readelf_1.log -P
        rlAssertNotGrep '\s\spt' readelf_1.log -P

        # the library object file should contain only the library symbols
        rlRun 'readelf -wp libtest.o >readelf_2.log'
        rlAssertNotGrep '\s\smain' readelf_2.log -P
        rlAssertNotGrep '\s\svariable' readelf_2.log -P
        rlAssertGrep '\s\sf' readelf_2.log -P
        rlAssertGrep '\s\si' readelf_2.log -P
        rlAssertGrep '\s\spt' readelf_2.log -P

        # the final executable should contain all the symbols
        rlRun 'readelf -wp progtest.x >readelf_3.log'
        rlAssertGrep '\s\smain' readelf_3.log -P
        rlAssertGrep '\s\svariable' readelf_3.log -P
        rlAssertGrep '\s\sf' readelf_3.log -P
        rlAssertGrep '\s\si' readelf_3.log -P
        rlAssertGrep '\s\spt' readelf_3.log -P

    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun popd
        rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
