// gcc -fanalyzer

#include <stddef.h>

int *
func1(int *ptr) {
	if (!ptr)
		return NULL;
	return ++ptr;
}

int
main() {
	int stack;
	int *a = &stack;
	a = func1(a);
	a = func1(a);
	return *a;
}
