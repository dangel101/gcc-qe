#!/usr/bin/env bash
#
# Copyright: Red Hat 2008-2009
# License: GPL
#
# Author:  Petr Muller <pmuller@redhat.com>
# Rewrite: Michal Nowak <mnowak@redhat.com>
# Rewrite: Michael Petlan <mpetlan@redhat.com>
#

# Include rhts environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

##############################################################################
# Note on relevancy
##############################################################################
#
# Newer Botan versions aren't compatible with old systems, old GCC versions and
# especially with Python 2. That excludes RHEL 6 and older from the play but we
# can afford it, RHEL 6 is already in a late support phase and GCC won't any
# development. Further, system GCC on RHEL-7 would need much massaging to build
# Botan so let's skip it there, too.
#
# Note that newer GGCs on aarch64 (regardless RHEL version) are affected by
#   https://bugzilla.redhat.com/show_bug.cgi?id=2098202
#   https://gcc.gnu.org/bugzilla/show_bug.cgi?id=106069
# Once we have better grasp of the bug and know where it won't be fixed
# we should expand the relevancy rules accordingly.
#
# Suggested TCMS relevancy:
#   distro < rhel-7: False
#   distro < rhel-8 && collection !defined && arch = ppc64, ppc64le: False
#
##############################################################################
# Note on results
##############################################################################
#
# There are no performance limits set in this test. Thus this test fails only
# when Botan can't be built or run. That makes it not really a performance
# tests. If you need to use it to measure performance then collect the data
# reported by this tests and compare against data from  different GCC build
# but on the same (or similar enough) machine.


rlJournalStart
    rlPhaseStartSetup
        # RHEL-8 hack - find *some* python to use
        test -z "$PYTHON" && /usr/bin/env python -V && export PYTHON="/usr/bin/env python"
        test -z "$PYTHON" && /usr/bin/env python3 -V && export PYTHON="/usr/bin/env python3"
        test -z "$PYTHON" && /usr/bin/env python2 -V && export PYTHON="/usr/bin/env python2"
        test -z "$PYTHON" && /usr/libexec/platform-python -V && export PYTHON="/usr/libexec/platform-python"
        test -z "$PYTHON" && rlDie "NO PYTHON FOUND"

        rlLog "Using GCC: $(rpmquery -f $(type -P gcc))"
        rlShowRunningKernel

        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "pushd $TmpDir"

        # Botan sources - copied from https://botan.randombit.net/releases
        # to our local cache. See
        #   https://wiki.test.redhat.com/Scratch/BeakerInventoryTcms#supportforhugetestdata
        #   https://wiki.test.redhat.com/Faq/Beaker/DealingWithLargeFiles
        rlRun 'TARBALL=Botan-2.19.2.tar.xz'
        rlRun "URL=http://download.eng.bos.redhat.com/qa/rhts/lookaside/$TARBALL"
        rlRun "wget $URL"
        rlRun "tar xJf $TARBALL"
        rlRun "rm -f $TARBALL"
        rlRun 'chown -R root:root Botan-*' # kill AVC messages
        rlRun 'pushd Botan-*'
        rlRun "$PYTHON ./configure.py --without-documentation"

        rlRun 'TEST_RUNS=2'
        rlRun 'OPTIMALISATION_LEVELS="1 2 3"'
    rlPhaseEnd


    rlPhaseStartTest
        for optimalisation in $OPTIMALISATION_LEVELS; do
            rlRun "make clean; make -j$(nproc) CXXFLAGS='-O$optimalisation'"
            rlAssertExists botan-test
            rlLogInfo "Running botan-test: just warm up; -O used: $optimalisation ..."
            LD_LIBRARY_PATH+=:. ./botan-test >botan.log
            exit_value=$?
            [[ $exit_value -eq 0 ]] && rlLogInfo 'Finished.' || rlFail "Exit value: $exit_value"
            total_bench[$optimalisation]='0.0'
            for testrun in $(seq 1 $TEST_RUNS); do
                rlLogInfo "Running botan-test: -O used: $optimalisation (run #$testrun) ..."
                LD_LIBRARY_PATH+=:. ./botan-test >botan_$testrun.log
                if [[ $exit_value -eq 0 ]]; then
                    rlLogInfo 'Finished.'
                else
                    rlFail "Exit value: $exit_value"
                    rlFileSubmit botan_$testrun.log "opt-{$optimalisation}_run-${testrun}.log"
                fi
                rlLogInfo "Benchmark $testrun $(tail -n 1 botan_$testrun.log)"
                total_bench[$optimalisation]=$(echo "${total_bench[$optimalisation]} + $(tail -n 1 botan_$testrun.log | perl -ne 'print "$1" if /n ([\d\.]+) sec/')" | bc)
            done
            bench_time[$optimalisation]=$(echo "scale=5; ${total_bench[$optimalisation]} / $testrun" | bc)
            rlLogInfo "[opt=${optimalisation}] Average bench performance: ${bench_time[$optimalisation]} secs"
        done

        for i in $OPTIMALISATION_LEVELS; do
            if [ -z "${bench_time[$i]}" ]; then
                rlReport $TEST/O${i}_BENCH "FAIL" 0
            else
                echo -e "Benchmark with -O$i:\t${bench_time[$i]}"
                rlReport $TEST/O${i}_BENCH "PASS" ${bench_time[$i]}
            fi
        done

        make clean
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun popd
        rlRun popd
        rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
