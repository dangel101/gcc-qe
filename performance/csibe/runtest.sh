#!/usr/bin/env bash
#
# Copyright: Red Hat 2008-2009
# License: GPL
#
# Author:  Petr Muller <pmuller@redhat.com>
# Rewrite: Michal Nowak <mnowak@redhat.com>
# Another rewrite: Marek Polacek <polacek@redhat.com>

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

##############################################################################
# Note on relevancy
##############################################################################
#
# http://www.csibe.org/ switched to Python 3 so it wouldn't be easy to run it
# on RHEL 6. Since RHEL 6 is already in a late support phase and GCC won't see
# any development there then let's just drop it.
#
# Suggested TCMS relevancy:
#   distro < rhel-7: False
#
##############################################################################
# Note on results
##############################################################################
#
# There are no performance limits set in this test. Thus this test fails only
# when CSiBE can't be built or run. That makes it not really a performance
# tests. If you need to use it to measure performance then collect the data
# reported by this tests and compare against data from  different GCC build.
#
# TODO: -Os should provide the best result when compared to any other 
#       optimization levels used. That could be checked here regardless
#       data from other GCC builds.

rlJournalStart
    rlPhaseStartSetup
        for p in gcc gcc-c++ bc cmake libgomp patch python3 time wget which; do
          rpm -q "$p" &>/dev/null || rlRun "yum -y install $p"
        done; unset p
        rlRun "TmpDir=\$(mktemp -d)"
        rlRun "pushd $TmpDir"

        # CSiBE sources - copied from https://github.com/szeged/csibe
        # to our local cache. See
        #   https://wiki.test.redhat.com/Scratch/BeakerInventoryTcms#supportforhugetestdata
        #   https://wiki.test.redhat.com/Faq/Beaker/DealingWithLargeFiles
        rlRun 'TARBALL=csibe-snapshot-20220317.tar.gz'
        rlRun "URL=http://download.eng.bos.redhat.com/qa/rhts/lookaside/$TARBALL"
        rlRun "wget $URL"
        rlRun "tar xzf $TARBALL"
        rlRun "rm -f $TARBALL"
        rlRun 'chown -R root:root csibe' # kill AVC messages
        rlRun 'pushd csibe'
        rlRun 'mkdir __result_files'

        rlRun 'EXPECTED_RESULT_FILES="build/native/gen/flex-2.6.0/result.csv build/native/gen/bzip2-1.0.6/result.csv"'
        rlRun 'OPTIMIZATION_LEVELS="s 0 1 2 3"'
    rlPhaseEnd

    rlPhaseStartTest
        for optimization in $OPTIMIZATION_LEVELS; do
            rm -rf build
            rlRun "./csibe.py --toolchain native -O$optimization"
            for result_file in $EXPECTED_RESULT_FILES; do
                rlAssertGrep . "$result_file" # must exist, non-empty
                target_name=${result_file##build/native/gen/} # remove useless beginning
                target_name=${target_name//\//_}              # replace shashes with underscores
                target_name=O${optimization}_${target_name}   # prefix with the optimization level
                rlRun "mv '$result_file' '__result_files/${target_name}'"
            done
        done
        rlRun 'pushd __result_files'
        ls | sort | while read i; do
            rlFileSubmit "$i" "$i"
            cat "$i" | while read line; do
                rlLogInfo "${i},${line}"
            done
        done
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun popd
        rlRun popd
        rlRun popd
        rlRun "rm -r $TmpDir"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
