#!/bin/bash
#
# Copyright (c) 2009, 2012 Red Hat, Inc. All rights reserved.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# 
# Author: Michal Nowak <mnowak@redhat.com>
#	  Marek Polacek <polacek@redhat.com>

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGES=(gcc libgomp bc time patch which)

# Choose the compiler.
GCC=${GCC:-gcc}

rlJournalStart
  rlPhaseStartSetup
    # Check for needed packages.
    for p in "${PACKAGES[@]}"; do
      rlAssertRpm "$p"
    done; unset p
    if [[ $(uname -i) == "ppc64" ]]; then
      rlRun "yum -y install libgomp.ppc"
    fi
    if [[ $(uname -i) == "i386" ]] || [[ $(uname -i) == "i686" ]]; then 
      arch="-i386"
    fi
    rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
    # We need the reproducer.h
    rlRun "cp -v clomp-gcc43-fix-i386.patch clomp-gcc43-fix.patch clomp-gcc44-fix-i386.patch \
           clomp-gcc44-fix.patch clomp-gcc4-fix-i386.patch clomp-gcc4-fix.patch \
	   clomp-gcc-fix-i386.patch clomp-gcc-fix.patch clomp_v1.0.tar.gz $TmpDir"
    rlRun "pushd $TmpDir"
    # hack around the KVM guest OpenMP problem
    if arch | grep -q 86 && [ "`virt-what`" = "kvm" ]; then
        export OMP_WAIT_POLICY=passive
        export OMP_NUM_THREADS=2
    fi
  rlPhaseEnd

  rlPhaseStartTest "Creating and running benchmark"
    # Prepare
    rlRun "tar xvvzf clomp_v1.0.tar.gz"
    ccbasename=$(basename $GCC)
    rlRun "patch -p0 < clomp-${ccbasename}-fix${arch}.patch" 0 "[${ccbasename}] Patch the sources"
    cd clomp_v1.0/
    isSSETwo="$(grep flags /proc/cpuinfo | grep sse2)"
    if [ -z "${isSSETwo}" ]; then
      sed -i -e "s/-msse2 -mfpmath=sse/-ffloat-store/g" Makefile
    fi
    # Compile
    rlRun "CC=$GCC make gcc4" 0 "[${ccbasename}] Create testing binary" # Test
    HOSTNAME="$(hostname -s)"
    for N in $(seq 1 2); do
      test_start_time="$(date '+%s')"
      rlRun "./clomp_gcc4_$HOSTNAME -1 1 64 100 32 1 100 &> clomp_gcc.out" 0 "[${ccbasename}] #${N}: Run the benchmark"
      test_end_time="$(date '+%s')"
      test_duration="$(echo "($test_end_time - $test_start_time)" | bc)"
      cat clomp_gcc.out
      if [ "${test_duration}" -lt "4" ]; then
	RESULT="FAIL"
      else
	RESULT="PASS"
      fi
      rlReport $TEST/BENCH-$ccbasename-${N} "$RESULT" ${test_duration}
    done
    # Backup clomp_gcc.out"
    mv -v clomp_gcc.out ../clomp_gcc-${ccbasename}.out
    # Return from clomp dir back to main one"
    cd ..
    # Kill the dir
    rm -rf clomp_v1.0/
  rlPhaseEnd

  rlPhaseStartCleanup
    rlBundleLogs gcc-outputs clomp_gcc-*.out
    rlRun "popd"
    rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
  rlPhaseEnd
rlJournalPrintText
rlJournalEnd
