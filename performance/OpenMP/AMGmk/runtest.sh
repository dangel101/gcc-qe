#!/bin/bash
#
# Copyright (c) 2009, 2012 Red Hat, Inc. All rights reserved.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# 
# Author: Michal Nowak <mnowak@redhat.com>
# Rewrite: Marek Polacek <polacek@redhat.com>

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGES=(gcc libgomp bc time patch which)

# Choose the compiler.
GCC=${GCC:-gcc}

# Max acceptable times
declare -A MAX_ACCEPTED_TIMES
MAX_ACCEPTED_TIMES["aarch64"]=15
MAX_ACCEPTED_TIMES["i386"]=8
MAX_ACCEPTED_TIMES["i686"]=${MAX_ACCEPTED_TIMES[i386]}
MAX_ACCEPTED_TIMES["i386_kvm"]=8
MAX_ACCEPTED_TIMES["i686_kvm"]=${MAX_ACCEPTED_TIMES[i386_kvm]}
MAX_ACCEPTED_TIMES["ppc64le"]=20
MAX_ACCEPTED_TIMES["ppc64"]=20
MAX_ACCEPTED_TIMES["s390x"]=20
MAX_ACCEPTED_TIMES["x86_64"]=15
MAX_ACCEPTED_TIMES["x86_64_kvm"]=25

rlJournalStart
  rlPhaseStartSetup
    # Check for needed packages.
    for p in "${PACKAGES[@]}"; do
      rlAssertRpm "$p"
    done; unset p
    rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
    # We need the reproducer.
    rlRun "cp -v AMGmk_v1.0.tar.gz AMGmk-Makefile.patch $TmpDir"
    rlRun "pushd $TmpDir"
    # Prepare
    rlRun "tar xvvfz AMGmk_v1.0.tar.gz && cd AMGmk_v1.0 && patch < ../AMGmk-Makefile.patch"
    rlRun "cd .."
  rlPhaseEnd

  rlPhaseStartTest "Testing the C executables"
    rlRun "cd AMGmk_v1.0/"
    # Compile
    rlRun "make CC=$GCC" 0 "Compiling"
    # Test
    rlRun "./AMGMk | tee $TmpDir/run.log" 0 "Performed AMGMk testsuite"
    gcc_pkg=$(which $GCC)
    rlRun "make clean"
    cd ..
  rlPhaseEnd

  rlPhaseStart WARN "Result evaluation"
    # this is an informative phase replacing the legacy awful way of
    # making sure whether the results are somehow sane
    # it is just "WARN" because I am uncure what we really expect
    TOTAL_WALL_TIME=`perl -ne 'print "$1\n" if /Total\sWall\stime\s=\s([\d\.]+)\sseconds\./' $TmpDir/run.log | tail -1`
    rlLogInfo "TOTAL_WALL_TIME=$TOTAL_WALL_TIME"
    # we want it finish in (default) 5 seconds, otherwise warn
    KEY=`arch`
    if arch | grep -q 86 && [ -n "`virt-what`" ]; then
      # for virtual guests we are more tolerant and also we need to tweak the environment
      KEY=`arch`_`virt-what`
      export OMP_WAIT_POLICY=passive
      export OMP_NUM_THREADS=2
    fi
    MAX_ACCEPTED_TIME=${MAX_ACCEPTED_TIMES[$KEY]:-5}
    rlAssert0 "Test ($TOTAL_WALL_TIME) was fast enough (under $MAX_ACCEPTED_TIME seconds)" `echo "$MAX_ACCEPTED_TIME <= $TOTAL_WALL_TIME" | bc`
    test $? -eq 0 || rlLog "KEY was $KEY"
  rlPhaseEnd

  rlPhaseStartCleanup
    rlBundleLogs "gcc-outputs" RESULTS
    rlRun "popd"
    rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
  rlPhaseEnd
rlJournalPrintText
rlJournalEnd
