#include <omp.h>
#include <stdio.h>
#include <stdlib.h>

#define v do { __asm volatile ("" : : : "memory"); } while (0)

__attribute__((noinline))
void
barrier_bench (void)
{
  #pragma omp parallel
    for (int i = 0; i < 10000; i++)
      {
        v;
        #pragma omp barrier
	v;
        #pragma omp barrier
	v;
        #pragma omp barrier
	v;
        #pragma omp barrier
	v;
        #pragma omp barrier
	v;
        #pragma omp barrier
	v;
        #pragma omp barrier
	v;
        #pragma omp barrier
	v;
        #pragma omp barrier
	v;
        #pragma omp barrier
	v;
      }
}

__attribute__((noinline))
void
parallel_bench (void)
{
  for (int i = 0; i < 20000; i++)
    #pragma omp parallel
      v;
}

__attribute__((noinline))
void
static_bench (void)
{
  for (int i = 0; i < 1000; i++)
    #pragma omp parallel for schedule (static)
      for (int j = 0; j < 100; j++)
        v;
}

__attribute__((noinline))
void
dynamic_bench (void)
{
  #pragma omp parallel for schedule (dynamic)
  for (int j = 0; j < 100000; j++)
    v;
}

__attribute__((noinline))
void
guided_bench (void)
{
  #pragma omp parallel for schedule (guided)
  for (int j = 0; j < 100000; j++)
    v;
}

__attribute__((noinline))
void
runtime_bench (void)
{
  #pragma omp parallel for schedule (runtime)
  for (int j = 0; j < 100000; j++)
    v;
}

__attribute__((noinline))
void
single_bench (void)
{
  #pragma omp parallel
  for (int i = 0; i < 10000; i++)
    {
      #pragma omp single
	v;
    }
}

__attribute__((noinline))
void
single_nowait_bench (void)
{
  #pragma omp parallel
  for (int i = 0; i < 100; i++)
    {
      for (int j = 0; j < 99; j++)
	#pragma omp single nowait
	  v;
      #pragma omp single
      v;
    }
}

__attribute__((noinline))
void
short_dynamic_bench (void)
{
  #pragma omp parallel
  for (int i = 0; i < 10000; i++)
    {
      #pragma omp for schedule(dynamic, 1)
      for (int j = 0; j < 4; j++)
	v;
    }
}

int
dblcmp (const void *a, const void *b)
{
  const double *da = (const double *)a;
  const double *db = (const double *)b;
  if (*da < *db)
    return -1;
  if (*da > *db)
    return 1;
  return 0;
}

int
main (void)
{
  double res[14], sum;
  
#define B(x) \
  x##_bench ();								\
  for (int i = 0; i < 14; i++)						\
    {									\
      double start = omp_get_wtime ();					\
      for (int j = 0; j < 20; j++)					\
        x##_bench ();							\
      res[i] = omp_get_wtime () - start;				\
    }									\
  qsort (res, 14, sizeof (double), dblcmp);				\
  sum = 0.0;								\
  for (int i = 2; i < 12; i++)						\
    sum += res[i];							\
  printf (#x " bench min %g max %g avg %g\n", res[2], res[11], sum / 10.0);

  B(barrier)
  B(parallel)
  B(static)
  B(dynamic)
  B(guided)
  B(runtime)
  B(single)
  B(single_nowait)
  B(short_dynamic)
  return 0;
}
