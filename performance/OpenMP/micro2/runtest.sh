#!/bin/bash
#
# Copyright (c) 2008 Red Hat, Inc. All rights reserved.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# 
# Author: Michal Nowak <mnowak@redhat.com>
#	  Marek Polacek <polacek@redhat.com>

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGES=(gcc libgomp)

# Choose the compiler.
GCC=${GCC:-gcc}

rlJournalStart
  rlPhaseStartSetup
    # Check for needed packages.
    for p in "${PACKAGES[@]}"; do
      rlAssertRpm "$p"
    done; unset p
    rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
    # We need the reproducer.
    rlRun "cp -v micro2.c $TmpDir"
    rlRun "pushd $TmpDir"
    # hack around the KVM guest OpenMP problem
    if arch | grep -q 86 && [ "`virt-what`" = "kvm" ]; then
        export OMP_WAIT_POLICY=passive
        export OMP_NUM_THREADS=2
    fi
  rlPhaseEnd

  rlPhaseStartTest "Testing the executable"
    # Compile
    rlRun "$GCC -O3 --openmp -o micro2 micro2.c -std=c99" 0 "Compiling the executable"
    # Test
    test_start_time=$(date '+%s')
    rlRun "./micro2 2>&1 | tee micro2-$GCC.out"
    test_end_time=$(date '+%s')
    test_duration=$(echo "($test_end_time - $test_start_time)" | bc)
    rlRun "echo duration: $test_duration"
    if [ $test_duration -le 5 ]; then
      RESULT="FAIL"
    else
      RESULT="PASS"
    fi
    rlRun "if [ \"${RESULT}\" = \"PASS\" ]; then true; else false; fi" 0 "Run micro2 test case"
    rlReport $TEST/RUN_TIME "$RESULT" ${test_duration}
  rlPhaseEnd

  rlPhaseStartCleanup
    rlBundleLogs "gcc-outputs" micro2-*.out
    rlRun "popd"
    rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
  rlPhaseEnd
rlJournalPrintText
rlJournalEnd
