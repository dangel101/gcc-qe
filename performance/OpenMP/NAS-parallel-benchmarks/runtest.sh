#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/gcc/performance/OpenMP/NAS-parallel-benchmarks
#   Description: The test compiles NPB suite
#   Author: Michael Petlan <mpetlan@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2017 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="gcc"

export GFORTRAN=${GFORTRAN:-gfortran}
export TIMEOUT=${TIMEOUT:-200}

rlJournalStart
	rlPhaseStartSetup
		rlAssertRpm $PACKAGE
		rlRun -l "$GFORTRAN -v" 0 "Log gfortran version"
		rlRun "zcat NPB.tar.gz | tar x" 0 "Unpack the suite"
		rlRun "pushd NPB3.3-OMP"
		rlLog "You have `nproc` processors"
	rlPhaseEnd

	rlPhaseStartTest
		rlRun "perl -e \"alarm $TIMEOUT; exec @ARGV\" \"make suite\"" 0 "Compilation of the suite should PASS and finish in $TIMEOUT seconds"
		RETVAL=$?
		if [ $RETVAL -eq 0 ]; then
			rlPass "Building finished in time and passed."
		else
			if [ $RETVAL -eq 142 ]; then
				rlFail "Build took too long. If manual run takes more than few minutes, it is a bug/regression."
			else
				rlFail "Build finished, but failed."
			fi
		fi
	rlPhaseEnd

	rlPhaseStartCleanup
		rlRun "popd"
		rlRun "rm -r NPB3.3-OMP" 0 "Removing build directory"
	rlPhaseEnd
rlJournalPrintText
rlJournalEnd
